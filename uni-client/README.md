# flutter_uni_client


## UI 使用 
- [UI](https://www.figma.com/file/JmnfcsRZXC5Po7QDfvDENU/b-ball--3?node-id=85%3A959&t=7QQGe0JkqbHTQXqq-0)


### 参考页面标准 
```
base里面一个功能一个文件夹，比如login，splash,各个子模块所有的controller,view,binding,provider建议用一个文件夹，
命名xxx_controller等，具体看下home_page模块，
路由一个模块一个主路由(home），其他的都写子路由(home/xx)，
getx 模版设置 请参考
https://juejin.cn/post/6924104248275763208
```

### 配置地址: lib/live.dart 

## 文件夹介绍


1. app定义页面，app初始化
   main 包含启动工程所需的功能

2. packages 存放功能分割的插件 把每个功能都单独进行分离 根据不同打包的配置加载依赖
   packages/base 模块之间的公共文件放到这里 （登录、注册等）
   packages/base/app_config.dart 应用需要配置的地方
   packages/base/themes 存放主题的地方 需要用到字体大小 字体颜色 等资源
   packages//base/common/utils 共同的工具类 不依赖其他的自己定义的基础库 避免循环引用
   packages/base/common/widget 存放通用的基础组件 比如 固定样式的按钮 固定样式的文本文件
   packages/base_page/ 存放模块之间的公共页面 比如 登录，注册， 启动页面
   packages/base_service/ 存储公共页面所需要的接口服务 多个模块共同使用
   packages/service 与接口相关的数据放到这里
   packages/demo_page 测试模块
   packages/home_page 首页、直播相关的功能模块
   packages/game_page 游戏、彩票相关的功能模块
   packages/wallet_page 充值中心、钱包、提现等相关的功能模块
   packages/mine_page 我的功能模块
   packages/video_page 视频页面

3. base_page 存储一些公共的页面 比如 启动页面 公共的展示页面

4. models   存放模型文件 
5. app/app_init.dart 存放应用需要初始化的地方 尽量都写在这里 便于后面分包处理


##  常用的开发点

1. 页面之前传递使用GetX系列进行传递
2. 用到的颜色或字体 themes 定义编译切换主题
3. 大功能模块尽量使用package或plugins 模式开发 用于根据不同的需求进行打包
4. 命名尽量使用flutter建议，工程里面尽量把警告处理掉
5. 命名以OL前缀开始 例如 ol_flash_widget 
6. 重要的逻辑和参数 需要写注释


## git使用注意
1. 大家都在 dev上开发
2.尽量每天都提交代码，避免2提交一次， 做一个小功能完成原则上就需要提交代码，早提交少点合并代码的烦恼


## json 使用规范  参考baseService/lib/Users 下面生成
https://app.quicktype.io/ 参考splash页面

## 资源使用 
1. 所有的图片 都放在 assets/
2. 添加图片需要运行命令 flutter packages pub run build_runner build
3. 生成后，把packages/base/asset.gen.dart 是生成的资源导航 需要引入

### 图片展示组件 
OLImage 支持aes解密 OLImage(imageUrl:'you image url',)

### loading 显示组件  
OLEasyLoading 显示toast

## 环境配置

目前环境是根据预编译的参数来进行配置的 在运行的时候，通过配置
定义环境参数: PACKAGE_ENV
在android里面设置 

https://stackoverflow.com/questions/55004302/how-do-you-pass-arguments-from-command-line-to-main-in-flutter-dart

--dart-define=PACKAGE_ENV=pro 正式环境
--dart-define=PACKAGE_ENV=pre 预发布环境
--dart-define=PACKAGE_ENV=dev  开发环境
--dart-define=PACKAGE_ENV=uat  uav环境

需要在脚本运行的时候 则需要使用这个脚本
flutter run --dart-define=PACKAGE_ENV=dev
flutter run --dart-define=PACKAGE_ENV=pro

## GetX使用规则

1. 定义监听的变量 只需添加 .obs 作为value的属性 推荐使用


*** 在AndroidStudio 安装插件GetX和GetX Template Generator 辅助使用gex状态管理

```
   如果使用Getx管理状态，需要继承于GetXBaseController使用，目前包含主题的改变 后续公共的一些状态处理就放到这个类里面
   注意事项:
   如果在页面监听状态 ，则需要 放到GetX的前面
   Color? selectColor1 = controller.currentCustomThemeData().colors0x000000;
   
   bottomNavigationBar: GetX<GetRootController>(
          builder: (_) {
            Color? selectColor2 = controller.currentCustomThemeData().colors0x000000;
            
          BottomNavigationBar(
              backgroundColor: Colors.white,
              selectedItemColor: selectColor1,
              //selectedItemColor: selectColor2,
              
              
```

*** 结果是selectColor1是监听不到， selectColor2是可以监听的 所有在使用的时候请注意


## 主题使用 controller.currentCustomThemeData() 获取当前主题
```
   使用主题的时候请注意了
   xxGetController 是继承GetXBaseController 
   final controller = xxGetController 
   如果当前使用GetX方式进行处理， 则使用 controller.currentCustomThemeData() 获取到当前的主题 
   会根据当前主题进行全局更换
```

*** 如果没有使用GetXController管理状态 则需要自己去实现监听


### 缓存使用

// 对于大数据则使用这个 尽量少用UserDefine之类的存储
package:base/cache/cache_manager.dart

void putData(String key, Map<String,dynamic> data) 存入一个数据
void putString(String key, String data) 存储一个字符串
Future<Map<String, dynamic>> getData(String key) 读取一个缓存的数据
Future<String> getString(String key) 读取一个缓存的字符串

在接口请求的时候注意 // 默认空是处理需要受版本控制的接口 （主要针对受版本控制）
await HttpUtil().post(Apis.verApi, needCache : false) 

// ios 和 android 自带的 缓存 就用 class SpUtils 


## 打包注意 如果要编译相应的包通过xcode或Android Studio 则需要改 main.dart

```
  ios : uni-client/ios/flutter/Generated.xcconfig 里面的 FLUTTER_TARGET=lib/main.dart
  android : 直接通过运行的配置就可以了

```


##GetX
### getx的路由跳转命令
```
Get.to(TabBarPage()); // 直接跳转页面 （=Navigation.push()）
Get.toNamed("/home")  // 通过路由表中的名字进行跳转
Get.back();   	     // 关闭当前页面（=Navigation.pop()）
Get.off(TabBarPage())// 跳转页面并关闭之前的页面 (等于 Navigation.pushReplacement() )
Get.offAll(TabBarPage()) //跳转页面并关闭之前全部的页面 (= Navigation.pushAndRemoveUntil)
Get.offNamed("/home")  //  使用路由名 功能同上
Get.offAllNamed("/home")  //  使用路由名 功能同上
```