#import "BaseServicePlugin.h"
#if __has_include(<base_service/base_service-Swift.h>)
#import <base_service/base_service-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "base_service-Swift.h"
#endif

@implementation BaseServicePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBaseServicePlugin registerWithRegistrar:registrar];
}
@end
