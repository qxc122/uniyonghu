import 'package:base/caches/ol_cache_manager.dart';
import 'package:services/models/res/wallet/user_wallet_model.dart';

const kOLUserWalletInfoModelModelKey = "kOLUserWalletInfoModelModelKey";

/// 用户钱包信息
class OLUserWalletManager {
  static final shared = OLUserWalletManager();

  UserWalletModel? info;
  OLUserWalletManager({this.info});
  Future<void> load() async {
    final userInfo =
        await OLCacheManager.shared.getData(kOLUserWalletInfoModelModelKey);
    if (userInfo.isNotEmpty) {
      final userInfoModel = UserWalletModel.fromJson(userInfo);
      info = userInfoModel;
    }
  }

  void save(UserWalletModel userInfo) {
    info = userInfo;
    final json = userInfo.toJson();
    OLCacheManager.shared.putData(kOLUserWalletInfoModelModelKey, json);
  }
}
