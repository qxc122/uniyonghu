import 'package:base/caches/ol_cache_manager.dart';
import 'package:services/models/res/user/user_detail_model.dart';

const kOLUserInfoDetailModelModelKey = "kOLUserInfoDetailModelModelKey";

/// 用户详细信息
class UserDetailManager {
  static final shared = UserDetailManager();

  UserDetailModel? info;
  UserDetailManager({this.info});
  Future<void> load() async {
    final userInfo =
        await OLCacheManager.shared.getData(kOLUserInfoDetailModelModelKey);
    if (userInfo.isNotEmpty) {
      final userInfoModel = UserDetailModel.fromJson(userInfo);
      info = userInfoModel;
    }
  }

  void save(UserDetailModel userInfo) {
    info = userInfo;
    final json = userInfo.toJson();
    OLCacheManager.shared.putData(kOLUserInfoDetailModelModelKey, json);
  }
}
