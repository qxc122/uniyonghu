import 'package:base/caches/ol_cache_manager.dart';
import 'package:services/models/res/login/user_info_model.dart';

/// 用户登录状态处理
class OLUserManager {
  static final shared = OLUserManager();

  OLUserInfoModel? info;
  OLUserManager({this.info});
  Future<void> load() async {
    final userInfo =
        await OLCacheManager.shared.getData(kOLUserInfoModelModelKey);
    if (userInfo.isNotEmpty) {
      final userInfoModel = OLUserInfoModel.fromJson(userInfo);
      info = userInfoModel;
    }
  }

  bool isLogin() {
    return (shared.info?.accessToken ?? "") != "";
  }

  void save(OLUserInfoModel userInfo) {
    info = userInfo;
    final json = userInfo.toJson();
    OLCacheManager.shared.putData(kOLUserInfoModelModelKey, json);
  }
}
