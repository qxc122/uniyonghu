#import "OlImageCachePlugin.h"
#if __has_include(<ol_image_cache/ol_image_cache-Swift.h>)
#import <ol_image_cache/ol_image_cache-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "ol_image_cache-Swift.h"
#endif

@implementation OlImageCachePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftOlImageCachePlugin registerWithRegistrar:registrar];
}
@end
