import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ol_image_cache/ol_image_cache.dart';

void main() {
  const MethodChannel channel = MethodChannel('ol_image_cache');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await OlImageCache.platformVersion, '42');
  });
}
