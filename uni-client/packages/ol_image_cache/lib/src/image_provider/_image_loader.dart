import 'dart:async';
import 'dart:ui' as ui;

import 'package:ol_image_cache/cached_network_image_platform_interface.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:ol_image_cache/cached_network_image_platform_interface.dart'
    as platform show ImageLoader;
import 'package:ol_image_cache/cached_network_image_platform_interface.dart'
    show ImageRenderMethodForWeb;
import 'package:base/commons/utils/aes_utils.dart';
import 'dart:io';
import 'package:path/path.dart' as path;

/// ImageLoader class to load images on IO platforms.
class ImageLoader implements platform.ImageLoader {
  @override
  Stream<ui.Codec> loadAsync(
    String url,
    String? cacheKey,
    StreamController<ImageChunkEvent> chunkEvents,
    DecoderCallback decode,
    BaseCacheManager cacheManager,
    int? maxHeight,
    int? maxWidth,
    Map<String, String>? headers,
    Function()? errorListener,
    ImageRenderMethodForWeb imageRenderMethodForWeb,
    Function() evictImage,
  ) async* {
    try {
      assert(
          cacheManager is ImageCacheManager ||
              (maxWidth == null && maxHeight == null),
          'To resize the image with a CacheManager the '
          'CacheManager needs to be an ImageCacheManager. maxWidth and '
          'maxHeight will be ignored when a normal CacheManager is used.');

      // 获取key
      https: //img.91momo50.vip/picture/
      // 2023/01/04/1777807018482906688.w.jpg?auth_key=1675834127099-30ed06958ea6477692f6221ee1fc9f3f-0-80e0faf0cd3c243bfd0553964124e990',
      var imageKey = '';
      if (url.toString().contains('.w.')) {
        imageKey = url.toString().split(".w.")[0].split("/").last + '.w.';
      } else {
        final cons = url.toString().split('?');
        if (cons.isNotEmpty) {
          var path1 = cons[0];

          imageKey = path.extension(path1);
          if (imageKey.isEmpty) {
            imageKey = url.toString();
          }
        }
      }

      Stream<FileResponse> stream;
      if (cacheManager is ImageCacheManager) {
        stream = cacheManager.getImageFile(url,
            maxHeight: maxHeight,
            maxWidth: maxWidth,
            withProgress: true,
            headers: headers,
            key: imageKey);
      } else {
        stream = cacheManager.getFileStream(url,
            withProgress: true, headers: headers, key: imageKey);
      }

      await for (var result in stream) {
        // if (result is DownloadProgress) {
        //   chunkEvents.add(ImageChunkEvent(
        //     cumulativeBytesLoaded: result.downloaded,
        //     expectedTotalBytes: result.totalSize,
        //   ));
        // }
        if (result is FileInfo) {
          var file = result.file;
          var bytes = await file.readAsBytes();

          var newBytes = bytes;
          if (imageKey.contains('.w.') && bytes.isNotEmpty) {
            // 解密 解密
            newBytes = EncryptUtil.encodeImg(bytes);
          }

          var decoded = await decode(newBytes);
          yield decoded;
        }
      }
    } catch (e) {
      // Depending on where the exception was thrown, the image cache may not
      // have had a chance to track the key in the cache at all.
      // Schedule a microtask to give the cache a chance to add the key.
      scheduleMicrotask(() {
        evictImage();
      });

      errorListener?.call();
      rethrow;
    } finally {
      await chunkEvents.close();
    }
  }
}
