import 'package:base/assets.gen.dart';
import 'package:base_service/manager/user_detail_manager.dart';
import 'package:get/get.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:services/models/req/login.dart';
import 'package:services/models/res/base_response.dart';
import 'package:services/models/res/user/user_detail_model.dart';

class MineRootPageController extends GetXBaseController {
  var info = UserDetailModel().obs;

  var arryWalletDic = [
    {
      "title": "钱包",
      "icon": Assets.minePage.mineQianbao.path,
    },
    {
      "title": "提现",
      "icon": Assets.minePage.mineTiXiang.path,
    },
    {
      "title": "活动中心",
      "icon": Assets.minePage.mineHuoDong.path,
    },
    {
      "title": "专属客服",
      "icon": Assets.minePage.mineKefu.path,
    },
  ];

  var arryDic = [
    {
      "title": "交易明细",
      "icon": Assets.minePage.mineMingXi.path,
    },
    {
      "title": "游戏记录",
      "icon": Assets.minePage.mineGameRecord.path,
    },
    {
      "title": "手机认证",
      "icon": Assets.minePage.minePhone.path,
    },
    {
      "title": "等级",
      "icon": Assets.minePage.mineLevel.path,
    },
    {
      "title": "粉丝交流群",
      "icon": Assets.minePage.mineFanGroup.path,
    },
    {
      "title": "设置",
      "icon": Assets.minePage.mineSet.path,
    },
  ];

  @override
  void onInit() {
    getUserInfo();
    super.onInit();
  }

  getUserInfo() async {
    try {
      BaseResponse response = await Login.of.call().getUserInfo();
      info.value = UserDetailModel.fromJson(response.data);
      UserDetailManager.shared.save(info.value);
    } catch (e) {
      print(e);
    }
  }
}
