import 'package:base/bases/get_base_view.dart';
import 'package:base/themes/custom_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'transaction_detail_controller.dart';
import 'widgets/transaction_item.dart';

class TransactionDetailPage extends GetBaseView<TransactionDetailController> {
  const TransactionDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final customTheme = controller.currentCustomThemeData();
    return Scaffold(
        backgroundColor: customTheme.colors0xF6F5FB,
        appBar: AppBar(
          iconTheme: IconThemeData(color: customTheme.colors0x000000),
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          title: Text(
            "交易明细",
            style: TextStyle(color: customTheme.colors0x000000, fontSize: 16),
          ),
          actions: [
            UnconstrainedBox(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  GestureDetector(
                    onTap: () async {
                      final DateTime? selectedDate =
                          await showDatePicker(context);
                      if (selectedDate != null) {
                        final formattedDate =
                            '${selectedDate.year}-${selectedDate.month.toString().padLeft(2, '0')}-${selectedDate.day.toString().padLeft(2, '0')}';
                        // 处理选择后的日期
                      }
                    },
                    child: Text(
                      '2022-11-11',
                      style: TextStyle(
                          color: customTheme.colors0x000000, fontSize: 12),
                    ),
                  ),
                  InkWell(
                    onTap: controller.openFilter,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Text(
                        '筛选',
                        style: TextStyle(
                            color: customTheme.colors0x000000, fontSize: 12),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
        body: GetBuilder<TransactionDetailController>(
          builder: (controller) => Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(10).copyWith(left: 14),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _HeaderItem(
                      customTheme,
                      title: '支出：',
                      value1:
                          '${controller.transactionModel?.goldOutcome ?? '0'}',
                      value2:
                          '${controller.transactionModel?.silverOutcome ?? '0'}',
                      value3:
                          '${controller.transactionModel?.balanceOutcome ?? '0'}',
                    ),
                    _HeaderItem(
                      customTheme,
                      title: '收入：',
                      value1:
                          '${controller.transactionModel?.goldIncome ?? '0'}',
                      value2:
                          '${controller.transactionModel?.silverIncome ?? '0'}',
                      value3:
                          '${controller.transactionModel?.balanceIncome ?? '0'}',
                    ),
                  ],
                ),
              ),
              Divider(
                  height: 2, thickness: 2, color: customTheme.colors0xE6E6E6),
              Expanded(
                child: SmartRefresher(
                  controller: controller.refreshController,
                  enablePullUp: true,
                  onRefresh: controller.onRefresh,
                  onLoading: controller.onLoading,
                  child: ListView.separated(
                    padding: EdgeInsets.zero,
                    itemCount: controller.transactionList.length,
                    itemBuilder: (BuildContext context, int index) {
                      final item = controller.transactionList[index];
                      return InkWell(
                        onTap: (){
                          controller.toInfo(item);
                        },
                        child: TransactionItem(
                          detail: item,
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return Divider(
                          height: 1,
                          thickness: 1,
                          color: customTheme.colors0xE6E6E6);
                    },
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  Future<DateTime?> showDatePicker(BuildContext context) async {
    DateTime? selectedDate;
    await showModalBottomSheet(
      context: context,
      builder: (BuildContext builderContext) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CupertinoButton(
                  child: Text('取消'),
                  onPressed: () => Navigator.of(context).pop(),
                ),
                CupertinoButton(
                  child: Text('确定'),
                  onPressed: () => Navigator.of(context).pop(selectedDate),
                ),
              ],
            ),
            Container(
              height: 200.0,
              child: CupertinoDatePicker(
                mode: CupertinoDatePickerMode.date,
                onDateTimeChanged: (DateTime dateTime) {
                  selectedDate = dateTime;
                },
                initialDateTime: DateTime.now(),
              ),
            ),
          ],
        );
      },
    );
    return selectedDate;
  }
}

class _HeaderItem extends StatelessWidget {
  final CustomTheme customTheme;
  final String title;
  final String value1;
  final String value2;
  final String value3;

  const _HeaderItem(
    this.customTheme, {
    Key? key,
    required this.title,
    required this.value1,
    required this.value2,
    required this.value3,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          title,
          style: TextStyle(color: customTheme.colors0x000000, fontSize: 12),
        ),
        DefaultTextStyle(
          style: TextStyle(color: customTheme.colors0xA0A0A0, fontSize: 12),
          child: Column(
            children: [
              Text('金币：$value1'),
              Text('钻石：$value1'),
              Text('余额：$value1'),
            ],
          ),
        ),
      ],
    );
  }
}
