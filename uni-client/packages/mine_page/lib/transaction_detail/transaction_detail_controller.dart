import 'package:base/app_routes.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:base/commons/utils/log_utils.dart';
import 'package:get/get.dart';
import 'package:mine_page/transaction_detail/models/transaction_type_bean.dart';
import 'package:mine_page/transaction_detail/widgets/transaction_filter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:services/models/res/game/game_transaction_list.dart';

import 'transaction_detail_provider.dart';

enum TransactionType {
  income,
  expenditure,
}

class TransactionDetailController extends GetXBaseController
    with GetSingleTickerProviderStateMixin {
  static TransactionDetailController get to => Get.find();

  late RefreshController refreshController;

  int pageSize = 10;
  int lastItemId = 0;
  bool _noMore = false;

  List<DetailList> transactionList = [];

  TransactionDetailController();

  GameTransactionModel? transactionModel;
  final List<TransactionTypeBean> _transactionTypes = [];
  final List<TransactionTypeBean> _selectedTransactionTypes = [];

  Future<void> openFilter() async {
    if (_transactionTypes.isEmpty) {
      final types = await TransactionDetailProvider.getTransactionTypeList();
      if (types.isNotEmpty && !isClosed) {
        _transactionTypes.addAll(types);
      }
    }
    Get.bottomSheet(
      TransactionFilter(
        selectedTransactionTypes: _selectedTransactionTypes,
        transactionTypes: _transactionTypes,
        onConfirm: (selectedTypes) {
          bool isSame = false;
          if (selectedTypes.length == _selectedTransactionTypes.length) {
            isSame = true;
            for (var bean in selectedTypes) {
              for (var oldBean in _selectedTransactionTypes) {
                if (bean.code != oldBean.code) {
                  isSame = false;
                  break;
                }
              }
            }
          }
          if (!isSame) {
            _selectedTransactionTypes.clear();
            _selectedTransactionTypes.addAll(selectedTypes);
          }
          // 刷新数据
          onRefresh();
        },
      ),
    );
  }

  void toInfo(DetailList info) {
    Get.toNamed(
      AppRoutes.transactionInfo,
      arguments: info,
    );
  }

  Future<void> _getData({bool isRefresh = false}) async {
    if (isRefresh) lastItemId = 0;
    TransactionDetailProvider.getBill(
      lastID: lastItemId,
      transactionTypes: _selectedTransactionTypes
          .map((e) => e.code.toString())
          .toList()
          .join(','),
    ).then((value) {
      if (value != null) {
        transactionModel = value;
        fetchList(value, isRefresh);
      }
    });
  }

  fetchList(GameTransactionModel model, bool isRefresh) {
    final items = (model.detailList?.isNotEmpty == true)
        ? model.detailList!
        :
        // 测试数据
        [
            DetailList(
              amount: 1400,
              changeTypeName: '游戏投注',
              orderStatus: 1,
              createTime: '2023-02-06 09:30:32',
              billType: 1,
            ),
            DetailList(
              amount: 1400,
              changeTypeName: '游戏投注',
              orderStatus: 2,
              createTime: '2023-02-06 09:30:32',
              billType: 2,
            ),
            DetailList(
              amount: 1400,
              changeTypeName: '游戏投注',
              orderStatus: 3,
              createTime: '2023-02-06 09:30:32',
              billType: 3,
            ),
            DetailList(
              amount: 1400,
              changeTypeName: '游戏投注',
              orderStatus: 4,
              createTime: '2023-02-06 09:30:32',
              billType: 1,
            ),
            DetailList(
              amount: 1400,
              changeTypeName: '游戏投注',
              orderStatus: 5,
              createTime: '2023-02-06 09:30:32',
              billType: 1,
            ),
            DetailList(
              amount: 1400,
              changeTypeName: '游戏投注',
              orderStatus: 6,
              createTime: '2023-02-06 09:30:32',
              billType: 1,
            ),
            DetailList(
              amount: 1400,
              changeTypeName: '游戏投注',
              orderStatus: 7,
              createTime: '2023-02-06 09:30:32',
              billType: 1,
            ),
          ];
    if (isRefresh) {
      transactionList.clear();
    }
    transactionList.addAll(items);
    Log.d('transactionList.length: ${transactionList.length}');
    if (items.isNotEmpty) {
      _noMore = lastItemId == items.last.id;
      lastItemId = items.last.id ?? 0;
    } else {
      _noMore = true;
    }
    update();
  }

  void onRefresh() {
    _getData(isRefresh: true).then((_) {
      refreshController.refreshCompleted(resetFooterState: true);
    }).catchError((_) {
      refreshController.refreshFailed();
    });
  }

  void onLoading() {
    if (!_noMore) {
      _getData().then((_) {
        refreshController.loadComplete();
      }).catchError((_) {
        refreshController.loadFailed();
      });
    } else {
      refreshController.loadNoData();
    }
  }

  @override
  void onInit() {
    refreshController = RefreshController();
    super.onInit();
  }

  @override
  void onReady() {
    onRefresh();
    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
