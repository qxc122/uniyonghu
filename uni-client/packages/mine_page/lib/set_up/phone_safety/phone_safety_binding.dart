import 'package:get/get.dart';

import 'phone_safety_controller.dart';

/// 登录binding
class PhoneSafetyBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PhoneSafetyController>(() => PhoneSafetyController());
  }
}
