/// @date 2023-02-05
/// @author a-bert
/// @des 尺寸统一管理
class AppDimens {
  AppDimens._();

  ///*****************************************************
  ///***              design width height            *****
  ///*****************************************************
  static const double design_w = 750.0;
  static const double design_h = 1334.0;

  ///*****************************************************
  ///***                  width                      *****
  ///*****************************************************
  static const double w_1 = 1.0;
  static const double w_2 = 2.0;
  static const double w_3 = 3.0;
  static const double w_4 = 4.0;
  static const double w_5 = 5.0;
  static const double w_6 = 6.0;
  static const double w_7 = 7.0;
  static const double w_8 = 8.0;
  static const double w_9 = 9.0;
  static const double w_10 = 10.0;
  static const double w_11 = 11.0;
  static const double w_12 = 12.0;
  static const double w_13 = 13.0;
  static const double w_14 = 14.0;
  static const double w_16 = 16.0;
  static const double w_18 = 18.0;
  static const double w_20 = 20.0;
  static const double w_22 = 22.0;
  static const double w_24 = 24.0;
  static const double w_28 = 28.0;
  static const double w_30 = 30.0;
  static const double w_32 = 32.0;
  static const double w_36 = 36.0;
  static const double w_40 = 40.0;
  static const double w_50 = 50.0;
  static const double w_60 = 60.0;
  static const double w_70 = 70.0;
  static const double w_87 = 87.0;
  static const double w_90 = 90.0;
  static const double w_100 = 100.0;
  static const double w_120 = 120.0;
  static const double w_131 = 131.0;
  static const double w_150 = 150.0;
  static const double w_160 = 160.0;
  static const double w_170 = 170.0;
  static const double w_180 = 180.0;
  static const double w_200 = 200.0;
  static const double w_250 = 250.0;
  static const double w_264 = 264.0;
  static const double w_280 = 280.0;
  static const double w_460 = 460.0;

  ///*****************************************************
  ///***                  height                     *****
  ///*****************************************************
  static const double h_1 = 1.0;
  static const double h_2 = 2.0;
  static const double h_3 = 3.0;
  static const double h_4 = 4.0;
  static const double h_5 = 5.0;
  static const double h_6 = 6.0;
  static const double h_7 = 7.0;
  static const double h_8 = 8.0;
  static const double h_10 = 10.0;
  static const double h_12 = 12.0;
  static const double h_13 = 13.0;
  static const double h_16 = 16.0;
  static const double h_20 = 20.0;
  static const double h_24 = 24.0;
  static const double h_25 = 25.0;
  static const double h_26 = 26.0;
  static const double h_28 = 28.0;
  static const double h_30 = 30.0;
  static const double h_36 = 36.0;
  static const double h_38 = 38.0;
  static const double h_40 = 40.0;
  static const double h_42 = 42.0;
  static const double h_45 = 45.0;
  static const double h_50 = 50.0;
  static const double h_60 = 60.0;
  static const double h_70 = 70.0;
  static const double h_80 = 80.0;
  static const double h_90 = 90.0;
  static const double h_100 = 100.0;
  static const double h_120 = 120.0;
  static const double h_130 = 130.0;
  static const double h_140 = 140.0;
  static const double h_150 = 150.0;
  static const double h_180 = 180.0;
  static const double h_200 = 200.0;
  static const double h_220 = 220.0;
  static const double h_222 = 222.0;
  static const double h_240 = 240.0;
  static const double h_250 = 250.0;
  static const double h_260 = 260.0;
  static const double h_280 = 280.0;
  static const double h_131 = 131.0;
  static const double h_300 = 300.0;
  static const double h_360 = 360.0;
  static const double h_400 = 400.0;
  static const double h_600 = 600.0;
}
