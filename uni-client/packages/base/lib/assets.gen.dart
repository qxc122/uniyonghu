/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// ignore_for_file: directives_ordering,unnecessary_import

import 'package:flutter/widgets.dart';

class $AssetsBasePageGen {
  const $AssetsBasePageGen();

  /// File path: assets/base_page/back.png
  AssetGenImage get back => const AssetGenImage('assets/base_page/back.png');

  /// File path: assets/base_page/back_black.png
  AssetGenImage get backBlack =>
      const AssetGenImage('assets/base_page/back_black.png');

  /// File path: assets/base_page/blank.png
  AssetGenImage get blank => const AssetGenImage('assets/base_page/blank.png');

  /// File path: assets/base_page/dagou.png
  AssetGenImage get dagou => const AssetGenImage('assets/base_page/dagou.png');

  /// File path: assets/base_page/ic_sex_man.png
  AssetGenImage get icSexMan =>
      const AssetGenImage('assets/base_page/ic_sex_man.png');

  /// File path: assets/base_page/ic_sex_secrecy.png
  AssetGenImage get icSexSecrecy =>
      const AssetGenImage('assets/base_page/ic_sex_secrecy.png');

  /// File path: assets/base_page/ic_sex_woman.png
  AssetGenImage get icSexWoman =>
      const AssetGenImage('assets/base_page/ic_sex_woman.png');

  /// File path: assets/base_page/ic_splash.png
  AssetGenImage get icSplash =>
      const AssetGenImage('assets/base_page/ic_splash.png');

  /// File path: assets/base_page/login_bg.png
  AssetGenImage get loginBg =>
      const AssetGenImage('assets/base_page/login_bg.png');

  /// File path: assets/base_page/login_bun_bg.png
  AssetGenImage get loginBunBg =>
      const AssetGenImage('assets/base_page/login_bun_bg.png');

  /// File path: assets/base_page/login_line.png
  AssetGenImage get loginLine =>
      const AssetGenImage('assets/base_page/login_line.png');

  /// File path: assets/base_page/login_type.png
  AssetGenImage get loginType =>
      const AssetGenImage('assets/base_page/login_type.png');

  /// File path: assets/base_page/more.png
  AssetGenImage get more => const AssetGenImage('assets/base_page/more.png');

  /// File path: assets/base_page/more_while.png
  AssetGenImage get moreWhile =>
      const AssetGenImage('assets/base_page/more_while.png');

  /// File path: assets/base_page/phone_black.png
  AssetGenImage get phoneBlack =>
      const AssetGenImage('assets/base_page/phone_black.png');

  /// File path: assets/base_page/phone_search.png
  AssetGenImage get phoneSearch =>
      const AssetGenImage('assets/base_page/phone_search.png');

  /// File path: assets/base_page/phone_whilt.png
  AssetGenImage get phoneWhilt =>
      const AssetGenImage('assets/base_page/phone_whilt.png');

  /// File path: assets/base_page/pwd_black.png
  AssetGenImage get pwdBlack =>
      const AssetGenImage('assets/base_page/pwd_black.png');

  /// File path: assets/base_page/pwd_os.png
  AssetGenImage get pwdOs => const AssetGenImage('assets/base_page/pwd_os.png');

  /// File path: assets/base_page/vistor_bg.png
  AssetGenImage get vistorBg =>
      const AssetGenImage('assets/base_page/vistor_bg.png');

  /// File path: assets/base_page/while_pwd.png
  AssetGenImage get whilePwd =>
      const AssetGenImage('assets/base_page/while_pwd.png');

  /// File path: assets/base_page/while_x.png
  AssetGenImage get whileX =>
      const AssetGenImage('assets/base_page/while_x.png');

  /// File path: assets/base_page/xx.png
  AssetGenImage get xx => const AssetGenImage('assets/base_page/xx.png');
}

class $AssetsGamePageGen {
  const $AssetsGamePageGen();

  /// File path: assets/game_page/ic_f_button.png
  AssetGenImage get icFButton =>
      const AssetGenImage('assets/game_page/ic_f_button.png');

  /// File path: assets/game_page/ic_game_selected_bg.png
  AssetGenImage get icGameSelectedBg =>
      const AssetGenImage('assets/game_page/ic_game_selected_bg.png');

  /// File path: assets/game_page/ic_game_unselected_bg.png
  AssetGenImage get icGameUnselectedBg =>
      const AssetGenImage('assets/game_page/ic_game_unselected_bg.png');

  /// File path: assets/game_page/icon_c_game.png
  AssetGenImage get iconCGame =>
      const AssetGenImage('assets/game_page/icon_c_game.png');

  /// File path: assets/game_page/icon_q_game.png
  AssetGenImage get iconQGame =>
      const AssetGenImage('assets/game_page/icon_q_game.png');

  /// File path: assets/game_page/icon_refresh.png
  AssetGenImage get iconRefresh =>
      const AssetGenImage('assets/game_page/icon_refresh.png');

  /// File path: assets/game_page/icon_youhui_game.png
  AssetGenImage get iconYouhuiGame =>
      const AssetGenImage('assets/game_page/icon_youhui_game.png');

  /// File path: assets/game_page/icon_yxjl_game.png
  AssetGenImage get iconYxjlGame =>
      const AssetGenImage('assets/game_page/icon_yxjl_game.png');

  /// File path: assets/game_page/logo_img.png
  AssetGenImage get logoImg =>
      const AssetGenImage('assets/game_page/logo_img.png');

  $AssetsGamePageNiuNiuGen get niuNiu => const $AssetsGamePageNiuNiuGen();

  /// File path: assets/game_page/notice.png
  AssetGenImage get notice =>
      const AssetGenImage('assets/game_page/notice.png');

  /// File path: assets/game_page/readme
  String get readme => 'assets/game_page/readme';

  /// File path: assets/game_page/recharge_withdraw_record.png
  AssetGenImage get rechargeWithdrawRecord =>
      const AssetGenImage('assets/game_page/recharge_withdraw_record.png');
}

class $AssetsHomePageGen {
  const $AssetsHomePageGen();

  /// File path: assets/home_page/ic_home_clear.png
  AssetGenImage get icHomeClear =>
      const AssetGenImage('assets/home_page/ic_home_clear.png');

  /// File path: assets/home_page/ic_home_followed.png
  AssetGenImage get icHomeFollowed =>
      const AssetGenImage('assets/home_page/ic_home_followed.png');

  /// File path: assets/home_page/ic_home_heat.png
  AssetGenImage get icHomeHeat =>
      const AssetGenImage('assets/home_page/ic_home_heat.png');

  /// File path: assets/home_page/ic_home_history.png
  AssetGenImage get icHomeHistory =>
      const AssetGenImage('assets/home_page/ic_home_history.png');

  /// File path: assets/home_page/ic_home_hot_video.png
  AssetGenImage get icHomeHotVideo =>
      const AssetGenImage('assets/home_page/ic_home_hot_video.png');

  /// File path: assets/home_page/ic_home_local.png
  AssetGenImage get icHomeLocal =>
      const AssetGenImage('assets/home_page/ic_home_local.png');

  /// File path: assets/home_page/ic_home_notice_bg.png
  AssetGenImage get icHomeNoticeBg =>
      const AssetGenImage('assets/home_page/ic_home_notice_bg.png');

  /// File path: assets/home_page/ic_home_rank.png
  AssetGenImage get icHomeRank =>
      const AssetGenImage('assets/home_page/ic_home_rank.png');

  /// File path: assets/home_page/ic_home_recommend.png
  AssetGenImage get icHomeRecommend =>
      const AssetGenImage('assets/home_page/ic_home_recommend.png');

  /// File path: assets/home_page/ic_home_search.png
  AssetGenImage get icHomeSearch =>
      const AssetGenImage('assets/home_page/ic_home_search.png');

  /// File path: assets/home_page/ic_home_search_gray.png
  AssetGenImage get icHomeSearchGray =>
      const AssetGenImage('assets/home_page/ic_home_search_gray.png');

  /// File path: assets/home_page/ic_home_search_no_data.png
  AssetGenImage get icHomeSearchNoData =>
      const AssetGenImage('assets/home_page/ic_home_search_no_data.png');

  /// File path: assets/home_page/ic_home_suona.png
  AssetGenImage get icHomeSuona =>
      const AssetGenImage('assets/home_page/ic_home_suona.png');

  /// File path: assets/home_page/ic_home_ticket_bg.png
  AssetGenImage get icHomeTicketBg =>
      const AssetGenImage('assets/home_page/ic_home_ticket_bg.png');

  /// File path: assets/home_page/ic_home_toy.png
  AssetGenImage get icHomeToy =>
      const AssetGenImage('assets/home_page/ic_home_toy.png');
}

class $AssetsLivePageGen {
  const $AssetsLivePageGen();

  /// File path: assets/live_page/anchor_NO_followed_bg.png
  AssetGenImage get anchorNOFollowedBg =>
      const AssetGenImage('assets/live_page/anchor_NO_followed_bg.png');

  /// File path: assets/live_page/anchor_NO_unfollow_bg.png
  AssetGenImage get anchorNOUnfollowBg =>
      const AssetGenImage('assets/live_page/anchor_NO_unfollow_bg.png');

  /// File path: assets/live_page/anchor_card.png
  AssetGenImage get anchorCard =>
      const AssetGenImage('assets/live_page/anchor_card.png');

  /// File path: assets/live_page/anchor_card_dialog_bg.png
  AssetGenImage get anchorCardDialogBg =>
      const AssetGenImage('assets/live_page/anchor_card_dialog_bg.png');

  /// File path: assets/live_page/anchor_card_wechat.png
  AssetGenImage get anchorCardWechat =>
      const AssetGenImage('assets/live_page/anchor_card_wechat.png');

  /// File path: assets/live_page/anchor_male.png
  AssetGenImage get anchorMale =>
      const AssetGenImage('assets/live_page/anchor_male.png');

  /// File path: assets/live_page/anchor_no.1.png
  AssetGenImage get anchorNo1 =>
      const AssetGenImage('assets/live_page/anchor_no.1.png');

  /// File path: assets/live_page/anchor_no.2.png
  AssetGenImage get anchorNo2 =>
      const AssetGenImage('assets/live_page/anchor_no.2.png');

  /// File path: assets/live_page/anchor_no.3.png
  AssetGenImage get anchorNo3 =>
      const AssetGenImage('assets/live_page/anchor_no.3.png');

  /// File path: assets/live_page/anchor_unfollow_bg.png
  AssetGenImage get anchorUnfollowBg =>
      const AssetGenImage('assets/live_page/anchor_unfollow_bg.png');

  /// File path: assets/live_page/arrow_right_white.png
  AssetGenImage get arrowRightWhite =>
      const AssetGenImage('assets/live_page/arrow_right_white.png');

  /// File path: assets/live_page/card_progress_begin.png
  AssetGenImage get cardProgressBegin =>
      const AssetGenImage('assets/live_page/card_progress_begin.png');

  /// File path: assets/live_page/card_progress_crown.png
  AssetGenImage get cardProgressCrown =>
      const AssetGenImage('assets/live_page/card_progress_crown.png');

  /// File path: assets/live_page/card_progress_end.png
  AssetGenImage get cardProgressEnd =>
      const AssetGenImage('assets/live_page/card_progress_end.png');

  /// File path: assets/live_page/contribute.png
  AssetGenImage get contribute =>
      const AssetGenImage('assets/live_page/contribute.png');

  /// File path: assets/live_page/exit_room_dialog_bg.png
  AssetGenImage get exitRoomDialogBg =>
      const AssetGenImage('assets/live_page/exit_room_dialog_bg.png');

  /// File path: assets/live_page/follow.png
  AssetGenImage get follow =>
      const AssetGenImage('assets/live_page/follow.png');

  /// File path: assets/live_page/followed.png
  AssetGenImage get followed =>
      const AssetGenImage('assets/live_page/followed.png');

  /// File path: assets/live_page/game_center.png
  AssetGenImage get gameCenter =>
      const AssetGenImage('assets/live_page/game_center.png');

  /// File path: assets/live_page/gift_diamond.png
  AssetGenImage get giftDiamond =>
      const AssetGenImage('assets/live_page/gift_diamond.png');

  /// File path: assets/live_page/gift_entrance.png
  AssetGenImage get giftEntrance =>
      const AssetGenImage('assets/live_page/gift_entrance.png');

  /// File path: assets/live_page/guard_gold.png
  AssetGenImage get guardGold =>
      const AssetGenImage('assets/live_page/guard_gold.png');

  /// File path: assets/live_page/guard_silver.png
  AssetGenImage get guardSilver =>
      const AssetGenImage('assets/live_page/guard_silver.png');

  /// File path: assets/live_page/live_end_guss_u_like.png
  AssetGenImage get liveEndGussULike =>
      const AssetGenImage('assets/live_page/live_end_guss_u_like.png');

  /// File path: assets/live_page/live_end_placeholder.png
  AssetGenImage get liveEndPlaceholder =>
      const AssetGenImage('assets/live_page/live_end_placeholder.png');

  /// File path: assets/live_page/live_load_bg.png
  AssetGenImage get liveLoadBg =>
      const AssetGenImage('assets/live_page/live_load_bg.png');

  /// File path: assets/live_page/live_recommend.png
  AssetGenImage get liveRecommend =>
      const AssetGenImage('assets/live_page/live_recommend.png');

  /// File path: assets/live_page/manager.png
  AssetGenImage get manager =>
      const AssetGenImage('assets/live_page/manager.png');

  /// File path: assets/live_page/marquis.png
  AssetGenImage get marquis =>
      const AssetGenImage('assets/live_page/marquis.png');

  /// File path: assets/live_page/nav_kf.png
  AssetGenImage get navKf => const AssetGenImage('assets/live_page/nav_kf.png');

  $AssetsLivePageNiuniuGen get niuniu => const $AssetsLivePageNiuniuGen();

  /// File path: assets/live_page/nobility_gold.png
  AssetGenImage get nobilityGold =>
      const AssetGenImage('assets/live_page/nobility_gold.png');

  /// File path: assets/live_page/nobility_silver.png
  AssetGenImage get nobilitySilver =>
      const AssetGenImage('assets/live_page/nobility_silver.png');

  /// File path: assets/live_page/ranking_NO.1.png
  AssetGenImage get rankingNO1 =>
      const AssetGenImage('assets/live_page/ranking_NO.1.png');

  /// File path: assets/live_page/ranking_NO.2.png
  AssetGenImage get rankingNO2 =>
      const AssetGenImage('assets/live_page/ranking_NO.2.png');

  /// File path: assets/live_page/ranking_NO.3.png
  AssetGenImage get rankingNO3 =>
      const AssetGenImage('assets/live_page/ranking_NO.3.png');

  /// File path: assets/live_page/ranking_anchor_bg.png
  AssetGenImage get rankingAnchorBg =>
      const AssetGenImage('assets/live_page/ranking_anchor_bg.png');

  /// File path: assets/live_page/ranking_bg.png
  AssetGenImage get rankingBg =>
      const AssetGenImage('assets/live_page/ranking_bg.png');

  /// File path: assets/live_page/ranking_nav_back.png
  AssetGenImage get rankingNavBack =>
      const AssetGenImage('assets/live_page/ranking_nav_back.png');

  /// File path: assets/live_page/ranking_toff_bg.png
  AssetGenImage get rankingToffBg =>
      const AssetGenImage('assets/live_page/ranking_toff_bg.png');

  /// File path: assets/live_page/recommend_lives_close.png
  AssetGenImage get recommendLivesClose =>
      const AssetGenImage('assets/live_page/recommend_lives_close.png');

  /// File path: assets/live_page/recommend_lives_close_bg.png
  AssetGenImage get recommendLivesCloseBg =>
      const AssetGenImage('assets/live_page/recommend_lives_close_bg.png');

  /// File path: assets/live_page/share_entrance.png
  AssetGenImage get shareEntrance =>
      const AssetGenImage('assets/live_page/share_entrance.png');

  /// File path: assets/live_page/vip_come_in_ diamond.png
  AssetGenImage get vipComeInDiamond =>
      const AssetGenImage('assets/live_page/vip_come_in_ diamond.png');
}

class $AssetsMinePageGen {
  const $AssetsMinePageGen();

  /// File path: assets/mine_page/add_bank.png
  AssetGenImage get addBank =>
      const AssetGenImage('assets/mine_page/add_bank.png');

  /// File path: assets/mine_page/add_bank_back.png
  AssetGenImage get addBankBack =>
      const AssetGenImage('assets/mine_page/add_bank_back.png');

  /// File path: assets/mine_page/an_quan.png
  AssetGenImage get anQuan =>
      const AssetGenImage('assets/mine_page/an_quan.png');

  /// File path: assets/mine_page/an_quan_money.png
  AssetGenImage get anQuanMoney =>
      const AssetGenImage('assets/mine_page/an_quan_money.png');

  /// File path: assets/mine_page/an_quan_money_nor.png
  AssetGenImage get anQuanMoneyNor =>
      const AssetGenImage('assets/mine_page/an_quan_money_nor.png');

  /// File path: assets/mine_page/an_quan_phone.png
  AssetGenImage get anQuanPhone =>
      const AssetGenImage('assets/mine_page/an_quan_phone.png');

  /// File path: assets/mine_page/an_quan_phone_nor.png
  AssetGenImage get anQuanPhoneNor =>
      const AssetGenImage('assets/mine_page/an_quan_phone_nor.png');

  /// File path: assets/mine_page/an_quan_pwd.png
  AssetGenImage get anQuanPwd =>
      const AssetGenImage('assets/mine_page/an_quan_pwd.png');

  /// File path: assets/mine_page/an_quan_pwd_nor.png
  AssetGenImage get anQuanPwdNor =>
      const AssetGenImage('assets/mine_page/an_quan_pwd_nor.png');

  /// File path: assets/mine_page/login_out.png
  AssetGenImage get loginOut =>
      const AssetGenImage('assets/mine_page/login_out.png');

  /// File path: assets/mine_page/min_back.png
  AssetGenImage get minBack =>
      const AssetGenImage('assets/mine_page/min_back.png');

  /// File path: assets/mine_page/mine_arrow_right.png
  AssetGenImage get mineArrowRight =>
      const AssetGenImage('assets/mine_page/mine_arrow_right.png');

  /// File path: assets/mine_page/mine_fan_group.png
  AssetGenImage get mineFanGroup =>
      const AssetGenImage('assets/mine_page/mine_fan_group.png');

  /// File path: assets/mine_page/mine_game_record.png
  AssetGenImage get mineGameRecord =>
      const AssetGenImage('assets/mine_page/mine_game_record.png');

  /// File path: assets/mine_page/mine_huo_dong.png
  AssetGenImage get mineHuoDong =>
      const AssetGenImage('assets/mine_page/mine_huo_dong.png');

  /// File path: assets/mine_page/mine_kefu.png
  AssetGenImage get mineKefu =>
      const AssetGenImage('assets/mine_page/mine_kefu.png');

  /// File path: assets/mine_page/mine_level.png
  AssetGenImage get mineLevel =>
      const AssetGenImage('assets/mine_page/mine_level.png');

  /// File path: assets/mine_page/mine_ming_xi.png
  AssetGenImage get mineMingXi =>
      const AssetGenImage('assets/mine_page/mine_ming_xi.png');

  /// File path: assets/mine_page/mine_phone.png
  AssetGenImage get minePhone =>
      const AssetGenImage('assets/mine_page/mine_phone.png');

  /// File path: assets/mine_page/mine_qianbao.png
  AssetGenImage get mineQianbao =>
      const AssetGenImage('assets/mine_page/mine_qianbao.png');

  /// File path: assets/mine_page/mine_set.png
  AssetGenImage get mineSet =>
      const AssetGenImage('assets/mine_page/mine_set.png');

  /// File path: assets/mine_page/mine_ti_xiang.png
  AssetGenImage get mineTiXiang =>
      const AssetGenImage('assets/mine_page/mine_ti_xiang.png');

  /// File path: assets/mine_page/nav_kf.png
  AssetGenImage get navKf => const AssetGenImage('assets/mine_page/nav_kf.png');

  /// File path: assets/mine_page/no_data.png
  AssetGenImage get noData =>
      const AssetGenImage('assets/mine_page/no_data.png');

  /// File path: assets/mine_page/readme
  String get readme => 'assets/mine_page/readme';

  /// File path: assets/mine_page/setting.png
  AssetGenImage get setting =>
      const AssetGenImage('assets/mine_page/setting.png');

  /// File path: assets/mine_page/sys_emial.png
  AssetGenImage get sysEmial =>
      const AssetGenImage('assets/mine_page/sys_emial.png');

  /// File path: assets/mine_page/sys_messges.png
  AssetGenImage get sysMessges =>
      const AssetGenImage('assets/mine_page/sys_messges.png');

  /// File path: assets/mine_page/sys_msg.png
  AssetGenImage get sysMsg =>
      const AssetGenImage('assets/mine_page/sys_msg.png');

  /// File path: assets/mine_page/sys_msg_detail.png
  AssetGenImage get sysMsgDetail =>
      const AssetGenImage('assets/mine_page/sys_msg_detail.png');

  $AssetsMinePageTransactionGen get transaction =>
      const $AssetsMinePageTransactionGen();

  /// File path: assets/mine_page/transaction_cancel_icon.png
  AssetGenImage get transactionCancelIcon =>
      const AssetGenImage('assets/mine_page/transaction_cancel_icon.png');

  /// File path: assets/mine_page/transaction_failed_icon.png
  AssetGenImage get transactionFailedIcon =>
      const AssetGenImage('assets/mine_page/transaction_failed_icon.png');

  /// File path: assets/mine_page/transaction_info_dotline.png
  AssetGenImage get transactionInfoDotline =>
      const AssetGenImage('assets/mine_page/transaction_info_dotline.png');

  /// File path: assets/mine_page/transaction_other_bg.png
  AssetGenImage get transactionOtherBg =>
      const AssetGenImage('assets/mine_page/transaction_other_bg.png');

  /// File path: assets/mine_page/transaction_process_bg.png
  AssetGenImage get transactionProcessBg =>
      const AssetGenImage('assets/mine_page/transaction_process_bg.png');

  /// File path: assets/mine_page/transaction_process_icon.png
  AssetGenImage get transactionProcessIcon =>
      const AssetGenImage('assets/mine_page/transaction_process_icon.png');

  /// File path: assets/mine_page/transaction_request_bg.png
  AssetGenImage get transactionRequestBg =>
      const AssetGenImage('assets/mine_page/transaction_request_bg.png');

  /// File path: assets/mine_page/transaction_request_icon.png
  AssetGenImage get transactionRequestIcon =>
      const AssetGenImage('assets/mine_page/transaction_request_icon.png');

  /// File path: assets/mine_page/transaction_revocation_icon.png
  AssetGenImage get transactionRevocationIcon =>
      const AssetGenImage('assets/mine_page/transaction_revocation_icon.png');

  /// File path: assets/mine_page/transaction_success_bg.png
  AssetGenImage get transactionSuccessBg =>
      const AssetGenImage('assets/mine_page/transaction_success_bg.png');

  /// File path: assets/mine_page/transaction_success_icon.png
  AssetGenImage get transactionSuccessIcon =>
      const AssetGenImage('assets/mine_page/transaction_success_icon.png');

  /// File path: assets/mine_page/transaction_timeout_icon.png
  AssetGenImage get transactionTimeoutIcon =>
      const AssetGenImage('assets/mine_page/transaction_timeout_icon.png');

  /// File path: assets/mine_page/tration_line.png
  AssetGenImage get trationLine =>
      const AssetGenImage('assets/mine_page/tration_line.png');

  /// File path: assets/mine_page/tre_detail_chen_xiao.png
  AssetGenImage get treDetailChenXiao =>
      const AssetGenImage('assets/mine_page/tre_detail_chen_xiao.png');

  /// File path: assets/mine_page/tre_detail_h.png
  AssetGenImage get treDetailH =>
      const AssetGenImage('assets/mine_page/tre_detail_h.png');

  /// File path: assets/mine_page/tre_detail_hui.png
  AssetGenImage get treDetailHui =>
      const AssetGenImage('assets/mine_page/tre_detail_hui.png');

  /// File path: assets/mine_page/tre_detail_ing.png
  AssetGenImage get treDetailIng =>
      const AssetGenImage('assets/mine_page/tre_detail_ing.png');

  /// File path: assets/mine_page/tre_detail_l.png
  AssetGenImage get treDetailL =>
      const AssetGenImage('assets/mine_page/tre_detail_l.png');

  /// File path: assets/mine_page/tre_detail_ok.png
  AssetGenImage get treDetailOk =>
      const AssetGenImage('assets/mine_page/tre_detail_ok.png');

  /// File path: assets/mine_page/tre_detail_quxiao.png
  AssetGenImage get treDetailQuxiao =>
      const AssetGenImage('assets/mine_page/tre_detail_quxiao.png');

  /// File path: assets/mine_page/tre_detail_quxiao_guanzhu.png
  AssetGenImage get treDetailQuxiaoGuanzhu =>
      const AssetGenImage('assets/mine_page/tre_detail_quxiao_guanzhu.png');

  /// File path: assets/mine_page/tre_detail_shengqing_ing.png
  AssetGenImage get treDetailShengqingIng =>
      const AssetGenImage('assets/mine_page/tre_detail_shengqing_ing.png');

  /// File path: assets/mine_page/tre_detail_time_out.png
  AssetGenImage get treDetailTimeOut =>
      const AssetGenImage('assets/mine_page/tre_detail_time_out.png');

  /// File path: assets/mine_page/tre_detail_z.png
  AssetGenImage get treDetailZ =>
      const AssetGenImage('assets/mine_page/tre_detail_z.png');

  /// File path: assets/mine_page/tre_time.png
  AssetGenImage get treTime =>
      const AssetGenImage('assets/mine_page/tre_time.png');
}

class $AssetsRootPageGen {
  const $AssetsRootPageGen();

  $AssetsRootPageAnimationsGen get animations =>
      const $AssetsRootPageAnimationsGen();
  $AssetsRootPageImagesGen get images => const $AssetsRootPageImagesGen();
}

class $AssetsVideoPageGen {
  const $AssetsVideoPageGen();

  /// File path: assets/video_page/banner.png
  AssetGenImage get banner =>
      const AssetGenImage('assets/video_page/banner.png');

  /// File path: assets/video_page/ff.png
  AssetGenImage get ff => const AssetGenImage('assets/video_page/ff.png');

  /// File path: assets/video_page/mf.png
  AssetGenImage get mf => const AssetGenImage('assets/video_page/mf.png');

  /// File path: assets/video_page/nodata.png
  AssetGenImage get nodata =>
      const AssetGenImage('assets/video_page/nodata.png');

  /// File path: assets/video_page/readme
  String get readme => 'assets/video_page/readme';

  /// File path: assets/video_page/upload.png
  AssetGenImage get upload =>
      const AssetGenImage('assets/video_page/upload.png');

  /// File path: assets/video_page/v_add.png
  AssetGenImage get vAdd => const AssetGenImage('assets/video_page/v_add.png');

  /// File path: assets/video_page/v_ava.png
  AssetGenImage get vAva => const AssetGenImage('assets/video_page/v_ava.png');

  /// File path: assets/video_page/v_back_black.png
  AssetGenImage get vBackBlack =>
      const AssetGenImage('assets/video_page/v_back_black.png');

  /// File path: assets/video_page/v_back_top.png
  AssetGenImage get vBackTop =>
      const AssetGenImage('assets/video_page/v_back_top.png');

  /// File path: assets/video_page/v_back_white.png
  AssetGenImage get vBackWhite =>
      const AssetGenImage('assets/video_page/v_back_white.png');

  /// File path: assets/video_page/v_cache.png
  AssetGenImage get vCache =>
      const AssetGenImage('assets/video_page/v_cache.png');

  /// File path: assets/video_page/v_channel.png
  AssetGenImage get vChannel =>
      const AssetGenImage('assets/video_page/v_channel.png');

  /// File path: assets/video_page/v_close.png
  AssetGenImage get vClose =>
      const AssetGenImage('assets/video_page/v_close.png');

  /// File path: assets/video_page/v_close_b.png
  AssetGenImage get vCloseB =>
      const AssetGenImage('assets/video_page/v_close_b.png');

  /// File path: assets/video_page/v_close_white.png
  AssetGenImage get vCloseWhite =>
      const AssetGenImage('assets/video_page/v_close_white.png');

  /// File path: assets/video_page/v_cover_bg.png
  AssetGenImage get vCoverBg =>
      const AssetGenImage('assets/video_page/v_cover_bg.png');

  /// File path: assets/video_page/v_detail_notice.png
  AssetGenImage get vDetailNotice =>
      const AssetGenImage('assets/video_page/v_detail_notice.png');

  /// File path: assets/video_page/v_down_icon.png
  AssetGenImage get vDownIcon =>
      const AssetGenImage('assets/video_page/v_down_icon.png');

  /// File path: assets/video_page/v_down_white.png
  AssetGenImage get vDownWhite =>
      const AssetGenImage('assets/video_page/v_down_white.png');

  /// File path: assets/video_page/v_download.png
  AssetGenImage get vDownload =>
      const AssetGenImage('assets/video_page/v_download.png');

  /// File path: assets/video_page/v_eye.png
  AssetGenImage get vEye => const AssetGenImage('assets/video_page/v_eye.png');

  /// File path: assets/video_page/v_follow.png
  AssetGenImage get vFollow =>
      const AssetGenImage('assets/video_page/v_follow.png');

  /// File path: assets/video_page/v_followed.png
  AssetGenImage get vFollowed =>
      const AssetGenImage('assets/video_page/v_followed.png');

  /// File path: assets/video_page/v_free.png
  AssetGenImage get vFree =>
      const AssetGenImage('assets/video_page/v_free.png');

  /// File path: assets/video_page/v_history.png
  AssetGenImage get vHistory =>
      const AssetGenImage('assets/video_page/v_history.png');

  /// File path: assets/video_page/v_img.png
  AssetGenImage get vImg => const AssetGenImage('assets/video_page/v_img.png');

  /// File path: assets/video_page/v_item_bg.png
  AssetGenImage get vItemBg =>
      const AssetGenImage('assets/video_page/v_item_bg.png');

  /// File path: assets/video_page/v_love.png
  AssetGenImage get vLove =>
      const AssetGenImage('assets/video_page/v_love.png');

  /// File path: assets/video_page/v_mine_bg.png
  AssetGenImage get vMineBg =>
      const AssetGenImage('assets/video_page/v_mine_bg.png');

  /// File path: assets/video_page/v_mine_center.png
  AssetGenImage get vMineCenter =>
      const AssetGenImage('assets/video_page/v_mine_center.png');

  /// File path: assets/video_page/v_mine_chat.png
  AssetGenImage get vMineChat =>
      const AssetGenImage('assets/video_page/v_mine_chat.png');

  /// File path: assets/video_page/v_mine_rp.png
  AssetGenImage get vMineRp =>
      const AssetGenImage('assets/video_page/v_mine_rp.png');

  /// File path: assets/video_page/v_mine_setting.png
  AssetGenImage get vMineSetting =>
      const AssetGenImage('assets/video_page/v_mine_setting.png');

  /// File path: assets/video_page/v_money_icon.png
  AssetGenImage get vMoneyIcon =>
      const AssetGenImage('assets/video_page/v_money_icon.png');

  /// File path: assets/video_page/v_more.png
  AssetGenImage get vMore =>
      const AssetGenImage('assets/video_page/v_more.png');

  /// File path: assets/video_page/v_my_like.png
  AssetGenImage get vMyLike =>
      const AssetGenImage('assets/video_page/v_my_like.png');

  /// File path: assets/video_page/v_notice.png
  AssetGenImage get vNotice =>
      const AssetGenImage('assets/video_page/v_notice.png');

  /// File path: assets/video_page/v_open_vip.png
  AssetGenImage get vOpenVip =>
      const AssetGenImage('assets/video_page/v_open_vip.png');

  /// File path: assets/video_page/v_opinion.png
  AssetGenImage get vOpinion =>
      const AssetGenImage('assets/video_page/v_opinion.png');

  /// File path: assets/video_page/v_opinion_h.png
  AssetGenImage get vOpinionH =>
      const AssetGenImage('assets/video_page/v_opinion_h.png');

  /// File path: assets/video_page/v_opinion_icon.png
  AssetGenImage get vOpinionIcon =>
      const AssetGenImage('assets/video_page/v_opinion_icon.png');

  /// File path: assets/video_page/v_pay.png
  AssetGenImage get vPay => const AssetGenImage('assets/video_page/v_pay.png');

  /// File path: assets/video_page/v_play_b.png
  AssetGenImage get vPlayB =>
      const AssetGenImage('assets/video_page/v_play_b.png');

  /// File path: assets/video_page/v_play_icon.png
  AssetGenImage get vPlayIcon =>
      const AssetGenImage('assets/video_page/v_play_icon.png');

  /// File path: assets/video_page/v_purchased.png
  AssetGenImage get vPurchased =>
      const AssetGenImage('assets/video_page/v_purchased.png');

  /// File path: assets/video_page/v_push.png
  AssetGenImage get vPush =>
      const AssetGenImage('assets/video_page/v_push.png');

  /// File path: assets/video_page/v_radio_select.png
  AssetGenImage get vRadioSelect =>
      const AssetGenImage('assets/video_page/v_radio_select.png');

  /// File path: assets/video_page/v_reduce.png
  AssetGenImage get vReduce =>
      const AssetGenImage('assets/video_page/v_reduce.png');

  /// File path: assets/video_page/v_report.png
  AssetGenImage get vReport =>
      const AssetGenImage('assets/video_page/v_report.png');

  /// File path: assets/video_page/v_rp_b.png
  AssetGenImage get vRpB => const AssetGenImage('assets/video_page/v_rp_b.png');

  /// File path: assets/video_page/v_rp_h.png
  AssetGenImage get vRpH => const AssetGenImage('assets/video_page/v_rp_h.png');

  /// File path: assets/video_page/v_rp_white.png
  AssetGenImage get vRpWhite =>
      const AssetGenImage('assets/video_page/v_rp_white.png');

  /// File path: assets/video_page/v_screen.png
  AssetGenImage get vScreen =>
      const AssetGenImage('assets/video_page/v_screen.png');

  /// File path: assets/video_page/v_seach_icon.png
  AssetGenImage get vSeachIcon =>
      const AssetGenImage('assets/video_page/v_seach_icon.png');

  /// File path: assets/video_page/v_selcet_video.png
  AssetGenImage get vSelcetVideo =>
      const AssetGenImage('assets/video_page/v_selcet_video.png');

  /// File path: assets/video_page/v_select.png
  AssetGenImage get vSelect =>
      const AssetGenImage('assets/video_page/v_select.png');

  /// File path: assets/video_page/v_service.png
  AssetGenImage get vService =>
      const AssetGenImage('assets/video_page/v_service.png');

  /// File path: assets/video_page/v_setting.png
  AssetGenImage get vSetting =>
      const AssetGenImage('assets/video_page/v_setting.png');

  /// File path: assets/video_page/v_share_link.png
  AssetGenImage get vShareLink =>
      const AssetGenImage('assets/video_page/v_share_link.png');

  /// File path: assets/video_page/v_star.png
  AssetGenImage get vStar =>
      const AssetGenImage('assets/video_page/v_star.png');

  /// File path: assets/video_page/v_swiper.png
  AssetGenImage get vSwiper =>
      const AssetGenImage('assets/video_page/v_swiper.png');

  /// File path: assets/video_page/v_thumbs.png
  AssetGenImage get vThumbs =>
      const AssetGenImage('assets/video_page/v_thumbs.png');

  /// File path: assets/video_page/v_un.png
  AssetGenImage get vUn => const AssetGenImage('assets/video_page/v_un.png');

  /// File path: assets/video_page/v_un_follow.png
  AssetGenImage get vUnFollow =>
      const AssetGenImage('assets/video_page/v_un_follow.png');

  /// File path: assets/video_page/v_up_icon.png
  AssetGenImage get vUpIcon =>
      const AssetGenImage('assets/video_page/v_up_icon.png');

  /// File path: assets/video_page/v_v.png
  AssetGenImage get vV => const AssetGenImage('assets/video_page/v_v.png');

  /// File path: assets/video_page/v_video.png
  AssetGenImage get vVideo =>
      const AssetGenImage('assets/video_page/v_video.png');

  /// File path: assets/video_page/v_vip_vip_icon.png
  AssetGenImage get vVipVipIcon =>
      const AssetGenImage('assets/video_page/v_vip_vip_icon.png');

  /// File path: assets/video_page/v_write.png
  AssetGenImage get vWrite =>
      const AssetGenImage('assets/video_page/v_write.png');

  /// File path: assets/video_page/vip.png
  AssetGenImage get vip => const AssetGenImage('assets/video_page/vip.png');

  /// File path: assets/video_page/vip_type.png
  AssetGenImage get vipType =>
      const AssetGenImage('assets/video_page/vip_type.png');

  /// File path: assets/video_page/voice.png
  AssetGenImage get voice => const AssetGenImage('assets/video_page/voice.png');
}

class $AssetsWalletPageGen {
  const $AssetsWalletPageGen();

  /// File path: assets/wallet_page/bei_jin.png
  AssetGenImage get beiJin =>
      const AssetGenImage('assets/wallet_page/bei_jin.png');

  /// File path: assets/wallet_page/dui_huan.png
  AssetGenImage get duiHuan =>
      const AssetGenImage('assets/wallet_page/dui_huan.png');

  /// File path: assets/wallet_page/ex_fail.png
  AssetGenImage get exFail =>
      const AssetGenImage('assets/wallet_page/ex_fail.png');

  /// File path: assets/wallet_page/ex_money.png
  AssetGenImage get exMoney =>
      const AssetGenImage('assets/wallet_page/ex_money.png');

  /// File path: assets/wallet_page/ex_recharge.png
  AssetGenImage get exRecharge =>
      const AssetGenImage('assets/wallet_page/ex_recharge.png');

  /// File path: assets/wallet_page/ex_success.png
  AssetGenImage get exSuccess =>
      const AssetGenImage('assets/wallet_page/ex_success.png');

  /// File path: assets/wallet_page/gong_gao.png
  AssetGenImage get gongGao =>
      const AssetGenImage('assets/wallet_page/gong_gao.png');

  /// File path: assets/wallet_page/line.png
  AssetGenImage get line => const AssetGenImage('assets/wallet_page/line.png');

  /// File path: assets/wallet_page/pay_money_bank_background.png
  AssetGenImage get payMoneyBankBackground =>
      const AssetGenImage('assets/wallet_page/pay_money_bank_background.png');

  /// File path: assets/wallet_page/readme
  String get readme => 'assets/wallet_page/readme';

  /// File path: assets/wallet_page/xuan_zhong.png
  AssetGenImage get xuanZhong =>
      const AssetGenImage('assets/wallet_page/xuan_zhong.png');
}

class $AssetsGamePageNiuNiuGen {
  const $AssetsGamePageNiuNiuGen();

  /// File path: assets/game_page/niu_niu/card_1.png
  AssetGenImage get card1 =>
      const AssetGenImage('assets/game_page/niu_niu/card_1.png');

  /// File path: assets/game_page/niu_niu/card_10.png
  AssetGenImage get card10 =>
      const AssetGenImage('assets/game_page/niu_niu/card_10.png');

  /// File path: assets/game_page/niu_niu/card_11.png
  AssetGenImage get card11 =>
      const AssetGenImage('assets/game_page/niu_niu/card_11.png');

  /// File path: assets/game_page/niu_niu/card_12.png
  AssetGenImage get card12 =>
      const AssetGenImage('assets/game_page/niu_niu/card_12.png');

  /// File path: assets/game_page/niu_niu/card_13.png
  AssetGenImage get card13 =>
      const AssetGenImage('assets/game_page/niu_niu/card_13.png');

  /// File path: assets/game_page/niu_niu/card_14.png
  AssetGenImage get card14 =>
      const AssetGenImage('assets/game_page/niu_niu/card_14.png');

  /// File path: assets/game_page/niu_niu/card_15.png
  AssetGenImage get card15 =>
      const AssetGenImage('assets/game_page/niu_niu/card_15.png');

  /// File path: assets/game_page/niu_niu/card_16.png
  AssetGenImage get card16 =>
      const AssetGenImage('assets/game_page/niu_niu/card_16.png');

  /// File path: assets/game_page/niu_niu/card_17.png
  AssetGenImage get card17 =>
      const AssetGenImage('assets/game_page/niu_niu/card_17.png');

  /// File path: assets/game_page/niu_niu/card_18.png
  AssetGenImage get card18 =>
      const AssetGenImage('assets/game_page/niu_niu/card_18.png');

  /// File path: assets/game_page/niu_niu/card_19.png
  AssetGenImage get card19 =>
      const AssetGenImage('assets/game_page/niu_niu/card_19.png');

  /// File path: assets/game_page/niu_niu/card_2.png
  AssetGenImage get card2 =>
      const AssetGenImage('assets/game_page/niu_niu/card_2.png');

  /// File path: assets/game_page/niu_niu/card_20.png
  AssetGenImage get card20 =>
      const AssetGenImage('assets/game_page/niu_niu/card_20.png');

  /// File path: assets/game_page/niu_niu/card_21.png
  AssetGenImage get card21 =>
      const AssetGenImage('assets/game_page/niu_niu/card_21.png');

  /// File path: assets/game_page/niu_niu/card_22.png
  AssetGenImage get card22 =>
      const AssetGenImage('assets/game_page/niu_niu/card_22.png');

  /// File path: assets/game_page/niu_niu/card_23.png
  AssetGenImage get card23 =>
      const AssetGenImage('assets/game_page/niu_niu/card_23.png');

  /// File path: assets/game_page/niu_niu/card_24.png
  AssetGenImage get card24 =>
      const AssetGenImage('assets/game_page/niu_niu/card_24.png');

  /// File path: assets/game_page/niu_niu/card_25.png
  AssetGenImage get card25 =>
      const AssetGenImage('assets/game_page/niu_niu/card_25.png');

  /// File path: assets/game_page/niu_niu/card_26.png
  AssetGenImage get card26 =>
      const AssetGenImage('assets/game_page/niu_niu/card_26.png');

  /// File path: assets/game_page/niu_niu/card_27.png
  AssetGenImage get card27 =>
      const AssetGenImage('assets/game_page/niu_niu/card_27.png');

  /// File path: assets/game_page/niu_niu/card_28.png
  AssetGenImage get card28 =>
      const AssetGenImage('assets/game_page/niu_niu/card_28.png');

  /// File path: assets/game_page/niu_niu/card_29.png
  AssetGenImage get card29 =>
      const AssetGenImage('assets/game_page/niu_niu/card_29.png');

  /// File path: assets/game_page/niu_niu/card_3.png
  AssetGenImage get card3 =>
      const AssetGenImage('assets/game_page/niu_niu/card_3.png');

  /// File path: assets/game_page/niu_niu/card_30.png
  AssetGenImage get card30 =>
      const AssetGenImage('assets/game_page/niu_niu/card_30.png');

  /// File path: assets/game_page/niu_niu/card_31.png
  AssetGenImage get card31 =>
      const AssetGenImage('assets/game_page/niu_niu/card_31.png');

  /// File path: assets/game_page/niu_niu/card_32.png
  AssetGenImage get card32 =>
      const AssetGenImage('assets/game_page/niu_niu/card_32.png');

  /// File path: assets/game_page/niu_niu/card_33.png
  AssetGenImage get card33 =>
      const AssetGenImage('assets/game_page/niu_niu/card_33.png');

  /// File path: assets/game_page/niu_niu/card_34.png
  AssetGenImage get card34 =>
      const AssetGenImage('assets/game_page/niu_niu/card_34.png');

  /// File path: assets/game_page/niu_niu/card_35.png
  AssetGenImage get card35 =>
      const AssetGenImage('assets/game_page/niu_niu/card_35.png');

  /// File path: assets/game_page/niu_niu/card_36.png
  AssetGenImage get card36 =>
      const AssetGenImage('assets/game_page/niu_niu/card_36.png');

  /// File path: assets/game_page/niu_niu/card_37.png
  AssetGenImage get card37 =>
      const AssetGenImage('assets/game_page/niu_niu/card_37.png');

  /// File path: assets/game_page/niu_niu/card_38.png
  AssetGenImage get card38 =>
      const AssetGenImage('assets/game_page/niu_niu/card_38.png');

  /// File path: assets/game_page/niu_niu/card_39.png
  AssetGenImage get card39 =>
      const AssetGenImage('assets/game_page/niu_niu/card_39.png');

  /// File path: assets/game_page/niu_niu/card_4.png
  AssetGenImage get card4 =>
      const AssetGenImage('assets/game_page/niu_niu/card_4.png');

  /// File path: assets/game_page/niu_niu/card_40.png
  AssetGenImage get card40 =>
      const AssetGenImage('assets/game_page/niu_niu/card_40.png');

  /// File path: assets/game_page/niu_niu/card_41.png
  AssetGenImage get card41 =>
      const AssetGenImage('assets/game_page/niu_niu/card_41.png');

  /// File path: assets/game_page/niu_niu/card_42.png
  AssetGenImage get card42 =>
      const AssetGenImage('assets/game_page/niu_niu/card_42.png');

  /// File path: assets/game_page/niu_niu/card_43.png
  AssetGenImage get card43 =>
      const AssetGenImage('assets/game_page/niu_niu/card_43.png');

  /// File path: assets/game_page/niu_niu/card_44.png
  AssetGenImage get card44 =>
      const AssetGenImage('assets/game_page/niu_niu/card_44.png');

  /// File path: assets/game_page/niu_niu/card_45.png
  AssetGenImage get card45 =>
      const AssetGenImage('assets/game_page/niu_niu/card_45.png');

  /// File path: assets/game_page/niu_niu/card_46.png
  AssetGenImage get card46 =>
      const AssetGenImage('assets/game_page/niu_niu/card_46.png');

  /// File path: assets/game_page/niu_niu/card_47.png
  AssetGenImage get card47 =>
      const AssetGenImage('assets/game_page/niu_niu/card_47.png');

  /// File path: assets/game_page/niu_niu/card_48.png
  AssetGenImage get card48 =>
      const AssetGenImage('assets/game_page/niu_niu/card_48.png');

  /// File path: assets/game_page/niu_niu/card_49.png
  AssetGenImage get card49 =>
      const AssetGenImage('assets/game_page/niu_niu/card_49.png');

  /// File path: assets/game_page/niu_niu/card_5.png
  AssetGenImage get card5 =>
      const AssetGenImage('assets/game_page/niu_niu/card_5.png');

  /// File path: assets/game_page/niu_niu/card_50.png
  AssetGenImage get card50 =>
      const AssetGenImage('assets/game_page/niu_niu/card_50.png');

  /// File path: assets/game_page/niu_niu/card_51.png
  AssetGenImage get card51 =>
      const AssetGenImage('assets/game_page/niu_niu/card_51.png');

  /// File path: assets/game_page/niu_niu/card_52.png
  AssetGenImage get card52 =>
      const AssetGenImage('assets/game_page/niu_niu/card_52.png');

  /// File path: assets/game_page/niu_niu/card_53.png
  AssetGenImage get card53 =>
      const AssetGenImage('assets/game_page/niu_niu/card_53.png');

  /// File path: assets/game_page/niu_niu/card_54.png
  AssetGenImage get card54 =>
      const AssetGenImage('assets/game_page/niu_niu/card_54.png');

  /// File path: assets/game_page/niu_niu/card_6.png
  AssetGenImage get card6 =>
      const AssetGenImage('assets/game_page/niu_niu/card_6.png');

  /// File path: assets/game_page/niu_niu/card_7.png
  AssetGenImage get card7 =>
      const AssetGenImage('assets/game_page/niu_niu/card_7.png');

  /// File path: assets/game_page/niu_niu/card_8.png
  AssetGenImage get card8 =>
      const AssetGenImage('assets/game_page/niu_niu/card_8.png');

  /// File path: assets/game_page/niu_niu/card_9.png
  AssetGenImage get card9 =>
      const AssetGenImage('assets/game_page/niu_niu/card_9.png');

  /// File path: assets/game_page/niu_niu/card_b_0.png
  AssetGenImage get cardB0 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_0.png');

  /// File path: assets/game_page/niu_niu/card_b_1.png
  AssetGenImage get cardB1 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_1.png');

  /// File path: assets/game_page/niu_niu/card_b_2.png
  AssetGenImage get cardB2 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_2.png');

  /// File path: assets/game_page/niu_niu/card_b_3.png
  AssetGenImage get cardB3 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_3.png');

  /// File path: assets/game_page/niu_niu/card_b_4.png
  AssetGenImage get cardB4 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_4.png');

  /// File path: assets/game_page/niu_niu/card_b_5.png
  AssetGenImage get cardB5 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_5.png');

  /// File path: assets/game_page/niu_niu/card_b_6.png
  AssetGenImage get cardB6 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_6.png');

  /// File path: assets/game_page/niu_niu/card_b_7.png
  AssetGenImage get cardB7 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_7.png');

  /// File path: assets/game_page/niu_niu/card_b_8.png
  AssetGenImage get cardB8 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_8.png');

  /// File path: assets/game_page/niu_niu/card_b_9.png
  AssetGenImage get cardB9 =>
      const AssetGenImage('assets/game_page/niu_niu/card_b_9.png');

  /// File path: assets/game_page/niu_niu/card_default.png
  AssetGenImage get cardDefault =>
      const AssetGenImage('assets/game_page/niu_niu/card_default.png');

  /// File path: assets/game_page/niu_niu/card_n_0.png
  AssetGenImage get cardN0 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_0.png');

  /// File path: assets/game_page/niu_niu/card_n_1.png
  AssetGenImage get cardN1 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_1.png');

  /// File path: assets/game_page/niu_niu/card_n_10.png
  AssetGenImage get cardN10 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_10.png');

  /// File path: assets/game_page/niu_niu/card_n_11.png
  AssetGenImage get cardN11 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_11.png');

  /// File path: assets/game_page/niu_niu/card_n_12.png
  AssetGenImage get cardN12 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_12.png');

  /// File path: assets/game_page/niu_niu/card_n_13.png
  AssetGenImage get cardN13 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_13.png');

  /// File path: assets/game_page/niu_niu/card_n_14.png
  AssetGenImage get cardN14 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_14.png');

  /// File path: assets/game_page/niu_niu/card_n_2.png
  AssetGenImage get cardN2 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_2.png');

  /// File path: assets/game_page/niu_niu/card_n_3.png
  AssetGenImage get cardN3 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_3.png');

  /// File path: assets/game_page/niu_niu/card_n_4.png
  AssetGenImage get cardN4 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_4.png');

  /// File path: assets/game_page/niu_niu/card_n_5.png
  AssetGenImage get cardN5 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_5.png');

  /// File path: assets/game_page/niu_niu/card_n_6.png
  AssetGenImage get cardN6 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_6.png');

  /// File path: assets/game_page/niu_niu/card_n_7.png
  AssetGenImage get cardN7 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_7.png');

  /// File path: assets/game_page/niu_niu/card_n_8.png
  AssetGenImage get cardN8 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_8.png');

  /// File path: assets/game_page/niu_niu/card_n_9.png
  AssetGenImage get cardN9 =>
      const AssetGenImage('assets/game_page/niu_niu/card_n_9.png');

  /// File path: assets/game_page/niu_niu/chip_1.webp
  AssetGenImage get chip1 =>
      const AssetGenImage('assets/game_page/niu_niu/chip_1.webp');

  /// File path: assets/game_page/niu_niu/chip_2.webp
  AssetGenImage get chip2 =>
      const AssetGenImage('assets/game_page/niu_niu/chip_2.webp');

  /// File path: assets/game_page/niu_niu/chip_3.webp
  AssetGenImage get chip3 =>
      const AssetGenImage('assets/game_page/niu_niu/chip_3.webp');

  /// File path: assets/game_page/niu_niu/chip_4.webp
  AssetGenImage get chip4 =>
      const AssetGenImage('assets/game_page/niu_niu/chip_4.webp');

  /// File path: assets/game_page/niu_niu/chip_5.webp
  AssetGenImage get chip5 =>
      const AssetGenImage('assets/game_page/niu_niu/chip_5.webp');

  /// File path: assets/game_page/niu_niu/chip_6.webp
  AssetGenImage get chip6 =>
      const AssetGenImage('assets/game_page/niu_niu/chip_6.webp');

  /// File path: assets/game_page/niu_niu/chip_7.webp
  AssetGenImage get chip7 =>
      const AssetGenImage('assets/game_page/niu_niu/chip_7.webp');

  /// File path: assets/game_page/niu_niu/chip_8.webp
  AssetGenImage get chip8 =>
      const AssetGenImage('assets/game_page/niu_niu/chip_8.webp');

  /// File path: assets/game_page/niu_niu/win_both.png
  AssetGenImage get winBoth =>
      const AssetGenImage('assets/game_page/niu_niu/win_both.png');

  /// File path: assets/game_page/niu_niu/win_left.png
  AssetGenImage get winLeft =>
      const AssetGenImage('assets/game_page/niu_niu/win_left.png');

  /// File path: assets/game_page/niu_niu/win_right.png
  AssetGenImage get winRight =>
      const AssetGenImage('assets/game_page/niu_niu/win_right.png');
}

class $AssetsLivePageNiuniuGen {
  const $AssetsLivePageNiuniuGen();

  /// File path: assets/live_page/niuniu/arrow_up.svg
  String get arrowUp => 'assets/live_page/niuniu/arrow_up.svg';

  /// File path: assets/live_page/niuniu/chip.png
  AssetGenImage get chip =>
      const AssetGenImage('assets/live_page/niuniu/chip.png');

  /// File path: assets/live_page/niuniu/niu_0.png
  AssetGenImage get niu0 =>
      const AssetGenImage('assets/live_page/niuniu/niu_0.png');

  /// File path: assets/live_page/niuniu/niu_1.png
  AssetGenImage get niu1 =>
      const AssetGenImage('assets/live_page/niuniu/niu_1.png');

  /// File path: assets/live_page/niuniu/niu_2.png
  AssetGenImage get niu2 =>
      const AssetGenImage('assets/live_page/niuniu/niu_2.png');

  /// File path: assets/live_page/niuniu/niu_3.png
  AssetGenImage get niu3 =>
      const AssetGenImage('assets/live_page/niuniu/niu_3.png');

  /// File path: assets/live_page/niuniu/niu_4.png
  AssetGenImage get niu4 =>
      const AssetGenImage('assets/live_page/niuniu/niu_4.png');

  /// File path: assets/live_page/niuniu/niu_5.png
  AssetGenImage get niu5 =>
      const AssetGenImage('assets/live_page/niuniu/niu_5.png');

  /// File path: assets/live_page/niuniu/niu_6.png
  AssetGenImage get niu6 =>
      const AssetGenImage('assets/live_page/niuniu/niu_6.png');

  /// File path: assets/live_page/niuniu/niu_7.png
  AssetGenImage get niu7 =>
      const AssetGenImage('assets/live_page/niuniu/niu_7.png');

  /// File path: assets/live_page/niuniu/niu_8.png
  AssetGenImage get niu8 =>
      const AssetGenImage('assets/live_page/niuniu/niu_8.png');

  /// File path: assets/live_page/niuniu/niu_9.png
  AssetGenImage get niu9 =>
      const AssetGenImage('assets/live_page/niuniu/niu_9.png');

  /// File path: assets/live_page/niuniu/niu_flower.png
  AssetGenImage get niuFlower =>
      const AssetGenImage('assets/live_page/niuniu/niu_flower.png');

  /// File path: assets/live_page/niuniu/niu_niu.png
  AssetGenImage get niuNiu =>
      const AssetGenImage('assets/live_page/niuniu/niu_niu.png');

  /// File path: assets/live_page/niuniu/niu_niu_play.svg
  String get niuNiuPlay => 'assets/live_page/niuniu/niu_niu_play.svg';

  /// File path: assets/live_page/niuniu/niu_silver.png
  AssetGenImage get niuSilver =>
      const AssetGenImage('assets/live_page/niuniu/niu_silver.png');

  /// File path: assets/live_page/niuniu/playing_card_1_1.png
  AssetGenImage get playingCard11 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_1.png');

  /// File path: assets/live_page/niuniu/playing_card_1_10.png
  AssetGenImage get playingCard110 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_10.png');

  /// File path: assets/live_page/niuniu/playing_card_1_11.png
  AssetGenImage get playingCard111 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_11.png');

  /// File path: assets/live_page/niuniu/playing_card_1_12.png
  AssetGenImage get playingCard112 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_12.png');

  /// File path: assets/live_page/niuniu/playing_card_1_13.png
  AssetGenImage get playingCard113 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_13.png');

  /// File path: assets/live_page/niuniu/playing_card_1_2.png
  AssetGenImage get playingCard12 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_2.png');

  /// File path: assets/live_page/niuniu/playing_card_1_3.png
  AssetGenImage get playingCard13 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_3.png');

  /// File path: assets/live_page/niuniu/playing_card_1_4.png
  AssetGenImage get playingCard14 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_4.png');

  /// File path: assets/live_page/niuniu/playing_card_1_5.png
  AssetGenImage get playingCard15 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_5.png');

  /// File path: assets/live_page/niuniu/playing_card_1_6.png
  AssetGenImage get playingCard16 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_6.png');

  /// File path: assets/live_page/niuniu/playing_card_1_7.png
  AssetGenImage get playingCard17 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_7.png');

  /// File path: assets/live_page/niuniu/playing_card_1_8.png
  AssetGenImage get playingCard18 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_8.png');

  /// File path: assets/live_page/niuniu/playing_card_1_9.png
  AssetGenImage get playingCard19 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_1_9.png');

  /// File path: assets/live_page/niuniu/playing_card_2_1.png
  AssetGenImage get playingCard21 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_1.png');

  /// File path: assets/live_page/niuniu/playing_card_2_10.png
  AssetGenImage get playingCard210 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_10.png');

  /// File path: assets/live_page/niuniu/playing_card_2_11.png
  AssetGenImage get playingCard211 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_11.png');

  /// File path: assets/live_page/niuniu/playing_card_2_12.png
  AssetGenImage get playingCard212 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_12.png');

  /// File path: assets/live_page/niuniu/playing_card_2_13.png
  AssetGenImage get playingCard213 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_13.png');

  /// File path: assets/live_page/niuniu/playing_card_2_2.png
  AssetGenImage get playingCard22 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_2.png');

  /// File path: assets/live_page/niuniu/playing_card_2_3.png
  AssetGenImage get playingCard23 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_3.png');

  /// File path: assets/live_page/niuniu/playing_card_2_4.png
  AssetGenImage get playingCard24 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_4.png');

  /// File path: assets/live_page/niuniu/playing_card_2_5.png
  AssetGenImage get playingCard25 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_5.png');

  /// File path: assets/live_page/niuniu/playing_card_2_6.png
  AssetGenImage get playingCard26 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_6.png');

  /// File path: assets/live_page/niuniu/playing_card_2_7.png
  AssetGenImage get playingCard27 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_7.png');

  /// File path: assets/live_page/niuniu/playing_card_2_8.png
  AssetGenImage get playingCard28 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_8.png');

  /// File path: assets/live_page/niuniu/playing_card_2_9.png
  AssetGenImage get playingCard29 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_2_9.png');

  /// File path: assets/live_page/niuniu/playing_card_3_1.png
  AssetGenImage get playingCard31 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_1.png');

  /// File path: assets/live_page/niuniu/playing_card_3_10.png
  AssetGenImage get playingCard310 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_10.png');

  /// File path: assets/live_page/niuniu/playing_card_3_11.png
  AssetGenImage get playingCard311 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_11.png');

  /// File path: assets/live_page/niuniu/playing_card_3_12.png
  AssetGenImage get playingCard312 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_12.png');

  /// File path: assets/live_page/niuniu/playing_card_3_13.png
  AssetGenImage get playingCard313 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_13.png');

  /// File path: assets/live_page/niuniu/playing_card_3_2.png
  AssetGenImage get playingCard32 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_2.png');

  /// File path: assets/live_page/niuniu/playing_card_3_3.png
  AssetGenImage get playingCard33 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_3.png');

  /// File path: assets/live_page/niuniu/playing_card_3_4.png
  AssetGenImage get playingCard34 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_4.png');

  /// File path: assets/live_page/niuniu/playing_card_3_5.png
  AssetGenImage get playingCard35 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_5.png');

  /// File path: assets/live_page/niuniu/playing_card_3_6.png
  AssetGenImage get playingCard36 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_6.png');

  /// File path: assets/live_page/niuniu/playing_card_3_7.png
  AssetGenImage get playingCard37 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_7.png');

  /// File path: assets/live_page/niuniu/playing_card_3_8.png
  AssetGenImage get playingCard38 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_8.png');

  /// File path: assets/live_page/niuniu/playing_card_3_9.png
  AssetGenImage get playingCard39 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_3_9.png');

  /// File path: assets/live_page/niuniu/playing_card_4_1.png
  AssetGenImage get playingCard41 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_1.png');

  /// File path: assets/live_page/niuniu/playing_card_4_10.png
  AssetGenImage get playingCard410 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_10.png');

  /// File path: assets/live_page/niuniu/playing_card_4_11.png
  AssetGenImage get playingCard411 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_11.png');

  /// File path: assets/live_page/niuniu/playing_card_4_12.png
  AssetGenImage get playingCard412 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_12.png');

  /// File path: assets/live_page/niuniu/playing_card_4_13.png
  AssetGenImage get playingCard413 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_13.png');

  /// File path: assets/live_page/niuniu/playing_card_4_2.png
  AssetGenImage get playingCard42 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_2.png');

  /// File path: assets/live_page/niuniu/playing_card_4_3.png
  AssetGenImage get playingCard43 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_3.png');

  /// File path: assets/live_page/niuniu/playing_card_4_4.png
  AssetGenImage get playingCard44 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_4.png');

  /// File path: assets/live_page/niuniu/playing_card_4_5.png
  AssetGenImage get playingCard45 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_5.png');

  /// File path: assets/live_page/niuniu/playing_card_4_6.png
  AssetGenImage get playingCard46 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_6.png');

  /// File path: assets/live_page/niuniu/playing_card_4_7.png
  AssetGenImage get playingCard47 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_7.png');

  /// File path: assets/live_page/niuniu/playing_card_4_8.png
  AssetGenImage get playingCard48 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_8.png');

  /// File path: assets/live_page/niuniu/playing_card_4_9.png
  AssetGenImage get playingCard49 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_4_9.png');

  /// File path: assets/live_page/niuniu/playing_card_53.png
  AssetGenImage get playingCard53 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_53.png');

  /// File path: assets/live_page/niuniu/playing_card_54.png
  AssetGenImage get playingCard54 =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_54.png');

  /// File path: assets/live_page/niuniu/playing_card_default.png
  AssetGenImage get playingCardDefault =>
      const AssetGenImage('assets/live_page/niuniu/playing_card_default.png');

  /// File path: assets/live_page/niuniu/refresh.svg
  String get refresh => 'assets/live_page/niuniu/refresh.svg';

  /// File path: assets/live_page/niuniu/time_0.png
  AssetGenImage get time0 =>
      const AssetGenImage('assets/live_page/niuniu/time_0.png');

  /// File path: assets/live_page/niuniu/time_1.png
  AssetGenImage get time1 =>
      const AssetGenImage('assets/live_page/niuniu/time_1.png');

  /// File path: assets/live_page/niuniu/time_2.png
  AssetGenImage get time2 =>
      const AssetGenImage('assets/live_page/niuniu/time_2.png');

  /// File path: assets/live_page/niuniu/time_3.png
  AssetGenImage get time3 =>
      const AssetGenImage('assets/live_page/niuniu/time_3.png');

  /// File path: assets/live_page/niuniu/time_4.png
  AssetGenImage get time4 =>
      const AssetGenImage('assets/live_page/niuniu/time_4.png');

  /// File path: assets/live_page/niuniu/time_5.png
  AssetGenImage get time5 =>
      const AssetGenImage('assets/live_page/niuniu/time_5.png');

  /// File path: assets/live_page/niuniu/time_6.png
  AssetGenImage get time6 =>
      const AssetGenImage('assets/live_page/niuniu/time_6.png');

  /// File path: assets/live_page/niuniu/time_7.png
  AssetGenImage get time7 =>
      const AssetGenImage('assets/live_page/niuniu/time_7.png');

  /// File path: assets/live_page/niuniu/time_8.png
  AssetGenImage get time8 =>
      const AssetGenImage('assets/live_page/niuniu/time_8.png');

  /// File path: assets/live_page/niuniu/time_9.png
  AssetGenImage get time9 =>
      const AssetGenImage('assets/live_page/niuniu/time_9.png');

  /// File path: assets/live_page/niuniu/win_both.png
  AssetGenImage get winBoth =>
      const AssetGenImage('assets/live_page/niuniu/win_both.png');

  /// File path: assets/live_page/niuniu/win_left.png
  AssetGenImage get winLeft =>
      const AssetGenImage('assets/live_page/niuniu/win_left.png');

  /// File path: assets/live_page/niuniu/win_right.png
  AssetGenImage get winRight =>
      const AssetGenImage('assets/live_page/niuniu/win_right.png');
}

class $AssetsMinePageTransactionGen {
  const $AssetsMinePageTransactionGen();

  /// File path: assets/mine_page/transaction/transaction_cancel_icon.png
  AssetGenImage get transactionCancelIcon => const AssetGenImage(
      'assets/mine_page/transaction/transaction_cancel_icon.png');

  /// File path: assets/mine_page/transaction/transaction_failed_icon.png
  AssetGenImage get transactionFailedIcon => const AssetGenImage(
      'assets/mine_page/transaction/transaction_failed_icon.png');

  /// File path: assets/mine_page/transaction/transaction_info_dotline.png
  AssetGenImage get transactionInfoDotline => const AssetGenImage(
      'assets/mine_page/transaction/transaction_info_dotline.png');

  /// File path: assets/mine_page/transaction/transaction_other_bg.png
  AssetGenImage get transactionOtherBg => const AssetGenImage(
      'assets/mine_page/transaction/transaction_other_bg.png');

  /// File path: assets/mine_page/transaction/transaction_process_bg.png
  AssetGenImage get transactionProcessBg => const AssetGenImage(
      'assets/mine_page/transaction/transaction_process_bg.png');

  /// File path: assets/mine_page/transaction/transaction_process_icon.png
  AssetGenImage get transactionProcessIcon => const AssetGenImage(
      'assets/mine_page/transaction/transaction_process_icon.png');

  /// File path: assets/mine_page/transaction/transaction_request_bg.png
  AssetGenImage get transactionRequestBg => const AssetGenImage(
      'assets/mine_page/transaction/transaction_request_bg.png');

  /// File path: assets/mine_page/transaction/transaction_request_icon.png
  AssetGenImage get transactionRequestIcon => const AssetGenImage(
      'assets/mine_page/transaction/transaction_request_icon.png');

  /// File path: assets/mine_page/transaction/transaction_revocation_icon.png
  AssetGenImage get transactionRevocationIcon => const AssetGenImage(
      'assets/mine_page/transaction/transaction_revocation_icon.png');

  /// File path: assets/mine_page/transaction/transaction_success_bg.png
  AssetGenImage get transactionSuccessBg => const AssetGenImage(
      'assets/mine_page/transaction/transaction_success_bg.png');

  /// File path: assets/mine_page/transaction/transaction_success_icon.png
  AssetGenImage get transactionSuccessIcon => const AssetGenImage(
      'assets/mine_page/transaction/transaction_success_icon.png');

  /// File path: assets/mine_page/transaction/transaction_timeout_icon.png
  AssetGenImage get transactionTimeoutIcon => const AssetGenImage(
      'assets/mine_page/transaction/transaction_timeout_icon.png');
}

class $AssetsRootPageAnimationsGen {
  const $AssetsRootPageAnimationsGen();

  /// File path: assets/root_page/animations/game_lottie.json
  String get gameLottie => 'assets/root_page/animations/game_lottie.json';

  /// File path: assets/root_page/animations/home_lottie.json
  String get homeLottie => 'assets/root_page/animations/home_lottie.json';

  /// File path: assets/root_page/animations/mine_lottie.json
  String get mineLottie => 'assets/root_page/animations/mine_lottie.json';

  /// File path: assets/root_page/animations/video_lottie.json
  String get videoLottie => 'assets/root_page/animations/video_lottie.json';

  /// File path: assets/root_page/animations/wallet_lottie.json
  String get walletLottie => 'assets/root_page/animations/wallet_lottie.json';
}

class $AssetsRootPageImagesGen {
  const $AssetsRootPageImagesGen();

  /// File path: assets/root_page/images/nav_kf.png
  AssetGenImage get navKf =>
      const AssetGenImage('assets/root_page/images/nav_kf.png');

  /// File path: assets/root_page/images/tab_game_normal.png
  AssetGenImage get tabGameNormal =>
      const AssetGenImage('assets/root_page/images/tab_game_normal.png');

  /// File path: assets/root_page/images/tab_home_normal.png
  AssetGenImage get tabHomeNormal =>
      const AssetGenImage('assets/root_page/images/tab_home_normal.png');

  /// File path: assets/root_page/images/tab_my_normal.png
  AssetGenImage get tabMyNormal =>
      const AssetGenImage('assets/root_page/images/tab_my_normal.png');

  /// File path: assets/root_page/images/tab_pay_normal.png
  AssetGenImage get tabPayNormal =>
      const AssetGenImage('assets/root_page/images/tab_pay_normal.png');

  /// File path: assets/root_page/images/tab_video_normal.png
  AssetGenImage get tabVideoNormal =>
      const AssetGenImage('assets/root_page/images/tab_video_normal.png');
}

class Assets {
  Assets._();

  static const $AssetsBasePageGen basePage = $AssetsBasePageGen();
  static const $AssetsGamePageGen gamePage = $AssetsGamePageGen();
  static const $AssetsHomePageGen homePage = $AssetsHomePageGen();
  static const $AssetsLivePageGen livePage = $AssetsLivePageGen();
  static const $AssetsMinePageGen minePage = $AssetsMinePageGen();
  static const $AssetsRootPageGen rootPage = $AssetsRootPageGen();
  static const $AssetsVideoPageGen videoPage = $AssetsVideoPageGen();
  static const $AssetsWalletPageGen walletPage = $AssetsWalletPageGen();
}

class AssetGenImage extends AssetImage {
  const AssetGenImage(String assetName) : super(assetName);

  Image image({
    Key? key,
    ImageFrameBuilder? frameBuilder,
    ImageLoadingBuilder? loadingBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? width,
    double? height,
    Color? color,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    FilterQuality filterQuality = FilterQuality.low,
  }) {
    return Image(
      key: key,
      image: this,
      frameBuilder: frameBuilder,
      loadingBuilder: loadingBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      width: width,
      height: height,
      color: color,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      filterQuality: filterQuality,
    );
  }

  String get path => assetName;
}
