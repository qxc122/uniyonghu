/// 路由常量统一管理
abstract class AppRoutes {
  /// 启动页/广告页
  static const splash = '/';

  ///兑换金币/钻石
  static const exchangeCoins = '/ExchangeCoins';

  ///在线客服
  static const onlineService = '/onlineService';

  /// 主页
  static const mainPage = '/main_page';

  /// 首页
  static const home = '/home';

  /// 关注
  static const homeFollow = '/follow';

  /// 推荐
  static const homeRecommend = '/recommend';

  /// 附近
  static const homeOther = '/other';

  /// 星秀
  static const homeVideo = '/video';
  /// 首页搜索
  static const homeSearch = '/search';

  /// 游戏
  static const game = '/game';
  static const gameRecord = '/gameRecord';
  static const gameTransaction = '/gameTransaction';
  static const gameWeb = '/gameWeb';

  static const transactionDetail = '/transactionDetail';
  static const transactionInfo = '/transactionInfo';

  static const notFound = "/not";
  static const THEME = '/theme'; // theme page

  static const loginPage = '/loginPage'; // 登陆页
  static const recommendPage = '/recommendPage';

  /// 今日推荐
  static const searchTypePage = '/searchTypePage';

  /// 搜索类型
  static const searchResultPage = '/searchResultPage';

  /// 搜索结果
  static const videoDetailPage = '/videoDetailPage';

  /// 视频详情
  static const feedback = '/feedback';

  /// 意见反馈
  static const follow = '/follow';

  /// 关注
  static const memberCenter = '/memberCenter';

  /// 会员中心
  static const mine = '/mine';

  /// 我的
  static const myLike = '/myLike';

  /// 我的喜欢
  static const myRequest = '/myRequest';

  /// 我的求片
  static const offlineCache = '/offlineCache';

  /// 离线缓存
  static const personalizedChannel = '/personalizedChannel';

  /// 个性化频道
  static const problemDetails = '/problemDetails';

  /// 问题详情
  static const purchasedMovies = '/purchasedMovies';

  /// 购买视频
  static const record = '/record';

  /// 观看记录
  static const screen = '/screen';

  /// 筛选
  static const slicing = '/slicing';

  /// 求片
  static const areaCode = '/areaCode'; // 选择区号
  static const register = '/register'; // 注册
  static const forgetPassword = '/forgetPassword'; // 忘记密码
  static const changePassword = '/changePassword'; // 修改密码

  static const sysMsgDetail = '/sysMsgDetail'; // 系统消息详情
  static const sysMsg = '/sysMsg'; //系统消息
  static const phoneSafety = '/phoneSafety'; // 安全中心
  static const phoneBind = '/phoneBind'; // 绑定手机号
  static const sign = '/sign'; // 签名
  static const nickName = '/nickName'; // 昵称
  static const editInformation = '/editInformation'; // 修改个人信息
  static const mineHome = '/mineHome'; // 个人主页
  static const setUp = '/setUp'; // 设置
  static const myGrade = '/myGrade'; // 我的等级
  static const mineGameRecord = '/mineGameRecord'; // 我的游戏记录
  static const mineFocus = '/mineFocus'; // 我的关注
  static const mineFans = '/mineFans'; // 我的粉丝
  static const fanGroup = '/fanGroup'; // 粉丝交流群
  static const customerService = '/customerService'; // 客服中心
  static const activityCenter = '/activityCenter'; // 活动中心

  static const live = '/live'; // 直播
  static const rankingTab = '/rankingTab'; // 排行榜
  static const liveShare = '/liveShare'; // 分享
  static const liveEnd = '/liveEnd'; // 直播已结束

  static const walletRoot = '/walletRoot'; // 充值

  /// 下面试测试专用
  static const homeTest = '/home_test'; // 首页
  static const discoverPage = '/discover';
  static const refreshListPage = '/refreshListPage';

  //  测试视频播放
  static const videoTestPage = "/videoTestPage";
}
