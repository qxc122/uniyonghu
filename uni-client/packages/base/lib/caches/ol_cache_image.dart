
import 'package:flutter/cupertino.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:base/commons/utils/aes_utils.dart';
import 'package:base/caches/ol_cache_manager.dart';


// 自定义缓存管理器 用于
class OLImageCacheManager extends CacheManager with ImageCacheManager {
  static const key = 'libCachedImageData';

  static final OLImageCacheManager _instance = OLImageCacheManager._();
  factory OLImageCacheManager() {
    return _instance;
  }
  OLImageCacheManager._() : super(Config(key));

  @override
  Future<FileInfo?> getFileFromCache(String key, {bool ignoreMemCache = false}) async {
    FileInfo? res = await super.getFileFromCache(key, ignoreMemCache: ignoreMemCache);
    return res;
  }

}
