import 'package:flutter/material.dart';
import 'package:get/get_connect/connect.dart';
import 'package:get/get_instance/src/bindings_interface.dart';

import '../commons/utils/log_utils.dart';

/// @date 2023/02/15  13:15
/// @author a-bert
/// @des GetConnect基类

abstract class GetBaseConnect extends GetConnect {}
