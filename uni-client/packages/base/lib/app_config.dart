import 'dart:io';

// 支持的类型
enum EnvType {
  prod,
  dev,
  uat,
  pre,
}

enum ThemeType {
  dark,
  day,
}

// 存放应用需要配置的地方 公共配置
class AppConfig {
  static void config() {
    String envValue = const String.fromEnvironment("PACKAGE_ENV");
    if (envValue == 'dev') {
      AppConfig.env = EnvType.dev;
    } else if (envValue == 'prod') {
      AppConfig.env = EnvType.prod;
    } else if (envValue == 'uat') {
      AppConfig.env = EnvType.uat;
    } else if (envValue == 'pre') {
      AppConfig.env = EnvType.pre;
    }
  }

  static EnvType env = EnvType.dev;

  ///当前版本号和版本名称，由于ios打tf包只能用1.0版本，所以升级判断要用自己的配置文件
  static int andVersionCode = 100;
  static String andVersionName = '100';
  static int iosVersionCode = 100;
  static String iosVersionName = '100';

  //aes加密得key
  static String aesKey = '==';

  // keys
  static String parameterKey = "";
  static String deviceType = Platform.isIOS ? 'ios' : 'android';

  // 响应 1 不加密 0 加密
  static String isTest = AppConfig.env == EnvType.prod ? '0' : '1';

  // 声网appId
  static String appId = "";

  // 通过配置和环境变量来获取环境
  // host
  static String get apiHost {
    switch (env) {
      case EnvType.prod:
        appId = "22f4eb5c247a462d9df410743125f0db";
        return "https://api.shopkarterj.com";
      case EnvType.pre:
        appId = "246f2c236bf7457bac630b3fa2aeb8a4";
        return "https://api.pre.radiotimucin.com";
      case EnvType.dev:
        appId = "c30b10fa91e949b0b47800dd87def391";
        return "https://api.91momo50.vip";
      case EnvType.uat:
      default:
        appId = "1dd9c1416e754ea9b7651d47eac10590";
        return "http://api.uat.radiotimucin.com";
    }
  }
}
