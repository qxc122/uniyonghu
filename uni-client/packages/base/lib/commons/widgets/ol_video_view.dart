import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fijkplayer/fijkplayer.dart';

/// 视频播放视图 提供简单的播放控制 如果需要特殊需求 请按照这个方式进行处理
class OLVideoView extends StatefulWidget {

  OLVideoView({Key? key}) : super(key: key);
  String? videoUrl;
  _OLVideoViewState? state;

  @override
  _OLVideoViewState createState() => _OLVideoViewState();


  /// 设置播放地址
  void setVideoUrl(String url) {
    state?.setVideoUrl(url);
  }

  /// 开始播放
  Future<void> play() async {
    await state?.play();
  }

  /// 暂停播放
  Future<void> pause() async {
    await state?.pause();
  }

  /// 停止播放
  Future<void> stop() async {
    await state?.stop();
  }

  /// 重置播放
  Future<void> reset() async {
    await state?.reset();
  }

  /// 全屏设置
  void fullScreen(bool isFull) {
    state?.fullScreen(isFull);
  }
}

class _OLVideoViewState extends State<OLVideoView> {
  FijkPlayer? player;
  Timer? _timer;

  @override
  void initState() {
    /// 设置日志模式
    FijkLog.setLevel(FijkLogLevel.Silent);
    super.initState();
    widget.state = this;
    super.initState();
    player = FijkPlayer();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      child: FijkView(
        player: player!,
        panelBuilder: fijkPanel2Builder(snapShot: false),
        fit: FijkFit.fitWidth,
        color: Colors.black,
      ),
    );
  }

  void setVideoUrl(String url) {
    widget.videoUrl = url;
    play();
  }

  Future<void> play() async {
    if (widget.videoUrl != null) {
      await player?.setDataSource(widget.videoUrl ?? "", autoPlay: true);
      await player?.prepareAsync();
      await player?.start();
    }
  }

  Future<void> pause() async {
    if (widget.videoUrl != null) {
      await player?.pause();
    }
  }

  Future<void> stop() async {
    if (widget.videoUrl != null) {
      await player?.stop();
    }
  }

  Future<void> reset() async {
    if (widget.videoUrl != null) {
      await player?.reset();
    }
  }

  void fullScreen(bool isFull) {
    if (isFull) {
      player?.enterFullScreen();
    } else {
      player?.exitFullScreen();
    }
  }

  @override
  void dispose() {
    super.dispose();
    player?.release();
  }
}
