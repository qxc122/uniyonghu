#import "DemoPagePlugin.h"
#if __has_include(<demo_page/demo_page-Swift.h>)
#import <demo_page/demo_page-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "demo_page-Swift.h"
#endif

@implementation DemoPagePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftDemoPagePlugin registerWithRegistrar:registrar];
}
@end
