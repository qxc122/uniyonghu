import 'dart:developer';

import 'package:base/app_routes.dart';
import 'package:base/caches/ol_cache_image.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ol_image_cache/cached_network_image.dart';
import 'package:services/api/common_apis.dart';
import 'package:services/common//cache/page_cache_manager.dart';

import 'get_discover_page_controller.dart';

class DiscoverPage extends StatefulWidget {
  int tindex = 0;
  String name = "";
  VoidCallback? themeCallback;

  DiscoverPage(
      {Key? key, required this.tindex, required this.name, this.themeCallback})
      : super(key: key);

  @override
  State<DiscoverPage> createState() => _TestDiscoverPageState();
}

class _TestDiscoverPageState extends State<DiscoverPage>
    with AutomaticKeepAliveClientMixin {
  @override
  final dis_ctrl = Get.put(GetDisCoverPageController());

  Widget build(BuildContext context) {
    int sIndex = widget.tindex;
    String sName = widget.name;
    log("_TestHomePageState sindex $sIndex");
    // TODO: implement build

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        //导航栏
        title: Text("$sName$sIndex"),
        actions: <Widget>[
          //导航栏右侧菜单
          IconButton(icon: const Icon(Icons.share), onPressed: () {}),
        ],
      ),
      body: GetX<GetDisCoverPageController>(
          builder: (_) => Container(
              color: Colors.amber,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      color: Colors.white,
                      height: 80,
                      alignment: Alignment.topLeft,
                      child: Center(
                          child: MaterialButton(
                        textColor: Colors.blueGrey,
                        onPressed: () async {
                          if (widget.themeCallback != null) {
                            widget.themeCallback!();
                          }
                        },
                        child: Text('测试主题 z${dis_ctrl.count.value.toString()}'),
                      )),
                    ),
                    Container(
                      color: Colors.white,
                      height: 80,
                      alignment: Alignment.topLeft,
                      child: Center(
                          child: MaterialButton(
                        textColor: Colors.blueGrey,
                        onPressed: () async {
                          final res = await CommonApis.of().getApiVerInfo();
                          final data = res["data"];

                          PageCacheManager.shared.storeCacheVersion(data);
                          log("获取到的版本信息是多少:$res");

                          // PageCacheManager.shared.storeCacheVersion('{"serialization":"fun"}');
                          log("测试版本");
                          // final res = await Common.of().getPublicConfig();
                          // log("MaterialButton$res");
                        },
                        child: Text('测试版本 ${dis_ctrl.count.value.toString()}'),
                      )),
                    ),
                    Container(
                      color: Colors.white,
                      height: 80,
                      alignment: Alignment.topLeft,
                      child: Center(
                          child: MaterialButton(
                        textColor: Colors.blueGrey,
                        onPressed: () async {
                          Get.toNamed(AppRoutes.refreshListPage);
                        },
                        child: Text('测试列表 ${dis_ctrl.count.value.toString()}'),
                      )),
                    ),
                    Container(
                      color: Colors.white,
                      height: 80,
                      alignment: Alignment.topLeft,
                      child: Center(
                          child: MaterialButton(
                        textColor: Colors.blueGrey,
                        onPressed: () async {
                          Get.toNamed(AppRoutes.videoTestPage);
                          // OLEasyLoading.showToast("success");
                        },
                        child: const Text('测试视频播放}'),
                      )),
                    ),
                    SizedBox(
                        height: 100,
                        width: 300,
                        child: OLImage(
                          cacheManager: OLImageCacheManager(),
                          imageUrl:
                            "https://res.cloudinary.com/cloudinary-marketing/images/c_fill,w_700/f_auto,q_auto/v1649720751/Web_Assets/blog/Mario_1/Mario_1-gif?_i=AA",
                              // 'https://gd-hbimg.huaban.com/7a7483874c4ad0b0ccad7970bbdd572a442bb4cc22a8-0jsdQL_fw240webp',
                        )),
                    Container(
                      color: Colors.white,
                      height: 80,
                      alignment: Alignment.topLeft,
                      child: Center(
                          child: MaterialButton(
                        textColor: Colors.blueGrey,
                        onPressed: () async {
                          // final res = await Common.of().getApiVerInfo();
                          // log("获取到的版本信息是多少:$res");

                          // PageCacheManager.shared.storeCacheVersion({});
                          log("测试缓存");
                          // final res = await Common.of().getPublicConfig();
                          // log("MaterialButton$res");
                        },
                        child: Text('写入缓存 ${dis_ctrl.count.value.toString()}'),
                      )),
                    ),
                    Container(
                      color: Colors.white,
                      height: 80,
                      alignment: Alignment.topLeft,
                      child: Center(
                          child: MaterialButton(
                        textColor: Colors.blueGrey,
                        onPressed: () async {
                          final data =
                              await PageCacheManager.shared.cacheVersions;
                          log("读取缓存$data");
                          // final res = await Common.of().getPublicConfig();
                          // log("MaterialButton$res");
                        },
                        child: Text('读取缓存 ${dis_ctrl.count.value.toString()}'),
                      )),
                    ),
                    Container(
                      color: Colors.red,
                      height: 20,
                    ),
                    Container(
                      color: Colors.red,
                      height: 20,
                    ),
                    Container(
                      color: Colors.white,
                      height: 80,
                      alignment: Alignment.topLeft,
                      child: Center(
                          child: MaterialButton(
                        textColor: Colors.blueGrey,
                        onPressed: () async {
                          dis_ctrl.increase();
                          final res = await CommonApis.of().getPublicConfig();
                          log("MaterialButton$res");
                        },
                        child: Text('测试网络 ${dis_ctrl.count.value.toString()}'),
                      )),
                    ),
                    Container(
                      color: Colors.white,
                      height: 80,
                      alignment: Alignment.topLeft,
                      child: Center(
                        child: MaterialButton(
                          textColor: Colors.blueGrey,
                          onPressed: () {
                            Get.toNamed(AppRoutes.live);
                          },
                          child: const Text('测试直播'),
                        ),
                      ),
                    ),
                  ],
                ),
              ))),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
