import 'dart:developer';
import 'package:get/get.dart';

class GetDisCoverPageController extends GetxController {
  //TODO: Implement RootController

  // 选择的那个tab_bar
  final count = 0.obs;

  @override
  void onInit() {
    super.onInit();
    log("onInit init");
  }

  @override
  void onReady() {
    super.onReady();
    log("onReady");
  }

  @override
  void onClose() {}

  void increase() {
    count.value = count.value + 1;
  }

}