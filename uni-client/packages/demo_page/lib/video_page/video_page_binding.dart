import 'package:get/get.dart';

import 'video_page_controller.dart';

class Video_pageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => Video_pagecontroller());
  }
}
