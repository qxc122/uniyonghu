import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:base/commons/widgets/ol_video_view.dart';

import 'video_page_controller.dart';

/// 视频测试页面
class Video_pagePage extends StatelessWidget {
  const Video_pagePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.find<Video_pagecontroller>();
    const videoKey = Key("videoPlayer");
    final videoView = OLVideoView(
      key: videoKey,
    );
    return Container(
        padding: const EdgeInsets.all(0),
        child: Stack(
          children: [
            SizedBox(
              child: videoView,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 100, 0, 10),
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: ElevatedButton(
                        onPressed: () async {
                          final box = videoView;
                          box.state?.setVideoUrl(
                              "https://new.qqaku.com/20211017/3erFtzDL/index.m3u8");
                          await box.state?.reset();
                          await box.state?.play();
                        },
                        child: const Text("播放")),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: ElevatedButton(
                        onPressed: () async {
                          final box = videoView;
                          await box.pause();
                        },
                        child: const Text("暂停")),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: ElevatedButton(
                        onPressed: () async {
                          final box = videoView;
                          await box.stop();
                        },
                        child: const Text("停止")),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: ElevatedButton(
                        onPressed: () async {
                          final box = videoView;
                          box.fullScreen(true);
                        },
                        child: const Text("全屏")),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: ElevatedButton(
                        onPressed: () async {
                          final box = videoView;
                          box.fullScreen(false);
                        },
                        child: const Text("退出全屏")),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(0),
                    child: ElevatedButton(
                        onPressed: () async {
                          final box = videoView;
                          await box.stop();
                          Get.back();
                        },
                        child: const Text("退出")),
                  )
                ],
              ),
            )
          ],
        ));
  }
}
