import 'dart:developer';

import 'package:base/app_config.dart';
import 'package:base/commons/widgets/ol_rectangle_indicator.dart';
import 'package:flutter/material.dart';

class TestHomePage extends StatefulWidget {
  int tindex = 0;
  String name = "";

  TestHomePage({Key? key, required this.tindex, required this.name})
      : super(key: key);

  @override
  State<TestHomePage> createState() => _TestHomePageState();
}

class _TestHomePageState extends State<TestHomePage>
    with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    int sIndex = widget.tindex;
    String sName = widget.name;
    late final _controller = TabController(length: 4, vsync: this);
    log("_TestHomePageState sindex $sIndex");
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        //导航栏
        title: Text("$sName$sIndex"),
        actions: <Widget>[
          //导航栏右侧菜单
          IconButton(icon: const Icon(Icons.share), onPressed: () {}),
        ],
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          children: [
            Container(
                height: 50.0,
                color: Colors.blue,
                child: Container(
                  // alignment: Alignment.topLeft,
                  width: double.infinity,
                  height: 50,
                  padding: EdgeInsets.all(0),
                  // color: Colors.red,
                  // height: 100,
                  child: TabBar(
                    // padding: EdgeInsets.all(0),
                    isScrollable: true,
                    controller: _controller,
                    indicator: const OLRectangleIndicator(),
                    labelColor: Colors.black45,
                    unselectedLabelColor: Colors.black45,
                    tabs: const [
                      Text("1"),
                      Text("2"),
                      Text("3"),
                      Text("4"),
                    ],
                  ),
                )),
            Expanded(
              child: Container(
                width: double.infinity,
                height: double.infinity,
                child: TabBarView(
                  controller: _controller,
                  children: [
                    Container(
                      color: Colors.blueGrey,
                    ),
                    Container(
                      color: Colors.black,
                    ),
                    Container(
                      color: Colors.red,
                    ),
                    Container(
                      color: Colors.yellow,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _TestHomePageState() {
    log("current env ${AppConfig.env.name}");
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
