import 'package:get/get.dart';

import 'refresh_list_page_controller.dart';

class RefreshListPageBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RefreshListPageController());
  }
}
