import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'refresh_list_page_controller.dart';
import 'package:base/commons/widgets/ol_list_view.dart';

class RefreshListPagePage extends StatelessWidget {
  RefreshListPagePage({Key? key}) : super(key: key);
  final controller = Get.put(RefreshListPageController());
  int index = 0;
  Future<bool> refreshCallback(_) async {
    await Future.delayed(const Duration(milliseconds: 2000));
    index = index + 1;
    controller.dataList.add("test$index");
    return false;
  }

  Future<bool> loadMoreCallback(_) async {
    await Future.delayed(const Duration(milliseconds: 2000));
    index = index + 1;
    controller.dataList.add("test$index");
    return true;
  }

  Widget buildItemWidget(Widget parentView, int index) {
    final value = controller.dataList[index];
    return Text("show $value");
  }

  @override
  Widget build(BuildContext context) {
    return  Container(
      padding: const EdgeInsets.all(100),
      child: OLListView(
        dataList: controller.dataList,
        refreshCallback: refreshCallback,
        loadMoreCallback: loadMoreCallback,
        buildWidgetCallback: buildItemWidget,
      ),
    );
  }
}
