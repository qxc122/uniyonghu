import 'dart:convert';
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert' as convert;
import 'package:base/app_config.dart';

// md5 加密
String generateMD5(String data) {
  var content = new Utf8Encoder().convert(data);
  var digest = md5.convert(content);
  // 这里其实就是 digest.toString()
  return hex.encode(digest.bytes);
}

class ParameterUtil {
  static transformParameter(json) {
    json['sign'] = '';
    List<String> keys = json.keys.toList();
    // key排序
    keys.sort((a, b) {
      List<int> al = a.codeUnits;
      List<int> bl = b.codeUnits;
      for (int i = 0; i < al.length; i++) {
        if (bl.length <= i) return 1;
        if (al[i] > bl[i]) {
          return 1;
        } else if (al[i] < bl[i]) return -1;
      }
      return 0;
    });
    var treeMap = Map();
    print(keys);
    keys.forEach((element) {
      treeMap[element] =
          json[element].toString().isNotEmpty ? json[element] : "";
    });
    print(treeMap);
    String jsonString = convert.jsonEncode(treeMap);
    print(jsonString);
    print(5555);
    var sign = generateMD5(jsonString + AppConfig.parameterKey);
    json["sign"] = sign;
    print(json);
    return json;
  }

  static regPhoneExp(String phone) {
    RegExp exp = RegExp(
        r'^((13[0-9])|(14[0-9])|(15[0-9])|(16[0-9])|(17[0-9])|(18[0-9])|(19[0-9]))\d{8}$');
    bool matched = exp.hasMatch(phone);
    print(phone);
    print(matched);
    return matched;
  }

  // 去掉小数末尾的0
  static cutZero(old) {
    // 拷贝一份 返回去掉零的新串
    return double.parse(old.toString()).toString();
  }
}
