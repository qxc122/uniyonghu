import 'package:services/api/apis.dart';
import 'package:services/http.dart';

/// @date 2023/02/12
/// @author bert
/// @des 直播相关api

class LiveApis {
  static LiveApis of() => LiveApis();

  /// anchor info 接口
  getAnchorInfo({
    required String studioNum,
  }) async {
    return await HttpUtil().post(
      Apis.getAnchorInfo,
      data: {
        'studioNum': studioNum,
      },
    );
  }

  /// anchor card 接口
  getAnchorCard({
    required String studioNum,
    required int userId,
  }) async {
    return await HttpUtil().post(
      Apis.getAnchorCard,
      data: {
        'studioNum': studioNum,
        'userId': userId,
      },
    );
  }

  // 关注
  // https://api.uat.radiotimucin.com/anchor/user/app/v1/focusUser
  // 取消关注
  // https://api.uat.radiotimucin.com/anchor/user/app/v1/cancelUser
  // 贡献榜
  // https://api.uat.radiotimucin.com/api/ranking/app/v1/room
  // type	查询类型 1：日 2:周 3：月 4：总 [必填]
  // userId	主播的 userId

  // 在线人数（前50）
  // https://api.uat.radiotimucin.com/api/room/app/v1/liveStudioTop50
  // 直播间详情
  // /api/live/app/v1/getRoomDetail

  /// 直播间推荐直播列表 接口
  getLiveRecommendList({
    required int pageNum,
    int pageSize = 10,
    required String studioNum,
  }) async {
    return await HttpUtil().post(
      Apis.getLiveRecommendList,
      data: {
        "pageNum": pageNum,
        "pageSize": pageSize,
        "studioNum": studioNum,
      },
    );
  }

  /// 直播间推荐直播列表 接口
  ///	1 日榜 2周榜 3月榜
  getContributionList({
    int dateType = 10,
  }) async {
    return await HttpUtil().post(
      Apis.getContributionList,
      data: {
        "dateType": dateType,
      },
    );
  }

  /// 平台主播排行榜
  getAnchorList({
    bool isPrevious = false,
    required int type,
  }) async {
    return await HttpUtil().post(
      Apis.getAnchorList,
      data: {
        "isPrevious": isPrevious,
        "type": type,
      },
    );
  }

  /// 平台土豪排行榜
  getToffList({
    bool isPrevious = false,
    required int type,
  }) async {
    return await HttpUtil().post(
      Apis.getToffList,
      data: {
        "isPrevious": isPrevious,
        "type": type,
      },
    );
  }

  /// 直播间前五十用户
  liveStudioTop50({
    required String studioNum,
  }) async {
    return await HttpUtil().post(
      Apis.liveStudioTop50,
      data: {
        "studioNum": studioNum,
      },
    );
  }
}
