/// @date 2023/02/01  10:38
/// @author bert
/// @des 所有的接口常量统一管理类

class Apis {
  /// 全局配置
  static const String publicConfig = "/api/sys/v1/getPublicConfig";

  /// 启动广告页面
  static const String getSlide = "/api/index/app/v1/getSlide";

  /// 获取接口版本信息 用于缓存
  static const String verApi = "/api/sys/v1/ver";

  /// 轮播图广告位查询，根据code
  static const String getBannerList = "/api/index/app/v1/getAdvByCode";

  /// 公告接口
  static const String getAdvNotice = "/api/advNotice/app/v1/getByType";

  // userApi 用户登陆相关的api
  static const String userVisitorLogin = "/api/login/app/v1/visitorLogin";
  static const String queryAllCountry = "/api/sys/queryAllCountry";
  static const String visitorCheck = "/api/login/app/v1/visitor/check";
  static const String visitorLogin = "/api/login/app/v1/visitorLogin";
  static const String getUserInfo = "/api/userCenter/app/v1/getUserInfo";
  static const String sendSms = "/api/sms/v1/sendSms";
  static const String randCode = "/api/sys/v1/randCode";
  static const String getOnlineService =
      "/api/onlineservice/app/v1/getOnlineService";

  static const String findPassword = "/api/login/app/v1/findPassword";
  static const String userRegisterAndLogin =
      "/api/login/app/v1/registerAndLogin";
  static const String userBindPhone = "/api/login/app/v1/visitor/bindPhone";
  static const String userCheck = "/api/login/app/v1/visitor/check";
  static const String userLogin = "/api/login/app/v1/logout";

  /// 首页tab接口
  static const String getColumn = "/api/index/app/v1/getColumn";

  /// 根据栏目code查询直播房间列表(除了推荐和关注栏目外)
  static const String getLiveByColumnCode =
      "/api/index/app/v1/getLiveByColumnCode";

  /// 首页推荐列表顶部游戏
  static const String getTopGameList =
      "/api/index/app/v1/queryIndexTopGameList";

  /// 首页推荐列+直播列表
  static const String getLiveList = "/api/index/v1/getLiveList";

  /// 关注的直播房间列表
  static const String getLiveFocusList = "/api/index/app/v1/getLiveFocusList";

  /// 关注页推荐的直播房间列表,不包含已经关注的房间
  static const String getLiveFocusRecommendList =
      "/api/index/app/v1/getLiveFocusRecommendList";

  /// 首页热门视频+星秀
  static const String getSearchVideo = "/api/index/app/v1/searchVideo";

  /// 热门视频 获取10个视频(随机)
  static const String getHotVideoList = "/api/index/app/v1/getVideoList";

  /// 定位当前国家省
  static const String getCountryProvince =
      "/api/liveStudioRegion/app/v1/countryProvince";

  /// 获取国家省信息
  static const String getCountryProvinceList =
      "/api/liveStudioRegion/app/v1/listSysCountryProvince";

  /// 获取最近浏览
  static const String getBrowseHistory = "/api/index/app/v1/browseHistory";

  /// 获取直播搜索
  static const String getLiveSearch = "/api/index/app/v1/search";

  /// 游戏data接口
  static const String getGameData = "/api/game/app/v1/list";

  /// 直播间主播头部信息
  static const String getAnchorInfo = "/api/anchor/app/v1/getAnchorInfo";

  /// 直播间主播名片进度
  static const String getAnchorCard = "/api/live/app/v1/getAnchorCard";

  /// 直播间推荐直播列表
  static const String getLiveRecommendList =
      "/api/live/app/v1/getLiveRecommendList";

  /// 直播间贡献榜
  static const String getContributionList =
      "/anchor/live/app/v1/getContributionList";

  /// 平台主播排行榜
  static const String getAnchorList = "/api/ranking/app/v1/anchor";

  /// 平台土豪排行榜
  static const String getToffList = "/api/ranking/app/v1/user";

  /// 直播间前五十用户
  static const String liveStudioTop50 = "/api/room/app/v1/liveStudioTop50";

  ///userWallet
  static const String getUserWallet = "/api/user/app/v1/getUserWallet";

  ///兑换金币列表
  static const String getGoldOptions = "/pay/pay/app/v1/getGoldOptions";

  ///余额兑换金币
  static const String exchangeGold = "/api/silverBean/app/V1/exchangeGold";

  ///兑换钻石列表
  static const String getSilverBeanOptions =
      "/pay/pay/app/v1/getSilverBeanOptions";

  ///余额兑换钻石
  static const String exchangeSilver = "/api/silverBean/app/V1/exchangeSilver";

  ///支付方式
  static const String getPayData = "/pay/pay/app/v1/getPayData";

  ///充值
  static const String recharge = "/pay/pay/app/v1/recharge";

  ///游戏记录
  static const String getGameRecordList =
      "/api/userCenter/app/v1/getGameRecordList";

  ///充提记录
  static const String getBill = "/pay/transaction/app/v1/getBill";

  ///取消充提操作
  static const String cancelWithdraw = "/pay/pay/app/v1/cancelWithdraw";

  ///游戏登陆
  static const String gameLogin = "/api/game/app/v1/login";

  static const String getRecentInfo = "/api/ticket_kj/app/v1/recent_info";
  static const String getInfo = "/api/ticket_bet/app/v1/info";
  static const String getExplain = "/api/ticket_help/app/v1/info";
  static const String getLotteryList = "/api/ticket_kj/app/v1/list";
  static const String getBetList = "/api/ticket_tz/app/v1/list";
  static const String niuNiuBet = "/api/ticket_bet/app/v1/bet";
  static const String getGameTypeList = "/api/ticket/app/v1/list";
  static const String statistics = "/api/ticket_tz/app/v1/statistics";
  static const String getGameCodeList = "/api/ticket/app/v1/getGameCodeList";
  static const String getLiveGameList = "/api/ticket/app/v1/getLiveGameList";

  /// 交易类型
  static const String transactionTypeList =
      "/pay/transaction/app/v1/transactionTypeList";

  /// 关注主播
  static const String focusUser = "/anchor/user/app/v1/focusUser";

  /// 取关主播
  static const String unfocusUser = "/anchor/user/app/v1/cancelUser";
}
