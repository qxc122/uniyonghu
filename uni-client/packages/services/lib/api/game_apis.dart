import 'package:services/api/apis.dart';
import 'package:services/http.dart';

import '../models/res/game/niu_niu/niu_bet_post_model.dart';

/// @date 2023/02/12
/// @author bert
/// @des 游戏、彩票相关api

class GameApis {
  static GameApis of() => GameApis();

  /// game banner接口
  getBannerList() async {
    return await HttpUtil().post(
      Apis.getBannerList,
      data: {
        'flashviewCode': 'game',
      },
    );
  }

  /// game data接口
  getGameData() async {
    return await HttpUtil().post(Apis.getGameData);
  }

  /// game notice接口
  getAdvNotice() async {
    return await HttpUtil().post(
      Apis.getAdvNotice,
      data: {
        'type': 6,
      },
    );
  }

  getUserWallet() async {
    return await HttpUtil().post(
      Apis.getUserWallet,
      data: {
        'walletType': 1,
      },
    );
  }

  getGameRecordList(bool isToday) async {
    return await HttpUtil().post(
      Apis.getGameRecordList,
      data: {
        'queryDate': isToday ? 1 : 2,
      },
    );
  }

  getBill({
    int lastID = 0,
    String transactionTypes = '',
    String billTypes = '',
    String orderStatus = '',
    String rangeMonth = '',
  }) async {
    return await HttpUtil().post(
      Apis.getBill,
      data: {
        "pageSize": 10,
        // "billTypes": billTypes,
        // 订单状态: 1处理中,2成功,3失败,4取消,5申请中,6超时,7撤单
        // "orderStatus": orderStatus,
        // 当前页最后一条的数据的 ID
        // "lastID": lastID,
        "transactionTypes": transactionTypes,
        // "rangeMonth": rangeMonth,
      },
    );
  }

  getTransactionTypeList() async {
    return await HttpUtil().post(
      Apis.transactionTypeList,
    );
  }

  cancelWithdraw(
    int? userId,
    String withdrawOrderNo,
  ) async {
    return await HttpUtil().post(
      Apis.getBill,
      data: {
        "cancelExplain": "",
        "userId": userId,
        "withdrawOrderNo": withdrawOrderNo,
      },
    );
  }

  gameLogin(String gameType, int categoryId) async {
    return await HttpUtil().post(
      Apis.gameLogin,
      data: {
        'gameType': gameType,
        'categoryId': categoryId,
        'channelCode': "official",
      },
    );
  }

  getRecentInfo(int ticketId) async {
    return await HttpUtil().post(
      '${Apis.getRecentInfo}?ticketId=$ticketId',
    );
  }

  getInfo(int liveFlag, int ticketId) async {
    return await HttpUtil().post(
      Apis.getInfo,
      data: {
        'liveFlag': liveFlag,
        'ticketId': ticketId,
      },
    );
  }

  getExplain(int ticketId) async {
    return await HttpUtil().post(
      '${Apis.getExplain}?ticketId=$ticketId',
    );
  }

  getLotteryList(
    int ticketId,
    int pageNum,
    int pageSize,
  ) async {
    return await HttpUtil().post(
      Apis.getLotteryList,
      data: {
        "ticketId": ticketId,
        "pageNum": pageNum,
        "pageSize": pageSize,
      },
    );
  }

  getBetList(
    int pageNum,
    int pageSize,
    num? ticketId,
    String? tzDate,
    String? zjStatus,
  ) async {
    return await HttpUtil().post(
      Apis.getBetList,
      data: {
        "ticketId": ticketId,
        "tzDate": tzDate,
        "zjStatus": zjStatus,
        "pageNum": pageNum,
        "pageSize": pageSize,
      },
    );
  }

  niuNiuBet(NiuPostBetModel model) async {
    return await HttpUtil().post(
      Apis.niuNiuBet,
      data: model.toJson(),
    );
  }

  getStatistics(
    num? ticketId,
    String? tzDate,
    String? zjStatus,
  ) async {
    return await HttpUtil().post(
      Apis.statistics,
      data: {
        "ticketId": ticketId,
        "tzDate": tzDate,
        "zjStatus": zjStatus,
      },
    );
  }

  getGameTypeList() async {
    return await HttpUtil().post(
      '${Apis.getGameTypeList}?liveFlag=0',
    );
  }
}
