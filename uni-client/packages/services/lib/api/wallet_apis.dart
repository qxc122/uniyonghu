import 'dart:ffi';

import 'package:services/api/apis.dart';
import 'package:services/http.dart';
import 'package:services/models/res/wallet/user_wallet_model.dart';

import '../models/res/wallet/diamond_model.dart';
import '../models/res/wallet/pay_way_model.dart';

/// @date 2023/02/14
/// @author bert
/// @des 钱包相关api

class WalletApis {
  static WalletApis of() => WalletApis();

  ///兑换金币
  exchangeGold(int balanceAmount) async {
    Map<dynamic, dynamic> data = {
      "balanceAmount": balanceAmount,
    };
    return await HttpUtil().post(Apis.exchangeGold, data: data);
  }

  ///金币列表
  getGoldOptions() async {
    var response = await HttpUtil().post(Apis.getGoldOptions);
    List<dynamic> data = response.data;
    return data.map((m) => DiamondModel.fromJson(m)).toList();
  }

  ///兑换钻石
  exchangeSilver(int balanceAmount) async {
    Map<dynamic, dynamic> data = {
      "balanceAmount": balanceAmount,
    };
    return await HttpUtil().post(Apis.exchangeSilver, data: data);
  }

  ///钻石列表
  getSilverBeanOptions() async {
    var response = await HttpUtil().post(Apis.getSilverBeanOptions);
    List<dynamic> data = response.data;
    return data.map((m) => DiamondModel.fromJson(m)).toList();
  }

  ///用户钱包
  getUserWallet({int walletType = 1}) async {
    Map<dynamic, dynamic> data = {
      "walletType": walletType,
    };
    var response = await HttpUtil().post(Apis.getUserWallet, data: data);
    return UserWalletModel.fromJson(response.data);
  }

  ///支付方式
  getPayData() async {
    var response = await HttpUtil().post(Apis.getPayData);
    List<dynamic> data = response.data;
    return data.map((m) => PayWayModel.fromJson(m)).toList();
  }

  ///充值
  recharge(
    String? backCode,
    String? payWayId,
    String? price,
    String? payName,
    int? accountType,
    int? activityType,
  ) async {
    Map<dynamic, dynamic> data = {
      "backCode": backCode,
      "payWayId": payWayId,
      "price": price,
      "payName": payName,
      "accountType": accountType,
      "activityType": activityType,
    };
    var response = await HttpUtil().post(Apis.recharge, data: data);
    return response;
  }
}
