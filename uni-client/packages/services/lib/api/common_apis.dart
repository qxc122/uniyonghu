import 'package:services/models/res/login/image_code_model.dart';

import '../http.dart';
import '../models/res/login/online_service_model.dart';
import 'apis.dart';

/// @date 2023/02/01  10:28
/// @author bert
/// @des 通用api

class CommonApis {
  static CommonApis of() => CommonApis();

  /// 全局配置信息
  getPublicConfig() async {
    var response = await HttpUtil().post(Apis.publicConfig);
    return response;
  }

  /// 获取广告
  getSlide() async {
    return await HttpUtil().post(Apis.getSlide);
  }

  /// 获取接口的版本信息
  getApiVerInfo() async {
    var response = await HttpUtil().post(Apis.verApi);
    return response;
  }

  ///发送短信验证码
  sendSms(
    String phone,
    int sendType,
    String areaCode, {
    String? imgCode,
    String? captchaKey,
  }) async {
    Map<dynamic, dynamic> data = {
      "phone": phone,
      "sendType": sendType,
      "areaCode": areaCode,
      "imgCode": imgCode,
      "captchaKey": captchaKey,
    };
    var response = await HttpUtil().post(Apis.sendSms, data: data);
    return response;
  }

  ///随机图片
  randCode() async {
    var response = await HttpUtil().post(Apis.randCode);
    return ImageCodeModel.fromJson(response.data);
  }

  ///在线客服
  getOnlineService() async {
    var response = await HttpUtil().post(Apis.getOnlineService);
    return OnlineServiceModel.fromJson(response.data);
  }

  /// 轮播图广告位查询，根据code
  /// 参数flashviewCode说明：热门:hot, 首页(推荐):index, 游戏:game, 推荐置顶轮播:recommend
  getBannerList(Map<String, String> params) async {
    return await HttpUtil().post(Apis.getBannerList, data: params);
  }

  /// 全局公告,一条数据
  /// 参数type的说明：  1：直播间公告 2：开场公告（一个语言只有一条） 3：循环公告 4：首页公告 5：推荐栏目跑马灯公告 6：游戏列表跑马灯公告 7:兑换钻石公告 8：充值中心
  getAdvNotice(Map<String, int> params) async {
    return await HttpUtil().post(Apis.getAdvNotice, data: params);
  }

  /// 关注主播
  focusUser(Map<String, dynamic> params) async {
    return await HttpUtil().post(
      Apis.focusUser,
      data: params,
    );
  }

  /// 取关主播
  unfocusUser(Map<String, dynamic> params) async {
    return await HttpUtil().post(
      Apis.unfocusUser,
      data: params,
    );
  }

  getGameCodeList() async {
    return await HttpUtil().post(
      Apis.getGameCodeList,
    );
  }

  getLiveGameList(String code) async {
    return await HttpUtil().post(Apis.getLiveGameList, data: {'code': code});
  }
}
