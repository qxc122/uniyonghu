import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:base/app_config.dart';
import 'package:base/app_routes.dart';
import 'package:base/caches/ol_cache_manager.dart';
import 'package:base/commons/utils/aes_utils.dart';
import 'package:base/commons/utils/device_utils.dart';
import 'package:base/commons/utils/log_utils.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:get/get.dart';
import 'package:services/common/cache/page_cache_manager.dart';

import 'common/data/local_repository_impl.dart';
import 'models/res/base_response.dart';
import 'models/res/login/user_info_model.dart';

class HttpUtil {
  final LocalRepositoryImpl localRepositoryImpl = LocalRepositoryImpl();

  // 单例模式
  static final HttpUtil _instance = HttpUtil._internal();

  factory HttpUtil() => _instance;

  HttpUtil._internal() {
    init();
  }

  Dio? dio;

  init() {
    // BaseOptions、Options、RequestOptions 都可以配置参数，优先级别依次递增，且可以根据优先级别覆盖参数
    BaseOptions options = BaseOptions(
      // 请求基地址,可以包含子路径
      baseUrl: AppConfig.apiHost,

      // baseUrl: storage.read(key: STORAGE_KEY_APIURL) ?? SERVICE_API_BASEURL,
      //连接服务器超时时间，单位是毫秒.
      connectTimeout: 20000,

      // 响应流上前后两次接受到数据的间隔，单位为毫秒。
      receiveTimeout: 5000,

      // Http请求头.
      headers: {},
      // "Authorization": "Bearer " + localRepositoryImpl.getToken().toString(),
      contentType: 'application/json; charset=utf-8',
      responseType: ResponseType.json,
    );

    dio = Dio(options);

    num timeNum = 0;

    // Cookie管理
    CookieJar cookieJar = CookieJar();
    dio?.interceptors.add(CookieManager(cookieJar));
    dio?.interceptors
        .add(InterceptorsWrapper(onRequest: (options, handler) async {
      String timep = DateTime.now().millisecondsSinceEpoch.toString();
      String randomNum = Random().nextInt(100).toString();
      String url = options.path.split("/").last;

      /// 如果用户已经登录 则需要处理
      String token = '';
      final userMapInfo =
          await OLCacheManager.shared.getData(kOLUserInfoModelModelKey);
      if (userMapInfo.isNotEmpty) {
        OLUserInfoModel userInfo = OLUserInfoModel.fromJson(userMapInfo);
        token = userInfo.accessToken ?? "";
      }
      options.headers["lang"] = "zh_CN";
      options.headers["unilive-appleType"] = "1";
      options.headers["unilive-deviceId"] = await Device.getDeviceId();
      options.headers["unilive-devices"] = "HONORRNA-AN00";
      options.headers["unilive-random"] = randomNum;
      options.headers["unilive-signature"] =
          await EncryptUtil.of().aesEncrypt("$timep||$randomNum||$token||$url");
      options.headers["unilive-source"] = AppConfig.deviceType;
      options.headers["unilive-timestamp"] = timep;
      options.headers["unilive-url"] = url;
      options.headers["isTest"] = AppConfig.isTest;

      if (token != '') {
        options.headers["Authorization"] = token;
      }
      if (options.uri.toString().contains("upload")) {
        options.headers['content-type'] = "multipart/form-data";
      }
      // Do something before request is sent
      return handler.next(options); //continue
      // 如果你想完成请求并返回一些自定义数据，你可以resolve一个Response对象 `handler.resolve(response)`。
      // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
      //
      // 如果你想终止请求并触发一个错误,你可以返回一个`DioError`对象,如`handler.reject(error)`，
      // 这样请求将被中止并触发异常，上层catchError会被调用。
    }, onResponse: (response, handler) {
      Log.d("响应：$response");
      var code = response.data["code"];
      if (code == 200) {
        return handler.next(response);
      } else if (code == 401) {
        Get.offAllNamed(AppRoutes.loginPage);
      } else {
        OLEasyLoading.showToast(response.data["msg"] ?? "请求失败，请稍后重试！");
      }
      // Do something with response data
//      return handler.next(response); // continue
      // 如果你想终止请求并触发一个错误,你可以 reject 一个`DioError`对象,如`handler.reject(error)`，
      // 这样请求将被中止并触发异常，上层catchError会被调用。
    }, onError: (DioError e, handler) {
      // Do something with response error
      Log.e("【请求异常：message：${e.message}\r\nresponse：${e.response??""}】");
      OLEasyLoading.showToast(e.message);
      // return handler.reject(e); //continue
      // 如果你想完成请求并返回一些自定义数据，可以resolve 一个`Response`,如`handler.resolve(response)`。
      // 这样请求将会被终止，上层then会被调用，then中返回的数据将是你的自定义response.
    }));
  }

  /// restful get 操作
  Future get(
    String path, {
    dynamic queryParameters,
    Options? options,
  }) async {
    Log.d("【请求域名：${AppConfig.apiHost}\r\n请求路径：$path\r\n请求参数：$queryParameters\r\n配置信息：$options】");
    var response = await dio?.get(
      path,
      queryParameters: queryParameters,
      options: options,
    );
    return BaseResponse.fromJson(response?.data);
  }

  // needCache 是否受版本接口的影响
  Future<BaseResponse> post(
    String path, {
    dynamic data,
    Options? options,
    bool? needCache,
  }) async {
    Log.d(
        "【请求域名：${AppConfig.apiHost}\r\n请求路径：$path\r\n请求参数：$data\r\n配置信息：$options】");
    // 处理缓存
    final isNeed = await PageCacheManager.shared.isNeedLoadCacheData(path);
    if (!isNeed) {
      if (needCache == null || needCache) {
        final cacheData = await OLCacheManager.shared.getData(path);
        return BaseResponse.fromJson(cacheData);
        // return cacheData;
      }
    }

    var response = await dio?.post(
      path,
      data: data,
      options: options,
    );

    // 返回的数据需要缓存
    final responseData = response?.data;
    PageCacheManager.shared.putCache(path, responseData);
    // return responseData;
    return BaseResponse.fromJson(response?.data);
  }

  Future delete(
    String path, {
    dynamic data,
    required Options options,
  }) async {
    print(path);
    var response = await dio?.delete(
      path,
      data: data,
      options: options,
    );
    return response?.data;
  }

  Future put(
    String path, {
    dynamic data,
    required Options options,
  }) async {
    print(path);
    print(options);
    print(data);
    var response = await dio?.put(
      path,
      data: data,
      options: options,
    );
    return response?.data;
  }

  ///清除授权
  clearAuthorization() async {
    await localRepositoryImpl.clearAllData();
  }

  ///获取授权token
  getAuthorization() async {
    return await localRepositoryImpl.getToken();
  }
}
