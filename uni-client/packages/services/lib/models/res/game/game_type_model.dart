/// id : 1
/// name : "百人牛牛"
/// image : ""
/// type : "NN"

class GameTypeModel {
  GameTypeModel({
      this.id, 
      this.name, 
      this.image, 
      this.type,});

  GameTypeModel.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
    type = json['type'];
  }
  num? id;
  String? name;
  String? image;
  String? type;
GameTypeModel copyWith({  num? id,
  String? name,
  String? image,
  String? type,
}) => GameTypeModel(  id: id ?? this.id,
  name: name ?? this.name,
  image: image ?? this.image,
  type: type ?? this.type,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['image'] = image;
    map['type'] = type;
    return map;
  }

}