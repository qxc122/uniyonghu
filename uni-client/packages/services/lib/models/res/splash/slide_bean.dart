/// advImg : "https://img.91momo50.vip/picture/2023/01/24/1785257961136122560.w.png?auth_key=1676208052971-a89e3153c86f44e9a209a5c1c0911b5b-0-8025a3db271daab86739d2e81dc7e81b"
/// slideType : null
/// showTime : 3

class SlideBean {
  SlideBean({
      String? advImg, 
      dynamic slideType, 
      num? showTime,}){
    _advImg = advImg;
    _slideType = slideType;
    _showTime = showTime;
}

  SlideBean.fromJson(dynamic json) {
    _advImg = json['advImg'];
    _slideType = json['slideType'];
    _showTime = json['showTime'];
  }
  String? _advImg;
  dynamic _slideType;
  num? _showTime;
SlideBean copyWith({  String? advImg,
  dynamic slideType,
  num? showTime,
}) => SlideBean(  advImg: advImg ?? _advImg,
  slideType: slideType ?? _slideType,
  showTime: showTime ?? _showTime,
);
  String? get advImg => _advImg;
  dynamic get slideType => _slideType;
  num? get showTime => _showTime;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['advImg'] = _advImg;
    map['slideType'] = _slideType;
    map['showTime'] = _showTime;
    return map;
  }

}