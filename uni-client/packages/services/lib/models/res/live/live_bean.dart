// To parse this JSON data, do
//
//     final liveBean = liveBeanFromJson(jsonString);

import 'dart:convert';

LiveBean liveBeanFromJson(String str) => LiveBean.fromJson(json.decode(str));

String liveBeanToJson(LiveBean data) => json.encode(data.toJson());

class LiveBean {
  LiveBean({
    this.studioId = 0,
    this.studioNum = '',
    this.cityName = '',
    this.userId,
    this.anchorId = -1,
    this.gameName = '',
    this.gameId,
    this.trySeeTime = 0,
    this.onlineNum = 0,
    this.sharpness = '',
    this.studioStatus = -1,
    this.countryCode = '',
    this.countryName = '',
    this.colour = -1,
    this.columnCode = '',
    this.studioBackground = '',
    this.studioThumbImage = '',
    this.studioTitle = '',
    this.studioHeat = 0,
    this.chargeType = -1,
    this.price = 0,
    this.isCharge = false,
    this.isLightYearRecommend = false,
    this.isRelateToy = false,
    this.studioType = 0,
    this.roomDetail,
    this.toyBluetoothStatus = -1,
    this.provinceCode = '',
    this.provinceName = '',
    this.weightsNum,
    this.startTime,
  });

  final int studioId;
  final String studioNum;
  final String cityName;
  final BigInt? userId;
  final int anchorId;
  final String gameName;
  final BigInt? gameId;
  final int trySeeTime;
  // 在线登录观看人数
  final int onlineNum;
  // 画质
  final String sharpness;
  // 直播间状态 0：未开播，1：开播，2：网络状态不好
  final int studioStatus;
  final String countryCode;
  final String countryName;
  // 默认0:绿播 1:黄播
  final int colour;
  // 首页栏目ID—关联— live_column.columnCode
  final String columnCode;
  // 直播间封面图（大图）
  final String studioBackground;
  // 直播间封面图（小图,暂无用，如有小图设置此值）
  final String studioThumbImage;
  // 直播间标题
  final String studioTitle;
  // 直播间火力值
  final int studioHeat;
  // 收费类型 6：按时收费 7:按场收费
  final int chargeType;
  final num price;
  // 是否开通收费直播
  final bool isCharge;
  // 是否光年推荐
  final bool isLightYearRecommend;
  // 是否关联玩具
  final bool isRelateToy;
  // 体育项目ID
  final int studioType;
  final dynamic roomDetail;
  // 跳蛋蓝牙状态 1：开启 0：关闭
  final int toyBluetoothStatus;
  final String provinceCode;
  final String provinceName;
  final BigInt? weightsNum;
  final DateTime? startTime;

  factory LiveBean.fromJson(Map<String, dynamic> json) => LiveBean(
        studioId: json["studioId"],
        studioNum: json["studioNum"],
        cityName: json["cityName"],
        userId: json["userId"],
        anchorId: json["anchorId"],
        gameName: json["gameName"],
        gameId: json["gameId"],
        trySeeTime: json["trySeeTime"],
        onlineNum: json["onlineNum"],
        sharpness: json["sharpness"],
        studioStatus: json["studioStatus"],
        countryCode: json["countryCode"],
        countryName: json["countryName"],
        colour: json["colour"],
        columnCode: json["columnCode"],
        studioBackground: json["studioBackground"],
        studioThumbImage: json["studioThumbImage"],
        studioTitle: json["studioTitle"],
        studioHeat: json["studioHeat"],
        chargeType: json["chargeType"],
        price: json["price"],
        isCharge: json["isCharge"],
        isLightYearRecommend: json["isLightYearRecommend"],
        isRelateToy: json["isRelateToy"],
        studioType: json["studioType"],
        roomDetail: json["roomDetail"],
        toyBluetoothStatus: json["toyBluetoothStatus"],
        provinceCode: json["provinceCode"],
        provinceName: json["provinceName"],
        weightsNum: json["weightsNum"],
        startTime: json["startTime"],
      );

  Map<String, dynamic> toJson() => {
        "studioId": studioId,
        "studioNum": studioNum,
        "cityName": cityName,
        "userId": userId,
        "anchorId": anchorId,
        "gameName": gameName,
        "gameId": gameId,
        "trySeeTime": trySeeTime,
        "onlineNum": onlineNum,
        "sharpness": sharpness,
        "studioStatus": studioStatus,
        "countryCode": countryCode,
        "countryName": countryName,
        "colour": colour,
        "columnCode": columnCode,
        "studioBackground": studioBackground,
        "studioThumbImage": studioThumbImage,
        "studioTitle": studioTitle,
        "studioHeat": studioHeat,
        "chargeType": chargeType,
        "price": price,
        "isCharge": isCharge,
        "isLightYearRecommend": isLightYearRecommend,
        "isRelateToy": isRelateToy,
        "studioType": studioType,
        "roomDetail": roomDetail,
        "toyBluetoothStatus": toyBluetoothStatus,
        "provinceCode": provinceCode,
        "provinceName": provinceName,
        "weightsNum": weightsNum,
        "startTime": startTime,
      };
}
