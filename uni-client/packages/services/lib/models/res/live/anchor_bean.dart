// To parse this JSON data, do
//
//     final anchorBean = anchorBeanFromJson(jsonString);

import 'dart:convert';

AnchorBean anchorBeanFromJson(String str) =>
    AnchorBean.fromJson(json.decode(str));

String anchorBeanToJson(AnchorBean data) => json.encode(data.toJson());

class AnchorBean {
  AnchorBean({
    this.accno = '',
    this.avatar = '',
    this.cardEnabled = false,
    this.fansCount = 0,
    this.isFocus = false,
    this.nickName = '',
    this.userAccount = '',
    this.userId = 0,
    this.userType = 0,
  });

  // 主播会员ID
  final String accno;
  final String avatar;
  // 是否开启了名片
  final bool cardEnabled;
  final int fansCount;
  // 当前用户是否已关注主播 false否true是
  bool isFocus;
  final String nickName;
  final String userAccount;
  final int userId;
  // 用户类型：0-普通用户 、1-游客用户、2-主播、3-家族长
  final int userType;

  factory AnchorBean.fromJson(Map<String, dynamic>? json) => AnchorBean(
        accno: json?["accno"]?.toString() ?? '',
        avatar: json?["avatar"]?.toString() ?? '',
        cardEnabled: json?["cardEnabled"] as bool? ?? false,
        fansCount: json?["fansCount"] as int? ?? 0,
        isFocus: json?["isFocus"] as bool? ?? false,
        nickName: json?["nickName"]?.toString() ?? '',
        userAccount: json?["userAccount"]?.toString() ?? '',
        userId: json?["userId"] as int? ?? 0,
        userType: json?["userType"] as int? ?? -1,
      );

  Map<String, dynamic> toJson() => {
        "accno": accno,
        "avatar": avatar,
        "cardEnabled": cardEnabled,
        "fansCount": fansCount,
        "isFocus": isFocus,
        "nickName": nickName,
        "userAccount": userAccount,
        "userId": userId,
        "userType": userType,
      };
}
