/// walletId : 4857
/// walletType : 1
/// userId : 1757
/// walletName : ""
/// balance : 0.00
/// amount : 0.00
/// silverBean : 0
/// sumGiveSilverBean : 0
/// accountDml : 0.00
/// sumAccountDml : 0.00
/// sumWithdrawAmount : 0.00
/// sumRechargeAmount : 0.00
/// isWithdrawal : true
/// payMax : 0.00
/// payFirst : 0.00
/// payNum : 0
/// withdrawalMax : 0.00
/// withdrawalFirst : 0.00
/// withdrawalNum : 0
/// gameWallet : null
/// shortcutOptionsUnit : "元"
/// txExChange : "1"
/// czExChange : "1"
/// currencyUint : "元"
/// fee : 0
/// minWithdraw : 100
/// desc : "{\"context\":\"1.提现金额100起；\\n2.提现成功后到账时间为1-30分钟，如遇特殊情况，可能会延长，如果长时间未到账，请联系客服；\\n3.禁止在本平台进行洗黑钱、恶意套利等行为，如若发现将严肃处理。\"}"

class WalletModel {
  WalletModel({
      this.walletId, 
      this.walletType, 
      this.userId, 
      this.walletName, 
      this.balance, 
      this.amount, 
      this.silverBean, 
      this.sumGiveSilverBean, 
      this.accountDml, 
      this.sumAccountDml, 
      this.sumWithdrawAmount, 
      this.sumRechargeAmount, 
      this.isWithdrawal, 
      this.payMax, 
      this.payFirst, 
      this.payNum, 
      this.withdrawalMax, 
      this.withdrawalFirst, 
      this.withdrawalNum, 
      this.gameWallet, 
      this.shortcutOptionsUnit, 
      this.txExChange, 
      this.czExChange, 
      this.currencyUint, 
      this.fee, 
      this.minWithdraw, 
      this.desc,});

  WalletModel.fromJson(dynamic json) {
    walletId = json['walletId'];
    walletType = json['walletType'];
    userId = json['userId'];
    walletName = json['walletName'];
    balance = json['balance'];
    amount = json['amount'];
    silverBean = json['silverBean'];
    sumGiveSilverBean = json['sumGiveSilverBean'];
    accountDml = json['accountDml'];
    sumAccountDml = json['sumAccountDml'];
    sumWithdrawAmount = json['sumWithdrawAmount'];
    sumRechargeAmount = json['sumRechargeAmount'];
    isWithdrawal = json['isWithdrawal'];
    payMax = json['payMax'];
    payFirst = json['payFirst'];
    payNum = json['payNum'];
    withdrawalMax = json['withdrawalMax'];
    withdrawalFirst = json['withdrawalFirst'];
    withdrawalNum = json['withdrawalNum'];
    gameWallet = json['gameWallet'];
    shortcutOptionsUnit = json['shortcutOptionsUnit'];
    txExChange = json['txExChange'];
    czExChange = json['czExChange'];
    currencyUint = json['currencyUint'];
    fee = json['fee'];
    minWithdraw = json['minWithdraw'];
    desc = json['desc'];
  }
  int? walletId;
  int? walletType;
  int? userId;
  String? walletName;
  double? balance;
  double? amount;
  int? silverBean;
  int? sumGiveSilverBean;
  double? accountDml;
  double? sumAccountDml;
  double? sumWithdrawAmount;
  double? sumRechargeAmount;
  bool? isWithdrawal;
  double? payMax;
  double? payFirst;
  int? payNum;
  double? withdrawalMax;
  double? withdrawalFirst;
  int? withdrawalNum;
  dynamic gameWallet;
  String? shortcutOptionsUnit;
  String? txExChange;
  String? czExChange;
  String? currencyUint;
  int? fee;
  int? minWithdraw;
  String? desc;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['walletId'] = walletId;
    map['walletType'] = walletType;
    map['userId'] = userId;
    map['walletName'] = walletName;
    map['balance'] = balance;
    map['amount'] = amount;
    map['silverBean'] = silverBean;
    map['sumGiveSilverBean'] = sumGiveSilverBean;
    map['accountDml'] = accountDml;
    map['sumAccountDml'] = sumAccountDml;
    map['sumWithdrawAmount'] = sumWithdrawAmount;
    map['sumRechargeAmount'] = sumRechargeAmount;
    map['isWithdrawal'] = isWithdrawal;
    map['payMax'] = payMax;
    map['payFirst'] = payFirst;
    map['payNum'] = payNum;
    map['withdrawalMax'] = withdrawalMax;
    map['withdrawalFirst'] = withdrawalFirst;
    map['withdrawalNum'] = withdrawalNum;
    map['gameWallet'] = gameWallet;
    map['shortcutOptionsUnit'] = shortcutOptionsUnit;
    map['txExChange'] = txExChange;
    map['czExChange'] = czExChange;
    map['currencyUint'] = currencyUint;
    map['fee'] = fee;
    map['minWithdraw'] = minWithdraw;
    map['desc'] = desc;
    return map;
  }

}