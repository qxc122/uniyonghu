///我的用户详细信息
class UserDetailModel {
  String? registerCountryCode;
  String? birthday;
  String? nickName;
  int? focusNum;
  String? registerArea;
  int? sex;
  String? personalSignature;
  String? accno;
  int? maritalStatus;
  bool? isOpenLive;
  String? mobilePhone;
  int? userType;
  String? occupationCode;
  int? userLevel;
  String? userAccount;
  int? fansNum;
  String? registerAreaCode;
  int? id;
  String? isLoginPassword;
  bool? nickNameStatus;
  String? firepower;
  bool? isPayPassword;
  String? avatar;
  String? hometown;

  UserDetailModel(
      {registerCountryCode,
      birthday,
      nickName,
      focusNum,
      registerArea,
      sex,
      personalSignature,
      accno,
      maritalStatus,
      isOpenLive,
      mobilePhone,
      userType,
      occupationCode,
      userLevel,
      userAccount,
      fansNum,
      registerAreaCode,
      id,
      isLoginPassword,
      nickNameStatus,
      firepower,
      isPayPassword,
      avatar,
      hometown});

  UserDetailModel.fromJson(Map<String, dynamic> json) {
    registerCountryCode = json['registerCountryCode'];
    birthday = json['birthday'];
    nickName = json['nickName'];
    focusNum = json['focusNum'];
    registerArea = json['registerArea'];
    sex = json['sex'];
    personalSignature = json['personalSignature'];
    accno = json['accno'];
    maritalStatus = json['maritalStatus'];
    isOpenLive = json['isOpenLive'];
    mobilePhone = json['mobilePhone'];
    userType = json['userType'];
    occupationCode = json['occupationCode'];
    userLevel = json['userLevel'];
    userAccount = json['userAccount'];
    fansNum = json['fansNum'];
    registerAreaCode = json['registerAreaCode'];
    id = json['id'];
    isLoginPassword = json['isLoginPassword'];
    nickNameStatus = json['nickNameStatus'];
    firepower = json['firepower'];
    isPayPassword = json['isPayPassword'];
    avatar = json['avatar'];
    hometown = json['hometown'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['registerCountryCode'] = registerCountryCode;
    data['birthday'] = birthday;
    data['nickName'] = nickName;
    data['focusNum'] = focusNum;
    data['registerArea'] = registerArea;
    data['sex'] = sex;
    data['personalSignature'] = personalSignature;
    data['accno'] = accno;
    data['maritalStatus'] = maritalStatus;
    data['isOpenLive'] = isOpenLive;
    data['mobilePhone'] = mobilePhone;
    data['userType'] = userType;
    data['occupationCode'] = occupationCode;
    data['userLevel'] = userLevel;
    data['userAccount'] = userAccount;
    data['fansNum'] = fansNum;
    data['registerAreaCode'] = registerAreaCode;
    data['id'] = id;
    data['isLoginPassword'] = isLoginPassword;
    data['nickNameStatus'] = nickNameStatus;
    data['firepower'] = firepower;
    data['isPayPassword'] = isPayPassword;
    data['avatar'] = avatar;
    data['hometown'] = hometown;
    return data;
  }
}
