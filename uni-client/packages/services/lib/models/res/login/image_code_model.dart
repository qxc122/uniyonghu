import 'dart:convert';
import 'dart:developer';

import 'package:base/commons/utils/log_utils.dart';
import 'package:flutter/foundation.dart';
import 'package:services/models/req/login.dart';

class ImageCodeModel {
  String? captchaKey;
  String? img64Code;
  String? imgCode;

  imageurl() {
    var tmp = img64Code ?? "";
    tmp = tmp.replaceAll("\n", "");
    tmp = tmp.replaceAll("\r", "");
    tmp = tmp.replaceAll("%2B", "");
    return base64Decode(tmp);
  }

  ImageCodeModel({this.captchaKey, this.img64Code, this.imgCode});

  ImageCodeModel.fromJson(Map<String, dynamic> json) {
    captchaKey = json['captchaKey'];
    img64Code = json['img64Code'];
    imgCode = json['imgCode'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['captchaKey'] = captchaKey;
    data['img64Code'] = img64Code;
    data['imgCode'] = imgCode;
    return data;
  }
}
