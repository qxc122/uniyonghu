import 'package:json_annotation/json_annotation.dart';
part 'user_info_model.g.dart';

const kOLUserInfoModelModelKey = "kOLUserInfoModelModelKey";

@JsonSerializable()
class OLUserInfoModel {
  @JsonKey(name: "acctoken")
  String? accessToken;
  String? channelCode;
  int? id = 0;
  String? inviteCode;
  String? registerCountryCode;

  OLUserInfoModel(this.accessToken, this.channelCode, this.id, this.inviteCode,
      this.registerCountryCode);
  factory OLUserInfoModel.fromJson(Map<String, dynamic> json) =>
      _$OLUserInfoModelFromJson(json);
  Map<String, dynamic> toJson() => _$OLUserInfoModelToJson(this);
}
