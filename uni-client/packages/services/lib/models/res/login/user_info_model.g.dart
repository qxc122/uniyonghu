// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_info_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OLUserInfoModel _$OLUserInfoModelFromJson(Map<String, dynamic> json) =>
    OLUserInfoModel(
      json['acctoken'] as String?,
      json['channelCode'] as String?,
      json['id'] as int?,
      json['inviteCode'] as String?,
      json['registerCountryCode'] as String?,
    );

Map<String, dynamic> _$OLUserInfoModelToJson(OLUserInfoModel instance) =>
    <String, dynamic>{
      'acctoken': instance.accessToken,
      'channelCode': instance.channelCode,
      'id': instance.id,
      'inviteCode': instance.inviteCode,
      'registerCountryCode': instance.registerCountryCode,
    };
