class OLCountryCodeModel {
  String? name;
  String? countryCode;
  String? areaCode;
  String? lang;

  String? olAreaCode() {
    var code = areaCode;
    if (code?.contains("86") ?? false) {
      return code?.replaceAll("00", "+");
    } else {
      return code;
    }
  }

  OLCountryCodeModel({name, countryCode, areaCode, lang});

  OLCountryCodeModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    countryCode = json['countryCode'];
    areaCode = json['areaCode'];
    lang = json['lang'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['name'] = name;
    data['countryCode'] = countryCode;
    data['areaCode'] = areaCode;
    data['lang'] = lang;
    return data;
  }
}
