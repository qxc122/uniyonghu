import 'dart:math';

import 'package:base/commons/utils/logger.dart';

import '../../http.dart';
import '../../api/apis.dart';
import '../res/base_response.dart';
import '../res/login/user_info_model.dart';
import '../res/login/country_code_model.dart';

/// @date 2023/02/01  10:28
/// @author bert
/// @des 通用api

class Login {
  static Login of() => Login();

  ///普通会员找回密码/支付密码接口
  Future<BaseResponse> findPassword(
    String type,
    String mobilePhone,
    String areaCode,
    String countryCode, {
    String? password,
    String? smsCode,
    String? captchaKey,
    String? imgCode,
  }) async {
    Map<dynamic, dynamic> data = {
      "type": type, //密码 type=1  支付密码  type=2
      "mobilePhone": mobilePhone,
      "password": password,
      "areaCode": areaCode,
      "countryCode": countryCode,
      "smsCode": smsCode,
      "captchaKey": captchaKey,
      "imgCode": imgCode,
    };
    return await HttpUtil().post(Apis.findPassword, data: data);
  }

  ///手机号或者密码登陆
  userRegisterAndLogin(
    String flag,
    String mobilePhone,
    String areaCode,
    String countryCode, {
    String? password,
    String? channelCode,
    String? inviteCode,
    String? smsCode,
    String? captchaKey,
    String? imgCode,
  }) async {
    Map<dynamic, dynamic> data = {
      "flag": flag, //密码 flag=2 必须输入密码
      "mobilePhone": mobilePhone,
      "password": password,
      "channelCode": channelCode,
      "inviteCode": inviteCode,
      "areaCode": areaCode,
      "countryCode": countryCode,
      "smsCode": smsCode,
      "captchaKey": captchaKey,
      "imgCode": imgCode,
      // "appVersion": appVersion,
      // "wifiData": wifiData,
    };
    return await HttpUtil().post(Apis.userRegisterAndLogin, data: data);

    // var response = await HttpUtil().post(Apis.userRegisterAndLogin, data: data);
    // return OLUserInfoModel.fromJson(response.data);
  }

  ///国家编号
  Future<List<OLCountryCodeModel>> queryAllCountry() async {
    var response = await HttpUtil().post(Apis.queryAllCountry);
    List<dynamic> data = response.data;
    return data.map((m) => OLCountryCodeModel.fromJson(m)).toList();
  }

  ///判断游客已绑定手机号
  Future<bool> visitorCheck() async {
    var response = await HttpUtil().post(Apis.visitorCheck);
    return response.data;
  }

  ///游客登录
  Future<OLUserInfoModel> visitorLogin({
    String? channelCode,
    String? inviteCode,
    String? appVersion,
  }) async {
    Map<dynamic, dynamic> data = {
      "channelCode": channelCode,
      "inviteCode": inviteCode,
      "appVersion": appVersion,
      // "wifiData": wifiData,
    };
    var response = await HttpUtil().post(Apis.visitorLogin, data: data);
    return OLUserInfoModel.fromJson(response.data);
  }

  getUserInfo() async {
    return await HttpUtil().post(Apis.getUserInfo);
    // return OLUserInfoModel.fromJson(response.data);
  }

  userVisitorLogin(String s, String username, String t, String u,
      {required String password}) {}
}
