import 'package:shared_preferences/shared_preferences.dart';

import '../../local_storage_repository.dart';

const kSurToken = 'SUR_TOKEN';
const kSurUser = 'SUR_USER';
const kIsFirst = 'ISFIRST';

class LocalRepositoryImpl extends LocalRepositoryInterface {
  @override
  Future<void> clearAllData() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.clear();
  }

  @override
  Future<String> saveToken(String token) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString(kSurToken, token);
    return token;
  }

  @override
  Future<String> getToken() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String token = sp.getString(kSurToken).toString();
    return token;
  }

  @override
  Future<bool> getIsFirst() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    bool isFirst = sp.getBool(kIsFirst) ?? false;
    return Future.value(isFirst);
  }

  @override
  Future<bool> saveIsFirst(bool isFirst) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setBool(kIsFirst, isFirst);
    return Future.value(isFirst);
  }
}
