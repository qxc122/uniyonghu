import 'package:base/app_routes.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:base/commons/widgets/ol_keep_alive_widget.dart';
import 'package:base_page/common_provider.dart';
import 'package:base_page/constant/notice_type.dart';
import 'package:base_page/login/visitor/visitor_page.dart';
import 'package:base_service/manager/ol_user_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:home_page/provider/home_provider.dart';
import 'package:services/models/res/home/home_tab_bean.dart';
import 'package:services/models/res/notice/notice_bean.dart';

import '../view/home_follow_page.dart';
import '../view/home_nearby_page.dart';
import '../view/home_other_page.dart';
import '../view/home_recommend_page.dart';
import '../view/home_video_page.dart';
import '../widget/notice_dialog_widget.dart';

/// 首页controller
class HomeController extends GetXBaseController
    with StateMixin<List<HomeTabBean>?> {
  /// 首页弹框公告
  Rx<NoticeBean?> noticeDialogBeanData = NoticeBean().obs;

  /// 当前tab
  RxString? curTabCode = 'recommend'.obs;

  @override
  void onInit() {
    _getColumn();
    super.onInit();
  }

  @override
  void onReady() {
    // 没有登录，进入登录页面
    if (!OLUserManager.shared.isLogin()) {
      Future.delayed(const Duration(seconds: 5), () {
        Get.bottomSheet(
          const VisitorPage(),
          elevation: 1,
          enableDrag: false,
          persistent: true,
          ignoreSafeArea: true,
          isScrollControlled: true,
          isDismissible: false,
          enterBottomSheetDuration: const Duration(seconds: 0),
          exitBottomSheetDuration: const Duration(seconds: 0),
          barrierColor: currentCustomThemeData().colors0x000000_60,
        );
      });
    }
    // 首页公告弹框
    _getAdvNotice();
    super.onReady();
  }

  ///获取首页tab数据
  _getColumn() async {
    HomeProvider.getColumn().then(
        (value) => {
              change(value, status: RxStatus.success())
            },
        onError: (err) =>
            {change(null, status: RxStatus.error(err.toString()))});
  }

  /// 获取tab对应的页面
  List<Widget> getTabBarViews(List<HomeTabBean>? tabs) {
    List<Widget> list = [];
    tabs?.forEach((element) {
      if (element.columnCode == "focus") {
        // 关注
        list.add(const OlKeepAliveWidget(child: HomeFollowPage()));
      } else if (element.columnCode == "recommend") {
        //推荐
        list.add(const OlKeepAliveWidget(child: HomeRecommendPage()));
      } else if (element.columnCode == "nearby") {
        //附近
        list.add(const OlKeepAliveWidget(child: HomeNearbyPage()));
      } else if (element.columnCode == "star") {
        //星秀
        list.add(const OlKeepAliveWidget(child: HomeVideoPage()));
      } else {
        list.add(OlKeepAliveWidget(
            child: HomeOtherPage(
          columnCode: element.columnCode,
        )));
      }
    });
    return list;
  }

  /// 全局公告,一条数据
  _getAdvNotice() {
    CommonProvider.getAdvNotice(NoticeType.homeNotice).then((value) {
      noticeDialogBeanData.value = value;
      showNoticeDialog();
    });
  }

  /// 展示公告dialog
  showNoticeDialog() {
    NoticeBean? bean = noticeDialogBeanData.value;
    String noticeContent = bean?.noticeContent ?? "";
    if (noticeContent.isEmpty) {
      return;
    }
    Get.dialog(NoticeDialogWidget(controller: this, bean: bean));
  }
}
