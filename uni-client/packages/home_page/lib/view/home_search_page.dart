import 'package:base/assets.gen.dart';
import 'package:base/bases/get_base_view.dart';
import 'package:base/commons/widgets/ol_blank_view.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:base/res/app_dimens.dart';
import 'package:base/res/font_dimens.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/home_search_controller.dart';
import '../widget/home_widget.dart';
import '../widget/live_item_widget.dart';

/// 首页搜索
class HomeSearchPage extends GetBaseView<HomeSearchController> {
  const HomeSearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var tabCode = Get.arguments;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: _appBarTitle(tabCode),
        actions: [
          Center(
            child: InkWell(
              child: Text(
                controller.homePageString('cancel'),
                style: TextStyle(
                    color: controller.currentCustomThemeData().colors0x9F44FF,
                    fontSize: FontDimens.fontSp16),
              ),
              onTap: () {
                Get.back();
              },
            ),
          ),
          const SizedBox(width: AppDimens.w_10)
        ],
      ),
      body: CustomScrollView(
        slivers: [
          SliverToBoxAdapter(
            child: Visibility(
                child: HomeWidget.of().subtitle(controller,
                    Assets.homePage.icHomeHistory.path, 'search_history'),
                visible: controller.historyData.isNotEmpty),
          ),
          _searchHistory(),
          SliverToBoxAdapter(
            child: HomeWidget.of().subtitle(controller,
                Assets.homePage.icHomeRecommend.path, 'recommend_anchor',
                changeData: true,
                onPressed: () => controller.getLiveFocusRecommendList()),
          ),
          _liveDataWidget()
        ],
      ),
    );
  }

  /// 搜索框
  Widget _appBarTitle(String tabCode) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: AppDimens.w_8),
      height: AppDimens.h_30,
      decoration: BoxDecoration(
          color: controller.currentCustomThemeData().colors0xF5F5F5,
          borderRadius: BorderRadius.circular(AppDimens.w_20)),
      child: Row(
        children: [
          AssetGenImage(Assets.homePage.icHomeSearchGray.path)
              .image(width: AppDimens.w_13, height: AppDimens.h_13),
          const SizedBox(width: AppDimens.w_8),
          Expanded(
              child: TextField(
            controller: controller.searchTec,
            cursorColor: controller.currentCustomThemeData().colors0x9F44FF,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: controller.homePageString('hint_uid_nickname'),
                hintStyle: TextStyle(
                    color: controller.currentCustomThemeData().colors0xC8C8C8,
                    fontSize: FontDimens.fontSp12)),
            onChanged: (text) {
              // 延迟一秒进行搜索
              Future.delayed(const Duration(seconds: 1), () {
                controller.handleSearch(tabCode, controller.searchTec.text);
              });
            },
            onEditingComplete: () {
              controller.handleSearch(tabCode, controller.searchTec.text);
            },
          )),
          const SizedBox(width: AppDimens.w_8),
          InkWell(
            child: AssetGenImage(Assets.homePage.icHomeClear.path)
                .image(width: AppDimens.w_16, height: AppDimens.h_16),
            onTap: () => controller.searchTec.clear(),
          ),
        ],
      ),
    );
  }

  /// 空视图
  SliverToBoxAdapter _buildEmpty() {
    return SliverToBoxAdapter(
      child: OLBlankView(),
    );
  }

  /// 搜索历史
  Widget _searchHistory() => Obx(() {
        var data = controller.historyData;
        return data.isEmpty
            ? SliverToBoxAdapter(child: Text(""),)
            : SliverPadding(
                padding: const EdgeInsets.symmetric(
                    horizontal: AppDimens.w_10, vertical: AppDimens.w_5),
                sliver: SliverGrid(
                    delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                      return LiveHomeWidget(
                          controller: controller, item: data[index]);
                    }, childCount: data.length),
                    gridDelegate: HomeWidget.of().getGridDelegate()),
              );
      });

  Widget _liveDataWidget() => Obx(() {
        var data = controller.focusRecommendData;
        return data.isEmpty
            ? _buildEmpty()
            : SliverPadding(
                padding: const EdgeInsets.symmetric(
                    horizontal: AppDimens.w_10, vertical: AppDimens.w_5),
                sliver: SliverGrid(
                    delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                      return LiveHomeWidget(
                          controller: controller, item: data[index]);
                    }, childCount: data.length),
                    gridDelegate: HomeWidget.of().getGridDelegate()),
              );
      });
}
