import 'package:base/bases/get_base_binding.dart';
import 'package:get/get.dart';

import '../controller/home_follow_controller.dart';

/// 首页关注binding
class HomeFollowBinding extends GetBaseBindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeFollowController>(() => HomeFollowController());
  }
}
