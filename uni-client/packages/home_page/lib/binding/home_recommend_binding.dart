import 'package:base/bases/get_base_binding.dart';
import 'package:get/get.dart';

import '../controller/home_recommend_controller.dart';

/// 首页推荐binding
class HomeRecommendBinding extends GetBaseBindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeRecommendController>(() => HomeRecommendController());
  }
}
