import 'package:base/bases/get_base_binding.dart';
import 'package:get/get.dart';

import '../controller/home_nearby_controller.dart';

/// 首页附近binding
class HomeNearbyBinding extends GetBaseBindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeNearbyController>(() => HomeNearbyController());
  }
}
