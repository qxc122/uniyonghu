import 'package:base/bases/get_base_binding.dart';
import 'package:get/get.dart';

import '../controller/home_video_controller.dart';

/// 首页星秀binding
class HomeVideoBinding extends GetBaseBindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeVideoController>(() => HomeVideoController());
  }
}
