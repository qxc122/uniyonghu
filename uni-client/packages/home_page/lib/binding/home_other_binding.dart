import 'package:base/bases/get_base_binding.dart';
import 'package:get/get.dart';

import '../controller/home_other_controller.dart';

/// 首页热门、游戏、附近、体育binding
class HomeOtherBinding extends GetBaseBindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeOtherController>(() => HomeOtherController());
  }
}
