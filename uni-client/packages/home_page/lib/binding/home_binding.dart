import 'package:base/bases/get_base_binding.dart';
import 'package:get/get.dart';
import 'package:home_page/controller/home_controller.dart';

/// 首页binding
class HomeBinding extends GetBaseBindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
  }
}
