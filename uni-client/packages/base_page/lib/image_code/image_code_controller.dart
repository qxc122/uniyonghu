import 'package:base/bases/get_base_controller.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:get/get.dart';
import 'package:services/api/common_apis.dart';
import 'package:services/models/res/login/image_code_model.dart';

/// 登录controller
class ImageCodeController extends GetXBaseController {
  Rx<ImageCodeModel> imagecode = ImageCodeModel().obs;

  @override
  void onInit() {
    randCode();
    super.onInit();
  }

  randCode() async {
    try {
      imagecode.value = await CommonApis.of().randCode();
    } catch (e) {
      OLEasyLoading.showToast(basePageString("图片验证码获取失败,请重试"));
      Get.back();
    }
  }
}
