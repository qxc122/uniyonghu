import 'dart:developer';
import 'package:base/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'visitor_controller.dart';

class VisitorPage extends GetView<VisitorController> {
  const VisitorPage({Key? key}) : super(key: key);

  show() {
    Get.bottomSheet(
      this,
      elevation: 1,
      enableDrag: false,
      persistent: true,
      ignoreSafeArea: true,
      isScrollControlled: true,
      isDismissible: false,
      enterBottomSheetDuration: const Duration(seconds: 0),
      exitBottomSheetDuration: const Duration(seconds: 0),
      barrierColor: controller.currentCustomThemeData().colors0x000000_60,
    );
  }

  @override
  Widget build(BuildContext context) {
    Get.put(VisitorController());
    return Center(
      child: Container(
        padding: const EdgeInsets.fromLTRB(21, 43, 21, 10),
        width: 330,
        decoration: BoxDecoration(
          color: Colors.transparent,
          image: DecorationImage(
            image: AssetImage(Assets.basePage.vistorBg.path),
            fit: BoxFit.fill, // 完全填充
          ),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Image.asset(
              Assets.basePage.loginType.path,
              height: 30,
              width: 182,
            ),
            const SizedBox(
              height: 12,
            ),
            Image.asset(
              Assets.basePage.loginLine.path,
            ),
            const SizedBox(
              height: 11,
            ),
            Text(
              controller.basePageString("已注册会员，请选择手机登录"),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16,
                color: controller.currentCustomThemeData().colors0x000000,
              ),
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              controller.basePageString("为了账户安全建议使用手机注册"),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 13,
                color: controller.currentCustomThemeData().colors0x000000_30,
              ),
            ),
            const SizedBox(
              height: 44,
            ),
            MaterialButton(
              padding: EdgeInsets.zero,
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              onPressed: () {
                log("点击游客登陆");
                controller.visitorLogin();
              },
              child: Text(
                controller.basePageString("vistor_login"),
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 16,
                  color: controller.currentCustomThemeData().colors0x000000_50,
                ),
              ),
            ),
            const SizedBox(
              height: 3,
            ),
            MaterialButton(
              padding: EdgeInsets.zero,
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              height: 33,
              minWidth: 150,
              onPressed: () {
                log("点击登录");
                controller.phoneLogin();
              },
              child: Container(
                width: 155,
                height: 35,
                decoration: BoxDecoration(
                  borderRadius:
                      const BorderRadius.all(Radius.circular(35 / 2.0)),
                  gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      controller.currentCustomThemeData().colors0x6129FF,
                      controller.currentCustomThemeData().colors0xD96CFF,
                    ],
                  ),
                ),
                child: Center(
                  child: Text(
                    controller.basePageString("login"),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: controller.currentCustomThemeData().colors0xFFFFFF,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
