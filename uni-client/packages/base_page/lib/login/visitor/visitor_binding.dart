import 'package:get/get.dart';

import 'visitor_controller.dart';

/// 登录binding
class VisitorBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VisitorController>(() => VisitorController());
  }
}
