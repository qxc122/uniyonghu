import 'dart:developer';

import 'package:base/app_routes.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:base_service/manager/ol_user_manager.dart';
import 'package:get/get.dart';
import 'package:services/models/req/login.dart';
import 'package:services/models/res/login/user_info_model.dart';

/// 登录controller
class VisitorController extends GetXBaseController {
  phoneLogin() {
    Get.back();
    Get.toNamed(AppRoutes.loginPage);
    // Get.off(AppRoutes.loginPage);
  }

  ///游客登录
  visitorLogin({
    String? channelCode,
    String? inviteCode,
    String? appVersion,
    String? wifiData,
  }) async {
    try {
      OLEasyLoading.showLoading(basePageString("login_ing"));
      bool check = await Login.of.call().visitorCheck();
      if (check) {
        OLEasyLoading.dismiss();
        OLEasyLoading.showToast(basePageString("bind_phone_already"));
      } else {
        OLUserInfoModel res = await Login.of.call().visitorLogin(
              channelCode: channelCode,
              inviteCode: inviteCode,
              appVersion: appVersion,
            );
        log("游客token ${res.accessToken ?? ""}");
        OLUserManager.shared.save(res);
        OLEasyLoading.dismiss();
        OLEasyLoading.showToast(basePageString("login_success"));
        Get.offAndToNamed(AppRoutes.mainPage);
      }
    } catch (e) {
      print(e);
      OLEasyLoading.dismiss();
      OLEasyLoading.showToast(basePageString("please_retry"));
    }
  }
}
