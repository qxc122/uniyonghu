import 'dart:developer';

import 'package:base/app_routes.dart';
import 'package:base/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'login_controller.dart';

/// 登录view
class LoginPage extends GetView<LoginController> {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Image.asset(
          Assets.basePage.loginBg.path,
          width: double.infinity,
          height: double.infinity,
          fit: BoxFit.cover,
        ),
        Positioned(
          top: 100,
          left: 54,
          right: 54,
          child: SizedBox(
            // width: 267,
            // height: 375,
            child: Container(
              padding: const EdgeInsets.fromLTRB(13, 15, 13, 13),
              decoration: BoxDecoration(
                color: controller.currentCustomThemeData().colors0x000000_30,
                borderRadius: const BorderRadius.all(
                  Radius.circular(20.0),
                ),
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: MaterialButton(
                          padding: EdgeInsets.zero,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          onPressed: () {
                            log("点击密码登陆");
                            controller.tab.value = 0;
                          },
                          child: Obx(() {
                            return Text(
                              controller.basePageString("pwd_login"),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: controller.tab.value == 0
                                    ? controller
                                        .currentCustomThemeData()
                                        .colors0xFFFFFF
                                    : controller
                                        .currentCustomThemeData()
                                        .colors0xFFFFFF_50,
                              ),
                            );
                          }),
                        ),
                        flex: 1,
                      ),
                      Expanded(
                        child: MaterialButton(
                          padding: EdgeInsets.zero,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          onPressed: () {
                            log("点击手机登陆");
                            controller.tab.value = 1;
                          },
                          child: Obx(() {
                            return Text(
                              controller.basePageString("phone_login"),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18,
                                color: controller.tab.value == 1
                                    ? controller
                                        .currentCustomThemeData()
                                        .colors0xFFFFFF
                                    : controller
                                        .currentCustomThemeData()
                                        .colors0xFFFFFF_50,
                              ),
                            );
                          }),
                        ),
                        flex: 1,
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  Row(
                    children: [
                      Text(
                        controller.basePageString("country_area"),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 14,
                          color: controller
                              .currentCustomThemeData()
                              .colors0xFFFFFF,
                        ),
                      ),
                      Expanded(child: Container()),
                      MaterialButton(
                        minWidth: 0,
                        padding: EdgeInsets.zero,
                        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        // height: 10,
                        onPressed: () {
                          controller.areaCodePage();
                          log("选择区号");
                        },
                        child: Row(
                          children: [
                            Obx(() {
                              return Text(
                                controller.code.value.name ?? "",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0xFFFFFF,
                                ),
                              );
                            }),
                            const SizedBox(
                              width: 8,
                            ),
                            Image.asset(
                              Assets.basePage.moreWhile.path,
                              width: 12,
                              height: 12,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  const Divider(
                    height: 0.5,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  field(
                    controller.phone,
                    "请输入手机号",
                    prefix: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Image.asset(
                          Assets.basePage.phoneWhilt.path,
                          width: 10,
                          height: 15,
                        ),
                        const SizedBox(
                          width: 6,
                        ),
                        Obx(() {
                          return Text(
                            controller.code.value.olAreaCode() ?? "",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontWeight: FontWeight.w500,
                              fontSize: 14,
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0xFFFFFF,
                            ),
                          );
                        }),
                        const SizedBox(
                          width: 5,
                        ),
                        Container(
                          width: 0.5,
                          height: 22,
                          decoration: BoxDecoration(
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0xFFFFFF),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                      ],
                    ),
                  ),
                  const Divider(
                    height: 0.5,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Obx(() {
                    return Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 0, 6, 0),
                          child: Image.asset(
                            Assets.basePage.whilePwd.path,
                            width: 10,
                            height: 15,
                          ),
                        ),
                        Expanded(
                          child: field(
                            controller.tab.value == 0
                                ? controller.password
                                : controller.sms,
                            controller.tab.value == 0
                                ? controller.basePageString("请输入密码")
                                : controller.basePageString("请输入短信验证码"),
                            obscureText: controller.tab.value == 0,
                          ),
                        ),
                        controller.tab.value == 0
                            ? Container()
                            : MaterialButton(
                                padding: EdgeInsets.zero,
                                materialTapTargetSize:
                                    MaterialTapTargetSize.shrinkWrap,
                                onPressed: () {
                                  log("获取短信");
                                  controller.sendSms(controller.phone.text);
                                },
                                child: Container(
                                  padding:
                                      const EdgeInsets.fromLTRB(4, 4, 4, 4),
                                  decoration: BoxDecoration(
                                    color: controller.countdownTime.value < 61
                                        ? controller
                                            .currentCustomThemeData()
                                            .colors0xE5E5E5
                                        : controller
                                            .currentCustomThemeData()
                                            .colors0x7032FF,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                  ),
                                  child: Text(
                                    controller.countdownTime.value < 61
                                        ? "${controller.countdownTime.value}s${controller.basePageString("后重发")}"
                                        : controller.basePageString("sms_get"),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 12,
                                      color: controller.countdownTime.value < 61
                                          ? controller
                                              .currentCustomThemeData()
                                              .colors0x000000
                                          : controller
                                              .currentCustomThemeData()
                                              .colors0xFFFFFF,
                                    ),
                                  ),
                                ),
                              ),
                      ],
                    );
                  }),
                  const Divider(
                    height: 0.5,
                    color: Colors.white,
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Obx(() {
                    if (controller.tab.value == 0) {
                      return Row(
                        children: [
                          MaterialButton(
                            height: 30,
                            onPressed: () {
                              log("点击注册");
                              Get.toNamed(AppRoutes.register, arguments: 1);
                            },
                            minWidth: 0,
                            padding: EdgeInsets.zero,
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            child: Text(
                              controller.basePageString("register"),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: controller
                                    .currentCustomThemeData()
                                    .colors0xFFFFFF,
                              ),
                            ),
                          ),
                          Expanded(child: Container()),
                          MaterialButton(
                            height: 30,
                            minWidth: 0,
                            onPressed: () {
                              log("点击忘记密码");
                              Get.toNamed(AppRoutes.register, arguments: 2);
                            },
                            padding: EdgeInsets.zero,
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            child: Text(
                              controller.basePageString("forget_pwd"),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                                color: controller
                                    .currentCustomThemeData()
                                    .colors0xFFFFFF,
                              ),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return const SizedBox(
                        height: 30,
                      );
                    }
                  }),
                  const SizedBox(
                    height: 28,
                  ),
                  MaterialButton(
                    padding: EdgeInsets.zero,
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    onPressed: () {
                      log("点击游客登陆");
                      controller.visitorLogin();
                    },
                    child: Text(
                      controller.basePageString("vistor_login"),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: controller
                            .currentCustomThemeData()
                            .colors0xFFFFFF_70,
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  MaterialButton(
                    padding: EdgeInsets.zero,
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    height: 33,
                    minWidth: 150,
                    onPressed: () {
                      log("点击登录");
                      controller.login();
                    },
                    child: Container(
                      width: 150,
                      height: 33,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: ExactAssetImage(
                            Assets.basePage.loginBunBg.path,
                          ),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          controller.basePageString("login"),
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 16,
                            color: controller
                                .currentCustomThemeData()
                                .colors0xFFFFFF,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  TextField field(TextEditingController textController, String hintText,
      {bool obscureText = false, Widget? prefix}) {
    return TextField(
      controller: textController,
      // maxLength: 15,
      style: TextStyle(
        color: controller.currentCustomThemeData().colors0xFFFFFF,
        fontWeight: FontWeight.w500,
        fontSize: 14,
      ),
      keyboardType: TextInputType.phone,
      decoration: InputDecoration(
        // labelText: "手机号",
        border: InputBorder.none, // 去掉下滑线
        counterText: '', // 去除输入框底部的字符计数
        hintText: hintText,
        hintStyle: TextStyle(
          color: controller.currentCustomThemeData().colors0xFFFFFF_40,
          fontWeight: FontWeight.w500,
          fontSize: 14,
        ),
        // prefix: prefix,
        prefixIcon: prefix,
      ),
      obscureText: obscureText,
    );
  }
}
