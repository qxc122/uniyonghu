import 'dart:async';
import 'dart:developer';
import 'package:base/app_routes.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:get/get.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:base_service/manager/ol_user_manager.dart';
import 'package:services/api/common_apis.dart';
import 'package:services/models/req/login.dart';
import 'package:services/models/res/base_response.dart';
import 'package:services/models/res/login/user_info_model.dart';
import 'package:services/models/res/login/country_code_model.dart';
import 'package:flutter/material.dart';
import '../image_code/image_code_page.dart';

/// 登录controller
class LoginController extends GetXBaseController {
  var pwdLoginNeedImageCode = false;
  var smsNeedImageCode = false;
  var phoneLoginNeedImageCode = false;
  var smsCodeIsOk = false;

  final TextEditingController phone = TextEditingController();
  final TextEditingController password = TextEditingController();
  final TextEditingController sms = TextEditingController();

  var tab = 1.obs;
  late Timer timer;
  var countdownTime = 61.obs;
  Rx<OLCountryCodeModel> code = OLCountryCodeModel().obs;

  void startCountdownTimer() {
    var oneSec = const Duration(seconds: 1);
    timer = Timer.periodic(oneSec, (timer) {
      if (countdownTime < 1) {
        countdownTime.value = 61;
        timer.cancel();
      } else {
        countdownTime.value = countdownTime.value - 1;
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  void onInit() {
    queryAllCountry();
    super.onInit();
  }

  ///手机号或者密码登陆
  login({
    String? channelCode,
    String? inviteCode,
    String? captchaKey,
    String? imgCode,
  }) async {
    try {
      if (phone.text.isEmpty) {
        OLEasyLoading.showToast(basePageString("please_phone_first"));
        return;
      }
      var flag = "";
      if (tab.value == 1) {
        flag = "1";
      } else {
        flag = "2";
      }
      if (flag == "1") {
        //手机登录
        if (sms.text.isEmpty) {
          OLEasyLoading.showToast(basePageString("please_sms"));
          return;
        }
        if (!smsCodeIsOk) {
          OLEasyLoading.showToast(basePageString("请先获取短信验证码"));
          return;
        }
      } else {
        if (password.text.isEmpty) {
          OLEasyLoading.showToast(basePageString("please_pwd"));
          return;
        }
      }
      if (tab.value == 0 &&
          pwdLoginNeedImageCode &&
          (captchaKey?.isEmpty ?? true)) {
        showImageCode();
        return;
      }
      if (tab.value == 1 &&
          phoneLoginNeedImageCode &&
          (captchaKey?.isEmpty ?? true)) {
        showImageCode();
        return;
      }
      OLEasyLoading.showLoading(basePageString("login_ing"));
      BaseResponse response = await Login.of.call().userRegisterAndLogin(
            flag,
            phone.text,
            code.value.olAreaCode() ?? "",
            code.value.countryCode ?? "",
            password: tab.value == 0 ? password.text : null,
            channelCode: channelCode,
            inviteCode: inviteCode,
            smsCode: tab.value == 1 ? sms.text : null,
            captchaKey: captchaKey,
            imgCode: imgCode,
          );
      OLEasyLoading.dismiss();
      if (response.code == 200) {
        OLUserInfoModel res = OLUserInfoModel.fromJson(response.data);
        log("token ${res.accessToken ?? ""}");
        OLUserManager.shared.save(res);
        OLEasyLoading.showToast(basePageString("login_success"));
        Get.offAndToNamed(AppRoutes.mainPage);
      } else if (response.code == 1005 ||
          response.code == 1006 ||
          (response.msg?.contains("错误") ?? false)) {
        if (tab.value == 0) {
          pwdLoginNeedImageCode = true;
          phoneLoginNeedImageCode = false;
          smsNeedImageCode = false;
        } else {
          pwdLoginNeedImageCode = false;
          phoneLoginNeedImageCode = true;
          smsNeedImageCode = false;
        }
        if ((response.msg?.contains("错误") ?? false)) {
          password.text = "";
          OLEasyLoading.showToast(response.msg ?? "");
        } else {
          OLEasyLoading.showToast(response.msg ?? "");
          showImageCode();
        }
      } else {
        OLEasyLoading.showToast(response.msg ?? "");
      }
    } catch (e) {
      print(e);
      OLEasyLoading.dismiss();
      OLEasyLoading.showToast(basePageString("please_retry"));
    }
  }

  ///国家编号
  queryAllCountry() async {
    try {
      List<OLCountryCodeModel> res = await Login.of.call().queryAllCountry();
      for (var obj in res) {
        if (obj.areaCode?.contains("86") ?? false) {
          code.value = obj;
          break;
        }
      }
    } catch (e) {
      print(e);
    }
  }

  void areaCodePage() async {
    await Get.toNamed(AppRoutes.areaCode, arguments: code.value)
        ?.then((value) => {
              if (value != null) {code.value = value}
            });
  }

  sendSms(
    String phone, {
    String? imgCode,
    String? captchaKey,
  }) async {
    try {
      if (countdownTime.value < 61) {
        return;
      }
      if (smsNeedImageCode && (captchaKey?.isEmpty ?? true)) {
        showImageCode();
        return;
      }
      if ((code.value.areaCode?.length ?? 0) <= 0) {
        OLEasyLoading.showToast(basePageString("please_country"));
        return;
      }
      if (phone.isEmpty) {
        OLEasyLoading.showToast(basePageString("please_phone_first"));
        return;
      }
      OLEasyLoading.showLoading(basePageString("send_ing"));
      BaseResponse res = await CommonApis.of.call().sendSms(
            phone,
            1,
            code.value.olAreaCode() ?? "",
            imgCode: imgCode,
            captchaKey: captchaKey,
          );
      OLEasyLoading.dismiss();
      if (res.code == 200) {
        smsCodeIsOk = true;
        OLEasyLoading.showToast(basePageString("send_sms_success"));
        countdownTime.value = 60;
        startCountdownTimer();
      } else if (res.code == 1005 || res.code == 1006) {
        pwdLoginNeedImageCode = false;
        phoneLoginNeedImageCode = false;
        smsNeedImageCode = true;
        showImageCode();
      } else {
        OLEasyLoading.showToast(res.msg ?? "");
      }
    } catch (e) {
      print(e);
      OLEasyLoading.dismiss();
      OLEasyLoading.showToast(basePageString("please_retry"));
    }
  }

  ///游客登录
  visitorLogin({
    String? channelCode,
    String? inviteCode,
    String? appVersion,
    String? wifiData,
  }) async {
    // Get.bottomSheet(
    //   const VisitorPage(),
    //   elevation: 1,
    //   enableDrag: false,
    //   persistent: true,
    //   ignoreSafeArea: true,
    //   isScrollControlled: true,
    //   isDismissible: false,
    //   enterBottomSheetDuration: const Duration(seconds: 0),
    //   exitBottomSheetDuration: const Duration(seconds: 0),
    //   barrierColor: currentCustomThemeData().colors0x000000_60,
    // );

    // return;
    try {
      OLEasyLoading.showLoading(basePageString("login_ing"));
      bool check = await Login.of.call().visitorCheck();
      if (check) {
        OLEasyLoading.dismiss();
        OLEasyLoading.showToast(basePageString("bind_phone_already"));
      } else {
        OLUserInfoModel res = await Login.of.call().visitorLogin(
              channelCode: channelCode,
              inviteCode: inviteCode,
              appVersion: appVersion,
            );
        log("游客token ${res.accessToken ?? ""}");
        OLUserManager.shared.save(res);
        OLEasyLoading.dismiss();
        OLEasyLoading.showToast(basePageString("login_success"));
        Get.offAndToNamed(AppRoutes.mainPage);
      }
    } catch (e) {
      print(e);
      OLEasyLoading.dismiss();
      OLEasyLoading.showToast(basePageString("please_retry"));
    }
  }

  showImageCode() {
    Get.bottomSheet(
      ImageCodePage(
        submit: (p0) {
          log("121212 showImageCode ${p0?.imgCode ?? ""} ${p0?.captchaKey ?? ""}");
          if (smsNeedImageCode) {
            smsNeedImageCode = false;
            sendSms(phone.text,
                imgCode: p0?.imgCode, captchaKey: p0?.captchaKey);
          } else {
            phoneLoginNeedImageCode = false;
            pwdLoginNeedImageCode = false;
            login(imgCode: p0?.imgCode, captchaKey: p0?.captchaKey);
          }
        },
      ),
      elevation: 1,
      enableDrag: false,
      persistent: true,
      ignoreSafeArea: true,
      isScrollControlled: true,
      isDismissible: false,
      enterBottomSheetDuration: const Duration(seconds: 0),
      exitBottomSheetDuration: const Duration(seconds: 0),
      barrierColor: currentCustomThemeData().colors0x000000_60,
    );
  }
}
