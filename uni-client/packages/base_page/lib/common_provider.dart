import 'package:get/get.dart';
import 'package:services/api/common_apis.dart';
import 'package:services/models/res/banner/banner_bean.dart';
import 'package:services/models/res/base_response.dart';
import 'package:services/models/res/notice/notice_bean.dart';

/// 一些通用的请求，比如公告、banner等
class CommonProvider extends GetConnect{

  /// 轮播图广告位查询，根据code
  static Future<List<BannerBean>?> getBannerList(String code) async {
    Map<String, String> params = {"flashviewCode": code};
    BaseResponse response = await CommonApis.of().getBannerList(params);
    if (GetUtils.isNull(response) == true) {
      return null;
    }
    return (response.data as List<dynamic>)
        .map((e) => BannerBean.fromJson(e as Map<String, dynamic>))
        .toList();
  }

  /// 全局公告,一条数据
  static Future<NoticeBean?> getAdvNotice(int type) async {
    Map<String, int> params = {"type": type};
    BaseResponse response = await CommonApis.of().getAdvNotice(params);
    if (GetUtils.isNull(response) == true) {
      return null;
    }
    return NoticeBean.fromJson(response.data);
  }
}