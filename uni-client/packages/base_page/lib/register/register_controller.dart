import 'dart:async';
import 'dart:developer';

import 'package:base/app_routes.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:base_page/image_code/image_code_page.dart';
import 'package:base_service/manager/ol_user_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:services/api/common_apis.dart';
import 'package:services/models/req/login.dart';
import 'package:services/models/res/base_response.dart';
import 'package:services/models/res/login/country_code_model.dart';
import 'package:services/models/res/login/user_info_model.dart';

class RegisterController extends GetXBaseController {
  final TextEditingController phone = TextEditingController();
  final TextEditingController smscode = TextEditingController();
  final TextEditingController password = TextEditingController();

  var subMitNeedImageCode = false;
  var smsNeedImageCode = false;
  var smsCodeIsOk = false;

  ///1 注册 2忘记密码  4  忘记支付密码
  var type = 0.obs;
  var typeTitle = "".obs;
  var pageTitle = "".obs;

  late Timer timer;
  var countdownTime = 61.obs;
  //密码登录是否需要图片验证码
  var passWordLoginNeeDImageCode = false;
  //手机登录是否需要图片验证码
  var phoneLoginNeeDImageCode = false;

  Rx<OLCountryCodeModel> code = OLCountryCodeModel().obs;

  @override
  void onInit() {
    queryAllCountry();
    type.value = Get.arguments ?? 1;
    // type.value = 4;
    if (type.value == 1) {
      typeTitle.value = basePageString("请设置密码（6-18位字符）");
      pageTitle.value = basePageString("注册");
    } else if (type.value == 2) {
      typeTitle.value = basePageString("请设置密码（6-18位字符）");
      pageTitle.value = basePageString("忘记密码");
    } else if (type.value == 4) {
      typeTitle.value = basePageString("请设置密码（6位数字）");
      pageTitle.value = basePageString("忘记支付密码");
    }
    super.onInit();
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  void imageCodeShow() {
    Get.bottomSheet(
      ImageCodePage(
        submit: (p0) {
          log("121212 showImageCode");
          // sendSms(phone.text, imgCode: p0?.imgCode, captchaKey: p0?.captchaKey);
        },
      ),
      elevation: 1,
      // backgroundColor: currentCustomThemeData().colors0xFFFFFF,
      enableDrag: false,
      persistent: true,
      ignoreSafeArea: true,
      isScrollControlled: true,
      isDismissible: false,
      enterBottomSheetDuration: const Duration(seconds: 0),
      exitBottomSheetDuration: const Duration(seconds: 0),
      barrierColor: currentCustomThemeData().colors0x000000_60,
    );
  }

  void startCountdownTimer() {
    var oneSec = const Duration(seconds: 1);
    timer = Timer.periodic(oneSec, (timer) {
      if (countdownTime < 1) {
        countdownTime.value = 61;
        timer.cancel();
      } else {
        countdownTime.value = countdownTime.value - 1;
      }
    });
  }

  void areaCodePage() async {
    await Get.toNamed(AppRoutes.areaCode, arguments: code.value)
        ?.then((value) => {
              if (value != null) {code.value = value}
            });
  }

  sendSms(
    String phone, {
    String? imgCode,
    String? captchaKey,
  }) async {
    try {
      if (countdownTime.value < 61) {
        return;
      }
      if ((code.value.areaCode?.length ?? 0) <= 0) {
        OLEasyLoading.showToast(basePageString("please_country"));
        return;
      }
      if (phone.isEmpty) {
        OLEasyLoading.showToast(basePageString("please_phone_first"));
        return;
      }
      OLEasyLoading.showLoading(basePageString("send_ing"));
      BaseResponse res = await CommonApis.of.call().sendSms(
            phone,
            type.value,
            code.value.olAreaCode() ?? "",
            imgCode: imgCode,
            captchaKey: captchaKey,
          );
      OLEasyLoading.dismiss();
      if (res.code == 200) {
        smsCodeIsOk = true;
        OLEasyLoading.showToast(basePageString("send_sms_success"));
        countdownTime.value = 60;
        startCountdownTimer();
      } else if (res.code == 1005 || res.code == 1006) {
        subMitNeedImageCode = false;
        smsNeedImageCode = true;
        showImageCode();
        OLEasyLoading.showToast(res.msg ?? "");
      } else {
        OLEasyLoading.showToast(res.msg ?? "");
      }
    } catch (e) {
      print(e);
      OLEasyLoading.dismiss();
      OLEasyLoading.showToast(basePageString("please_retry"));
    }
  }

  ///国家编号
  queryAllCountry() async {
    try {
      List<OLCountryCodeModel> res = await Login.of.call().queryAllCountry();
      for (var obj in res) {
        if (obj.areaCode?.contains("86") ?? false) {
          code.value = obj;
          break;
        }
      }
    } catch (e) {
      print(e);
    }
  }

  findPassword(
    String type,
    String? mobilePhone,
    String? password,
    String? smsCode, {
    String? captchaKey,
    String? imgCode,
  }) async {
    try {
      if (mobilePhone?.isEmpty ?? true) {
        OLEasyLoading.showToast(basePageString("please_phone_first"));
        return;
      }
      if (smsCode?.isEmpty ?? true) {
        OLEasyLoading.showToast(basePageString("please_sms"));
        return;
      }
      if (password?.isEmpty ?? true) {
        OLEasyLoading.showToast(basePageString("please_pwd"));
        return;
      }
      if (!smsCodeIsOk) {
        OLEasyLoading.showToast(basePageString("请先获取短信验证码"));
        return;
      }
      if (subMitNeedImageCode && (captchaKey?.isEmpty ?? true)) {
        showImageCode();
        return;
      }
      OLEasyLoading.showLoading(basePageString("修改中..."));
      BaseResponse response = await Login.of.call().findPassword(
            type,
            mobilePhone ?? "",
            code.value.olAreaCode() ?? "",
            code.value.countryCode ?? "",
            password: password,
            smsCode: smsCode,
            imgCode: imgCode,
            captchaKey: captchaKey,
          );
      OLEasyLoading.dismiss();
      if (response.code == 200) {
        OLEasyLoading.showToast(basePageString("修改成功"));
        Get.back();
      } else if (response.code == 1005 || response.code == 1006) {
        subMitNeedImageCode = true;
        smsNeedImageCode = false;
        showImageCode();
      } else {
        OLEasyLoading.showToast(response.msg ?? "");
      }
    } catch (e) {
      print(e);
      OLEasyLoading.dismiss();
      OLEasyLoading.showToast(basePageString("please_retry"));
      // Get.back();
    }
  }

  submit() {
    if (type.value == 1) {
      userRegisterAndLogin(
        phone.text,
        password: password.text,
        smsCode: smscode.text,
      );
    } else if (type.value == 2) {
      ///忘记密码
      findPassword("1", phone.text, password.text, smscode.text);
    } else if (type.value == 4) {
      ///忘记支付密码
      findPassword("2", phone.text, password.text, smscode.text);
    }
  }

  userRegisterAndLogin(
    String mobilePhone, {
    String? password,
    String? channelCode,
    String? inviteCode,
    String? smsCode,
    String? captchaKey,
    String? imgCode,
  }) async {
    try {
      if (mobilePhone.isEmpty) {
        OLEasyLoading.showToast(basePageString("please_phone_first"));
        return;
      }
      //手机登录
      if ((smsCode?.length ?? 0) <= 0) {
        OLEasyLoading.showToast(basePageString("please_sms"));
        return;
      }
      if ((password?.length ?? 0) <= 0) {
        OLEasyLoading.showToast(basePageString("please_pwd"));
        return;
      }
      if (!smsCodeIsOk) {
        OLEasyLoading.showToast(basePageString("请先获取短信验证码"));
        return;
      }
      OLEasyLoading.showLoading(basePageString("注册中..."));
      BaseResponse response = await Login.of.call().userRegisterAndLogin(
            "3",
            mobilePhone,
            code.value.olAreaCode() ?? "",
            code.value.countryCode ?? "",
            password: password,
            channelCode: channelCode,
            inviteCode: inviteCode,
            smsCode: smsCode,
            captchaKey: captchaKey,
            imgCode: imgCode,
          );
      OLEasyLoading.dismiss();
      if (response.code == 200) {
        OLUserInfoModel res = OLUserInfoModel.fromJson(response.data);
        log("token ${res.accessToken ?? ""}");
        OLUserManager.shared.save(res);
        OLEasyLoading.showToast(basePageString("login_success"));
        Get.offAndToNamed(AppRoutes.mainPage);
      } else if (response.code == 1005 || response.code == 1006) {
        subMitNeedImageCode = true;
        smsNeedImageCode = false;
        showImageCode();
      } else {
        OLEasyLoading.showToast(response.msg ?? "");
      }
    } catch (e) {
      print(e);
      OLEasyLoading.dismiss();
      OLEasyLoading.showToast(basePageString("please_retry"));
      // Get.back();
    }
  }

  showImageCode() {
    Get.bottomSheet(
      ImageCodePage(
        submit: (p0) {
          log("121212 showImageCode ${p0?.imgCode ?? ""} ${p0?.captchaKey ?? ""}");
          if (smsNeedImageCode) {
            smsNeedImageCode = false;
            sendSms(phone.text,
                imgCode: p0?.imgCode, captchaKey: p0?.captchaKey);
          } else {
            // phoneLoginNeedImageCode = false;
            // pwdLoginNeedImageCode = false;
            // login(imgCode: p0?.imgCode, captchaKey: p0?.captchaKey);
          }
        },
      ),
      elevation: 1,
      enableDrag: false,
      persistent: true,
      ignoreSafeArea: true,
      isScrollControlled: true,
      isDismissible: false,
      enterBottomSheetDuration: const Duration(seconds: 0),
      exitBottomSheetDuration: const Duration(seconds: 0),
      barrierColor: currentCustomThemeData().colors0x000000_60,
    );
  }
}
