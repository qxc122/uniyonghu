import 'package:get/get.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:services/api/common_apis.dart';
import 'package:services/models/res/login/online_service_model.dart';

class OnlineServiceController extends GetXBaseController {
  Rx<OnlineServiceModel> online = OnlineServiceModel().obs;

  @override
  void onInit() {
    queryAllCountry();
    super.onInit();
  }

  queryAllCountry() async {
    try {
      online.value = await CommonApis.of.call().getOnlineService();
    } catch (e) {
      print(e);
    }
  }
}
