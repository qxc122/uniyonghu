import 'package:get/get.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:services/models/req/login.dart';
import 'package:services/models/res/login/country_code_model.dart';

class AreaCodeController extends GetXBaseController {
  Rx<OLCountryCodeModel> code = OLCountryCodeModel().obs;
  RxList arry = [].obs;
  List<OLCountryCodeModel> oldArry = [];

  @override
  void onInit() {
    queryAllCountry();
    code.value = Get.arguments ?? OLCountryCodeModel();
    super.onInit();
  }

  key(String? key) {
    if (key?.isEmpty ?? true) {
      arry.value = oldArry;
    } else {
      arry.value = oldArry
          .where((element) => element.name?.contains(key ?? "") ?? false)
          .toList();
    }
  }

  ///国家编号
  queryAllCountry() async {
    try {
      var res = await Login.of.call().queryAllCountry();
      arry.value = res;
      oldArry = res;
    } catch (e) {
      print(e);
    }
  }
}
