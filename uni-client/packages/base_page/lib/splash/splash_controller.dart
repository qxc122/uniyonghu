import 'package:base/app_routes.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:base_page/splash/splash_provider.dart';
import 'package:get/get.dart';
import 'package:services/models/res/splash/slide_bean.dart';

class SplashController extends GetXBaseController with StateMixin<SlideBean?> {
  /// 广告页倒计时总时间
  var adSeconds = 3.obs;

  @override
  void onInit() {
    _getSlide();
    super.onInit();
  }

  /// 广告页
  _getSlide() async {
    SplashProvider.getSlide().then(
        (value) => {_goNextPage(), change(value, status: RxStatus.success())},
        onError: (err) =>
            {change(null, status: RxStatus.error(err.toString()))});
  }

  ///  进去下一个页面
  _goNextPage() {
    Future.delayed(Duration(seconds: adSeconds.value), () {
      // 进入主页
      Get.offNamed(AppRoutes.mainPage);
    });
  }
}
