import 'package:base/bases/get_base_connect.dart';
import 'package:get/get.dart';
import 'package:services/api/common_apis.dart';
import 'package:services/models/res/base_response.dart';
import 'package:services/models/res/splash/slide_bean.dart';

class SplashProvider extends GetBaseConnect {
  static Future<SlideBean?> getSlide() async {
    BaseResponse response = await CommonApis.of().getSlide();
    if (GetUtils.isNull(response) == true) {
      return null;
    }
    return SlideBean.fromJson(response.data);
  }
}
