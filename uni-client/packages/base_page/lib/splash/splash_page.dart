import 'package:base/assets.gen.dart';
import 'package:base/bases/get_base_view.dart';
import 'package:base_page/splash/splash_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:ol_image_cache/src/ol_image_widget.dart';

/// 广告页
class SplashPage extends GetBaseView<SplashController> {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      // 启动背景
      decoration: BoxDecoration(
          image: DecorationImage(
              fit: BoxFit.cover,
              image: ExactAssetImage(Assets.basePage.icSplash.path))),
      // 广告页
      child: controller.obx(
        (state) => ConstrainedBox(
          constraints: const BoxConstraints.expand(
              width: double.infinity, height: double.infinity), // 自适应屏幕
          child: OLImage(
            imageUrl: state?.advImg ?? "",
            fit: BoxFit.cover,
          ),
        ),
      ),
    ));
  }
}
