import 'dart:developer';

import 'package:base/app_routes.dart';
import 'package:base/assets.gen.dart';
import 'package:base/res/font_dimens.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../wallet_root_controller.dart';

class WalletRootWalletView extends StatelessWidget {
  final WalletRootController controller = Get.find();

  WalletRootWalletView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(10, 9, 10, 9),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(
              Assets.walletPage.beiJin.path,
            ),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 16, 0, 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Obx(() {
                return Text(
                  "${controller.walletPageString("余额:")}${controller.wallet.value.balance ?? ""}",
                  style: TextStyle(
                    color: controller.currentCustomThemeData().colors0xFFFFFF,
                    fontSize: FontDimens.fontSp20,
                    fontWeight: FontWeight.w500,
                  ),
                );
              }),
              const SizedBox(
                height: 10,
              ),
              row(1),
              const SizedBox(
                height: 10,
              ),
              row(2),
            ],
          ),
        ),
      ),
    );
  }

  row(int index) {
    return Row(
      children: [
        Obx(() {
          return Text(
            title(index),
            style: TextStyle(
              color: controller.currentCustomThemeData().colors0xFFFFFF,
              fontSize: FontDimens.fontSp20,
              fontWeight: FontWeight.w500,
            ),
          );
        }),
        const SizedBox(
          width: 11,
        ),
        index == 2
            ? MaterialButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                padding: EdgeInsets.zero,
                minWidth: 0,
                onPressed: () {
                  log("去充值金币");
                  Get.toNamed(AppRoutes.walletRoot,
                      arguments: {"accountType": "2"});
                },
                child: Container(
                  width: 50,
                  height: 20,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: controller.currentCustomThemeData().colors0xFFFFFF,
                  ),
                  child: Center(
                    child: Text(
                      controller.walletPageString("充值"),
                      style: TextStyle(
                        color:
                            controller.currentCustomThemeData().colors0x7032FF,
                        fontSize: FontDimens.fontSp12,
                      ),
                    ),
                  ),
                ),
              )
            : Container(),
        Expanded(child: Container()),
        MaterialButton(
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          padding: EdgeInsets.zero,
          minWidth: 0,
          onPressed: () {
            if (index == 1) {
              log("兑换金币");
              Get.toNamed(AppRoutes.exchangeCoins, arguments: 1);
            } else {
              log("兑换砖石");
              Get.toNamed(AppRoutes.exchangeCoins, arguments: 0);
            }
          },
          child: Container(
            width: 79,
            height: 25,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  Assets.walletPage.duiHuan.path,
                ),
              ),
            ),
            child: Center(
              child: Text(
                btnTitle(index),
                style: TextStyle(
                  color: controller.currentCustomThemeData().colors0xFFFFFF,
                  fontSize: FontDimens.fontSp14,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  title(int index) {
    return (index == 1)
        ? "${controller.walletPageString("钻石:")}${controller.wallet.value.silverBean ?? ""}"
        : "${controller.walletPageString("金币:")}${controller.wallet.value.amount ?? ""}";
  }

  btnTitle(int index) {
    return (index == 1)
        ? controller.walletPageString("兑换钻石")
        : controller.walletPageString("兑换金币");
  }
}
