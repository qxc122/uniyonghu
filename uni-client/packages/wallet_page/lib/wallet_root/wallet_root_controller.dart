import 'dart:developer';

import 'package:base/bases/get_base_controller.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:get/get.dart';
import 'package:services/api/wallet_apis.dart';
import 'package:services/models/res/base_response.dart';
import 'package:services/models/res/wallet/on_line_pay_model.dart';
import 'package:services/models/res/wallet/user_wallet_model.dart';
import 'package:url_launcher/url_launcher.dart';

class WalletRootController extends GetXBaseController {
  Rx<UserWalletModel> wallet = UserWalletModel().obs;
  RxList ways = [].obs;
  var seleWays = 0.obs;
  var seleTongDao = 0.obs;
  var seleMoeny = 0.obs;

  ///0 1余额 2金币  默认余额
  var accountType = 0.obs;

  var activityType = 100.obs;

  @override
  void onInit() {
    Map<String, dynamic>? arguments = Get.arguments;
    if (arguments?.isNotEmpty ?? false) {
      int? type = arguments?["accountType"];
      log("${type ?? 0}");
      accountType.value = type ?? 0;

      int? activity = arguments?["activity"];
      log("${activity ?? 0}");
      activityType.value = activity ?? 0;
    }
    getUserWallet();
    getPayData();
    super.onInit();
  }

  getUserWallet() async {
    try {
      wallet.value = await WalletApis.of.call().getUserWallet();
      // OLUserManager.shared.save(res);
    } catch (e) {
      print(e);
    }
  }

  getPayData() async {
    try {
      ways.value = await WalletApis.of.call().getPayData();
      // OLUserManager.shared.save(res);
    } catch (e) {
      print(e);
    }
  }

  recharge() async {
    try {
      var account = accountType.value;
      if (account == 0) {
        account = 1;
      }
      int? activity;
      if (activityType.value != 100) {
        activity = activityType.value;
      }

      OLEasyLoading.showLoading(walletPageString("提交中..."));
      BaseResponse res = await WalletApis.of.call().recharge(
            ways[seleWays.value]
                    .payChannelVOList?[seleTongDao.value]
                    .channelCode ??
                "",
            ways[seleWays.value]
                    .payChannelVOList?[seleTongDao.value]
                    .payWayId ??
                0,
            ways[seleWays.value]
                    .payChannelVOList?[seleTongDao.value]
                    .moneys()[seleMoeny.value] ??
                "",
            null,
            account,
            activity,
          );
      OLEasyLoading.dismiss();
      if (res.code == 200) {
        OnLinePayModel data = OnLinePayModel.fromJson(res.data);
        var url = data.url ?? "";
        if (!url.contains("https://")) {
          url = "https://" + url;
        }
        log("外部网页");
        if (await canLaunchUrl(
          Uri.parse(url),
        )) {
          await launchUrl(
            Uri.parse(url),
          );
        } else {
          OLEasyLoading.showToast(basePageString("无法打开") + url);
        }
      } else {
        OLEasyLoading.showToast(res.msg ?? basePageString("请重试"));
      }
    } catch (e) {
      print(e);
      OLEasyLoading.dismiss();
      OLEasyLoading.showToast(basePageString("请重试"));
    }
  }
}
