import 'dart:developer';

import 'package:base/app_routes.dart';
import 'package:base/assets.gen.dart';
import 'package:base/res/font_dimens.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wallet_page/wallet_root/views/wallet_root_please_choose_view.dart';
import 'views/wallet_root_channel_view.dart';
import 'views/wallet_root_please_channel_view.dart';
import 'views/wallet_root_please_moeny_view.dart';
import 'wallet_root_controller.dart';
import 'views/wallet_root_wallet_view.dart';

class WalletRootPage extends GetView<WalletRootController> {
  WalletRootPage({Key? key}) : super(key: key);

  final TextEditingController zhanghao = TextEditingController();
  final TextEditingController mima = TextEditingController();

  @override
  Widget build(BuildContext context) {
    // Get.put(WalletRootController());
    return Scaffold(
      appBar: AppBar(
          leading: Obx(() {
            return (controller.accountType.value == 0
                ? Container()
                : IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: () {
                      Get.back();
                    },
                    icon: Image.asset(
                      Assets.basePage.backBlack.path,
                      width: 7,
                      height: 11,
                    ),
                  ));
          }),
          actions: [
            IconButton(
              padding: EdgeInsets.zero,
              onPressed: () {
                log("去客服中心");
                Get.toNamed(AppRoutes.onlineService);
              },
              icon: Image.asset(
                Assets.minePage.navKf.path,
                width: 26,
                height: 26,
              ),
            ),
          ],
          title: Obx(() {
            return Text(
              controller.accountType.value == 2
                  ? controller.walletPageString("充值金币")
                  : controller.walletPageString("充值余额"),
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: FontDimens.fontSp18,
                color: controller.currentCustomThemeData().colors0x000000,
              ),
            );
          })),
      body: ListView.builder(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
          itemCount: 9,
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return announcement();
            } else if (index == 1) {
              return WalletRootWalletView();
            } else if (index == 2) {
              return WalletRootChannel(
                balance: ["", "", "", "", "", "", "", ""],
              );
            } else if (index == 3) {
              return WalletRootPleaseChoose(
                  tip: controller.walletPageString("请选择支付通道"));
            } else if (index == 4) {
              return WalletRootPleaseChannel(
                balance: ["", "", "", "", "", ""],
              );
            } else if (index == 5) {
              return WalletRootPleaseChoose(
                  tip: controller.walletPageString("请选择充值金额"));
            } else if (index == 6) {
              return WalletRootPleaseMoney(
                balance: ["", "", "", "", "", ""],
              );
            } else if (index == 7) {
              return Padding(
                padding: const EdgeInsets.fromLTRB(10, 25, 10, 10),
                child: Obx(() {
                  return Text(
                    controller.ways.isEmpty
                        ? ""
                        : controller
                                .ways[controller.seleWays.value]
                                .payChannelVOList?[controller.seleTongDao.value]
                                .tips ??
                            "",
                    maxLines: 1000,
                    style: TextStyle(
                      color: controller.currentCustomThemeData().colors0xFF3D3D,
                      fontSize: FontDimens.fontSp12,
                    ),
                  );
                }),
              );
            } else {
              return MaterialButton(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                minWidth: 0,
                padding: EdgeInsets.zero,
                onPressed: () {
                  log("点击充值");
                  // Get.toNamed(AppRoutes.walletRoot);
                  controller.recharge();
                },
                child: Container(
                  height: 27,
                  width: 150,
                  decoration: BoxDecoration(
                    color: controller.currentCustomThemeData().colors0x9F44FF,
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                  ),
                  child: Center(
                    child: Text(
                      controller.walletPageString("立即充值"),
                      style: TextStyle(
                        color:
                            controller.currentCustomThemeData().colors0xFFFFFF,
                        fontSize: FontDimens.fontSp14,
                      ),
                    ),
                  ),
                ),
              );
            }
          }),
    );
  }

  ///公告
  announcement() {
    return Container(
      height: 32,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(3),
        image: DecorationImage(
          image: AssetImage(Assets.walletPage.gongGao.path),
        ),
      ),
    );
  }
}
