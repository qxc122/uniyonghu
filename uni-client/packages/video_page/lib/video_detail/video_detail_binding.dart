import 'package:get/get.dart';
import 'package:video_page/video_detail/video_detail_controller.dart';
class VideoDetailBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VideoDetailController>(()=> VideoDetailController());
  }
}
