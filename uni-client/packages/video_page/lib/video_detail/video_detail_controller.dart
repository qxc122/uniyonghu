// import 'package:device_info_plus/device_info_plus.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class VideoDetailController extends GetXBaseController {
  RxString titleString = "home".obs;

  RxInt currentType = 1.obs;
  RxBool playing = false.obs;


  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
  }

  setCurrentType(int index){
    currentType.value = index;
    update();
  }

  changePlay() {
    playing.value = !playing.value;
  }
}
