import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/commons/widgets/ol_video_view.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/components/card_item/card_item_page.dart';
import 'package:video_page/video_detail/video_detail_controller.dart';

class VideoDetailPage extends GetView<VideoDetailController> {
  const VideoDetailPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const videoKey = Key("videoPlayer");
    final videoView = OLVideoView(
      key: videoKey,
    );

    return Scaffold(
        backgroundColor: controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          toolbarHeight: 0,
        ),
        body: GetX<VideoDetailController>(
            init: controller,
            builder: (_) => Container(
                  alignment: Alignment.topLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                          height: width(420),
                          child: Stack(
                            children: [
                              InkWell(
                                onTap: () async {
                                  final box = videoView;
                                  await box.state?.pause();
                                  controller.changePlay();
                                },
                                child: SizedBox(
                                  child: videoView,
                                ),
                              ),
                              Positioned(
                                left: width(28),
                                width: width(15),
                                top: width(20),
                                child: InkWell(
                                  onTap: () {
                                    Get.back();
                                  },
                                  child: Image.asset(
                                      Assets.videoPage.vBackWhite.path,
                                      fit: BoxFit.cover),)
                                ,
                              ),
                              controller.playing.value ?Container():Positioned(
                                left: width(340),
                                width: width(70),
                                top: width(140),
                                child: Column(
                                  children: [
                                    InkWell(
                                      onTap: () async {
                                        final box = videoView;
                                        box.state?.setVideoUrl(
                                            "https://new.qqaku.com/20211017/3erFtzDL/index.m3u8");
                                        // await box.state?.reset();
                                        await box.state?.play();
                                        controller.changePlay();
                                      },
                                      child: Image.asset(
                                          Assets.videoPage.vPlayIcon.path,
                                          fit: BoxFit.cover),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          )
                        // Stack(
                        //   children: [
                        //     // Image.asset(Assets.videoPage.vCoverBg.path,
                        //     //     fit: BoxFit.cover),
                        //     Positioned(
                        //       left: width(28),
                        //       width: width(15),
                        //       top: width(20),
                        //       child: Image.asset(
                        //           Assets.videoPage.vBackWhite.path,
                        //           fit: BoxFit.cover),
                        //     )
                        //   ],
                        // )
                      ),
                      Container(
                        height: width(88),
                        decoration: BoxDecoration(
                            border: Border(
                                bottom: BorderSide(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0xD9D9D9,
                                    width: width(1)))),
                        child: Row(
                          children: [
                            Expanded(
                              child: InkWell(
                                  onTap: () {
                                    controller.setCurrentType(1);
                                  },
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        alignment: Alignment.center,
                                        child: Text(
                                          videoPageTranslations['details']
                                              .toString(),
                                          style: TextStyle(
                                              color: controller
                                                  .currentCustomThemeData()
                                                  .colors0x000000,
                                              fontSize: sp(28)),
                                        ),
                                      ),
                                      controller.currentType.value == 1
                                          ? Container(
                                              width: width(32),
                                              height: width(4),
                                              margin: EdgeInsets.only(
                                                  top: width(6)),
                                              decoration: const BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(4)),
                                                  gradient: LinearGradient(
                                                      begin:
                                                          Alignment.centerLeft,
                                                      end:
                                                          Alignment.centerRight,
                                                      colors: [
                                                        Color(0xff6129FF),
                                                        Color(0xffD96CFF)
                                                      ])),
                                            )
                                          : Container(
                                              width: width(32),
                                              height: width(4),
                                              margin: EdgeInsets.only(
                                                  top: width(6)),
                                            )
                                    ],
                                  )),
                            ),
                            Expanded(
                              child: InkWell(
                                  onTap: () {
                                    controller.setCurrentType(2);
                                  },
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            videoPageTranslations['comment']
                                                .toString(),
                                            style: TextStyle(
                                                color: controller
                                                    .currentCustomThemeData()
                                                    .colors0x979797,
                                                fontSize: sp(28)),
                                          ),
                                          Container(
                                            margin:
                                                EdgeInsets.only(top: width(4)),
                                            child: Text(
                                              "(112)",
                                              style: TextStyle(
                                                  color: controller
                                                      .currentCustomThemeData()
                                                      .colors0x979797,
                                                  fontSize: sp(20)),
                                            ),
                                          ),
                                        ],
                                      ),
                                      controller.currentType.value == 2
                                          ? Container(
                                              width: width(32),
                                              height: width(4),
                                              margin: EdgeInsets.only(
                                                  top: width(6)),
                                              decoration: const BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(4)),
                                                  gradient: LinearGradient(
                                                      begin:
                                                          Alignment.centerLeft,
                                                      end:
                                                          Alignment.centerRight,
                                                      colors: [
                                                        Color(0xff6129FF),
                                                        Color(0xffD96CFF)
                                                      ])),
                                            )
                                          : Container(
                                              width: width(32),
                                              height: width(4),
                                              margin: EdgeInsets.only(
                                                  top: width(6)),
                                            )
                                    ],
                                  )),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            /// 详情
                            controller.currentType.value == 1
                                ? Container(
                                    padding: EdgeInsets.symmetric(
                                            horizontal: width(15))
                                        .copyWith(top: width(20)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                            height: width(140),
                                            margin: EdgeInsets.only(
                                                bottom: width(16)),
                                            alignment: Alignment.center,
                                            child: Image.asset(
                                                Assets.videoPage.banner.path,
                                                fit: BoxFit.fitWidth)),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Expanded(
                                              child: Text(
                                                "名字可以很长很长很长很长很长很长很长很长很长很可以长到这里这里这里这里...",
                                                style: TextStyle(
                                                    color: controller
                                                        .currentCustomThemeData()
                                                        .colors0x000000,
                                                    fontSize: sp(28)),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          margin: EdgeInsets.symmetric(
                                              vertical: width(10)),
                                          child: Text(
                                            "7.6" +
                                                videoPageTranslations['branch']
                                                    .toString() +
                                                "·664" +
                                                videoPageTranslations['second']
                                                    .toString() +
                                                videoPageTranslations['play']
                                                    .toString() +
                                                " · 2022-12-28",
                                            style: TextStyle(
                                                color: controller
                                                    .currentCustomThemeData()
                                                    .colors0x979797,
                                                fontSize: sp(24)),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: width(10)),
                                              margin: EdgeInsets.only(
                                                  right: width(10)),
                                              height: width(40),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              width(8))),
                                                  color: controller
                                                      .currentCustomThemeData()
                                                      .colors0xF0F0F0),
                                              child: Text(
                                                "清纯大学生",
                                                style: TextStyle(
                                                    color: controller
                                                        .currentCustomThemeData()
                                                        .colors0x868686,
                                                    fontSize: sp(20),
                                                    height: 1.7),
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: width(10)),
                                              margin: EdgeInsets.only(
                                                  right: width(10)),
                                              height: width(40),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(
                                                              width(8))),
                                                  color: controller
                                                      .currentCustomThemeData()
                                                      .colors0xF0F0F0),
                                              child: Text(
                                                "精品资源",
                                                style: TextStyle(
                                                    color: controller
                                                        .currentCustomThemeData()
                                                        .colors0x868686,
                                                    fontSize: sp(20),
                                                    height: 1.7),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Container(
                                          padding: EdgeInsets.symmetric(
                                                  horizontal: width(60))
                                              .copyWith(
                                                  top: width(48),
                                                  bottom: width(26)),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              InkWell(
                                                onTap: () {
                                                  Get.toNamed("/myLike");
                                                },
                                                child: Row(
                                                  children: [
                                                    Container(
                                                        width: width(40),
                                                        height: width(30),
                                                        margin: EdgeInsets.only(
                                                            right: width(10)),
                                                        child: Image.asset(
                                                            Assets.videoPage
                                                                .vLove.path,
                                                            fit: BoxFit
                                                                .fitWidth)),
                                                    Text(
                                                      "190",
                                                      style: TextStyle(
                                                          color: controller
                                                              .currentCustomThemeData()
                                                              .colors0x979797,
                                                          fontSize: sp(24)),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  Get.toNamed("/offlineCache");
                                                },
                                                child: Row(
                                                  children: [
                                                    Container(
                                                        width: width(40),
                                                        height: width(30),
                                                        margin: EdgeInsets.only(
                                                            right: width(10)),
                                                        child: Image.asset(
                                                            Assets.videoPage
                                                                .vDownload.path,
                                                            fit: BoxFit
                                                                .fitWidth)),
                                                    Text(
                                                      videoPageTranslations[
                                                              'download']
                                                          .toString(),
                                                      style: TextStyle(
                                                          color: controller
                                                              .currentCustomThemeData()
                                                              .colors0x979797,
                                                          fontSize: sp(24)),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              InkWell(
                                                child: Row(
                                                  children: [
                                                    Container(
                                                        width: width(40),
                                                        height: width(30),
                                                        margin: EdgeInsets.only(
                                                            right: width(10)),
                                                        child: Image.asset(
                                                            Assets
                                                                .videoPage
                                                                .vShareLink
                                                                .path,
                                                            fit: BoxFit
                                                                .fitWidth)),
                                                    Text(
                                                      videoPageTranslations[
                                                              'share']
                                                          .toString(),
                                                      style: TextStyle(
                                                          color: controller
                                                              .currentCustomThemeData()
                                                              .colors0x979797,
                                                          fontSize: sp(24)),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                              InkWell(
                                                onTap: () {
                                                  Get.toNamed("/feedback");
                                                },
                                                child: Row(
                                                  children: [
                                                    Container(
                                                        width: width(40),
                                                        height: width(30),
                                                        margin: EdgeInsets.only(
                                                            right: width(10)),
                                                        child: Image.asset(
                                                            Assets.videoPage
                                                                .vOpinion.path,
                                                            fit: BoxFit
                                                                .fitWidth)),
                                                    Text(
                                                      videoPageTranslations[
                                                              'feedback']
                                                          .toString(),
                                                      style: TextStyle(
                                                          color: controller
                                                              .currentCustomThemeData()
                                                              .colors0x979797,
                                                          fontSize: sp(24)),
                                                    ),
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        // Row(
                                        //   mainAxisAlignment:
                                        //   MainAxisAlignment.spaceBetween,
                                        //   children: [
                                        //     Row(
                                        //       children: [
                                        //         InkWell(
                                        //           onTap: () {
                                        //             Get.toNamed("/mine");
                                        //           },
                                        //           child: Container(
                                        //               width: width(80),
                                        //               height: width(80),
                                        //               margin: EdgeInsets.only(
                                        //                   right: width(16)),
                                        //               decoration: BoxDecoration(
                                        //                   borderRadius:
                                        //                   BorderRadius.all(
                                        //                       Radius.circular(
                                        //                           width(80)))),
                                        //               child: Image.asset(
                                        //                   Assets.videoPage.vAva.path,
                                        //                   fit: BoxFit.cover)),
                                        //         ),
                                        //         Column(
                                        //           crossAxisAlignment:
                                        //           CrossAxisAlignment.start,
                                        //           children: [
                                        //             Row(
                                        //               children: [
                                        //                 Container(
                                        //                   margin: EdgeInsets.only(
                                        //                       right: width(6)),
                                        //                   child: Text(
                                        //                     "老色批",
                                        //                     style: TextStyle(
                                        //                         color: controller
                                        //                             .currentCustomThemeData()
                                        //                             .colors0x000000,
                                        //                         fontSize: sp(28)),
                                        //                   ),
                                        //                 ),
                                        //                 Container(
                                        //                   margin: EdgeInsets.only(
                                        //                       right: width(10)),
                                        //                   padding:
                                        //                   EdgeInsets.symmetric(
                                        //                       horizontal:
                                        //                       width(5)),
                                        //                   height: width(26),
                                        //                   decoration: BoxDecoration(
                                        //                       borderRadius:
                                        //                       BorderRadius.all(
                                        //                           Radius.circular(
                                        //                               width(4))),
                                        //                       color: controller
                                        //                           .currentCustomThemeData()
                                        //                           .colors0x7032FF),
                                        //                   child: Text(
                                        //                     "特约小伙伴",
                                        //                     style: TextStyle(
                                        //                         color: controller
                                        //                             .currentCustomThemeData()
                                        //                             .colors0xFFFFFF,
                                        //                         fontSize: sp(18)),
                                        //                   ),
                                        //                 ),
                                        //                 Container(
                                        //                   margin: EdgeInsets.only(
                                        //                       right: width(10)),
                                        //                   padding:
                                        //                   EdgeInsets.symmetric(
                                        //                       horizontal:
                                        //                       width(5)),
                                        //                   height: width(26),
                                        //                   decoration: BoxDecoration(
                                        //                       borderRadius:
                                        //                       BorderRadius.all(
                                        //                           Radius.circular(
                                        //                               width(4))),
                                        //                       color: controller
                                        //                           .currentCustomThemeData()
                                        //                           .colors0xFF32F7),
                                        //                   child: Text(
                                        //                     "老司机",
                                        //                     style: TextStyle(
                                        //                         color: controller
                                        //                             .currentCustomThemeData()
                                        //                             .colors0xFFFFFF,
                                        //                         fontSize: sp(18)),
                                        //                   ),
                                        //                 ),
                                        //               ],
                                        //             ),
                                        //             Text(
                                        //               "6826" +
                                        //                   videoPageTranslations[
                                        //                   'fans']
                                        //                       .toString() +
                                        //                   " · 763" +
                                        //                   videoPageTranslations[
                                        //                   'part']
                                        //                       .toString() +
                                        //                   videoPageTranslations[
                                        //                   'video']
                                        //                       .toString(),
                                        //               style: TextStyle(
                                        //                   color: controller
                                        //                       .currentCustomThemeData()
                                        //                       .colors0x979797,
                                        //                   fontSize: sp(20)),
                                        //             ),
                                        //           ],
                                        //         )
                                        //       ],
                                        //     ),
                                        //     InkWell(
                                        //       onTap: () {
                                        //         Get.toNamed("/follow");
                                        //       },
                                        //       child: Container(
                                        //         width: width(100),
                                        //         height: width(40),
                                        //         alignment: Alignment.center,
                                        //         decoration: BoxDecoration(
                                        //             border: Border.all(
                                        //                 color: controller
                                        //                     .currentCustomThemeData()
                                        //                     .colors0x7032FF,
                                        //                 width: width(1)),
                                        //             borderRadius: BorderRadius.all(
                                        //                 Radius.circular(width(40)))),
                                        //         child: Text(
                                        //           "+" +
                                        //               videoPageTranslations['follow']
                                        //                   .toString(),
                                        //           style: TextStyle(
                                        //               color: controller
                                        //                   .currentCustomThemeData()
                                        //                   .colors0x7032FF,
                                        //               fontSize: sp(20)),
                                        //         ),
                                        //       ),
                                        //     )
                                        //   ],
                                        // ),
                                        Container(
                                          alignment: Alignment.center,
                                          height: width(2),
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0xE7E7E7,
                                        ),
                                        Container(
                                          margin:
                                              EdgeInsets.only(top: width(26)),
                                          child: Text(
                                            videoPageTranslations['guess_you']
                                                    .toString() +
                                                videoPageTranslations['like']
                                                    .toString(),
                                            style: TextStyle(
                                                color: controller
                                                    .currentCustomThemeData()
                                                    .colors0x262626,
                                                fontWeight: FontWeight.bold,
                                                fontSize: sp(30)),
                                          ),
                                        ),
                                        InkWell(
                                          child: Container(
                                            margin:
                                                EdgeInsets.only(top: width(30)),
                                            child: Row(
                                              children: [
                                                const CardView(
                                                  flag: false,
                                                ),
                                                const SizedBox(
                                                  width: 7.5,
                                                ),
                                                Expanded(
                                                  child: Container(
                                                    alignment:
                                                        Alignment.centerLeft,
                                                    height: width(192),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: [
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "萝莉姐妹花飞骑上外送员",
                                                              style: TextStyle(
                                                                  color: controller
                                                                      .currentCustomThemeData()
                                                                      .colors0x000000,
                                                                  fontSize:
                                                                      sp(28)),
                                                            ),
                                                            const SizedBox(
                                                              height: 8,
                                                            ),
                                                            Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                              children: [
                                                                Row(
                                                                  children: [
                                                                    SizedBox(
                                                                        width: width(
                                                                            24),
                                                                        height: width(
                                                                            24),
                                                                        child: Image.asset(
                                                                            Assets
                                                                                .videoPage.vMoneyIcon.path,
                                                                            fit:
                                                                                BoxFit.cover)),
                                                                    Container(
                                                                      margin: EdgeInsets.only(
                                                                          left:
                                                                              width(10)),
                                                                      child:
                                                                          Text(
                                                                        "5.00",
                                                                        style: TextStyle(
                                                                            color:
                                                                                controller.currentCustomThemeData().colors0xFFAA46,
                                                                            fontSize: sp(28)),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ],
                                                            ),
                                                            const SizedBox(
                                                              height: 5,
                                                            ),
                                                            Row(
                                                              children: [
                                                                Container(
                                                                  padding: EdgeInsets.symmetric(
                                                                      horizontal:
                                                                          width(
                                                                              10)),
                                                                  margin: EdgeInsets.only(
                                                                      right: width(
                                                                          10)),
                                                                  height:
                                                                      width(40),
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.all(Radius.circular(width(
                                                                              8))),
                                                                      color: controller
                                                                          .currentCustomThemeData()
                                                                          .colors0xF0F0F0),
                                                                  child: Text(
                                                                    "清纯大学生",
                                                                    style: TextStyle(
                                                                        color: controller
                                                                            .currentCustomThemeData()
                                                                            .colors0x868686,
                                                                        fontSize:
                                                                            sp(20)),
                                                                  ),
                                                                ),
                                                                Container(
                                                                  padding: EdgeInsets.symmetric(
                                                                      horizontal:
                                                                          width(
                                                                              10)),
                                                                  height:
                                                                      width(40),
                                                                  alignment:
                                                                      Alignment
                                                                          .center,
                                                                  decoration: BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.all(Radius.circular(width(
                                                                              8))),
                                                                      color: controller
                                                                          .currentCustomThemeData()
                                                                          .colors0xF0F0F0),
                                                                  child: Text(
                                                                    "精品资源",
                                                                    style: TextStyle(
                                                                        color: controller
                                                                            .currentCustomThemeData()
                                                                            .colors0x868686,
                                                                        fontSize:
                                                                            sp(20)),
                                                                  ),
                                                                ),
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                        Text(
                                                          "19.8万次播放",
                                                          style: TextStyle(
                                                              color: controller
                                                                  .currentCustomThemeData()
                                                                  .colors0x979797,
                                                              fontSize: sp(20)),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )

                                /// 评论
                                : Expanded(
                                    child: Stack(
                                      children: [
                                        Column(
                                          children: [
                                            Container(
                                              height: width(60),
                                              alignment: Alignment.centerLeft,
                                              padding: EdgeInsets.only(
                                                  left: width(20)),
                                              color: controller
                                                  .currentCustomThemeData()
                                                  .colors0xF5ECFF,
                                              child: Row(
                                                children: [
                                                  Image.asset(
                                                    Assets.videoPage
                                                        .vDetailNotice.path,
                                                    width: width(30),
                                                    height: width(28),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: width(12)),
                                                    child: Text(
                                                      "我是公告我是公告我是公告我是公告我是公告",
                                                      style: TextStyle(
                                                          color: controller
                                                              .currentCustomThemeData()
                                                              .colors0x9F44FF,
                                                          fontSize: sp(24)),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: width(20)),
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Container(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical:
                                                                width(20)),
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Container(
                                                          margin:
                                                              EdgeInsets.only(
                                                                  right: width(
                                                                      20)),
                                                          child: Image.asset(
                                                            Assets.videoPage
                                                                .vAva.path,
                                                            width: width(80),
                                                            height: width(80),
                                                          ),
                                                        ),
                                                        Expanded(
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Row(
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceBetween,
                                                                children: [
                                                                  Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        "老色批",
                                                                        style: TextStyle(
                                                                            color: controller
                                                                                .currentCustomThemeData()
                                                                                .colors0x979797,
                                                                            height:
                                                                                1.0,
                                                                            fontSize:
                                                                                sp(24)),
                                                                      ),
                                                                      Container(
                                                                        margin: EdgeInsets.only(
                                                                            top:
                                                                                width(16)),
                                                                        child:
                                                                            Text(
                                                                          "老色批老色批老色批老色批",
                                                                          style: TextStyle(
                                                                              color: controller.currentCustomThemeData().colors0x000000,
                                                                              height: 1.0,
                                                                              fontSize: sp(28)),
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      InkWell(
                                                                        child:
                                                                            Container(
                                                                          margin:
                                                                              EdgeInsets.only(left: width(20)),
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              Image.asset(
                                                                                Assets.videoPage.vPush.path,
                                                                                width: width(34),
                                                                                height: width(30),
                                                                              ),
                                                                              Container(
                                                                                margin: EdgeInsets.only(top: width(8)),
                                                                                child: Text(
                                                                                  videoPageTranslations['report'].toString(),
                                                                                  style: TextStyle(color: controller.currentCustomThemeData().colors0x979797, height: 1.0, fontSize: sp(20)),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                      InkWell(
                                                                        child:
                                                                            Container(
                                                                          margin:
                                                                              EdgeInsets.only(left: width(20)),
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              Image.asset(
                                                                                Assets.videoPage.vUnFollow.path,
                                                                                width: width(40),
                                                                                height: width(30),
                                                                              ),
                                                                              Container(
                                                                                margin: EdgeInsets.only(top: width(8)),
                                                                                child: Text(
                                                                                  videoPageTranslations['give_the_thumbs-up'].toString(),
                                                                                  style: TextStyle(color: controller.currentCustomThemeData().colors0x979797, height: 1.0, fontSize: sp(20)),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      )
                                                                    ],
                                                                  )
                                                                ],
                                                              ),
                                                              SizedBox(
                                                                height:
                                                                    width(16),
                                                              ),
                                                              Row(
                                                                children: [
                                                                  Container(
                                                                    margin: EdgeInsets.only(
                                                                        right: width(
                                                                            24)),
                                                                    child: Text(
                                                                      "30分钟前",
                                                                      style: TextStyle(
                                                                          color: controller
                                                                              .currentCustomThemeData()
                                                                              .colors0x979797,
                                                                          height:
                                                                              1.0,
                                                                          fontSize:
                                                                              sp(20)),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    margin: EdgeInsets.only(
                                                                        right: width(
                                                                            24)),
                                                                    child: Text(
                                                                      videoPageTranslations[
                                                                              'reply']
                                                                          .toString(),
                                                                      style: TextStyle(
                                                                          color: controller
                                                                              .currentCustomThemeData()
                                                                              .colors0x555555,
                                                                          fontSize:
                                                                              sp(20)),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    margin: EdgeInsets.only(
                                                                        right: width(
                                                                            24)),
                                                                    child: Text(
                                                                      videoPageTranslations[
                                                                              'delete']
                                                                          .toString(),
                                                                      style: TextStyle(
                                                                          color: controller
                                                                              .currentCustomThemeData()
                                                                              .colors0x555555,
                                                                          fontSize:
                                                                              sp(20)),
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                              SizedBox(
                                                                height:
                                                                    width(22),
                                                              ),
                                                              Row(
                                                                children: [
                                                                  Container(
                                                                    width:
                                                                        width(
                                                                            70),
                                                                    height:
                                                                        width(
                                                                            2),
                                                                    margin: EdgeInsets.only(
                                                                        right: width(
                                                                            8)),
                                                                    color: controller
                                                                        .currentCustomThemeData()
                                                                        .colors0x979797,
                                                                  ),
                                                                  Container(
                                                                    margin: EdgeInsets.only(
                                                                        right: width(
                                                                            8)),
                                                                    child: Text(
                                                                      videoPageTranslations['develop'].toString() +
                                                                          "6" +
                                                                          videoPageTranslations['twig']
                                                                              .toString() +
                                                                          videoPageTranslations['reply']
                                                                              .toString(),
                                                                      style: TextStyle(
                                                                          color: controller
                                                                              .currentCustomThemeData()
                                                                              .colors0x979797,
                                                                          fontSize:
                                                                              sp(24)),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                      width:
                                                                          width(
                                                                              12),
                                                                      height:
                                                                          width(
                                                                              10),
                                                                      margin: EdgeInsets.only(
                                                                          right: width(
                                                                              8)),
                                                                      child: Image.asset(
                                                                          Assets
                                                                              .videoPage
                                                                              .vDownIcon
                                                                              .path,
                                                                          fit: BoxFit
                                                                              .cover)),
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        Positioned(
                                          left: 0,
                                          bottom: 0,
                                          height: width(130),
                                          width: width(750),
                                          child: Container(
                                            alignment: Alignment.centerLeft,
                                            padding: EdgeInsets.symmetric(
                                                horizontal: width(28),
                                                vertical: width(30)),
                                            decoration: BoxDecoration(
                                                color: controller
                                                    .currentCustomThemeData()
                                                    .colors0xFFFFFF,
                                                border: Border(
                                                    top: BorderSide(
                                                  width: width(2),
                                                  color: controller
                                                      .currentCustomThemeData()
                                                      .colors0xF4F4F4,
                                                ))),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  child: Container(
                                                    height: width(70),
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            horizontal:
                                                                width(20)),
                                                    decoration: BoxDecoration(
                                                        color: controller
                                                            .currentCustomThemeData()
                                                            .colors0xF4F4F4,
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    width(
                                                                        40)))),
                                                    child: Row(
                                                      children: [
                                                        Container(
                                                            width: width(28),
                                                            height: width(28),
                                                            margin:
                                                                EdgeInsets.only(
                                                                    right: width(
                                                                        20)),
                                                            child: Image.asset(
                                                                Assets
                                                                    .videoPage
                                                                    .vWrite
                                                                    .path,
                                                                fit: BoxFit
                                                                    .cover)),
                                                        Text(
                                                          videoPageTranslations[
                                                                  'what_do_you_want_to_say']
                                                              .toString(),
                                                          style: TextStyle(
                                                              color: controller
                                                                  .currentCustomThemeData()
                                                                  .colors0xBCBCBC,
                                                              height: 1.0,
                                                              fontSize: sp(20)),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                                InkWell(
                                                  child: Container(
                                                    width: width(120),
                                                    height: width(70),
                                                    margin: EdgeInsets.only(
                                                        left: width(12)),
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(
                                                        color: controller
                                                            .currentCustomThemeData()
                                                            .colors0x9F44FF,
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    width(
                                                                        40)))),
                                                    child: Text(
                                                      videoPageTranslations[
                                                              'send']
                                                          .toString(),
                                                      style: TextStyle(
                                                          color: controller
                                                              .currentCustomThemeData()
                                                              .colors0xFFFFFF,
                                                          fontSize: sp(32)),
                                                    ),
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                          ],
                        ),
                      )
                    ],
                  ),
                )));
  }
}
