import 'package:get/get.dart';
import 'package:video_page/mine/mine_controller.dart';
class MineBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MineController>(()=> MineController());
  }
}
