import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/mine/mine_controller.dart';

class MinePage extends GetView<MineController> {
  const MinePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          backgroundColor: Colors.transparent,
          leading: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Get.back();
            },
            icon: Image.asset(
              Assets.basePage.backBlack.path,
              width: 7,
              height: 11,
            ),
          ),
          title: Text(videoPageTranslations['personal_center'].toString(),
              style: TextStyle(
                  color: controller.currentCustomThemeData().colors0x000000,
                  fontSize: sp(32),
                  fontWeight: FontWeight.w600)),
          actions: [
            InkWell(
                child: Container(
              margin: EdgeInsets.only(right: width(20)),
              child: Image.asset(
                Assets.videoPage.vNotice.path,
                width: width(40),
                height: width(40),
              ),
            )),
            InkWell(
                child: Container(
              margin: EdgeInsets.only(right: width(20)),
              child: Image.asset(
                Assets.videoPage.vSetting.path,
                width: width(40),
                height: width(40),
              ),
            ))
          ],
        ),
        body: Column(
          children: [
            Container(
              alignment: Alignment.center,
              height: width(550),
              padding: EdgeInsets.only(
                  left: width(40), right: width(20), top: width(204)),
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(Assets.videoPage.vMineBg.path),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Stack(
                            // overflow: Overflow.visible,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(width(65)))),
                                child: Image.asset(
                                  Assets.videoPage.vAva.path,
                                  width: width(130),
                                  height: width(130),
                                ),
                              ),
                              Positioned(
                                left: width(5),
                                bottom: -width(6),
                                child: Container(
                                  width: width(120),
                                  height: width(34),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(width(17))),
                                      color: controller
                                          .currentCustomThemeData()
                                          .colors0x9F44FF),
                                  alignment: Alignment.center,
                                  child: Text(
                                      videoPageTranslations['edit_data']
                                          .toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0xFFFFFF,
                                          fontSize: sp(24))),
                                ),
                              )
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(left: width(20)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("Latle今天喝拿铁",
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x000000,
                                        fontSize: sp(32))),
                                Container(
                                  margin: EdgeInsets.only(top: width(10)),
                                  child: Text("ID:1234567890",
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0x626262,
                                          fontSize: sp(24))),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Text(
                              videoPageTranslations['switch_live_mode']
                                  .toString(),
                              style: TextStyle(
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0x000000,
                                  fontSize: sp(28))),
                          Container(
                            margin: EdgeInsets.only(left: width(10)),
                            child: Image.asset(
                              Assets.videoPage.vMineRp.path,
                              width: width(20),
                              height: width(16),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(top: width(54)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text("0",
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x000000,
                                        fontSize: sp(32))),
                                Container(
                                  margin: EdgeInsets.only(top: width(12)),
                                  child: Text(
                                      videoPageTranslations['viewing_ticket']
                                          .toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0xB9B5C2,
                                          fontSize: sp(28))),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text("0/1",
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x000000,
                                        fontSize: sp(32))),
                                Container(
                                  margin: EdgeInsets.only(top: width(12)),
                                  child: Text(
                                      videoPageTranslations['number_of_caches']
                                          .toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0xB9B5C2,
                                          fontSize: sp(28))),
                                )
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text("0/1",
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x000000,
                                        fontSize: sp(32))),
                                Container(
                                  margin: EdgeInsets.only(top: width(12)),
                                  child: Text(
                                      videoPageTranslations['free_viewing']
                                          .toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0xB9B5C2,
                                          fontSize: sp(28))),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              transform: Matrix4.translationValues(0.0, -30.0, 0.0),
              decoration: BoxDecoration(
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage(Assets.videoPage.vMineCenter.path),
                ),
              ),
              padding: EdgeInsets.symmetric(horizontal: width(100)),
              height: width(266),
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  InkWell(
                    onTap: () {
                      Get.toNamed("/myLike");
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          Assets.videoPage.vMyLike.path,
                          width: width(120),
                          height: width(120),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: width(20)),
                          child: Text(videoPageTranslations['like'].toString(),
                              style: TextStyle(
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0x000000,
                                  fontSize: sp(24))),
                        )
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed("/offlineCache");
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          Assets.videoPage.vCache.path,
                          width: width(120),
                          height: width(120),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: width(20)),
                          child: Text(
                              videoPageTranslations['my'].toString() +
                                  videoPageTranslations['cache'].toString(),
                              style: TextStyle(
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0x000000,
                                  fontSize: sp(24))),
                        )
                      ],
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Get.toNamed("/record");
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          Assets.videoPage.vHistory.path,
                          width: width(120),
                          height: width(120),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: width(20)),
                          child: Text(
                              videoPageTranslations['watch_the_record']
                                  .toString(),
                              style: TextStyle(
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0x000000,
                                  fontSize: sp(24))),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              height: width(2),
              color: controller.currentCustomThemeData().colors0xF3F3F3,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.symmetric(horizontal: width(40)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Get.toNamed(
                            "/personalizedChannel",
                          );
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: width(20)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vStar.path,
                                      width: width(52),
                                      height: width(52),
                                    ),
                                  ),
                                  Text(
                                    videoPageTranslations['custom_channels']
                                        .toString(),
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x000000,
                                        fontSize: sp(24)),
                                  )
                                ],
                              ),
                              Image.asset(
                                Assets.videoPage.vRpH.path,
                                width: width(40),
                                height: width(16),
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: width(20)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vMineChat.path,
                                      width: width(52),
                                      height: width(52),
                                    ),
                                  ),
                                  Text(
                                    videoPageTranslations[
                                            'fans_communication_group']
                                        .toString(),
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x000000,
                                        fontSize: sp(24)),
                                  )
                                ],
                              ),
                              Image.asset(
                                Assets.videoPage.vRpH.path,
                                width: width(40),
                                height: width(16),
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: width(20)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vService.path,
                                      width: width(52),
                                      height: width(52),
                                    ),
                                  ),
                                  Text(
                                    videoPageTranslations['online_service']
                                        .toString(),
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x000000,
                                        fontSize: sp(24)),
                                  )
                                ],
                              ),
                              Image.asset(
                                Assets.videoPage.vRpH.path,
                                width: width(40),
                                height: width(16),
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          Get.toNamed("/feedback");
                        },
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: width(20)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vOpinionIcon.path,
                                      width: width(52),
                                      height: width(52),
                                    ),
                                  ),
                                  Text(
                                    videoPageTranslations[
                                            'suggestions_and_feedback']
                                        .toString(),
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x000000,
                                        fontSize: sp(24)),
                                  )
                                ],
                              ),
                              Image.asset(
                                Assets.videoPage.vRpH.path,
                                width: width(40),
                                height: width(16),
                              ),
                            ],
                          ),
                        ),
                      ),
                      InkWell(
                        child: Container(
                          padding: EdgeInsets.symmetric(vertical: width(20)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vMineSetting.path,
                                      width: width(52),
                                      height: width(52),
                                    ),
                                  ),
                                  Text(
                                    videoPageTranslations['set_up'].toString(),
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x000000,
                                        fontSize: sp(24)),
                                  )
                                ],
                              ),
                              Image.asset(
                                Assets.videoPage.vRpH.path,
                                width: width(40),
                                height: width(16),
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            )
          ],
        ));
  }
}
