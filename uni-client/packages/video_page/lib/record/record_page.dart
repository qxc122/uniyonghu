
import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/components/card_item/card_item_page.dart';
import 'package:video_page/record/record_controller.dart';

class RecordPage extends GetView<RecordController> {
  const RecordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          leading: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Get.back();
            },
            icon: Image.asset(
              Assets.basePage.backBlack.path,
              width: 7,
              height: 11,
            ),
          ),
          title: Text(videoPageTranslations['watch_the_record'].toString(),
              style: TextStyle(
                  color: controller.currentCustomThemeData().colors0x000000,
                  fontSize: sp(32),
                  fontWeight: FontWeight.w600)),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: width(28)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Wrap(
                  spacing: width(12),
                  runSpacing: width(18),
                  children: [
                    SizedBox(
                      width: width(340),
                      child: Column(
                        children: [
                          const CardView(flag: false,),
                          Container(
                            margin: EdgeInsets.only(top: width(10),bottom: width(48)),
                            child: Text(
                              "名字可以很长很长很长很长很长很长很长很长",
                              style: TextStyle(
                                  color: controller.currentCustomThemeData().colors0x000000, fontSize: sp(24)),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: width(340),
                      child: Column(
                        children: [
                          const CardView(flag: false,),
                          Container(
                            margin: EdgeInsets.only(top: width(10),bottom: width(48)),
                            child: Text(
                              "名字可以很长很长很长很长很长很长很长很长",
                              style: TextStyle(
                                  color: controller.currentCustomThemeData().colors0x000000, fontSize: sp(24)),
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: width(340),
                      child: Column(
                        children: [
                          const CardView(flag: false,),
                          Container(
                            margin: EdgeInsets.only(top: width(10),bottom: width(48)),
                            child: Text(
                              "名字可以很长很长很长很长很长很长很长很长",
                              style: TextStyle(
                                  color: controller.currentCustomThemeData().colors0x000000, fontSize: sp(24)),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
