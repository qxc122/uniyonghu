import 'package:get/get.dart';
import 'package:video_page/search_result/search_result_controller.dart';
class SearchResultBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SearchResultPageController>(()=> SearchResultPageController());
  }
}
