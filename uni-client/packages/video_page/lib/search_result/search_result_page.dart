import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/components/card_item/card_item_page.dart';
import 'package:video_page/search_result/search_result_controller.dart';

class SearchResultPage extends GetView<SearchResultPageController> {
  const SearchResultPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          // backgroundColor: Color(0xffF5F5F5),
          leading: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Get.back();
            },
            icon: Image.asset(
              Assets.basePage.backBlack.path,
              width: 7,
              height: 11,
            ),
          ),
          title: Container(
            height: width(60),
            padding: EdgeInsets.symmetric(horizontal: width(16)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(width(40))),
                color: controller.currentCustomThemeData().colors0xF8F7F7,
                border: Border.all(
                    color: controller.currentCustomThemeData().colors0xCFCFCF,
                    width: width(1))),
            child: Row(
              children: [
                Container(
                    margin: EdgeInsets.only(right: width(20)),
                    width: width(26),
                    height: width(24),
                    child: Image.asset(Assets.videoPage.vSeachIcon.path)),
                Text(
                  videoPageTranslations['search_what_you_want_to_see'].toString(),
                  style: TextStyle(
                      color: controller.currentCustomThemeData().colors0x979797,
                      fontSize: sp(24)),
                ),
              ],
            ),
          ),
          actions: [
            InkWell(
              onTap: () {
                Get.toNamed("/searchResultPage");
              },
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(right: width(30), left: width(30)),
                child: Text(
                  videoPageTranslations['search'].toString(),
                  style: TextStyle(
                      color: controller.currentCustomThemeData().colors0x979797,
                      fontSize: sp(32)),
                ),
              ),
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: width(30)),
                margin: EdgeInsets.only(top: width(14)),
                child: Row(
                  children: [
                    Container(
                        width: width(24),
                        height: width(20),
                        margin: EdgeInsets.only(right: width(8)),
                        child: Image.asset(Assets.videoPage.vDownIcon.path,
                            fit: BoxFit.cover)),
                    Text(
                      videoPageTranslations['screen'].toString(),
                      style: TextStyle(
                          color: controller
                              .currentCustomThemeData()
                              .colors0x979797,
                          fontSize: sp(24)),
                    ),
                  ],
                ),
              ),
              InkWell(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: width(20)),
                  margin: EdgeInsets.only(top: width(30)),
                  child: Row(
                    children: [
                      const CardView(
                        flag: false,
                      ),
                      const SizedBox(
                        width: 7.5,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "萝莉姐妹花飞骑上外送员",
                              style: TextStyle(
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0x000000,
                                  fontSize: sp(28)),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                        width: width(24),
                                        height: width(24),
                                        child: Image.asset(
                                            Assets.videoPage.vMoneyIcon.path,
                                            fit: BoxFit.cover)),
                                    Container(
                                      margin: EdgeInsets.only(left: width(10)),
                                      child: Text(
                                        "5.00",
                                        style: TextStyle(
                                            color: controller
                                                .currentCustomThemeData()
                                                .colors0xFFAA46,
                                            fontSize: sp(28)),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(right: width(6)),
                                      child: Text(
                                        "初级流氓",
                                        style: TextStyle(
                                            color: controller
                                                .currentCustomThemeData()
                                                .colors0xD7D7D7,
                                            fontSize: sp(28)),
                                      ),
                                    ),
                                    Container(
                                      width: width(64),
                                      height: width(26),
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(width(4))),
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0xFF323E),
                                      child: Text(
                                        "萝莉控",
                                        style: TextStyle(
                                            color: controller
                                                .currentCustomThemeData()
                                                .colors0xFFFFFF,
                                            fontSize: sp(20),
                                            height: 1.1),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width(10)),
                                  margin: EdgeInsets.only(right: width(10)),
                                  height: width(40),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(width(8))),
                                      color: controller
                                          .currentCustomThemeData()
                                          .colors0xF0F0F0),
                                  child: Text(
                                    "清纯大学生",
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x868686,
                                        fontSize: sp(20)),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width(10)),
                                  height: width(40),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(width(8))),
                                      color: controller
                                          .currentCustomThemeData()
                                          .colors0xF0F0F0),
                                  child: Text(
                                    "精品资源",
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x868686,
                                        fontSize: sp(20)),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
