// import 'package:device_info_plus/device_info_plus.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class PersonalizedChannelController extends GetXBaseController {
  RxString titleString = "home".obs;

  List<String> tabs = ['AV 中字', '欧美', '狼友上传', '性爱知识', '性爱知识', '性爱知识'];
  RxMap tabMap = {0:'AV 中字',27: "欧美", 32: "狼友上传", 14: "性爱知识", 34:"性爱知识", 26:"性爱知识"}.obs;
  RxInt currentIndex = 0.obs;


  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
  }


  changeIndex(int index){
    currentIndex.value = index;
    update();
  }
}
