
import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/personalized_channel/personalized_channel_controller.dart';

class PersonalizedChannelPage extends GetView<PersonalizedChannelController> {
  const PersonalizedChannelPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          leading: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Get.back();
            },
            icon: Image.asset(
              Assets.basePage.backBlack.path,
              width: 7,
              height: 11,
            ),
          ),
          title: Text(videoPageTranslations['personalized_channel'].toString(),
              style: TextStyle(
                  color: controller.currentCustomThemeData().colors0x000000,
                  fontSize: sp(32),
                  fontWeight: FontWeight.w600)),
          actions: [
            InkWell(
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(right: width(30)),
                child: Text(videoPageTranslations['reset'].toString(),
                    style: TextStyle(
                        color: controller.currentCustomThemeData().colors0x000000,
                        fontSize: sp(28),
                        fontWeight: FontWeight.w400)),
              ),
            )
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /// 当前选择的
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(20)).copyWith(top: width(10)),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(videoPageTranslations['current_use'].toString(),
                          style: TextStyle(
                              color: controller.currentCustomThemeData().colors0xACACAC,
                              fontSize: sp(26),
                              fontWeight: FontWeight.w400)),
                      Text('(9/5)',
                          style: TextStyle(
                              color: controller.currentCustomThemeData().colors0xACACAC,
                              fontSize: sp(26),
                              fontWeight: FontWeight.w400)),
                    ],
                  ),
                  SizedBox(height: width(16),),
                  Wrap(
                    spacing: width(10),
                    runSpacing: width(18),
                    children: [
                      Container(
                        width: width(230),
                        height: width(82),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius:BorderRadius.all(Radius.circular(width(8))),
                            border: Border.all(color: controller.currentCustomThemeData().colors0xE8E8E8,width: width(1))
                        ),
                        child: Text('关注',
                            style: TextStyle(
                                color: controller.currentCustomThemeData().colors0x000000,
                                fontSize: sp(24),
                                fontWeight: FontWeight.w400)),
                      ),
                      Container(
                          width: width(230),
                          height: width(82),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius:BorderRadius.all(Radius.circular(width(8))),
                              border: Border.all(color: controller.currentCustomThemeData().colors0xE8E8E8,width: width(1))
                          ),
                          child: Stack(
                            children: [
                              Container(
                                width: width(230),
                                height: width(82),
                                alignment: Alignment.center,
                                child: Text('关注',
                                    style: TextStyle(
                                        color: controller.currentCustomThemeData().colors0x000000,
                                        fontSize: sp(24),
                                        fontWeight: FontWeight.w400)),
                              ),
                              Positioned(
                                right: width(18),
                                top: width(16),
                                child: InkWell(
                                  child: SizedBox(
                                      width: width(16),
                                      child: Image.asset(
                                          Assets.videoPage.vReduce.path,
                                          fit: BoxFit.fitWidth)),
                                ),
                              )
                            ],
                          )
                      ),
                      Container(
                          width: width(230),
                          height: width(82),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius:BorderRadius.all(Radius.circular(width(8))),
                              border: Border.all(color: controller.currentCustomThemeData().colors0xE8E8E8,width: width(1))
                          ),
                          child: Stack(
                            children: [
                              Container(
                                width: width(230),
                                height: width(82),
                                alignment: Alignment.center,
                                child: Text('关注',
                                    style: TextStyle(
                                        color: controller.currentCustomThemeData().colors0x000000,
                                        fontSize: sp(24),
                                        fontWeight: FontWeight.w400)),
                              ),
                              Positioned(
                                right: width(18),
                                top: width(16),
                                child: InkWell(
                                  child: SizedBox(
                                      width: width(16),
                                      child: Image.asset(
                                          Assets.videoPage.vReduce.path,
                                          fit: BoxFit.fitWidth)),
                                ),
                              )
                            ],
                          )
                      ),
                      Container(
                          width: width(230),
                          height: width(82),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius:BorderRadius.all(Radius.circular(width(8))),
                              border: Border.all(color: controller.currentCustomThemeData().colors0xE8E8E8,width: width(1))
                          ),
                          child: Stack(
                            children: [
                              Container(
                                width: width(230),
                                height: width(82),
                                alignment: Alignment.center,
                                child: Text('关注',
                                    style: TextStyle(
                                        color: controller.currentCustomThemeData().colors0x000000,
                                        fontSize: sp(24),
                                        fontWeight: FontWeight.w400)),
                              ),
                              Positioned(
                                right: width(18),
                                top: width(16),
                                child: InkWell(
                                  child: SizedBox(
                                      width: width(16),
                                      child: Image.asset(
                                          Assets.videoPage.vReduce.path,
                                          fit: BoxFit.fitWidth)),
                                ),
                              )
                            ],
                          )
                      ),
                      Container(
                          width: width(230),
                          height: width(82),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius:BorderRadius.all(Radius.circular(width(8))),
                              border: Border.all(color: controller.currentCustomThemeData().colors0xE8E8E8,width: width(1))
                          ),
                          child: Stack(
                            children: [
                              Container(
                                width: width(230),
                                height: width(82),
                                alignment: Alignment.center,
                                child: Text('关注',
                                    style: TextStyle(
                                        color: controller.currentCustomThemeData().colors0x000000,
                                        fontSize: sp(24),
                                        fontWeight: FontWeight.w400)),
                              ),
                              Positioned(
                                right: width(18),
                                top: width(16),
                                child: InkWell(
                                  child: SizedBox(
                                      width: width(16),
                                      child: Image.asset(
                                          Assets.videoPage.vReduce.path,
                                          fit: BoxFit.fitWidth)),
                                ),
                              )
                            ],
                          )
                      ),
                    ],
                  )
                ],
              ),
            ),
            /// 所有的
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /// 导航
                Container(
                    padding:EdgeInsets.only(left: width(20),top: width(60)),
                    child: Row(
                      children: [
                        ...controller.tabMap.entries.map((e){
                          return Expanded(child: InkWell(
                            onTap: () {
                              controller.changeIndex(e.key);
                            },
                            child: Column(
                              children: [
                                Container(
                                  margin:EdgeInsets.only(bottom: width(20)),
                                  child: Text(
                                    "${e.value??''}",
                                    style: TextStyle(
                                      fontSize: sp(24),
                                      fontWeight: FontWeight.w400,
                                      color: controller.currentCustomThemeData().colors0x000000,
                                    ),
                                  ),
                                ),
                                e.key == controller.currentIndex.value ? Container(
                                  width:width(20),
                                  height:width(4),
                                  decoration: const BoxDecoration(
                                      borderRadius:BorderRadius.all(Radius.circular(4)),
                                      gradient: LinearGradient(
                                          begin: Alignment.centerLeft,
                                          end: Alignment.centerRight,
                                          colors:[Color(0xff6129FF), Color(0xffD96CFF
                                          )]
                                      )
                                  ),
                                ):SizedBox(width:width(20),
                                  height:width(4),)
                              ],
                            ),
                          ));
                        })
                      ],
                    )
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: width(20)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: width(40),),
                      Row(
                        children: [
                          Text('欧美',
                              style: TextStyle(
                                  color: controller.currentCustomThemeData().colors0x000000,
                                  fontSize: sp(28),
                                  fontWeight: FontWeight.w400)),
                          Container(
                              width: width(24),
                              height: width(24),
                              margin:
                              EdgeInsets.only(left: width(12)),
                              child: Image.asset(
                                  Assets.videoPage.vSelect.path,
                                  fit: BoxFit.fitWidth)),
                        ],
                      ),
                      SizedBox(height: width(16),),
                      Wrap(
                        spacing: width(10),
                        runSpacing: width(18),
                        children: [
                          Container(
                              width: width(230),
                              height: width(82),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius:BorderRadius.all(Radius.circular(width(8))),
                                  border: Border.all(color: controller.currentCustomThemeData().colors0xE8E8E8,width: width(1))
                              ),
                              child: Stack(
                                children: [
                                  Container(
                                    width: width(230),
                                    height: width(82),
                                    alignment: Alignment.center,
                                    child: Text('空乘制服',
                                        style: TextStyle(
                                            color: controller.currentCustomThemeData().colors0x000000,
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400)),
                                  ),
                                  Positioned(
                                    right: width(18),
                                    top: width(16),
                                    child: InkWell(
                                      child: SizedBox(
                                          width: width(16),
                                          child: Image.asset(
                                              Assets.videoPage.vAdd.path,
                                              fit: BoxFit.fitWidth)),
                                    ),
                                  )
                                ],
                              )
                          ),
                          Container(
                              width: width(230),
                              height: width(82),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius:BorderRadius.all(Radius.circular(width(8))),
                                  border: Border.all(color: controller.currentCustomThemeData().colors0xE8E8E8,width: width(1))
                              ),
                              child: Stack(
                                children: [
                                  Container(
                                    width: width(230),
                                    height: width(82),
                                    alignment: Alignment.center,
                                    child: Text('空乘制服',
                                        style: TextStyle(
                                            color: controller.currentCustomThemeData().colors0x000000,
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400)),
                                  ),
                                  Positioned(
                                    right: width(18),
                                    top: width(16),
                                    child: InkWell(
                                      child: SizedBox(
                                          width: width(16),
                                          child: Image.asset(
                                              Assets.videoPage.vAdd.path,
                                              fit: BoxFit.fitWidth)),
                                    ),
                                  )
                                ],
                              )
                          ),
                          Container(
                              width: width(230),
                              height: width(82),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius:BorderRadius.all(Radius.circular(width(8))),
                                  border: Border.all(color: controller.currentCustomThemeData().colors0xE8E8E8,width: width(1))
                              ),
                              child: Stack(
                                children: [
                                  Container(
                                    width: width(230),
                                    height: width(82),
                                    alignment: Alignment.center,
                                    child: Text('空乘制服',
                                        style: TextStyle(
                                            color: controller.currentCustomThemeData().colors0x000000,
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400)),
                                  ),
                                  Positioned(
                                    right: width(18),
                                    top: width(16),
                                    child: InkWell(
                                      child: SizedBox(
                                          width: width(16),
                                          child: Image.asset(
                                              Assets.videoPage.vAdd.path,
                                              fit: BoxFit.fitWidth)),
                                    ),
                                  )
                                ],
                              )
                          ),
                          Container(
                              width: width(230),
                              height: width(82),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius:BorderRadius.all(Radius.circular(width(8))),
                                  border: Border.all(color: controller.currentCustomThemeData().colors0xE8E8E8,width: width(1))
                              ),
                              child: Stack(
                                children: [
                                  Container(
                                    width: width(230),
                                    height: width(82),
                                    alignment: Alignment.center,
                                    child: Text('空乘制服',
                                        style: TextStyle(
                                            color: controller.currentCustomThemeData().colors0x000000,
                                            fontSize: sp(24),
                                            fontWeight: FontWeight.w400)),
                                  ),
                                  Positioned(
                                    right: width(18),
                                    top: width(16),
                                    child: InkWell(
                                      child: SizedBox(
                                          width: width(16),
                                          child: Image.asset(
                                              Assets.videoPage.vAdd.path,
                                              fit: BoxFit.fitWidth)),
                                    ),
                                  )
                                ],
                              )
                          ),
                        ],
                      )
                    ],
                  ),
                )
              ],
            ),
          ],
        ),);
  }
}
