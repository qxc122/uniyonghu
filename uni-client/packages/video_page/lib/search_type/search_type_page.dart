
import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/components/tag_item/tag_item_page.dart';
import 'package:video_page/search_type/search_type_controller.dart';

class SearchTypePage extends GetView<SearchTypePageController> {
  const SearchTypePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          leading: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Get.back();
            },
            icon: Image.asset(
              Assets.basePage.backBlack.path,
              width: 7,
              height: 11,
            ),
          ),
          title: Container(
            height: width(60),
            padding: EdgeInsets.symmetric(horizontal: width(16)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(width(40))),
                color: controller.currentCustomThemeData().colors0xF8F7F7,
                border: Border.all(color: controller.currentCustomThemeData().colors0xCFCFCF,width: width(1))
            ),
            child: Row(
              children: [
                Container(
                    margin: EdgeInsets.only(right: width(20)),
                    width: width(26),
                    height: width(24),
                    child: Image.asset(Assets.videoPage.vSeachIcon.path)
                ),
                Text(videoPageTranslations['search_what_you_want_to_see'].toString(),style: TextStyle(color:controller.currentCustomThemeData().colors0x979797,fontSize: sp(24)),),
              ],
            ),
          ),
          actions: [
            InkWell(
              onTap: () {
                Get.toNamed("/searchResultPage");
              },
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(right: width(30),left: width(30)),
                child: Text(videoPageTranslations['search'].toString(),style: TextStyle(color:controller.currentCustomThemeData().colors0x979797,fontSize: sp(32)),),
              ),
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children:  [
              Container(
                padding: EdgeInsets.symmetric(horizontal: width(20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin:EdgeInsets.only(bottom: width(20),top: width(56)),
                      child: Text(videoPageTranslations['hot'].toString() + videoPageTranslations['search'].toString(),style: TextStyle(color:controller.currentCustomThemeData().colors0x000000,fontSize: sp(32)),),
                    ),
                    Wrap(
                      spacing: width(8),
                      runSpacing: width(10),
                      children: const [
                        TagView(),
                        TagView(title: '教师',),
                        TagView(title: 'M系萝莉',),
                        TagView(title: '校园',),
                        TagView(title: '日出文化传媒',)
                      ],
                    )
                  ],
                ),
              )

            ],
          ),
        ));
  }
}
