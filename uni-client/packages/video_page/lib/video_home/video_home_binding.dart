import 'package:get/get.dart';
import 'package:video_page/video_home/video_home_controller.dart';
class VideoHomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VideoHomeController>(()=> VideoHomeController());
  }
}
