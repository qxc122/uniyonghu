// import 'package:device_info_plus/device_info_plus.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TypeListModel {
  String? title;
  String? commodityId;

  TypeListModel(
      {this.title,
        this.commodityId});

  TypeListModel.fromJson(Map<String, dynamic> json) {
    title = json['title'];
    commodityId = json['commodityId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['commodityId'] = this.commodityId;
    return data;
  }
}


class VideoHomeController extends GetXBaseController  with SingleGetTickerProviderMixin {
  RxString titleString = "home".obs;

  RxList<TypeListModel> tabList = RxList<TypeListModel>();
  List<String> tabs = ['关注', '最新', '精选', '空乘制服', '传媒机构', '日本AV'];
  TabController? tabController;
  RxBool buttonShow = false.obs;

  late ScrollController _scrollController;
  get scrollController => _scrollController;



  @override
  void onInit() {
    tabController = TabController(
        vsync: this, length: tabs.length);
    _scrollController =  ScrollController();
    _scrollController.addListener(isScroll);
    super.onInit();
  }

  @override
  void onReady() {
    tabList.add(TypeListModel(title: '12',commodityId: '12'));
    tabList.add(TypeListModel(title: '12',commodityId: '12'));
    tabList.add(TypeListModel(title: '12',commodityId: '12'));
    // 调用
  }
  @override
  void onClose() {
    _scrollController.removeListener(isScroll);
  }
  isScroll() {
    if (_scrollController.offset > 100) {
      buttonShow.value = true;
    } else {
      buttonShow.value = false;
    }
  }


  scrollToTop(context) {
    _scrollController.animateTo(0, duration: const Duration(milliseconds: 200),curve: Curves.easeIn);
  }
}
