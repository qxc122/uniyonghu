import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper_tv/flutter_swiper.dart';
import 'package:get/get.dart';
import 'package:video_page/components/TabBarIndicator.dart';
import 'package:video_page/components/card_item/card_item_page.dart';
import 'package:video_page/components/floatBtn.dart';
import 'package:video_page/video_home/video_home_controller.dart';

class VideoHomePage extends GetView<VideoHomeController> {
  const VideoHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          backgroundColor: Colors.white,
          title: InkWell(
              onTap: () {
                Get.toNamed(
                  "/searchTypePage",
                );
              },
              child: Container(
                height: width(60),
                margin: EdgeInsets.only(left: width(30)),
                padding: EdgeInsets.symmetric(horizontal: width(16)),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(width(40))),
                    color: controller.currentCustomThemeData().colors0xF8F7F7,
                    border:
                        Border.all(color: controller.currentCustomThemeData().colors0xCFCFCF, width: width(1))),
                child: Row(
                  children: [
                    Container(
                        margin: EdgeInsets.only(right: width(20)),
                        width: width(26),
                        height: width(24),
                        child: Image.asset(Assets.videoPage.vSeachIcon.path)),
                    Text(
                      videoPageTranslations['search_what_you_want_to_see'].toString(),
                      style: TextStyle(
                          color: controller.currentCustomThemeData().colors0x979797, fontSize: sp(24)),
                    ),
                  ],
                ),
              )),
          actions: [
            // InkWell(
            //   onTap: () {
            //     Get.toNamed(
            //       "/personalizedChannel",
            //     );
            //   },
            //   child: Container(
            //       margin: EdgeInsets.only(right: width(16), left: width(20)),
            //       width: width(36),
            //       height: width(36),
            //       child: Image.asset(Assets.videoPage.vScreen.path)),
            // ),
            InkWell(
              onTap: () {
                Get.toNamed(
                  "/screen",
                );
              },
              child: Container(
                  margin: EdgeInsets.only(right: width(16), left: width(20)),
                  width: width(36),
                  height: width(36),
                  child: Image.asset(Assets.videoPage.vChannel.path)),
            )
          ],
          bottom: TabBar(
            indicatorColor: controller.currentCustomThemeData().colors0xFFFFFF,
            labelColor: controller.currentCustomThemeData().colors0x000000,
            labelStyle: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
            // fontSize: sp(32)
            // unselectedLabelStyle:TextStyle(fontWeight: FontWeight.normal,fontSize: sp(26)),
            unselectedLabelColor: controller.currentCustomThemeData().colors0xA5A5A5,
            isScrollable: true,
            indicator: const TabBarIndicator(),
            tabs: controller.tabs.asMap().keys.map((index) {
              return Tab(
                  child: Text(
                controller.tabs[index].toString(),
              ));
            }).toList(),
            controller: controller.tabController, // 记得要带上tabController
          ),
        ),
        body: Stack(
          fit:StackFit.expand,
          children: [
            TabBarView(
              controller: controller.tabController,
              children: [
                SingleChildScrollView(
                  controller: controller.scrollController,
                    child: Column(
                      children: [
                        /// 轮播图
                        Container(
                          margin: EdgeInsets.only(bottom: width(17)),
                          height: width(332),
                          child: Swiper(
                            itemBuilder: (BuildContext context, int index) {
                              return SizedBox(
                                  height: width(332),
                                  child:
                                  Image.asset(Assets.videoPage.vSwiper.path));
                            },
                            itemCount: 3,
                            viewportFraction: 0.90,
                            scale: 0.95,
                          ),
                        ),

                        /// 公告
                        Container(
                          height: width(60),
                          color: controller.currentCustomThemeData().colors0xF0EAFF,
                          padding: EdgeInsets.symmetric(horizontal: width(30)),
                          child: Row(
                            children: [
                              Container(
                                  width: width(30),
                                  height: width(30),
                                  margin: EdgeInsets.only(right: width(20)),
                                  child:
                                  Image.asset(Assets.videoPage.vNotice.path)),
                              Text(
                                videoPageTranslations['congratulations_player'].toString()+ " ***250 "+videoPageTranslations['stay'].toString(),
                                style: TextStyle(
                                    color: controller.currentCustomThemeData().colors0x000000, fontSize: sp(24)),
                              ),
                              Text(
                                "一分快三",
                                style: TextStyle(
                                    color: controller.currentCustomThemeData().colors0x8F00FF, fontSize: sp(24)),
                              ),
                              Text(
                                videoPageTranslations['in_the_game'].toString() + videoPageTranslations['earned'].toString(),
                                style: TextStyle(
                                    color: controller.currentCustomThemeData().colors0x000000, fontSize: sp(24)),
                              ),
                              Text(
                                "¥ 10000"+ videoPageTranslations['element'].toString(),
                                style: TextStyle(
                                    color: controller.currentCustomThemeData().colors0x8F00FF, fontSize: sp(24)),
                              )
                            ],
                          ),
                        ),

                        /// 热门视频
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: width(30)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                    top: width(24), bottom: width(10)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    // ignore: avoid_unnecessary_containers
                                    Container(
                                      child: Text(
                                        videoPageTranslations['hot_video'].toString(),
                                        style: TextStyle(
                                            color: controller.currentCustomThemeData().colors0x262626,
                                            fontSize: sp(30),
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    InkWell(
                                        onTap: () {
                                          Get.toNamed("/recommendPage",
                                              arguments: {"title": videoPageTranslations['hot_video'].toString()});
                                        },
                                        child: Row(
                                          children: [
                                            Text(
                                              videoPageTranslations['more'].toString(),
                                              style: TextStyle(
                                                  color: controller.currentCustomThemeData().colors0xBBBBBB,
                                                  fontSize: sp(24)),
                                            ),
                                            Container(
                                                width: width(8),
                                                height: width(16),
                                                margin:
                                                EdgeInsets.only(left: width(8)),
                                                child: Image.asset(
                                                    Assets.videoPage.vRpH.path,
                                                    fit: BoxFit.fitWidth)),
                                          ],
                                        ))
                                  ],
                                ),
                              ),
                              Wrap(
                                spacing: width(10),
                                runSpacing: width(20),
                                children: const [
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                ],
                              )
                            ],
                          ),
                        ),

                        /// 今日推荐
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: width(30)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                margin: EdgeInsets.only(
                                    top: width(24), bottom: width(10)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      videoPageTranslations['recommended_today'].toString(),
                                      style: TextStyle(
                                          color: controller.currentCustomThemeData().colors0x262626,
                                          fontSize: sp(30),
                                          fontWeight: FontWeight.bold),
                                    ),
                                    InkWell(
                                      onTap: () {
                                        Get.toNamed("/recommendPage",
                                            arguments: {"title": videoPageTranslations['recommended_today'].toString()});
                                      },
                                      child: Row(
                                        children: [
                                          Text(
                                            videoPageTranslations['more'].toString(),
                                            style: TextStyle(
                                                color: controller.currentCustomThemeData().colors0xBBBBBB,
                                                fontSize: sp(24)),
                                          ),
                                          Container(
                                              width: width(8),
                                              height: width(16),
                                              margin:
                                              EdgeInsets.only(left: width(8)),
                                              child: Image.asset(
                                                  Assets.videoPage.vRpH.path,
                                                  fit: BoxFit.fitWidth)),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Wrap(
                                spacing: width(10),
                                runSpacing: width(20),
                                children: const [
                                  CardView(
                                    cW: 692,
                                    cH: 390,
                                    flag: false,
                                  ),
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                ],
                              )
                            ],
                          ),
                        ),

                        /// 我是广告页
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: width(30)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                alignment: Alignment.center,
                                height: width(200),
                                margin: EdgeInsets.symmetric(vertical: width(20)),
                                decoration: BoxDecoration(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(width(10))),
                                  color: controller.currentCustomThemeData().colors0xF0F0F0,
                                ),
                              ),
                              Wrap(
                                spacing: width(10),
                                runSpacing: width(20),
                                children: const [
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                  CardView(),
                                ],
                              )
                            ],
                          ),
                        ),
                      ],
                    )
                ),
                Container(),
                Container(),
                Container(),
                Container(),
                Container(),
              ],
            ),
            Positioned(
              left: 0,
              bottom: width(32),
              width: width(750),
              height: width(116),
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: width(95)),
                alignment: Alignment.center,
                clipBehavior: Clip.hardEdge,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(width(10))),
                  color: controller.currentCustomThemeData().colors0xFFFFFF,
                ),
                child: Row(
                  children: [
                    SizedBox(
                        width: width(200),
                        height: width(116),
                        child: Image.asset(
                          Assets.videoPage.vImg.path,
                          fit: BoxFit.cover,
                        )),
                    Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.only(left: width(10)),
                        height: width(116),
                        child: Row(
                          children: [
                            Text(
                              "就这几个字吖吖...",
                              style: TextStyle(
                                  color:controller.currentCustomThemeData().colors0x000000,
                                  fontSize: sp(28)),
                            ),
                            Container(
                                width: width(40),
                                height: width(40),
                                margin: EdgeInsets.only(left: width(10)),
                                child: Image.asset(
                                  Assets.videoPage.vPlayB.path,
                                  fit: BoxFit.cover,
                                )),
                            Container(
                                width: width(40),
                                height: width(40),
                                margin: EdgeInsets.only(left: width(20)),
                                child: Image.asset(
                                  Assets.videoPage.vCloseB.path,
                                  fit: BoxFit.cover,
                                )),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      floatingActionButton: Obx(()=>Visibility(
          visible: controller.buttonShow.value,
          child: SizedBox(
            width: width(100),
            height: width(100),
            child:FloatingActionButton(
              onPressed: () {
                controller.scrollToTop(context);
              },
              child: Image.asset(
                Assets.videoPage.vBackTop.path,
                width: width(100),
                height: width(100),
              ),
            ),
          )
      )),
      floatingActionButtonLocation: CustomFloatingActionButtonLocation(
          FloatingActionButtonLocation.endFloat,
          -width(22),
          -width(312)),
    );
  }
}
