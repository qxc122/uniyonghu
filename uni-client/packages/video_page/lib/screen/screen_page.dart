
import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/components/card_item/card_item_page.dart';
import 'package:video_page/screen/screen_controller.dart';

class ScreenPage extends GetView<ScreenController> {
  const ScreenPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          leading: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Get.back();
            },
            icon: Image.asset(
              Assets.basePage.backBlack.path,
              width: 7,
              height: 11,
            ),
          ),
          title: Text(videoPageTranslations['screen'].toString(),
              style: TextStyle(
                  color: controller.currentCustomThemeData().colors0x000000,
                  fontSize: sp(32),
                  fontWeight: FontWeight.w600)),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.only(left: width(20)),
                margin: EdgeInsets.only(bottom: width(8)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: width(44),
                      margin: EdgeInsets.only(bottom: width(28)),
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          InkWell(
                            child: Container(
                              height: width(44),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(Radius.circular(width(40))),
                                color: controller
                                    .currentCustomThemeData()
                                    .colors0xEFDFFF
                              ),
                              child: Text(
                                "全部地区",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x8E1EFF,
                                    fontSize: sp(24)),
                              ),
                            ),
                          ),
                          InkWell(
                            child: Container(
                              width: width(124),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              child: Text(
                                "大陆",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x979797,
                                    fontSize: sp(24)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: width(44),
                      margin: EdgeInsets.only(bottom: width(28)),
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          InkWell(
                            child: Container(
                              height: width(44),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(width(40))),
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0xEFDFFF
                              ),
                              child: Text(
                                "全部地区",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x8E1EFF,
                                    fontSize: sp(24)),
                              ),
                            ),
                          ),
                          InkWell(
                            child: Container(
                              width: width(124),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              child: Text(
                                "大陆",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x979797,
                                    fontSize: sp(24)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: width(44),
                      margin: EdgeInsets.only(bottom: width(28)),
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          InkWell(
                            child: Container(
                              height: width(44),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(width(40))),
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0xEFDFFF
                              ),
                              child: Text(
                                "全部地区",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x8E1EFF,
                                    fontSize: sp(24)),
                              ),
                            ),
                          ),
                          InkWell(
                            child: Container(
                              width: width(124),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              child: Text(
                                "大陆",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x979797,
                                    fontSize: sp(24)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: width(44),
                      margin: EdgeInsets.only(bottom: width(28)),
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          InkWell(
                            child: Container(
                              height: width(44),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(width(40))),
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0xEFDFFF
                              ),
                              child: Text(
                                "全部地区",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x8E1EFF,
                                    fontSize: sp(24)),
                              ),
                            ),
                          ),
                          InkWell(
                            child: Container(
                              width: width(124),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              child: Text(
                                "大陆",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x979797,
                                    fontSize: sp(24)),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: width(44),
                      margin: EdgeInsets.only(bottom: width(28)),
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          InkWell(
                            child: Container(
                              height: width(44),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(width(40))),
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0xEFDFFF
                              ),
                              child: Text(
                                "全部地区",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x8E1EFF,
                                    fontSize: sp(24)),
                              ),
                            ),
                          ),
                          InkWell(
                            child: Container(
                              width: width(124),
                              padding: EdgeInsets.symmetric(horizontal: width(14)),
                              alignment: Alignment.center,
                              child: Text(
                                "大陆",
                                style: TextStyle(
                                    color: controller
                                        .currentCustomThemeData()
                                        .colors0x979797,
                                    fontSize: sp(24)),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Divider(
                color: controller.currentCustomThemeData().colors0xF4F4F4,
                height: width(1),
              ),
              Container(
                padding: EdgeInsets.only(left: width(20),top: width(30),bottom: width(36)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Text(
                          videoPageTranslations['default'].toString(),
                          style: TextStyle(
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0x000000,
                              fontSize: sp(32)),
                        ),
                        Text(
                          "（"+videoPageTranslations['multiple_choices_can_be_selected_by_horizontal_sliding_up_to_5_can_be_selected'].toString()+"）",
                          style: TextStyle(
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0x000000,
                              fontSize: sp(20)),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        InkWell(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: width(20)),
                            margin: EdgeInsets.only(top: width(20),right: width(20)),
                            alignment: Alignment.center,
                            height: width(50),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(Radius.circular(width(8))),
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0xF1F1F1,
                            ),
                            child: Text(
                              "三个字",
                              style: TextStyle(
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0x000000,
                                  fontSize: sp(24)),
                            ),
                          ),
                        ),
                        InkWell(
                          child: Stack(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(horizontal: width(20)),
                                margin: EdgeInsets.only(top: width(20),right: width(20)),
                                alignment: Alignment.center,
                                height: width(50),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(width(8))),
                                  border:Border.all(width: width(2),color: controller.currentCustomThemeData().colors0x8E1EFF),
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0xF1E4FF,
                                ),
                                child: Text(
                                  "三个字",
                                  style: TextStyle(
                                      color: controller
                                          .currentCustomThemeData()
                                          .colors0x000000,
                                      fontSize: sp(24)),
                                ),
                              ),
                              Positioned(
                                left: 0,
                                top: width(20),
                                child: SizedBox(
                                    width: width(28),
                                    height: width(22),
                                    child: Image.asset(Assets.videoPage.vSelcetVideo.path))
                              )
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),

              Container(
                color: controller.currentCustomThemeData().colors0xF2F2F2,
                height: width(10),
              ),
              InkWell(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: width(20)),
                  margin: EdgeInsets.only(top: width(30)),
                  child: Row(
                    children: [
                      const CardView(
                        flag: false,
                      ),
                      const SizedBox(
                        width: 7.5,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "萝莉姐妹花飞骑上外送员",
                              style: TextStyle(
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0x000000,
                                  fontSize: sp(28)),
                            ),
                            const SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                        width: width(24),
                                        height: width(24),
                                        child: Image.asset(
                                            Assets.videoPage.vMoneyIcon.path,
                                            fit: BoxFit.cover)),
                                    Container(
                                      margin: EdgeInsets.only(left: width(10)),
                                      child: Text(
                                        "5.00",
                                        style: TextStyle(
                                            color: controller
                                                .currentCustomThemeData()
                                                .colors0xFFAA46,
                                            fontSize: sp(28)),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Container(
                                      margin: EdgeInsets.only(right: width(6)),
                                      child: Text(
                                        "初级流氓",
                                        style: TextStyle(
                                            color: controller
                                                .currentCustomThemeData()
                                                .colors0xD7D7D7,
                                            fontSize: sp(28)),
                                      ),
                                    ),
                                    Container(
                                      width: width(64),
                                      height: width(26),
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(width(4))),
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0xFF323E),
                                      child: Text(
                                        "萝莉控",
                                        style: TextStyle(
                                            color: controller
                                                .currentCustomThemeData()
                                                .colors0xFFFFFF,
                                            fontSize: sp(20),
                                            height: 1.1),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            Row(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width(10)),
                                  margin: EdgeInsets.only(right: width(10)),
                                  height: width(40),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(width(8))),
                                      color: controller
                                          .currentCustomThemeData()
                                          .colors0xF0F0F0),
                                  child: Text(
                                    "清纯大学生",
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x868686,
                                        fontSize: sp(20)),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: width(10)),
                                  height: width(40),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(width(8))),
                                      color: controller
                                          .currentCustomThemeData()
                                          .colors0xF0F0F0),
                                  child: Text(
                                    "精品资源",
                                    style: TextStyle(
                                        color: controller
                                            .currentCustomThemeData()
                                            .colors0x868686,
                                        fontSize: sp(20)),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
