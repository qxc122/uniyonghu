import 'package:get/get.dart';
import 'package:video_page/screen/screen_controller.dart';
class ScreenBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ScreenController>(()=> ScreenController());
  }
}
