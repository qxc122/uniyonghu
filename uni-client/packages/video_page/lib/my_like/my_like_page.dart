
import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/components/card_item/card_item_page.dart';
import 'package:video_page/my_like/my_like_controller.dart';

class MyLikePage extends GetView<MyLikeController> {
  const MyLikePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor:controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          leading: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Get.back();
            },
            icon: Image.asset(
              Assets.basePage.backBlack.path,
              width: 7,
              height: 11,
            ),
          ),
          title: Text(videoPageTranslations['my'].toString() + videoPageTranslations['like'].toString(),
              style: TextStyle(
                  color: controller.currentCustomThemeData().colors0x000000,
                  fontSize: sp(32),
                  fontWeight: FontWeight.w600)),
          actions: [
            Obx(()=>InkWell(
              onTap: () {
                controller.changeEdit();
              },
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.only(right: width(30)),
                  child: Text(
                      controller.isEdit.value ?videoPageTranslations['cancel'].toString(): videoPageTranslations['edit'].toString(),
                      style: TextStyle(
                          color: controller.currentCustomThemeData().colors0x7A7A7A,
                          fontSize: sp(28),
                          fontWeight: FontWeight.w400)),
                )
            ))
          ],
        ),
        body: GetX<MyLikeController>(
    init: controller,
    builder: (_) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.symmetric(horizontal: width(25)).copyWith(top: width(20),bottom: width(40)),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      height: width(60),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(width(40))),
                        color: controller.currentCustomThemeData().colors0xF8F7F7,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              margin: EdgeInsets.only(right: width(20)),
                              width: width(26),
                              height: width(24),
                              child: Image.asset(Assets.videoPage.vSeachIcon.path)),
                          Text(
                            videoPageTranslations['please_enter_the_movie_title'].toString(),
                            style: TextStyle(
                                color: controller.currentCustomThemeData().colors0x979797, fontSize: sp(24)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      // Get.toNamed("/searchResultPage");
                    },
                    child: Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(left: width(22)),
                      child: Text(videoPageTranslations['search'].toString(),style: TextStyle(color:controller.currentCustomThemeData().colors0x979797,fontSize: sp(32)),),
                    ),
                  )
                ],
              ),
            ),
            Expanded(child: Container(
              padding: EdgeInsets.symmetric(horizontal: width(28)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: width(340),
                    child: Column(
                      children: [
                        SizedBox(
                          width: width(340),
                          height: width(192),
                          child: Stack(
                            children: [
                              const CardView(flag: false,),
                              controller.isEdit.value ? Positioned(
                                left: 0,
                                top: 0,
                                width: width(340),
                                height: width(192),
                                child: Container(
                                  width: width(340),
                                  height: width(192),
                                  decoration: BoxDecoration(
                                      color: const Color.fromARGB(95, 0, 0, 0),
                                    borderRadius: BorderRadius.all(Radius.circular(width(10)))
                                  ),
                                ),
                              ):Container(),
                              controller.isEdit.value ? Positioned(
                                right: width(16),
                                top: width(22),
                                child: SizedBox(
                                    width: width(40),
                                    height: width(40),
                                    child: Image.asset(
                                        Assets.videoPage.vUn.path,
                                        fit: BoxFit.cover)),
                              ):Container()
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: width(10),bottom: width(48)),
                          child: Text(
                            "名字可以很长很长很长很长很长很长很长很长",
                            style: TextStyle(
                                color: controller.currentCustomThemeData().colors0x000000, fontSize: sp(24)),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            )),
            controller.isEdit.value ? Divider(
              color: controller.currentCustomThemeData().colors0xF4F4F4,
            ):Container(),
            controller.isEdit.value ? SizedBox(
              height: width(80),
              child: Row(
                children: [
                  Expanded(child: InkWell(child: Container(
                    alignment: Alignment.center,
                    child: Text(videoPageTranslations['select_all'].toString(),
                        style: TextStyle(
                            color: controller.currentCustomThemeData().colors0x000000,
                            fontSize: sp(28),
                            fontWeight: FontWeight.w400)),
                  ),)),
                  Container(
                    width: width(1),
                    height: width(30),
                    color: controller.currentCustomThemeData().colors0xF4F4F4,
                  ),
                  Expanded(child: InkWell(child: Container(
                    alignment: Alignment.center,
                    child: Text(videoPageTranslations['delete'].toString() + '(1)',
                        style: TextStyle(
                            color: controller.currentCustomThemeData().colors0xF82D2D,
                            fontSize: sp(28),
                            fontWeight: FontWeight.w400)),
                  ),)),
                ],
              ),
            ):Container()
          ],
        )));
  }
}
