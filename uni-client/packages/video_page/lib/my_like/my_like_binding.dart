import 'package:get/get.dart';
import 'package:video_page/my_like/my_like_controller.dart';
class MyLikeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<MyLikeController>(()=> MyLikeController());
  }
}
