import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:video_page/feedback/feedback_controller.dart';

class FeedBackPage extends GetView<FeedBackController> {
  const FeedBackPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          leading: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Get.back();
            },
            icon: Image.asset(
              Assets.basePage.backBlack.path,
              width: 7,
              height: 11,
            ),
          ),
          title: Text(videoPageTranslations['feedback'].toString(),
              style: TextStyle(
                  color: controller.currentCustomThemeData().colors0x000000,
                  fontSize: sp(32),
                  fontWeight: FontWeight.w600)),
        ),
        body: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(20))
                  .copyWith(top: width(60), bottom: width(24)),
              color: controller.currentCustomThemeData().colors0xF3F3F3,
              alignment: Alignment.centerLeft,
              child: Text(videoPageTranslations['common_film_related_questions'].toString(),
                  style: TextStyle(
                      color: controller.currentCustomThemeData().colors0x000000,
                      fontSize: sp(32),
                      fontWeight: FontWeight.w600)),
            ),
            InkWell(
              onTap: () {
                Get.toNamed("/problemDetails");
              },
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: width(20)),
                height: width(82),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(videoPageTranslations['how_to_obtain_unlimited_viewing_times'].toString(),
                        style: TextStyle(
                            color: controller
                                .currentCustomThemeData()
                                .colors0x000000,
                            fontSize: sp(28),
                            fontWeight: FontWeight.w400)),
                    SizedBox(
                      width: width(40),
                      height: width(24),
                      child: Image.asset(
                        Assets.videoPage.vRpH.path,
                        width: width(40),
                        height: width(24),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: width(20)),
              height: width(80),
              alignment: Alignment.centerLeft,
              color: controller.currentCustomThemeData().colors0xF3F3F3,
              child: Text(videoPageTranslations['if_you_can_t_solve_your_problem'].toString(),
                  style: TextStyle(
                      color: controller.currentCustomThemeData().colors0x979797,
                      fontSize: sp(24),
                      fontWeight: FontWeight.w400)),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: width(20), vertical: width(28)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text("*",
                          style: TextStyle(
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0xFF0000,
                              fontSize: sp(32),
                              fontWeight: FontWeight.w600)),
                      Text(videoPageTranslations['please_select_a_question_category'].toString(),
                          style: TextStyle(
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0x000000,
                              fontSize: sp(32),
                              fontWeight: FontWeight.w600)),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: width(16)),
                    child: Row(
                      children: [
                        Expanded(
                            child: Container(
                          margin: EdgeInsets.only(top: width(48)),
                          child: Row(
                            children: [
                              Container(
                                width: width(32),
                                height: width(32),
                                margin: EdgeInsets.only(right: width(16)),
                                child: Image.asset(
                                  Assets.videoPage.vUn.path,
                                  width: width(32),
                                  height: width(32),
                                ),
                              ),
                              Text(videoPageTranslations['cannot_play'].toString(),
                                  style: TextStyle(
                                      color: controller
                                          .currentCustomThemeData()
                                          .colors0x000000,
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400)),
                            ],
                          ),
                        )),
                        Expanded(
                            child: Container(
                          margin:
                              EdgeInsets.only(top: width(48), left: width(30)),
                          child: Row(
                            children: [
                              Container(
                                width: width(32),
                                height: width(32),
                                margin: EdgeInsets.only(right: width(16)),
                                child: Image.asset(
                                  Assets.videoPage.vRadioSelect.path,
                                  width: width(32),
                                  height: width(32),
                                ),
                              ),
                              Text(videoPageTranslations['the_film_is_playing'].toString(),
                                  style: TextStyle(
                                      color: controller
                                          .currentCustomThemeData()
                                          .colors0x000000,
                                      fontSize: sp(28),
                                      fontWeight: FontWeight.w400)),
                            ],
                          ),
                        )),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: width(16)),
                    child: Row(
                      children: [
                        Expanded(
                            child: Container(
                              margin: EdgeInsets.only(top: width(48)),
                              child: Row(
                                children: [
                                  Container(
                                    width: width(32),
                                    height: width(32),
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vUn.path,
                                      width: width(32),
                                      height: width(32),
                                    ),
                                  ),
                                  Text(videoPageTranslations['the_charging_video_cannot_be_viewed'].toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0x000000,
                                          fontSize: sp(28),
                                          fontWeight: FontWeight.w400)),
                                ],
                              ),
                            )),
                        Expanded(
                            child: Container(
                              margin:
                              EdgeInsets.only(top: width(48), left: width(30)),
                              child: Row(
                                children: [
                                  Container(
                                    width: width(32),
                                    height: width(32),
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vUn.path,
                                      width: width(32),
                                      height: width(32),
                                    ),
                                  ),
                                  Text(videoPageTranslations['unable_to_download_to_album'].toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0x000000,
                                          fontSize: sp(28),
                                          fontWeight: FontWeight.w400)),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: width(16)),
                    child: Row(
                      children: [
                        Expanded(
                            child: Container(
                              margin: EdgeInsets.only(top: width(48)),
                              child: Row(
                                children: [
                                  Container(
                                    width: width(32),
                                    height: width(32),
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vUn.path,
                                      width: width(32),
                                      height: width(32),
                                    ),
                                  ),
                                  Text(videoPageTranslations['cache_problems'].toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0x000000,
                                          fontSize: sp(28),
                                          fontWeight: FontWeight.w400)),
                                ],
                              ),
                            )),
                        Expanded(
                            child: Container(
                              margin:
                              EdgeInsets.only(top: width(48), left: width(30)),
                              child: Row(
                                children: [
                                  Container(
                                    width: width(32),
                                    height: width(32),
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vUn.path,
                                      width: width(32),
                                      height: width(32),
                                    ),
                                  ),
                                  Text(videoPageTranslations['film_request_content_suggestion'].toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0x000000,
                                          fontSize: sp(28),
                                          fontWeight: FontWeight.w400)),
                                ],
                              ),
                            )),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: width(16)),
                    child: Row(
                      children: [
                        Expanded(
                            child: Container(
                              margin: EdgeInsets.only(top: width(48)),
                              child: Row(
                                children: [
                                  Container(
                                    width: width(32),
                                    height: width(32),
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vUn.path,
                                      width: width(32),
                                      height: width(32),
                                    ),
                                  ),
                                  Text(videoPageTranslations['no_subtitles_or_sound'].toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0x000000,
                                          fontSize: sp(28),
                                          fontWeight: FontWeight.w400)),
                                ],
                              ),
                            )),
                        Expanded(
                            child: Container(
                              margin:
                              EdgeInsets.only(top: width(48), left: width(30)),
                              child: Row(
                                children: [
                                  Container(
                                    width: width(32),
                                    height: width(32),
                                    margin: EdgeInsets.only(right: width(16)),
                                    child: Image.asset(
                                      Assets.videoPage.vUn.path,
                                      width: width(32),
                                      height: width(32),
                                    ),
                                  ),
                                  Text(videoPageTranslations['other'].toString(),
                                      style: TextStyle(
                                          color: controller
                                              .currentCustomThemeData()
                                              .colors0x000000,
                                          fontSize: sp(28),
                                          fontWeight: FontWeight.w400)),
                                ],
                              ),
                            )),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: width(20),
              color: controller.currentCustomThemeData().colors0xF3F3F3,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: width(20), vertical: width(28)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(videoPageTranslations['detailed_description'].toString(),
                      style: TextStyle(
                          color: controller
                              .currentCustomThemeData()
                              .colors0x000000,
                          fontSize: sp(32),
                          fontWeight: FontWeight.w600)),
                  Container(
                    height: width(290),
                    margin: EdgeInsets.only(top: width(24)),
                    color: controller.currentCustomThemeData().colors0xF3F3F3,
                    padding: EdgeInsets.symmetric(horizontal: width(28))
                        .copyWith(bottom: width(12)),
                    child: Stack(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: TextField(
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(200)
                                ],
                                keyboardType: TextInputType.text,
                                cursorColor: controller.currentCustomThemeData().colors0x979797,
                                style: TextStyle(
                                    color: controller.currentCustomThemeData().colors0x979797,
                                    fontSize: sp(28)),
                                decoration: const InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "不可为空，上限200字符",
                                  hintStyle: TextStyle(
                                      color: Color(0xffC7C7CC), fontSize: 14.0),
                                  floatingLabelBehavior:
                                  FloatingLabelBehavior.never,
                                ),
                                // focusNode: controller.titleFocus,
                                // controller: controller.titleCtr,
                                // onChanged: (String text) {
                                //   controller.setTitle(text);
                                // },
                              ),
                            ),
                          ],
                        ),
                        Positioned(
                          right: width(0),
                          bottom: width(0),
                          child: Text("0/200",
                              style: TextStyle(
                                  color: controller
                                      .currentCustomThemeData()
                                      .colors0x979797,
                                  fontSize: sp(24),
                                  fontWeight: FontWeight.w400)),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: width(20),
              color: controller.currentCustomThemeData().colors0xF3F3F3,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                  horizontal: width(20), vertical: width(28)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(videoPageTranslations['upload_feedback_pictures'].toString(),
                          style: TextStyle(
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0x000000,
                              fontSize: sp(32),
                              fontWeight: FontWeight.w600)),
                      Text(videoPageTranslations['not_required'].toString(),
                          style: TextStyle(
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0xD7D7D7,
                              fontSize: sp(32),
                              fontWeight: FontWeight.w600)),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: width(32)),
                    child: Row(
                      children: [
                        Container(
                          width: width(200),
                          height: width(200),
                          margin: EdgeInsets.only(top: width(28)),
                          alignment: Alignment.center,
                          color: controller
                              .currentCustomThemeData()
                              .colors0xF7F8F8,
                          child: Image.asset(
                            Assets.videoPage.upload.path,
                            width: width(60),
                            height: width(60),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  child: Container(
                    margin: EdgeInsets.only(top: width(108)),
                    width: width(440),
                    height: width(80),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.all(Radius.circular(width(40))),
                      color: controller.currentCustomThemeData().colors0xD9D9D9,
                    ),
                    child: Text(videoPageTranslations['submit'].toString(),
                        style: TextStyle(
                            color: controller
                                .currentCustomThemeData()
                                .colors0xFFFFFF,
                            fontSize: sp(32),
                            fontWeight: FontWeight.w400)),
                  ),
                )
              ],
            ),
            SizedBox(
              height: width(40),
            )
          ],
        )));
  }
}
