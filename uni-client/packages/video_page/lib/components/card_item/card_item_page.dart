import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:base/assets.gen.dart';
import 'package:base/translations/zh-Hans/zh_cn_video_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/components/card_item/card_item_controller.dart';

class CardView extends GetView<CardItemController> {
  final int num;

  /// 左上角icon类型 1免费 2付费 3VIP
  final int cW;

  /// 卡片宽度
  final int cH;

  /// 卡片高度
  final String cover;

  /// 封面
  final String time;

  /// 时间
  final String likes;

  /// 点赞量
  final String title;

  /// 标题 最多8个字
  final bool flag;

  /// 是否展示底部title模块
  const CardView({
    Key? key,
    this.num = 1,
    this.cW = 340,
    this.cH = 192,
    this.cover = "",
    this.time = "01:56:20",
    this.likes = "6.2万",
    this.title = "只能出现最多八个字...",
    this.flag = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  InkWell(
      onTap: () {
        // Get.toNamed("/videoDetailPage");
      },
      child: SizedBox(
        width: width(double.parse(cW.toString())),
        // height: width(double.parse(cH.toString())),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            InkWell(
              onTap: () {
                Get.toNamed("/videoDetailPage");
              },
              child: Container(
                  alignment: Alignment.centerLeft,
                  clipBehavior: Clip.hardEdge,
                  decoration: flag
                      ? BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(width(10)),
                          topRight: Radius.circular(width(10))))
                      : BoxDecoration(
                      borderRadius:
                      BorderRadius.all(Radius.circular(width(10)))),
                  child: Stack(
                    children: [
                      SizedBox(
                          width: width(double.parse(cW.toString())),
                          height: width(double.parse(cH.toString())),
                          child: Image.asset(
                            Assets.videoPage.vImg.path,
                            fit: BoxFit.cover,
                          )),
                      Positioned(
                        left: -1,
                        top: 0,
                        child: SizedBox(
                            width: width(70),
                            height: width(28),
                            child: Image.asset(Assets.videoPage.ff.path)),
                      ),
                      Positioned(
                        left: 0,
                        bottom: 4,
                        child: Container(
                          width: width(double.parse(cW.toString())),
                          padding: EdgeInsets.symmetric(horizontal: width(8)),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                time,
                                style: TextStyle(
                                    color: controller.currentCustomThemeData().colors0xFFFFFF, fontSize: sp(20)),
                              ),
                              Container(
                                padding:
                                EdgeInsets.symmetric(horizontal: width(8)),
                                height: width(26),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(width(40))),
                                    color: controller.currentCustomThemeData().colors0x000000_30,
                                    border: Border.all(
                                        color: Colors.white, width: 1)),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                        width: width(12),
                                        height: width(10),
                                        child: Image.asset(
                                            Assets.videoPage.vThumbs.path,
                                            fit: BoxFit.cover)),
                                    Container(
                                      margin: EdgeInsets.only(left: width(4)),
                                      child: Text(
                                        "6.2"+ videoPageTranslations['ten_thousand'].toString(),
                                        style: TextStyle(
                                            color:controller.currentCustomThemeData().colors0xFFFFFF,
                                            fontSize: sp(16)),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  )),
            ),
            flag
                ? Container(
              height: width(50),
              padding: EdgeInsets.only(left: width(8),right: width(14)),
              decoration: BoxDecoration(
                  color: controller.currentCustomThemeData().colors0x18191D,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(width(10)),
                      bottomRight: Radius.circular(width(10)))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "只能出现最多八个字...",
                    style: TextStyle(
                        color: controller.currentCustomThemeData().colors0xFFFFFF, fontSize: sp(24)),
                  ),
                  InkWell(
                    onTap: () {
                      controller.showMore();
                    },
                    child: SizedBox(
                        width: width(26),
                        height: width(6),
                        child: Image.asset(Assets.videoPage.vMore.path,
                            fit: BoxFit.cover)),
                  ),
                ],
              ),
            )
                : Container()
          ],
        ),
      ),
    );
  }
}
