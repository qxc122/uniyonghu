// import 'package:device_info_plus/device_info_plus.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CardItemController extends GetXBaseController {
  RxString titleString = "只能出现最多八个字".obs;
  RxBool moreFlag = false.obs;


  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
  }

  showMore() {
    moreFlag.value = true;
  }
}
