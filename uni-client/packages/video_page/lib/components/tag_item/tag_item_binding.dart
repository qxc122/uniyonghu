import 'package:get/get.dart';
import 'package:video_page/components/tag_item/tag_item_controller.dart';
class TagViewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TagViewController>(()=> TagViewController());
  }
}
