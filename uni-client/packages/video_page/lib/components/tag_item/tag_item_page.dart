
import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/components/tag_item/tag_item_controller.dart';

class TagView extends GetView<TagViewController> {
  final String title; /// 标签内容
  const TagView({Key? key,this.title = '三上悠亚'}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var controller = Get.put(TagViewController());
    return InkWell(
      child: Container(
        width: width(170),
        height: width(60),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(width(10))),
          color: controller.currentCustomThemeData().colors0xF7F7F7
        ),
        child: Text(title,style: TextStyle(color:controller.currentCustomThemeData().colors0x979797,fontSize: sp(24)),),
      ),
    );
  }
}
