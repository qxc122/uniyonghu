
import 'package:base/assets.gen.dart';
import 'package:base/commons/utils/screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:video_page/components/card_item/card_item_page.dart';
import 'package:video_page/recommend/recommend_controller.dart';

class RecommendPage extends GetView<RecommendPageController> {
  const RecommendPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: controller.currentCustomThemeData().colors0xFFFFFF,
        extendBodyBehindAppBar: false,
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          titleSpacing: 0,
          backgroundColor: controller.currentCustomThemeData().colors0xF8F8F8,
          leading: IconButton(
            padding: EdgeInsets.zero,
            onPressed: () {
              Get.back();
            },
            icon: Image.asset(
              Assets.basePage.backBlack.path,
              width: 7,
              height: 11,
            ),
          ),
          title: Obx(()=>Text(controller.title.value,
              style: TextStyle(
                  color: controller.currentCustomThemeData().colors0x000000,
                  fontSize: sp(32),
                  fontWeight: FontWeight.w600))),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: width(30)).copyWith(top: width(20)),
            child: Column(
              children: [
                Wrap(
                  spacing: width(10),
                  runSpacing: width(20),
                  children: const [
                     CardView(),
                     CardView(),
                     CardView(),
                     CardView(),
                     CardView(),
                     CardView(),
                  ],
                )
              ],
            ),
          )
        ));
  }
}
