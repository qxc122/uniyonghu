// import 'package:device_info_plus/device_info_plus.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';


class RecommendPageController extends GetXBaseController {
  RxString titleString = "home".obs;
  RxString title = "".obs;


  @override
  void onInit() {
    title.value = Get.arguments?["title"] ?? '';
    super.onInit();
  }

  @override
  void onReady() {
    // 调用
  }
}
