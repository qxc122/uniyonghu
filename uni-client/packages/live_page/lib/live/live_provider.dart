import 'package:get/get.dart';
import 'package:live_page/online_user_list/models/live_toop50_user_bean.dart';
import 'package:services/api/common_apis.dart';
import 'package:services/api/live_apis.dart';
import 'package:services/models/res/base_response.dart';
import 'package:services/models/res/game/game_tab_model.dart';
import 'package:services/models/res/game/live_game_model.dart';
import 'package:services/models/res/live/anchor_bean.dart';
import 'package:services/models/res/live/anchor_card_bean.dart';
import 'package:services/models/res/live/live_bean.dart';

class LiveProvider extends GetConnect {
  /// 获取直播间主播头部信息
  static Future<AnchorBean?> getAnchorBean({
    required String studioNum,
  }) async {
    BaseResponse response =
        await LiveApis.of().getAnchorInfo(studioNum: studioNum);
    if (GetUtils.isNull(response) == true) {
      return null;
    }
    final dataJson = response.data as Map<String, dynamic>?;
    return dataJson == null ? null : AnchorBean.fromJson(dataJson);
  }

  /// 获取直播间主播名片
  static Future<AnchorCardBean?> getAnchorCard({
    required String studioNum,
    required int userId,
  }) async {
    BaseResponse response = await LiveApis.of().getAnchorCard(
      studioNum: studioNum,
      userId: userId,
    );
    if (GetUtils.isNull(response) == true) {
      return null;
    }
    final dataJson = response.data as Map<String, dynamic>?;
    return dataJson == null ? null : AnchorCardBean.fromJson(dataJson);
  }

  /// 获取直播间推荐直播
  static Future<List<LiveBean>> getLiveRecommendList({
    required String studioNum,
    required int pageNum,
    required int pageSize,
  }) async {
    BaseResponse response = await LiveApis.of().getLiveRecommendList(
      studioNum: studioNum,
      pageNum: pageNum,
      pageSize: pageSize,
    );
    if (GetUtils.isNull(response) == true) {
      return [];
    }

    return (response.data as List<dynamic>)
        .map((e) => LiveBean.fromJson(e as Map<String, dynamic>))
        .toList();
  }

  /// 关注主播
  static Future<bool> focusUser({
    bool isInRoom = true,
    required int focusUserId,
  }) async {
    BaseResponse response = await CommonApis.of().focusUser({
      "focusUserId": focusUserId,
      "isInRoom": isInRoom,
    });
    if (GetUtils.isNull(response) == true) {
      return false;
    }

    return response.code == 200;
  }

  /// 关注主播
  static Future<bool> unfocusUser({
    bool isInRoom = true,
    required int focusUserId,
  }) async {
    BaseResponse response = await CommonApis.of().unfocusUser({
      "focusUserId": focusUserId,
      "isInRoom": isInRoom,
    });
    if (GetUtils.isNull(response) == true) {
      return false;
    }

    return response.code == 200;
  }

  static Future<List<GameTabModel?>> getGameCodeList() async {
    BaseResponse response = await CommonApis.of().getGameCodeList();
    if (GetUtils.isNull(response) == true) {
      return [];
    }
    return (response.data as List<dynamic>?)
            ?.map(
              (e) => GameTabModel.fromJson(e as Map<String, dynamic>),
            )
            .toList() ??
        [];
  }

  static Future<List<LiveGameModel?>> getLiveGameList(
      {required String code}) async {
    BaseResponse response = await CommonApis.of().getLiveGameList(code);
    if (GetUtils.isNull(response) == true) {
      return [];
    }
    return (response.data as List<dynamic>?)
            ?.map(
              (e) => LiveGameModel.fromJson(e as Map<String, dynamic>),
            )
            .toList() ??
        [];
  }

  /// 直播间前五十用户
  static Future<LiveTop50UserWrapper?> liveStudioTop50({
    required String studioNum,
  }) async {
    BaseResponse response = await LiveApis.of().liveStudioTop50(
      studioNum: studioNum,
    );
    if (GetUtils.isNull(response.data) == true) {
      return null;
    }

    return LiveTop50UserWrapper.fromJson(response.data as Map<String, dynamic>);
  }
}
