import 'package:base/app_routes.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:base_service/manager/ol_user_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_page/anchor_card_dialog/anchor_card_dialog_page.dart';
import 'package:live_page/anchor_profile/anchor_profile_page.dart';
import 'package:live_page/exit_room_dialog/exit_room_dialog_page.dart';
import 'package:live_page/gift_list/gift_list_page.dart';
import 'package:live_page/live_game_center/live_game_center_page.dart';
import 'package:live_page/online_user_list/online_user_list_page.dart';
import 'package:live_page/recommend_lives/recommend_lives_page.dart';
import 'package:services/models/res/game/game_tab_model.dart';
import 'package:services/models/res/home/home_live_bean.dart';
import 'package:services/models/res/live/anchor_bean.dart';
import 'package:services/models/res/live/anchor_card_bean.dart';

import 'live_provider.dart';
import 'widgets/live_footer.dart';
import 'widgets/live_message_list.dart';
import 'widgets/live_top_area.dart';
import 'widgets/loading_placeholder.dart';
import 'widgets/vip_come_in.dart';

part 'live_binding.dart';
part 'live_controller.dart';

class LivePage extends GetView<LiveController> {
  const LivePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          LoadingPlaceholder(
            customTheme: controller.currentCustomThemeData(),
          ),

          // bottom
          DecoratedBox(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.black.withOpacity(0.05),
                  Colors.white,
                ],
                stops: const [
                  0.1,
                  1,
                ],
              ),
            ),
            child: SafeArea(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10)
                      .copyWith(bottom: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // msg
                      Container(
                        constraints: const BoxConstraints(maxHeight: 280),
                        child: Obx(
                          () => LiveMessageList(
                            messages: controller.messages,
                            scrollController: controller.msgScrollController,
                          ),
                        ),
                      ),
                      const VipComeIn(
                        vipLevel: 1,
                        nickname: '我是二百五',
                      ),
                      const SizedBox(height: 8),
                      LiveFooter(
                        editingController: controller.editingController,
                        sendMsg: controller.sendMsg,
                        showGifts: controller.showGifts,
                        showGames: controller.showGames,
                        openShare: controller.openShare,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Align(
            child: ElevatedButton(
              onPressed: () {
                Get.toNamed(AppRoutes.liveEnd);
              },
              child: const Text('直播结束'),
            ),
          ),
          // 手势
          GestureDetector(
            onHorizontalDragStart: controller.onHorizontalDragStart,
            onHorizontalDragUpdate: controller.onHorizontalDragUpdate,
          ),
          // top
          SafeArea(
            bottom: false,
            child: Align(
              alignment: Alignment.topLeft,
              child: Obx(
                () => LiveTopArea(
                  customTheme: controller.currentCustomThemeData(),
                  onlineUserAvatars: const [
                    '',
                    '',
                    '',
                    '',
                    '',
                  ],
                  onlineUserCount: 1200,
                  anchorBean: controller.anchorBean,
                  openOnlineUserList: controller.openOnlineUserList,
                ),
              ),
            ),
          ),
          Obx(
            () => controller.showRecommendLives
                ? const RecommendLivesPage()
                : const SizedBox(),
          ),
        ],
      ),
    );
  }
}
