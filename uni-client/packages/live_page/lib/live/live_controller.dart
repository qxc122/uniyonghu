part of 'live_page.dart';

class LiveController extends GetXBaseController {
  static LiveController get of => Get.find();

  HomeLiveBean? homeLive;

  late final TextEditingController editingController;
  late final ScrollController msgScrollController;

  List<String> get messages => _messages.value;
  final RxList<String> _messages = <String>[].obs;

  AnchorBean? get anchorBean => _anchorBean.value;
  final _anchorBean = AnchorBean().obs;

  bool get showRecommendLives => _showRecommendLives.value;
  final _showRecommendLives = false.obs;

  double onHorizontalDragStartX = 0;

  void onHorizontalDragStart(DragStartDetails details) {
    onHorizontalDragStartX = details.globalPosition.dx;
    print(details.globalPosition.dx);
  }

  void onHorizontalDragUpdate(DragUpdateDetails details) {
    final newX = details.globalPosition.dx;
    if ((newX - onHorizontalDragStartX).abs() < 50) return;
    if (newX < onHorizontalDragStartX &&
        onHorizontalDragStartX > Get.width / 2) {
      // 左滑
      onShowRecommendLives();
    } else if (newX > onHorizontalDragStartX &&
        onHorizontalDragStartX < Get.width / 2) {
      // 右滑
      if (showRecommendLives) {
        try {
          RecommendLivesController.of.onBack();
        } catch (e) {
          //
        }
      } else {
        // 清屏
      }
    }

    print(details.globalPosition.dx);
  }

  void onShowRecommendLives() {
    _showRecommendLives.value = true;
  }

  void onCloseRecommendLives() {
    _showRecommendLives.value = false;
  }

  Future<void> toggleFocus() async {
    if (anchorBean == null) return;
    final isFocus = anchorBean!.isFocus;
    OLEasyLoading.showLoading('');
    final succ = isFocus
        ? await LiveProvider.unfocusUser(
            focusUserId: anchorBean!.userId,
          )
        : await LiveProvider.focusUser(
            focusUserId: anchorBean!.userId,
          );
    OLEasyLoading.dismiss();
    if (succ) {
      _anchorBean.update((val) {
        val?.isFocus = !val.isFocus;
      });
    }
  }

  void openAnchorCardDialog() {
    Get.dialog(
      const AnchorCardDialogPage(),
      barrierColor: Colors.transparent,
    );
  }

  void openOnlineUserList() {
    Get.bottomSheet(
      const OnlineUserListPage(),
      barrierColor: Colors.transparent,
    );
  }

  void openAnchorProfile() {
    Get.bottomSheet(
      const AnchorProfilePage(),
      barrierColor: Colors.transparent,
    );
  }

  void openShare() {
    Get.toNamed(AppRoutes.liveShare);
  }

  void showGifts() {
    Get.bottomSheet(
      const GiftListPage(),
      barrierColor: Colors.transparent,
    );
  }

  void exitRoom() {
    Get.dialog(
      const ExitRoomDialogPage(),
      barrierColor: Colors.transparent,
    );
  }

  void sendMsg() {
    final msg = editingController.text.trim();
    if (msg.isEmpty) return;
    _messages.add(msg);
    editingController.clear();
    Future.delayed(const Duration(milliseconds: 200)).then((value) {
      msgScrollController.animateTo(
        msgScrollController.position.maxScrollExtent,
        duration: const Duration(milliseconds: 200),
        curve: Curves.linear,
      );
    });
  }

  @override
  void onInit() {
    editingController = TextEditingController();
    msgScrollController = ScrollController();
    homeLive = Get.arguments as HomeLiveBean?;
    _getAnchorBean();
    super.onInit();
  }

  @override
  void onClose() {
    editingController.dispose();
    msgScrollController.dispose();
    super.onClose();
  }

  void _getAnchorBean() async {
    if (homeLive == null) return;
    final res = await LiveProvider.getAnchorBean(
      studioNum: homeLive?.studioNum ?? '',
    );
    if (res != null) {
      _anchorBean.value = res;
    }
  }

  List<GameTabModel?> gameTabs = [];

  void showGames() async {
    if (gameTabs.isEmpty) {
      OLEasyLoading.showLoading('');
      gameTabs = await LiveProvider.getGameCodeList().whenComplete(
        () => OLEasyLoading.dismiss(),
      );
    }
    Get.bottomSheet(
      LiveGameCenterPage(
        gameTabs: gameTabs,
      ),
      barrierColor: Colors.transparent,
    );
  }
}
