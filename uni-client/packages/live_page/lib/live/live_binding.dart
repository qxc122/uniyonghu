part of 'live_page.dart';

class LiveBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<LiveController>(
      () => LiveController(),
    );
  }
}
