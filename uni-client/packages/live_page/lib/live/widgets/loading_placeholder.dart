import 'package:base/assets.gen.dart';
import 'package:base/themes/custom_theme.dart';
import 'package:flutter/cupertino.dart';

class LoadingPlaceholder extends StatelessWidget {
  final CustomTheme customTheme;

  const LoadingPlaceholder({
    Key? key,
    required this.customTheme,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
      child: Stack(
        alignment: Alignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 74),
            child: Assets.livePage.liveLoadBg.image(
              width: double.infinity,
              height: double.infinity,
              fit: BoxFit.cover,
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              const CupertinoActivityIndicator(
                radius: 20,
              ),
              const SizedBox(height: 22),
              Text(
                '不要着急\n小姐姐换好衣服就来',
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: customTheme.colors0x000000_40,
                  fontSize: 16,
                  height: 20 / 16,
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
