import 'package:base/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:live_page/widgets/capsule_container.dart';

class VipComeIn extends StatelessWidget {
  const VipComeIn({
    Key? key,
    required this.vipLevel,
    required this.nickname,
    this.msg = '',
  }) : super(key: key);

  final int vipLevel;
  final String nickname;
  final String msg;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (vipLevel > 0)
          CapsuleContainer(
            bgColor: const Color(0xFFE681FF),
            padding: const EdgeInsets.all(3),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Assets.livePage.vipComeInDiamond.image(
                  width: 7,
                  height: 6,
                ),
                const SizedBox(width: 1.5),
                Text(
                  'VIP $vipLevel',
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 8,
                  ),
                ),
              ],
            ),
          ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 4),
          child: Text(
            nickname + (msg.isNotEmpty ? ': ' : ''),
            style: const TextStyle(
              color: Color(0xFFE681FF),
              fontSize: 13,
            ),
          ),
        ),
        Text(
          msg.isNotEmpty ? msg : '来了！',
          style: const TextStyle(
            color: Colors.white,
            fontSize: 13,
          ),
        ),
      ],
    );
  }
}
