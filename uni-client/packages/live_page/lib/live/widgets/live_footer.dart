import 'package:base/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_page/live/live_page.dart';

import '../../widgets/capsule_container.dart';

class LiveFooter extends StatelessWidget {
  const LiveFooter({
    Key? key,
    required this.showGifts,
    required this.sendMsg,
    required this.editingController,
    required this.openShare,
    required this.showGames,
  }) : super(key: key);

  final VoidCallback showGifts;
  final VoidCallback sendMsg;
  final VoidCallback openShare;
  final VoidCallback showGames;
  final TextEditingController editingController;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: CapsuleContainer(
            padding: const EdgeInsets.symmetric(horizontal: 6),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Expanded(
                  child: TextField(
                    controller: editingController,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                    decoration: InputDecoration(
                      hintText: '说点什么叭~~~~',
                      hintStyle: TextStyle(
                        color: Colors.white.withOpacity(0.6),
                      ),
                      border: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      focusedBorder: InputBorder.none,
                    ),
                  ),
                ),
                InkWell(
                  onTap: sendMsg,
                  child: const Icon(
                    Icons.send,
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
        ),
        const SizedBox(width: 15),
        InkWell(
          onTap: openShare,
          child: Assets.livePage.shareEntrance.image(
            width: 35,
            height: 35,
          ),
        ),
        const SizedBox(width: 5),
        InkWell(
          onTap: showGames,
          child: Assets.livePage.gameCenter.image(
            width: 35,
            height: 35,
          ),
        ),
        const SizedBox(width: 5),
        InkWell(
          onTap: showGifts,
          child: Assets.livePage.giftEntrance.image(
            width: 35,
            height: 35,
          ),
        ),
        InkWell(
          onTap: LiveController.of.exitRoom,
          child: Icon(
            Icons.cancel,
            size: 35,
            color: Colors.black.withOpacity(0.4),
          ),
        ),
      ],
    );
  }
}
