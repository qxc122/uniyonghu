// ignore_for_file: unused_element

import 'package:base/assets.gen.dart';
import 'package:base/themes/custom_theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_page/contribution_list/contribution_list_page.dart';
import 'package:live_page/niu_niu/niu_niu_page.dart';
import 'package:live_page/widgets/capsule_container.dart';
import 'package:services/models/res/live/anchor_bean.dart';

import '../../widgets/common_image.dart';
import '../live_page.dart';

class LiveTopArea extends StatelessWidget {
  final CustomTheme customTheme;
  final List<String> onlineUserAvatars;
  final int onlineUserCount;
  final AnchorBean? anchorBean;
  final VoidCallback openOnlineUserList;

  const LiveTopArea({
    Key? key,
    required this.customTheme,
    required this.onlineUserAvatars,
    required this.onlineUserCount,
    required this.anchorBean,
    required this.openOnlineUserList,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(height: 12),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _AnchorInfoWidget(
                customTheme: customTheme,
                anchorBean: anchorBean,
              ),
              InkWell(
                onTap: openOnlineUserList,
                child: _OnlineUserWidget(
                  onlineUserAvatars: onlineUserAvatars,
                  customTheme: customTheme,
                  onlineUserCount: onlineUserCount,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 5),
        _DescRecommend(customTheme: customTheme),
        const SizedBox(height: 5),
        _RankingEnter(customTheme: customTheme),
        const SizedBox(height: 5),
        _GameResult(customTheme: customTheme),
        _NiuNiuResult(customTheme: customTheme),
      ],
    );
  }
}

class _AnchorInfoWidget extends StatelessWidget {
  const _AnchorInfoWidget({
    required this.customTheme,
    required this.anchorBean,
  });

  final CustomTheme customTheme;
  final AnchorBean? anchorBean;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: LiveController.of.openAnchorProfile,
      child: CapsuleContainer(
        padding: const EdgeInsets.all(1).copyWith(right: 3),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            ClipOval(
              child: CommonImage(
                width: 33,
                height: 33,
                imageUrl: anchorBean?.avatar ?? '',
              ),
            ),
            const SizedBox(width: 4),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  (GetUtils.isNullOrBlank(anchorBean?.nickName) == true)
                      ? '爱足球小仙女'
                      : anchorBean?.nickName ?? '',
                  style: TextStyle(
                    color: customTheme.colors0xFFFFFF,
                    fontSize: 14,
                  ),
                ),
                const SizedBox(height: 2),
                Text(
                  (GetUtils.isNullOrBlank(anchorBean?.accno) == true)
                      ? 'ID:12345678'
                      : anchorBean?.accno ?? '',
                  style: TextStyle(
                    color: customTheme.colors0xFFFFFF,
                    fontSize: 11,
                  ),
                ),
              ],
            ),
            const SizedBox(width: 2),
            InkWell(
              onTap: () {
                LiveController.of.toggleFocus();
              },
              child: anchorBean?.isFocus == true
                  ? Assets.livePage.followed.image(
                      width: 27,
                      height: 27,
                    )
                  : Assets.livePage.follow.image(
                      width: 27,
                      height: 27,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}

class _OnlineUserWidget extends StatelessWidget {
  const _OnlineUserWidget({
    required this.onlineUserAvatars,
    required this.customTheme,
    required this.onlineUserCount,
  });

  final List<String> onlineUserAvatars;
  final CustomTheme customTheme;
  final int onlineUserCount;

  String get _onlineUserCountStr =>
      onlineUserCount > 100 ? '99+' : '$onlineUserCount';

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: LiveController.of.openOnlineUserList,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Transform.translate(
            offset: Offset(
              (onlineUserAvatars.length - 1) * 5,
              0,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: List.generate(
                onlineUserAvatars.length,
                (index) {
                  return Transform.translate(
                    offset: Offset(
                      index == 0 ? 0 : -5.0 * index,
                      0,
                    ),
                    child: ClipOval(
                      child: CommonImage(
                        width: 32,
                        height: 32,
                        imageUrl: onlineUserAvatars[index],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
          const SizedBox(width: 5),
          Container(
            decoration: BoxDecoration(
              color: customTheme.colors0x000000_40,
              shape: BoxShape.circle,
            ),
            width: 32,
            height: 32,
            alignment: Alignment.center,
            child: Text(
              _onlineUserCountStr,
              style: TextStyle(
                color: customTheme.colors0xFFFFFF,
                fontSize: 12,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _DescRecommend extends StatelessWidget {
  const _DescRecommend({
    required this.customTheme,
  });

  final CustomTheme customTheme;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        CapsuleContainer(
          margin: const EdgeInsets.only(left: 10),
          child: Text(
            '叭叭叭叭叭叭叭叭叭叭叭叭叭叭叭叭',
            style: TextStyle(
              color: customTheme.colors0xFFFFFF,
              fontSize: 12,
            ),
          ),
        ),
        InkWell(
          onTap: LiveController.of.onShowRecommendLives,
          child: Container(
            decoration: BoxDecoration(
              color: customTheme.colors0x000000_40,
              borderRadius: const BorderRadius.horizontal(
                left: Radius.circular(50),
              ),
            ),
            alignment: Alignment.center,
            constraints: const BoxConstraints(minHeight: 20),
            padding: const EdgeInsets.only(left: 4, right: 2),
            child: Row(
              children: [
                Assets.livePage.liveRecommend.image(
                  width: 10.68,
                  height: 10.13,
                ),
                const SizedBox(width: 5),
                Text(
                  '为你推荐',
                  style: TextStyle(
                    color: customTheme.colors0xFFFFFF,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class _RankingEnter extends StatelessWidget {
  const _RankingEnter({
    Key? key,
    required this.customTheme,
  }) : super(key: key);

  final CustomTheme customTheme;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // OLEasyLoading.showToast("打开贡献榜");
        Get.bottomSheet(
          const ContributionListPage(),
        );
      },
      child: FittedBox(
        child: CapsuleContainer(
          margin: const EdgeInsets.only(left: 10),
          padding: const EdgeInsets.all(3),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Assets.livePage.contribute.image(
                width: 13.75,
                height: 15,
              ),
              const SizedBox(width: 5),
              Text(
                '贡献榜',
                style: TextStyle(
                  color: customTheme.colors0xFFFFFF,
                  fontSize: 12,
                ),
              ),
              const SizedBox(width: 5),
              Assets.livePage.arrowRightWhite.image(
                width: 4,
                height: 8,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _GameResult extends StatelessWidget {
  const _GameResult({
    required this.customTheme,
  });

  final CustomTheme customTheme;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          margin: const EdgeInsets.only(left: 10),
          decoration: BoxDecoration(
            color: customTheme.colors0x000000_40,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 9, vertical: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                constraints: const BoxConstraints(minHeight: 17),
                alignment: Alignment.center,
                child: const Text(
                  '一分快三 202301081440期',
                  style: TextStyle(
                    color: Color(0xFFFFDB5C),
                    fontSize: 12,
                  ),
                ),
              ),
              const SizedBox(height: 2),
              Container(
                constraints: const BoxConstraints(minWidth: 181),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: List.generate(
                    6,
                    (index) {
                      final size = index > 2 ? 15.0 : 26.0;
                      return ClipRRect(
                        borderRadius: BorderRadius.circular(3),
                        child: CommonImage(
                          imageUrl: '',
                          width: size,
                          height: size,
                        ),
                      );
                    },
                  ),
                ),
              )
            ],
          ),
        ),
        InkWell(
          onTap: LiveController.of.openAnchorCardDialog,
          child: Assets.livePage.anchorCard.image(
            width: 100,
            height: 70,
          ),
        ),
      ],
    );
  }
}

class _NiuNiuResult extends StatelessWidget {
  const _NiuNiuResult({
    required this.customTheme,
  });

  final CustomTheme customTheme;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        Get.bottomSheet(
          const NiuNiuPage(),
        );
      },
      child: Container(
        width: 270,
        margin: const EdgeInsets.only(left: 10),
        decoration: BoxDecoration(
          color: customTheme.colors0x000000_40,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 9, vertical: 5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              '百人牛牛 202301081440期',
              style: TextStyle(
                color: Color(0xFFFFDB5C),
                fontSize: 12,
              ),
            ),
            Row(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Container(
                        color: const Color(0xff112659),
                        padding: const EdgeInsets.all(4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: List.generate(
                            5,
                            (index) => AssetGenImage(
                                    Assets.livePage.niuniu.playingCard111.path)
                                .image(height: 30, fit: BoxFit.fitHeight),
                          ).toList(),
                        ),
                      ),
                      Positioned(
                        left: 0,
                        right: 0,
                        bottom: 0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Colors.transparent,
                                customTheme.colors0x000000,
                                Colors.transparent,
                              ],
                            ),
                          ),
                          child: AssetGenImage(Assets.livePage.niuniu.niu7.path)
                              .image(height: 18, fit: BoxFit.fitHeight),
                        ),
                      ),
                      Positioned(
                        left: 0,
                        top: 0,
                        child:
                            AssetGenImage(Assets.livePage.niuniu.winLeft.path)
                                .image(height: 20, fit: BoxFit.fitHeight),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Stack(
                    children: [
                      Container(
                        color: const Color(0xff481515),
                        padding: const EdgeInsets.all(4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: List.generate(
                            5,
                            (index) => AssetGenImage(
                                    Assets.livePage.niuniu.playingCard111.path)
                                .image(height: 30, fit: BoxFit.fitHeight),
                          ).toList(),
                        ),
                      ),
                      Positioned(
                        left: 0,
                        right: 0,
                        bottom: 0,
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Colors.transparent,
                                customTheme.colors0x000000,
                                Colors.transparent,
                              ],
                            ),
                          ),
                          child:
                              AssetGenImage(Assets.livePage.niuniu.niuNiu.path)
                                  .image(height: 18, fit: BoxFit.fitHeight),
                        ),
                      ),
                      Positioned(
                        right: 0,
                        top: 0,
                        child:
                            AssetGenImage(Assets.livePage.niuniu.winRight.path)
                                .image(height: 20, fit: BoxFit.fitHeight),
                      )
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
