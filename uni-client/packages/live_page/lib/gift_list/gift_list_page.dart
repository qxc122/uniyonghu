import 'package:base/bases/get_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_page/gift_list/widgets/gift_swiper.dart';

import 'widgets/gift_footer.dart';
import 'widgets/gift_tab.dart';

part 'gift_list_controller.dart';

class GiftListPage extends GetView<GiftListController> {
  const GiftListPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<GiftListController>(
      init: GiftListController(),
      builder: (controller) => Container(
        decoration: BoxDecoration(
          color: controller.currentCustomThemeData().colors0x000000_40,
          borderRadius: const BorderRadius.vertical(
            top: Radius.circular(12),
          ),
        ),
        constraints: BoxConstraints(
            maxHeight: 360 + MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
              child: GiftTab(),
            ),
            Expanded(
              child: GiftSwiper(
                giftList: List.generate(
                  45,
                  ((index) => ''),
                ).toList(),
              ),
            ),
            const SizedBox(height: 10),
            const GiftFooter(),
          ],
        ),
      ),
    );
  }
}
