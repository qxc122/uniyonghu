import 'dart:async';

import 'package:base/assets.gen.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:base/commons/utils/extension.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:base/themes/custom_theme.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:live_page/gift_list/widgets/gift_swiper.dart';
import 'package:live_page/niu_bet_history/niu_bet_history_page.dart';
import 'package:live_page/niu_lottery_history/niu_lottery_history_page.dart';
import 'package:live_page/niu_niu/widgets/bottom_chip.dart';
import 'package:live_page/widgets/cus_button.dart';
import 'package:services/models/res/game/niu_niu/niu_niu_detail_model.dart';
import 'package:services/models/res/game/niu_niu/niu_niu_model.dart';

import '../niu_bet/niu_bet_page.dart';
import 'niu_niu_provider.dart';
import 'utils/ticket_function_bean.dart';
import 'utils/ticket_utils.dart';
import 'widgets/bet_cell.dart';
import 'widgets/bet_cell_row.dart';
import 'widgets/bottom_explain.dart';
import 'widgets/lottery_item.dart';
import 'widgets/lottery_list.dart';
import 'widgets/niu_niu_indicator.dart';

part 'niu_niu_controller.dart';

class NiuNiuPage extends StatelessWidget {
  const NiuNiuPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(NiuNiuController());
    final CustomTheme customTheme = controller.currentCustomThemeData();
    return GetBuilder<NiuNiuController>(builder: (controller) {
      return Container(
        padding: EdgeInsets.only(bottom: MediaQuery.of(context).padding.bottom),
        decoration: BoxDecoration(
          color: customTheme.colors0x000000_40,
          borderRadius: const BorderRadius.vertical(
            top: Radius.circular(12),
          ),
        ),
        constraints: BoxConstraints(
            maxHeight: 450 + MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            if (controller.isShowLottery || controller.isShowBet) ...[
              Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    Expanded(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 40,
                            height: 40,
                            alignment: Alignment.center,
                            decoration: ShapeDecoration(
                              shape: const CircleBorder(),
                              color: customTheme.colors0xD9D9D9,
                            ),
                            child: Text(
                              "彩票",
                              style: TextStyle(
                                fontSize: 13,
                                color: customTheme.colors0x000000,
                              ),
                            ),
                          ),
                          const SizedBox(width: 6),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Text(
                                    "百人牛牛",
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: customTheme.colors0xFFFFFF,
                                    ),
                                  ),
                                  const SizedBox(width: 4),
                                  SvgPicture.asset(
                                    Assets.livePage.niuniu.niuNiuPlay,
                                  ),
                                ],
                              ),
                              _buildCountDown(),
                            ],
                          )
                        ],
                      ),
                    ),
                    CustomButton.text(
                      onPressed: controller.toggleBet,
                      size: CustomButtonSize.small,
                      foregroundColor: customTheme.colors0xFFFFFF,
                      radius: 20,
                      fontSize: 11,
                      child: Row(
                        children: [
                          Text(
                            '投注记录',
                            style: TextStyle(
                              color: customTheme.colors0xFFFFFF,
                            ),
                          ),
                          if (controller.isShowBet)
                            const Icon(Icons.keyboard_arrow_down, size: 12)
                        ],
                      ),
                    ),
                    const SizedBox(width: 5),
                    CustomButton.text(
                      onPressed: controller.toggleLottery,
                      size: CustomButtonSize.small,
                      foregroundColor: customTheme.colors0xFFFFFF,
                      radius: 20,
                      fontSize: 11,
                      child: Row(
                        children: [
                          Text(
                            '开奖记录',
                            style: TextStyle(
                              color: customTheme.colors0xFFFFFF,
                            ),
                          ),
                          if (controller.isShowLottery)
                            const Icon(Icons.keyboard_arrow_down, size: 12)
                        ],
                      ),
                    ),
                    const SizedBox(width: 5),
                    GestureDetector(
                      onTap: controller.toMoreRecord,
                      child: Text(
                        '更多记录',
                        style: TextStyle(
                          color: customTheme.colors0xFFFFFF,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              LotteryList(
                lotteryList: controller.expandedList,
                customTheme: customTheme,
              ),
            ] else ...[
              Padding(
                padding: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "第${controller.lastInfo?.kjNo ?? ''}期",
                            style: TextStyle(
                              fontSize: 12,
                              color: customTheme.colors0xFFFFFF,
                            ),
                          ),
                          _buildCountDown(),
                        ],
                      ),
                    ),
                    CustomButton.text(
                      onPressed: controller.toExplain,
                      size: CustomButtonSize.small,
                      foregroundColor: customTheme.colors0xFFFFFF,
                      radius: 20,
                      fontSize: 11,
                      child: Text(
                        '玩法说明',
                        style: TextStyle(
                          color: customTheme.colors0xFFFFFF,
                        ),
                      ),
                    ),
                    const SizedBox(width: 5),
                    CustomButton.text(
                      onPressed: controller.toggleBet,
                      size: CustomButtonSize.small,
                      foregroundColor: customTheme.colors0xFFFFFF,
                      radius: 20,
                      fontSize: 11,
                      child: Text(
                        '投注记录',
                        style: TextStyle(
                          color: customTheme.colors0xFFFFFF,
                        ),
                      ),
                    ),
                    const SizedBox(width: 5),
                    CustomButton.text(
                      onPressed: controller.toggleLottery,
                      size: CustomButtonSize.small,
                      foregroundColor: customTheme.colors0xFFFFFF,
                      radius: 20,
                      fontSize: 11,
                      child: Text(
                        '开奖记录',
                        style: TextStyle(
                          color: customTheme.colors0xFFFFFF,
                        ),
                      ),
                    ),
                    const SizedBox(width: 5),
                    SvgPicture.asset(
                      Assets.livePage.niuniu.niuNiuPlay,
                    ),
                  ],
                ),
              ),
              LotteryItem(
                lastInfoLeft: controller.lastInfoLeft,
                lastInfoRight: controller.lastInfoRight,
                customTheme: customTheme,
              ),
            ],
            if (controller.detailModelList.isNotEmpty)
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      height: 40,
                      margin: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 8),
                      decoration: BoxDecoration(
                        color: customTheme.colors0xFFFFFF_15,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(5)),
                      ),
                      child: TabBar(
                        indicatorColor: customTheme.colors0xFFFFFF,
                        labelColor: customTheme.colors0xFFFFFF,
                        unselectedLabelColor: customTheme.colors0xA5A5A5,
                        isScrollable: false,
                        indicator: const NiuNiuIndicator(),
                        labelPadding: EdgeInsets.zero,
                        tabs: controller.tabs.mapIndexed((item, index) {
                          return Tab(
                            child: Text(
                              controller.tabs[index],
                              textAlign: TextAlign.center,
                              maxLines: 1,
                              style: const TextStyle(fontSize: 12),
                            ),
                          );
                        }).toList(),
                        controller:
                            controller.tabController, // 记得要带上tabController
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: TabBarView(
                          controller: controller.tabController,
                          children: controller.detailModelList
                              .mapIndexed((item, index) {
                            return BetCellRow(
                              customTheme: customTheme,
                              typeIndex: index,
                              oddsList: item?.odds ?? [],
                              selectedOddsList: controller.selectedOddsList,
                              onSelectOdds: controller.onSelectOdds,
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            else
              const Expanded(
                  child: CupertinoActivityIndicator(
                color: Colors.white,
              )),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  GestureDetector(
                    onTap: () {},
                    child: Row(
                      children: [
                        Text(
                          "金币 ",
                          style: TextStyle(
                            fontSize: 14,
                            color: customTheme.colors0xFFFFFF,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(right: 8),
                          child: Text(
                            "0",
                            style: TextStyle(
                              fontSize: 14,
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0xFFEF62,
                            ),
                          ),
                        ),
                        SvgPicture.asset(
                          Assets.livePage.niuniu.refresh,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 26,
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: const BorderRadius.all(Radius.circular(20)),
                      gradient: LinearGradient(
                        begin: Alignment.centerRight,
                        end: Alignment.centerLeft,
                        colors: [
                          customTheme.colors0x6129FF,
                          customTheme.colors0xD96CFF,
                        ],
                      ),
                    ),
                    child: Text(
                      "充值",
                      style: TextStyle(
                        fontSize: 14,
                        color: customTheme.colors0xFFFFFF,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: controller.changeChip,
                    child: Row(
                      children: [
                        AssetGenImage(Assets.livePage.niuniu.chip.path)
                            .image(height: 22, fit: BoxFit.fitHeight),
                        Padding(
                          padding: const EdgeInsets.only(left: 5, right: 2),
                          child: Text(
                            '${controller.chip}',
                            style: TextStyle(
                              fontSize: 14,
                              color: controller
                                  .currentCustomThemeData()
                                  .colors0xFFEF62,
                            ),
                          ),
                        ),
                        SvgPicture.asset(
                          Assets.livePage.niuniu.arrowUp,
                        ),
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: controller.onBet,
                    child: Container(
                      height: 26,
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(20)),
                        gradient: LinearGradient(
                          begin: Alignment.centerRight,
                          end: Alignment.centerLeft,
                          colors: [
                            customTheme.colors0x6129FF,
                            customTheme.colors0xD96CFF,
                          ],
                        ),
                      ),
                      child: Text(
                        "下注",
                        style: TextStyle(
                          fontSize: 14,
                          color: customTheme.colors0xFFFFFF,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      );
    });
  }

  Widget _buildCountDown() {
    return GetBuilder<NiuNiuController>(
        id: 'countDown',
        builder: (countDownCtrl) {
          return Row(
            children: [
              Text(
                "本期截止：",
                style: TextStyle(
                  fontSize: 14,
                  color: countDownCtrl.currentCustomThemeData().colors0xFFFFFF,
                ),
              ),
              Text(
                countDownCtrl.currentSecStr,
                style: TextStyle(
                  fontSize: 14,
                  color: countDownCtrl.currentCustomThemeData().colors0x00F0FF,
                ),
              ),
            ],
          );
        });
  }
}
