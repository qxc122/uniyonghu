part of 'niu_niu_page.dart';

class NiuNiuController extends GetXBaseController
    with GetTickerProviderStateMixin {
  static NiuNiuController get to => Get.find();

  TabController? tabController;
  List<String> tabs = ['胜负', '龙虎 蓝1VS 红5', '蓝方牛', '红方牛'];

  NiuNiuModel? lastInfo;

  TicketFunctionBean? get lastInfoLeft =>
      TicketUtils.getResultImage(0, lastInfo?.lastKjNumber ?? '');

  TicketFunctionBean? get lastInfoRight =>
      TicketUtils.getResultImage(1, lastInfo?.lastKjNumber ?? '');

  List<NiuNiuDetailModel?> detailModelList = [];

  int _currentSec = 60;
  String currentSecStr = '';

  Timer? _timer;

  void startCountdown(int seconds) {
    const countdownDuration = Duration(seconds: 1);
    var remainingSeconds = seconds;

    _timer = Timer.periodic(countdownDuration, (timer) {
      remainingSeconds--;
      if (remainingSeconds == 0) {
        _timer?.cancel();
        getRecentInfo().whenComplete(() => update());
      }
      _currentSec = remainingSeconds;
      currentSecStr = TicketUtils.getCountDownTime(remainingSeconds);
      update(['countDown']);
    });
  }

  bool isShowLottery = false;
  bool isShowBet = false;
  List<NiuNiuModel?> expandedList = [];

  List<Odds> selectedOddsList = [];

  int chip = 5;

  @override
  void onInit() {
    super.onInit();
    getData();
  }

  @override
  void onReady() {
    super.onReady();
  }

  Future getRecentInfo() async {
    lastInfo = await NiuNiuProvider.getRecentInfo(1);
    startCountdown(lastInfo?.sec ?? 0);
  }

  Future getDetailInfo() async {
    detailModelList = await NiuNiuProvider.getDetailInfo(1, 1);
    tabs = detailModelList.map((e) => e?.bet ?? '').toList();
    tabController = TabController(vsync: this, length: tabs.length);
  }

  Future toExplain() async {
    OLEasyLoading.showLoading('');
    NiuNiuProvider.getExplain(1).then((model) {
      if (model?.content?.isNotEmpty ?? false) {
        Get.bottomSheet(BottomExplainPage(
          explain: model!.content!,
        ));
      }
    }).whenComplete(
      () => OLEasyLoading.dismiss(),
    );
  }

  getData() async {
    await Future.wait(
      [getRecentInfo(), getDetailInfo()],
    ).whenComplete(() {
      update();
    });
  }

  @override
  void onClose() {
    tabController?.dispose();
    _timer?.cancel();
    super.onClose();
  }

  Future getLotteryList() async {
    OLEasyLoading.showLoading('');
    NiuNiuProvider.getLotteryList(1).then((list) {
      expandedList = list;
      update();
    }).whenComplete(
      () => OLEasyLoading.dismiss(),
    );
  }

  void toggleLottery() {
    isShowLottery = !isShowLottery;
    update();
    if (isShowLottery) getLotteryList();
  }

  Future getBetList() async {
    OLEasyLoading.showLoading('');
    NiuNiuProvider.getBetList(ticketId: 1).then((list) {
      expandedList = list.map((e) => e?.toNiuNiuModel()).toList();
      update();
    }).whenComplete(
      () => OLEasyLoading.dismiss(),
    );
  }

  void toggleBet() {
    isShowBet = !isShowBet;
    update();
    if (isShowBet) getBetList();
  }

  void toMoreRecord() {
    Get.back();
    if (isShowBet) {
      Get.bottomSheet(const NiuBetHistoryPage());
    }
    if (isShowLottery) {
      Get.bottomSheet(const NiuLotteryHistoryPage());
    }
  }

  void onBet() {
    if (lastInfo == null) return;
    if (selectedOddsList.isEmpty) return;
    final controller = Get.put(NiuBetController());
    controller.betNumber = '${lastInfo!.kjNo ?? ''}';
    controller.currentSec = _currentSec;
    controller.startCountdown();
    Get.bottomSheet(const NiuBetPage());
  }

  onSelectOdds(Odds odds) {
    if (selectedOddsList.contains(odds)) {
      selectedOddsList.remove(odds);
    } else {
      selectedOddsList.insert(0, odds);
    }
    update();
  }

  void changeChip() async {
    chip = await Get.bottomSheet<int?>(
          BottomChipPage(
            chip: chip,
            customTheme: currentCustomThemeData(),
          ),
        ) ??
        5;
    update();
  }
}
