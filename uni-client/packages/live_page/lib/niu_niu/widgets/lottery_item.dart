import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base/assets.gen.dart';
import 'package:base/themes/custom_theme.dart';
import 'package:get/get.dart';

import '../niu_niu_page.dart';
import '../utils/ticket_function_bean.dart';

class LotteryItem extends StatelessWidget {
  final TicketFunctionBean? lastInfoLeft;
  final TicketFunctionBean? lastInfoRight;
  final CustomTheme customTheme;

  const LotteryItem({
    Key? key,
    required this.lastInfoLeft,
    required this.lastInfoRight,
    required this.customTheme,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Row(
          children: [
            Expanded(
              child: _buildInfoRow(lastInfoLeft),
            ),
            Expanded(
              child: _buildInfoRow(lastInfoRight, isLeft: false),
            ),
          ],
        ),
        if (lastInfoLeft?.winType == 2 || lastInfoRight?.winType == 2)
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              height: 24,
              alignment: Alignment.center,
              child: AssetGenImage(
                Assets.gamePage.niuNiu.winBoth.path,
              ).image(
                height: 20,
                fit: BoxFit.fitHeight,
              ),
            ),
          )
      ],
    );
  }

  Widget _buildInfoRow(
    TicketFunctionBean? ticket, {
    bool isLeft = true,
  }) {
    return Stack(
      children: [
        Container(
          color: isLeft ? const Color(0xff112659) : const Color(0xff481515),
          padding: const EdgeInsets.all(4),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text(
                isLeft ? "蓝方" : '红方',
                style: TextStyle(
                  fontSize: 12,
                  color: customTheme.colors0xFFFFFF,
                ),
                textAlign: isLeft ? TextAlign.end : TextAlign.start,
              ),
              const SizedBox(height: 2),
              _buildCardsRow(ticket)
            ],
          ),
        ),
        if (ticket?.statusNameResourceId?.isNotEmpty ?? false)
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              height: 24,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    Colors.transparent,
                    customTheme.colors0x000000,
                    Colors.transparent,
                  ],
                ),
              ),
              child: Image.asset(
                ticket?.statusNameResourceId ?? '',
                height: 18,
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        if (ticket?.winType == 1)
          Positioned(
            left: isLeft ? 0 : null,
            right: isLeft ? null : 0,
            top: 0,
            child: AssetGenImage(
              isLeft
                  ? Assets.gamePage.niuNiu.winLeft.path
                  : Assets.gamePage.niuNiu.winRight.path,
            ).image(
              height: 20,
              fit: BoxFit.fitHeight,
            ),
          ),
      ],
    );
  }

  Widget _buildCardsRow(TicketFunctionBean? ticket) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: (ticket != null && (ticket.resourceIds?.isNotEmpty ?? false))
          ? List.generate(
              ticket.resourceIds!.length,
              (index) => Image.asset(
                ticket.resourceIds![index],
                height: 42,
                fit: BoxFit.fitHeight,
              ),
            ).toList()
          : List.generate(
              5,
              (index) => AssetGenImage(
                Assets.gamePage.niuNiu.cardDefault.path,
              ).image(
                height: 42,
                fit: BoxFit.fitHeight,
              ),
            ).toList(),
    );
  }
}
