import 'package:base/themes/custom_theme.dart';
import 'package:flutter/material.dart';

class BetCell extends StatelessWidget {
  final CustomTheme customTheme;
  final String name;
  final String odds;
  final bool isSmall;
  final bool isSelected;

  const BetCell({
    Key? key,
    required this.customTheme,
    required this.name,
    required this.odds,
    this.isSmall = true,
    this.isSelected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: isSmall ? 40 : 65,
          height: isSmall ? 36 : 65,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: isSelected
                  ? customTheme.colors0xD96CFF
                  : customTheme.colors0xFFFFFF,
              borderRadius: const BorderRadius.all(Radius.circular(5))),
          child: Text(
            name,
            style: TextStyle(
              fontSize: isSmall ? 14 : 16,
              color: isSelected
                  ? customTheme.colors0xFFFFFF
                  : customTheme.colors0x000000,
            ),
          ),
        ),
        const SizedBox(height: 2),
        Text(
          odds,
          style: TextStyle(
            fontSize: 14,
            color: customTheme.colors0xFFFFFF,
          ),
        ),
      ],
    );
  }
}
