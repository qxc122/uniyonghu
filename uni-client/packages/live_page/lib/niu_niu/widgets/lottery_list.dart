import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:base/assets.gen.dart';
import 'package:base/themes/custom_theme.dart';
import 'package:get/get.dart';
import 'package:live_page/niu_niu/widgets/lottery_item.dart';
import 'package:services/models/res/game/niu_niu/niu_niu_model.dart';

import '../niu_niu_page.dart';
import '../utils/ticket_function_bean.dart';
import '../utils/ticket_utils.dart';

class LotteryList extends StatelessWidget {
  final CustomTheme customTheme;
  final List<NiuNiuModel?> lotteryList;

  const LotteryList({
    Key? key,
    required this.lotteryList,
    required this.customTheme,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (lotteryList.isEmpty) {
      return SizedBox(
        height: 120,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.info_outline,
              color: customTheme.colors0xFFFFFF,
              size: 15,
            ),
            const SizedBox(width: 4),
            Text(
              '暂无数据',
              style: TextStyle(
                color: customTheme.colors0xFFFFFF,
              ),
            ),
          ],
        ),
      );
    }
    return ConstrainedBox(
      constraints: const BoxConstraints(maxHeight: 120),
      child: SingleChildScrollView(
        child: Column(
          children: lotteryList
              .map(
                (lastInfo) => LotteryItem(
                  lastInfoLeft: TicketUtils.getResultImage(
                      0, lastInfo?.kjNumber ?? ''),
                  lastInfoRight: TicketUtils.getResultImage(
                      1, lastInfo?.kjNumber ?? ''),
                  customTheme: customTheme,
                ),
              )
              .toList(),
        ),
      ),
    );
  }
}
