import 'package:base/themes/custom_theme.dart';
import 'package:flutter/material.dart';
import 'package:services/models/res/game/niu_niu/niu_niu_detail_model.dart';

import 'bet_cell.dart';

class BetCellRow extends StatelessWidget {
  final CustomTheme customTheme;
  final List<Odds> oddsList;
  final List<Odds> selectedOddsList;
  final int typeIndex;
  final Function(Odds odds) onSelectOdds;

  const BetCellRow({
    Key? key,
    required this.customTheme,
    required this.oddsList,
    required this.selectedOddsList,
    required this.typeIndex,
    required this.onSelectOdds,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    switch (typeIndex) {
      case 0:
      case 1:
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: oddsList
              .map(
                (odds) => GestureDetector(
                  onTap: () => onSelectOdds(odds),
                  child: BetCell(
                    customTheme: customTheme,
                    name: odds.oddsName ?? '',
                    odds: (odds.odds ?? 0).toStringAsFixed(2),
                    isSmall: false,
                    isSelected:
                        selectedOddsList.indexWhere((e) => e.id == odds.id) !=
                            -1,
                  ),
                ),
              )
              .toList(),
        );
      case 2:
      case 3:
        return GridView(
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 6,
            childAspectRatio: 40 / 36,
          ),
          children: oddsList
              .map(
                (odds) => GestureDetector(
                  onTap: () => onSelectOdds(odds),
                  child: BetCell(
                    customTheme: customTheme,
                    name: odds.oddsName ?? '',
                    odds: (odds.odds ?? 0).toStringAsFixed(2),
                    isSelected:
                        selectedOddsList.indexWhere((e) => e.id == odds.id) !=
                            -1,
                  ),
                ),
              )
              .toList(),
        );
    }
    return const SizedBox.shrink();
  }
}
