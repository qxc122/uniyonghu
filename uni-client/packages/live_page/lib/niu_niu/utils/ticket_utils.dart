// ignore: avoid_print
import 'ticket_function_bean.dart';

class TicketUtils {
  static int getTicketId(String? gameCode) {
    if (gameCode != null && gameCode.isNotEmpty) {
      try {
        List<String> items = gameCode.split("_");
        return int.parse(items[items.length - 1]);
      } catch (e) {
        print(e);
      }
    }
    return -1;
  }

  static String getCountDownTime(int? time) {
    if (time == null || time <= 0) {
      return "00:00";
    }
    int m = time ~/ 60;
    int s = time % 60;
    return (m > 9 ? m.toString() : "0$m") +
        ":" +
        (s > 9 ? s.toString() : "0$s");
  }

  static int getPoints(String? result) {
    if (result != null && result.isNotEmpty) {
      int item = int.parse(result);
      if (item < 0 || item > 54) {
        return 0;
      } else if (item > 0 && item < 11) {
        return item;
      } else if (item > 13 && item < 24) {
        return item - 13;
      } else if (item > 26 && item < 37) {
        return item - 26;
      } else if (item > 39 && item < 50) {
        return item - 39;
      } else {
        return 10;
      }
    }
    return 0;
  }

  static int getRealPoints(String? result) {
    if (result != null && result.isNotEmpty) {
      int item = int.parse(result);
      if (item < 0 || item > 54) {
        return 0;
      } else if (item > 0 && item < 14) {
        return item;
      } else if (item > 13 && item < 27) {
        return item - 13;
      } else if (item > 26 && item < 40) {
        return item - 26;
      } else if (item > 39 && item < 53) {
        return item - 39;
      }
    }
    return 0;
  }

  static TicketFunctionBean getResultImage(int type, String lastKjNumber) {
    TicketFunctionBean item = TicketFunctionBean();
    List<String> items1 = [];
    List<String> items2 = [];
    List<int> items3 = [];
    if (type != 0 && type != 1) {
      type = 0;
    }
    try {
      List<String> result = lastKjNumber.split(",");
      int start = 0;
      int end = 5;
      if (type == 1) {
        start = 6;
        end = 11;
      }
      for (int i = start; i < end; i++) {
        items1.add(result[i]);
        items2.add('assets/game_page/niu_niu/card_${result[i]}.png');
        items3.add(getPoints(result[i]));
      }
      start = 5;
      if (type == 1) {
        start = 11;
      }
      int win = int.parse(result[result.length - 1]);
      if (win == 2) {
        item.winType = 2;
      } else if (win == 0) {
        if (type == 0) {
          item.winType = 1;
        } else {
          item.winType = 0;
        }
      } else {
        if (type == 0) {
          item.winType = 0;
        } else {
          item.winType = 1;
        }
      }
      item.results = items1;
      item.pointData = items3;
      item.resourceIds = items2;
      String statusName = result[start].split("-")[0];
      item.statusName = statusName;
      item.statusNameResourceId = getBNRRResultImage(statusName);
      if (statusName.startsWith("炸")) {
        // TODO
      } else if (statusName.startsWith("金牛") || statusName.startsWith("牛牛")) {
        // TODO
      }
    } catch (e) {
      print(e);
    }
    return item;
  }

  static String getBNRRResultImage(String statusName) {
    if (statusName.isNotEmpty) {
      if (statusName == "炸") {
        return 'assets/game_page/niu_niu/card_n_14.png';
      } else if (statusName == "银牛") {
        return 'assets/game_page/niu_niu/card_n_13.png';
      } else if (statusName == "牛牛") {
        return 'assets/game_page/niu_niu/card_n_12.png';
      } else if (statusName == "金牛") {
        return 'assets/game_page/niu_niu/card_n_11.png';
      } else if (statusName == "金色牛") {
        return 'assets/game_page/niu_niu/card_n_10.png';
      } else if (statusName == "无牛") {
        return 'assets/game_page/niu_niu/card_n_0.png';
      } else if (statusName.contains("牛")) {
        String position = statusName.replaceFirst("牛", "");
        return 'assets/game_page/niu_niu/card_n_$position.png';
      }
    }
    return 'assets/game_page/niu_niu/card_n_0.png';
  }
}
