import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

part 'report_sheet_controller.dart';

class ReportSheetPage extends GetView<ReportSheetController> {
  const ReportSheetPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ReportSheetController>(
      init: ReportSheetController(),
      builder: (controller) => Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: controller.reasons
                  .map(
                    (reason) => InkWell(
                      onTap: () {
                        controller.onTapReason(reason);
                      },
                      child: Container(
                        width: double.infinity,
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        decoration: reason != controller.reasons.last
                            ? const BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: Color(0xFFEBEBEB),
                                  ),
                                ),
                              )
                            : null,
                        child: Center(
                          child: Text(
                            reason,
                            style: const TextStyle(
                              color: Color(0xFF9F44FF),
                              fontSize: 24,
                            ),
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
          const SizedBox(height: 10),
          DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
            ),
            child: InkWell(
              onTap: () {
                Get.back();
              },
              child: const Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Center(
                  child: Text(
                    '取消',
                    style: TextStyle(
                      color: Color(0xFF777777),
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 34),
        ],
      ),
    );
  }
}
