part of 'report_sheet_page.dart';

class ReportSheetController extends GetxController {
  List<String> get reasons => _reasons;
  final List<String> _reasons = [
    '政治谣言',
    '色情低俗',
    '商业广告',
    '侮辱谩骂',
  ];

  onTapReason(String reason) {
    Get.back();
    OLEasyLoading.showToast('感谢您的举报，我们会尽快作出处理');
  }
}
