part of 'online_user_list_page.dart';

class OnlineUserListController extends GetXBaseController {
  List<LiveTop50UserBean> users = [];
  int onlineNum = 0;
  String get studioNum => LiveController.of.homeLive?.studioNum ?? '';

  @override
  void onInit() {
    _getTop50Users();
    super.onInit();
  }

  Future<void> _getTop50Users() async {
    if (studioNum.isEmpty) return;
    OLEasyLoading.showLoading('');
    final res = await LiveProvider.liveStudioTop50(studioNum: studioNum);
    if (res != null) {
      users.clear();
      onlineNum = res.onlineNum;
      users.addAll(res.list);
      update();
    }
    OLEasyLoading.dismiss();
  }
}
