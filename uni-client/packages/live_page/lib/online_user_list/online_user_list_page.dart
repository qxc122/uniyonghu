import 'package:base/bases/get_base_controller.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_page/live/live_page.dart';
import 'package:live_page/live/live_provider.dart';
import 'package:live_page/online_user_list/models/live_toop50_user_bean.dart';

import 'widgets/online_user_row.dart';

part 'online_user_list_controller.dart';

class OnlineUserListPage extends GetView<OnlineUserListController> {
  const OnlineUserListPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<OnlineUserListController>(
      init: OnlineUserListController(),
      builder: (controller) => Container(
        decoration: BoxDecoration(
          color: controller.currentCustomThemeData().colors0x000000_30,
          borderRadius: const BorderRadius.vertical(
            top: Radius.circular(12),
          ),
        ),
        constraints: BoxConstraints(
            maxHeight: 400 + MediaQuery.of(context).padding.bottom),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
              child: Center(
                child: Text(
                  '在线用户${controller.onlineNum > 0 ? '（${controller.onlineNum}）' : ''}',
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.separated(
                itemCount: controller.users.length,
                padding:
                    const EdgeInsets.symmetric(vertical: 16, horizontal: 20),
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                    color: Colors.black.withOpacity(0.1),
                  );
                },
                itemBuilder: (BuildContext context, int index) {
                  final user = controller.users[index];
                  return OnlineUserRow(
                    user: user,
                  );
                },
              ),
            ),
            SafeArea(
              top: false,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 12),
                child: Center(
                  child: Text(
                    '--- 只显示前50个在线用户 ---',
                    style: TextStyle(
                      color: Colors.white.withOpacity(0.5),
                      fontSize: 12,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
