// To parse this JSON data, do
//
//     final liveTop50UserBean = liveTop50UserBeanFromJson(jsonString);

import 'dart:convert';

LiveTop50UserWrapper liveTop50UserBeanFromJson(String str) =>
    LiveTop50UserWrapper.fromJson(json.decode(str));

String liveTop50UserBeanToJson(LiveTop50UserWrapper data) =>
    json.encode(data.toJson());

class LiveTop50UserWrapper {
  LiveTop50UserWrapper({
    this.list = const [],
    this.onlineNum = 0,
  });

  final List<LiveTop50UserBean> list;
  final int onlineNum;

  factory LiveTop50UserWrapper.fromJson(Map<String, dynamic> json) =>
      LiveTop50UserWrapper(
        list: json["list"] == null
            ? []
            : List<LiveTop50UserBean>.from(
                json["list"].map((x) => LiveTop50UserBean.fromJson(x))),
        onlineNum: json["onlineNum"],
      );

  Map<String, dynamic> toJson() => {
        "list": List<dynamic>.from(list.map((x) => x.toJson())),
        "onlineNum": onlineNum,
      };
}

class LiveTop50UserBean {
  LiveTop50UserBean({
    this.adminType = -1,
    this.avatar = '',
    this.countryCode = '',
    this.enterRoomTime = 0,
    this.level = 0,
    this.nickName = '',
    this.personalSignature = '',
    this.sex = -1,
    this.silver = 0,
    this.userAccount = '',
    this.userId = '',
    this.userType = -1,
  });

  /// 是否管理员 1 是 2否
  final int adminType;
  final String avatar;
  final String countryCode;
  final int enterRoomTime;
  final int level;
  final String nickName;
  final String personalSignature;

  /// 性别 0保密 1男 2女
  final int sex;
  final double silver;
  final String userAccount;
  final String userId;

  /// 用户类型：0-普通用户 、1-游客用户、2-主播、3-家族长
  final int userType;

  factory LiveTop50UserBean.fromJson(Map<String, dynamic> json) =>
      LiveTop50UserBean(
        adminType: json["adminType"] as int? ?? -1,
        avatar: json["avatar"]?.toString() ?? '',
        countryCode: json["countryCode"]?.toString() ?? '',
        enterRoomTime: json["enterRoomTime"] as int? ?? -1,
        level: json["level"] as int? ?? -1,
        nickName: json["nickName"]?.toString() ?? '',
        personalSignature: json["personalSignature"]?.toString() ?? '',
        sex: json["sex"] as int? ?? -1,
        silver: json["silver"] as double? ?? -1,
        userAccount: json["userAccount"]?.toString() ?? '',
        userId: json["userId"]?.toString() ?? '',
        userType: json["userType"] as int? ?? -1,
      );

  Map<String, dynamic> toJson() => {
        "adminType": adminType,
        "avatar": avatar,
        "countryCode": countryCode,
        "enterRoomTime": enterRoomTime,
        "level": level,
        "nickName": nickName,
        "personalSignature": personalSignature,
        "sex": sex,
        "silver": silver,
        "userAccount": userAccount,
        "userId": userId,
        "userType": userType,
      };
}
