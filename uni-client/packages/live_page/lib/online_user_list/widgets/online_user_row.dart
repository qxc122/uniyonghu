import 'package:flutter/material.dart';
import 'package:live_page/online_user_list/models/live_toop50_user_bean.dart';

import '../../widgets/common_image.dart';
import '../../widgets/common_manager.dart';
import '../../widgets/common_vip.dart';

class OnlineUserRow extends StatelessWidget {
  const OnlineUserRow({
    Key? key,
    required this.user,
  }) : super(key: key);

  final LiveTop50UserBean user;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 7),
      child: Row(
        children: [
          ClipOval(
            child: CommonImage(
              width: 40,
              height: 40,
              imageUrl: user.avatar,
            ),
          ),
          const SizedBox(width: 4),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                user.nickName,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
              const SizedBox(height: 5),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (user.level > 0)
                    CommonVip(
                      vipType: VipType.gold,
                      vipLevel: user.level,
                    ),
                  // const SizedBox(width: 5),
                  // const CommonNobilityTag(
                  //   nobilityTagType: NobilityTagType.marquis,
                  // ),
                  // const SizedBox(width: 5),
                  // const CommonGuard(
                  //   guardType: GuardType.gold,
                  // ),
                  const SizedBox(width: 5),
                  if (user.adminType == 1) const CommonManager(),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
