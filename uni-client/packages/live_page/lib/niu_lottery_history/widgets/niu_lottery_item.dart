import 'package:flutter/material.dart';
import 'package:base/assets.gen.dart';
import 'package:base/themes/custom_theme.dart';

class NiuLotteryItem extends StatelessWidget {
  final CustomTheme customTheme;

  const NiuLotteryItem({
    Key? key,
    required this.customTheme,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              "第202301031320期",
              style: TextStyle(
                fontSize: 12,
                color: customTheme.colors0xFFFFFF,
              ),
              textAlign: TextAlign.start,
            ),
            const SizedBox(height: 2),
            Row(
              children: [
                Expanded(
                  child: Stack(
                    children: [
                      Container(
                        color: const Color(0xff112659),
                        padding: const EdgeInsets.all(4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: List.generate(
                            5,
                            (index) => AssetGenImage(
                                    Assets.livePage.niuniu.playingCard111.path)
                                .image(height: 42, fit: BoxFit.fitHeight),
                          ).toList(),
                        ),
                      ),
                      Positioned(
                        left: 0,
                        right: 0,
                        bottom: 0,
                        child: Container(
                          height: 24,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Colors.transparent,
                                customTheme.colors0x000000,
                                Colors.transparent,
                              ],
                            ),
                          ),
                          child: AssetGenImage(Assets.livePage.niuniu.niu7.path)
                              .image(height: 18, fit: BoxFit.fitHeight),
                        ),
                      ),
                      Positioned(
                        left: 0,
                        top: 0,
                        child: AssetGenImage(Assets.livePage.niuniu.winLeft.path)
                            .image(height: 20, fit: BoxFit.fitHeight),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Stack(
                    children: [
                      Container(
                        color: const Color(0xff481515),
                        padding: const EdgeInsets.all(4),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: List.generate(
                            5,
                            (index) => AssetGenImage(
                                    Assets.livePage.niuniu.playingCard111.path)
                                .image(height: 42, fit: BoxFit.fitHeight),
                          ).toList(),
                        ),
                      ),
                      Positioned(
                        left: 0,
                        right: 0,
                        bottom: 0,
                        child: Container(
                          height: 24,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [
                                Colors.transparent,
                                customTheme.colors0x000000,
                                Colors.transparent,
                              ],
                            ),
                          ),
                          child: AssetGenImage(Assets.livePage.niuniu.niuNiu.path)
                              .image(height: 18, fit: BoxFit.fitHeight),
                        ),
                      ),
                      Positioned(
                        right: 0,
                        top: 0,
                        child: AssetGenImage(Assets.livePage.niuniu.winRight.path)
                            .image(height: 20, fit: BoxFit.fitHeight),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
        Positioned(
          bottom: 0,
          left: 0,
          right: 0,
          child: Container(
            height: 24,
            alignment: Alignment.center,
            child: AssetGenImage(Assets.livePage.niuniu.winBoth.path)
                .image(height: 20, fit: BoxFit.fitHeight),
          ),
        )
      ],
    );
  }
}
