import 'package:base/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_page/live/live_page.dart';
import 'package:services/models/res/live/live_bean.dart';

import '../live/live_provider.dart';
import 'widgets/recommend_live_card.dart';

part 'recommend_lives_controller.dart';

class RecommendLivesPage extends GetView<RecommendLivesController> {
  const RecommendLivesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RecommendLivesController>(
      init: RecommendLivesController(),
      builder: (_) => SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          fit: StackFit.expand,
          children: [
            Obx(
              () => AnimatedPositioned(
                duration: controller.aniDuration,
                right: controller.right,
                bottom: 0,
                top: 0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          EdgeInsets.only(top: 20 + Get.window.padding.top),
                      child: InkWell(
                        onTap: controller.onBack,
                        child: SizedBox(
                          width: controller.backButtonWidth,
                          height: 40,
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Assets.livePage.recommendLivesCloseBg.image(
                                width: controller.backButtonWidth,
                                height: 40,
                              ),
                              Assets.livePage.recommendLivesClose.image(
                                width: 20,
                                height: 20,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      color: Colors.black.withOpacity(0.8),
                      width: controller.listViewWidth,
                      child: Column(
                        children: [
                          const Padding(
                            padding: EdgeInsets.only(top: 30, bottom: 14),
                            child: Text(
                              '为您推荐',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                            ),
                          ),
                          Expanded(
                            child: ListView.separated(
                              padding: const EdgeInsets.symmetric(
                                      horizontal: 7, vertical: 15)
                                  .copyWith(top: 0),
                              itemCount: 20,
                              itemBuilder: (BuildContext context, int index) {
                                return const RecommendLiveCard();
                              },
                              separatorBuilder:
                                  (BuildContext context, int index) =>
                                      const SizedBox(height: 15),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
