part of 'recommend_lives_page.dart';

class RecommendLivesController extends GetxController {
  static RecommendLivesController get of => Get.find();

  double get listViewWidth => 114;
  double get backButtonWidth => 30.24;

  Duration get aniDuration => const Duration(milliseconds: 300);

  double get right => _right.value;
  late final Rx<double> _right;

  List<LiveBean> get recommendLives => _recommendLives;
  final _recommendLives = <LiveBean>[].obs;

  void onBack() {
    _right.value = -(listViewWidth + backButtonWidth);
    Future.delayed(aniDuration).then(
      (value) => LiveController.of.onCloseRecommendLives(),
    );
  }

  @override
  void onInit() {
    _right = RxDouble(-(listViewWidth + backButtonWidth));
    super.onInit();
  }

  @override
  void onReady() {
    _right.value = 0;
    super.onReady();
  }

  /// 获取直播间推荐直播
  void getBannerList() async {
    final res = await LiveProvider.getLiveRecommendList(
      studioNum: '1523_1',
      pageNum: 1,
      pageSize: 20,
    );
    if (res.isNotEmpty) {
      _recommendLives.value = res;
    }
  }
}
