import 'package:base/assets.gen.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_page/report_sheet/report_sheet_page.dart';
import 'package:live_page/widgets/common_image.dart';
import 'package:live_page/widgets/common_vip.dart';
import 'package:services/models/res/live/anchor_bean.dart';

import '../live/live_page.dart';
import 'widgets/footer_action_row.dart';
import 'widgets/social_data_row.dart';

part 'anchor_profile_controller.dart';

class AnchorProfilePage extends GetView<AnchorProfileController> {
  const AnchorProfilePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AnchorProfileController>(
      init: AnchorProfileController(),
      builder: (controller) => Container(
        constraints: BoxConstraints(
            maxHeight: 325 + MediaQuery.of(context).padding.bottom),
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(12),
                ),
              ),
              margin: const EdgeInsets.only(top: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: controller.onReport,
                    child: const Padding(
                      padding: EdgeInsets.only(left: 10, top: 9),
                      child: Text(
                        '举报',
                        style: TextStyle(
                          color: Color(0xFFB1B1B1),
                          fontSize: 12,
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 3),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        controller.anchor?.nickName ?? '',
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 18,
                        ),
                      ),
                      const SizedBox(width: 5),
                      Assets.livePage.anchorMale.image(
                        width: 20,
                        height: 14,
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        '会员ID：${controller.anchor?.userId ?? ''}',
                        style: const TextStyle(
                          color: Color(0xFFB1B1B1),
                          fontSize: 12,
                        ),
                      ),
                      const SizedBox(width: 5),
                      const Icon(
                        Icons.location_on_outlined,
                        color: Color(0xFFB1B1B1),
                      ),
                      const SizedBox(width: 2),
                      const Text(
                        '火星',
                        style: TextStyle(
                          color: Color(0xFFB1B1B1),
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 5),
                  const Center(
                    child: CommonVip(
                      vipType: VipType.gold,
                      vipLevel: 250,
                    ),
                  ),
                  const SizedBox(height: 17),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      '我是写了签名的哦~真的有写啦。。。。。我可以写很长很长的这么长这么长这么长',
                      maxLines: 2,
                      textAlign: TextAlign.center,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Color(0xFFB1B1B1),
                        fontSize: 14,
                        height: 19.6 / 14,
                      ),
                    ),
                  ),
                  const SizedBox(height: 39),
                  SocialDataRow(
                    followedCount: 9,
                    followerCount: controller.anchor?.fansCount ?? 0,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 18),
                    child: Divider(
                      color: Color(0xFFD9D9D9),
                      height: 0.6,
                    ),
                  ),
                  const FooterActionRow()
                ],
              ),
            ),
            ClipOval(
              child: CommonImage(
                width: 50,
                height: 50,
                imageUrl: controller.anchor?.avatar ?? '',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
