import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:live_page/live/live_page.dart';

class FooterActionRow extends StatelessWidget {
  const FooterActionRow({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: InkWell(
            onTap: () {
              OLEasyLoading.showToast('主播主页');
            },
            child: const Center(
              child: Text(
                '主页',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                ),
              ),
            ),
          ),
        ),
        const ColoredBox(
          color: Color(0xFFD9D9D9),
          child: SizedBox(
            width: 0.63,
            height: 16,
          ),
        ),
        Expanded(
          child: InkWell(
            onTap: () {
              LiveController.of.toggleFocus();
            },
            child: Center(
              child: Obx(
                () => Text(
                  LiveController.of.anchorBean?.isFocus == true ? '已关注' : '+关注',
                  style: const TextStyle(
                    color: Color(0xFF9F44FF),
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
