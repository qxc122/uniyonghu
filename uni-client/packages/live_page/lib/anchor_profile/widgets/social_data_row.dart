import 'package:flutter/material.dart';

class SocialDataRow extends StatelessWidget {
  const SocialDataRow({
    Key? key,
    required this.followedCount,
    required this.followerCount,
  }) : super(key: key);

  final int followedCount;
  final int followerCount;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        _buildColumn(count: followedCount, title: '关注'),
        _buildColumn(count: followerCount, title: '粉丝'),
      ],
    );
  }

  Widget _buildColumn({
    required int count,
    required String title,
  }) =>
      Column(
        children: [
          Text(
            '$count',
            style: const TextStyle(
              color: Colors.black,
              fontSize: 18,
            ),
          ),
          const SizedBox(height: 3),
          Text(
            title,
            style: const TextStyle(
              color: Color(0xFFB1B1B1),
              fontSize: 12,
            ),
          ),
        ],
      );
}
