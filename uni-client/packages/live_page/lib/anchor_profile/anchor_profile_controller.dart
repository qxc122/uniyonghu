part of 'anchor_profile_page.dart';

class AnchorProfileController extends GetXBaseController {
  AnchorBean? get anchor => LiveController.of.anchorBean;

  void onReport() {
    Get.back();
    Get.bottomSheet(
      const ReportSheetPage(),
      barrierColor: Colors.transparent,
    );
  }
}
