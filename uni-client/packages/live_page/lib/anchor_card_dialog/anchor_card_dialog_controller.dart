part of 'anchor_card_dialog_page.dart';

class AnchorCardDialogController extends GetxController {
  HomeLiveBean? get homeLive => LiveController.of.homeLive;
  AnchorBean? get anchor => LiveController.of.anchorBean;

  AnchorCardBean? anchorCard;

  void getWechatAccount() {
    if (anchorCard == null) return;
    if (anchorCard!.currentAmount < anchorCard!.maxLimit) {
      OLEasyLoading.showToast('获取名片进度未达到要求');
      return;
    }
  }

  @override
  void onInit() {
    _getAnchorCard();
    super.onInit();
  }

  void _getAnchorCard() async {
    if (homeLive == null) return;
    if (OLUserManager.shared.info?.id == null) return;
    OLEasyLoading.showLoading('');
    final res = await LiveProvider.getAnchorCard(
      studioNum: homeLive?.studioNum ?? '',
      userId: OLUserManager.shared.info?.id ?? -1,
    );
    if (res != null) {
      anchorCard = res;
      update();
    }
    OLEasyLoading.dismiss();
  }
}
