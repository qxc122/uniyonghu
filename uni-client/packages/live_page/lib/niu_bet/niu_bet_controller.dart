part of 'niu_bet_page.dart';

class NiuBetController extends GetXBaseController {
  static NiuBetController get to => Get.find();

  String betNumber = '';
  int currentSec = 60;
  String currentSecStr = '';

  Timer? _timer;

  List<int> multiples = [1, 2, 5, 10, 20];
  int currentMultiple = 1;

  String get betTotal =>
      '${NiuNiuController.to.selectedOddsList.length * NiuNiuController.to.chip * currentMultiple}';

  onSelectMultiple(int multiple) {
    currentMultiple = multiple;
    update();
  }

  void startCountdown() {
    const countdownDuration = Duration(seconds: 1);
    var remainingSeconds = currentSec;

    _timer?.cancel();
    _timer = Timer.periodic(countdownDuration, (timer) {
      remainingSeconds--;
      if (remainingSeconds == 0) {
        _timer?.cancel();
      }
      currentSecStr = TicketUtils.getCountDownTime(remainingSeconds);
      update();
    });
  }

  Future onBet() async {
    OLEasyLoading.showLoading('');
    NiuPostBetModel model = NiuPostBetModel();
    model.studioNum = '44929822';
    final betList = NiuNiuController.to.selectedOddsList;
    model.ticketBetOddsReqList = betList
        .map((e) => TicketBetOddsReqList(
              amount: NiuNiuController.to.chip * currentMultiple,
              bid: e.parentId,
              oid: [e.id ?? 0],
              pbid: 0,
              times: currentMultiple,
            ))
        .toList();
    model.ticketId = 1;
    NiuNiuProvider.bet(model).then((res) {
      if (res?.result ?? false) {
        OLEasyLoading.showToast('投注成功');
      }
    }).whenComplete(
      () => OLEasyLoading.dismiss(),
    );
  }

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    _timer?.cancel();
    super.onClose();
  }

  void onDeleteItem(Odds item) {
    if (NiuNiuController.to.selectedOddsList.length < 2) {
      OLEasyLoading.showToast('至少选择一条投注');
      return;
    }
    if (NiuNiuController.to.selectedOddsList.contains(item)) {
      NiuNiuController.to.selectedOddsList.remove(item);
      NiuNiuController.to.update();
      update();
    }
  }
}
