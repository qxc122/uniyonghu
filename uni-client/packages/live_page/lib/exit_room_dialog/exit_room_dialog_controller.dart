part of 'exit_room_dialog_page.dart';

class ExitRoomDialogController extends GetxController {
  AnchorBean? get anchor => LiveController.of.anchorBean;

  void exit() {
    Get.back();
    Get.back();
  }

  Future<void> followThenExit() async {
    if (anchor?.userId == 0) return;
    OLEasyLoading.showLoading('');
    final succ = await LiveProvider.focusUser(
      focusUserId: anchor!.userId,
    );
    OLEasyLoading.dismiss();
    if (succ) {
      await OLEasyLoading.showToast('关注成功');
    }
    exit();
  }
}
