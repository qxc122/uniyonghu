import 'package:agora_rtm/agora_rtm.dart';
import 'package:base/app_config.dart';
import 'package:base/commons/utils/log_utils.dart';

enum RtmType {
  /// IM消息
  typeIm,

  /// 彩票消息
  typeTicket,

  /// 切换彩票消息
  typeSwitchTicket
}

/// RTM 帮助类
class RTMUtils {
  static RTMUtils of() => RTMUtils();
  AgoraRtmClient? _rtmClient;

  /// im频道
  AgoraRtmChannel? _imChannel;

  /// 彩票频道
  AgoraRtmChannel? _ticketChannel;

  /// 切换彩票频道
  AgoraRtmChannel? _switchTicketChannel;

  /// 初始化rtm
  void initRtm() async {
    try {
      // sdk版本
      String sdkVersion = await AgoraRtmClient.getSdkVersion();
      Log.d("RTM版本：$sdkVersion");
      //创建rtm实例
      _rtmClient = await AgoraRtmClient.createInstance(AppConfig.appId);
      // 设置日志级别
      //_rtmClient?.setLog(15, 512, "/bball/rtm.log");
    } catch (error) {
      Log.e("初始化RTM失败！【$error】");
    }
  }

  /// 登录rtm
  void login(String? token, String userId) async {
    var res = await _rtmClient?.login(token, userId);
    Log.d("RTM登录：${res.toString()}");
  }

  /// 退出登录
  void logout() async {
    var res = await _rtmClient?.logout();
    Log.d("RTM退出登录：${res.toString()}");
  }

  /// 加入IM频道
  void joinChannel(String channelId, Function(String) callback,
      {RtmType type = RtmType.typeIm}) async {
    if (channelId.isEmpty) {
      Log.e("频道id不能为空！");
      return;
    }
    if (type == RtmType.typeIm) {
      //创建im频道
      _imChannel ??= await _rtmClient?.createChannel(channelId);
    } else if (type == RtmType.typeTicket) {
      //创建彩票频道
      _ticketChannel ??= await _rtmClient?.createChannel(channelId);
    } else {
      //创建切换彩票频道
      if (null != _switchTicketChannel) {
        leaveChannel(type: type);
        releaseChannel(channelId);
      }
      _switchTicketChannel = await _rtmClient?.createChannel(channelId);
    }

    if (_imChannel != null) {
      // 加入
      _imChannel?.onMemberJoined = (AgoraRtmMember member) {
        Log.d("加入im频道：userId：${member.userId}，channelId：${member.channelId}");
      };
      // 离开
      _imChannel?.onMemberLeft = (AgoraRtmMember member) {
        Log.d("离开im频道：userId：${member.userId}，channelId：${member.channelId}");
      };
      // 接收消息
      _imChannel?.onMessageReceived =
          (AgoraRtmMessage message, AgoraRtmMember member) {
        Log.d("收到im消息：userId：${member.userId}，text：${message.text}");
        callback(message.text);
      };
    }
  }

  /// 加入IM频道
  void handel(AgoraRtmChannel? channel, Function(String) callback,
      {RtmType type = RtmType.typeIm}) async {
    if (channel != null) {
      // 加入
      channel.onMemberJoined = (AgoraRtmMember member) {
        Log.d(
            "加入${type.name}频道：userId：${member.userId}，channelId：${member.channelId}");
      };
      // 离开
      channel.onMemberLeft = (AgoraRtmMember member) {
        Log.d(
            "离开${type.name}频道：userId：${member.userId}，channelId：${member.channelId}");
      };
      // 接收消息
      channel.onMessageReceived =
          (AgoraRtmMessage message, AgoraRtmMember member) {
        Log.d("收到${type.name}消息：userId：${member.userId}，text：${message.text}");
        callback(message.text);
      };
    }
  }

  /// 发送文本消息
  void sendMessage(String text, {RtmType type = RtmType.typeIm}) async {
    if (text.isEmpty) {
      Log.e("消息内容不能为空！");
      return;
    }

    try {
      if (type == RtmType.typeIm) {
        await _imChannel?.sendMessage(AgoraRtmMessage.fromText(text));
      } else if (type == RtmType.typeTicket) {
        await _ticketChannel?.sendMessage(AgoraRtmMessage.fromText(text));
      } else {
        await _switchTicketChannel?.sendMessage(AgoraRtmMessage.fromText(text));
      }
    } catch (error) {
      Log.e("发送${type.name}消息失败！【$error】");
    }
  }

  /// 发送json消息
  void sendJsonMessage(Map<dynamic, dynamic> text,
      {RtmType type = RtmType.typeIm}) async {
    if (text.isEmpty) {
      Log.e("消息内容不能为空！");
      return;
    }
    try {
      if (type == RtmType.typeIm) {
        await _imChannel?.sendMessage(AgoraRtmMessage.fromJson(text));
      } else if (type == RtmType.typeTicket) {
        await _ticketChannel?.sendMessage(AgoraRtmMessage.fromJson(text));
      } else {
        await _switchTicketChannel?.sendMessage(AgoraRtmMessage.fromJson(text));
      }
    } catch (error) {
      Log.e("发送${type.name}消息失败！【$error】");
    }
  }

  /// 离开RTM频道
  void leaveChannel({RtmType type = RtmType.typeIm}) async {
    if (type == RtmType.typeIm) {
      await _imChannel?.leave();
    } else if (type == RtmType.typeTicket) {
      await _ticketChannel?.leave();
    } else {
      await _switchTicketChannel?.leave();
    }
  }

  /// 释放资源
  void releaseChannel(String channelId) async {
    if (channelId.isEmpty) {
      Log.e("频道id不能为空！");
      return;
    }
    await _rtmClient?.releaseChannel(channelId);
  }
}
