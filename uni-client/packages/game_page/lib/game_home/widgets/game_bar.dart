import 'package:base/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';

class GameBar extends StatelessWidget {
  const GameBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: Image(
            image: AssetImage(
              Assets.gamePage.logoImg.path,
            ),
            height: 38,
            fit: BoxFit.fitHeight,
          ),
        ),
        const Spacer(),
        Padding(
          padding: const EdgeInsets.all(10),
          child: Image(
            image: AssetImage(
              Assets.rootPage.images.navKf.path,
            ),
            width: 30,
            fit: BoxFit.fitWidth,
          ),
        )
      ],
    );
  }
}
