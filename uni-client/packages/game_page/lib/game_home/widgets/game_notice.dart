import 'package:base/assets.gen.dart';
import 'package:flutter/material.dart';

import '../../widgets/marquee.dart';

class GameNotice extends StatelessWidget {
  final String marqueeContent;
  final VoidCallback? onTransactionTap;

  const GameNotice({
    Key? key,
    this.onTransactionTap,
    required this.marqueeContent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.fromLTRB(10, 10, 10, 5),
      child: GestureDetector(
        onTap: () {},
        child: Row(
          children: [
            Expanded(
              child: Container(
                height: 32,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius:
                      const BorderRadius.horizontal(left: Radius.circular(16)),
                  gradient: LinearGradient(
                    colors: [
                      const Color(0xffEDE4FF),
                      const Color(0xffF4F0FF).withOpacity(0),
                    ],
                  ),
                ),
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Image(
                        image: AssetImage(
                          Assets.gamePage.notice.path,
                        ),
                        width: 18,
                      ),
                    ),
                    Expanded(
                      child: MarqueeView(
                        child: Text(
                          marqueeContent,
                          style: const TextStyle(
                            fontSize: 14,
                            color: Color(0xff9F44FF),
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.fade,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: onTransactionTap,
              child: Image(
                image: AssetImage(
                  Assets.gamePage.rechargeWithdrawRecord.path,
                ),
                width: 75,
                fit: BoxFit.fitWidth,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
