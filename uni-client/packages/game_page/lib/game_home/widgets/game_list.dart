import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:ol_image_cache/cached_network_image.dart';
import 'package:services/models/res/game/game_bean.dart';

import '../game_home_controller.dart';

class GameListWidget extends StatelessWidget {
  final List<GameList> gameList;
  final Function(GameList) onTapItem;

  const GameListWidget({
    Key? key,
    required this.gameList,
    required this.onTapItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        mainAxisSpacing: 8,
        crossAxisSpacing: 6,
        childAspectRatio: 140 / 90,
      ),
      itemCount: min(8, gameList.length),
      padding: const EdgeInsets.symmetric(vertical: 10).copyWith(left: 10),
      itemBuilder: (ctx, index) {
        return GestureDetector(
          onTap: () => onTapItem(gameList[index]),
          child: Stack(
            fit: StackFit.expand,
            children: [
              OLImage(
                imageUrl: gameList[index].iconUrl ?? '',
                fit: BoxFit.fill,
              ),
            ],
          ),
        );
      },
    );
  }
}
