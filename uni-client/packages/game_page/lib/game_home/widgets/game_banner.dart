import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:ol_image_cache/src/ol_image_widget.dart';

class GameBanner extends StatelessWidget {
  final List<String> items;

  const GameBanner({
    Key? key,
    this.items = const [],
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CarouselSlider(
      options: CarouselOptions(
        viewportFraction: 1,
        autoPlay: true,
      ),
      items: items.map((url) {
        return Container(
          margin: const EdgeInsets.all(10),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(8)),
            child: OLImage(
              imageUrl: url,
              fit: BoxFit.fitHeight,
            ),
          ),
        );
      }).toList(),
    );
  }
}
