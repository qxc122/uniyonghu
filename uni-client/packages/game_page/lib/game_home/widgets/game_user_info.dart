import 'package:base/assets.gen.dart';
import 'package:base/caches/ol_cache_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:services/models/res/login/user_info_model.dart';
import 'package:base_service/manager/ol_user_manager.dart';
import 'package:base/app_routes.dart';

class GameUserInfo extends StatefulWidget {
  final VoidCallback? onRefresh;
  final double balance;

  const GameUserInfo({
    Key? key,
    required this.balance,
    this.onRefresh,
  }) : super(key: key);

  @override
  State<GameUserInfo> createState() => _GameUserInfoState();
}

class _GameUserInfoState extends State<GameUserInfo>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> animation;

  @override
  void initState() {
    _animationController = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    );
    animation = CurvedAnimation(
        parent: _animationController, curve: Curves.bounceInOut);
    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.symmetric(vertical: 10),
        padding: const EdgeInsets.all(10),
        decoration:  BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors:const [Color(0xffFFFFFF), Color(0xffF9F3FF)],
          ),
        ),
        child: Row(
          children: [
            Expanded(
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () {
                  _animationController.forward();
                  widget.onRefresh?.call();
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      '${OLUserManager.shared.info?.id}',
                      style: const TextStyle(
                        fontSize: 12,
                        color: Color(0xff808080),
                      ),
                    ),
                    const SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "¥${widget.balance}",
                          style: const TextStyle(
                            fontSize: 18,
                            color: Color(0xff333333),
                          ),
                        ),
                        const SizedBox(width: 5),
                        RotationTransition(
                          alignment: Alignment.center,
                          turns: _animationController,
                          child: Image.asset(
                            Assets.gamePage.iconRefresh.path,
                            width: 18,
                            fit: BoxFit.fitWidth,
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
            Container(
              width: 1,
              height: 45,
              margin: const EdgeInsets.symmetric(horizontal: 15),
              color: const Color(0xffEAD7FF),
            ),
            _ImageButton(
              name: '存款',
              iconPath: Assets.gamePage.iconCGame.path,
              onTap: () {},
            ),
            const SizedBox(width: 18),
            _ImageButton(
              name: '取款',
              iconPath: Assets.gamePage.iconQGame.path,
              onTap: () {},
            ),
            const SizedBox(width: 18),
            _ImageButton(
              name: '优惠',
              iconPath: Assets.gamePage.iconYouhuiGame.path,
              onTap: () {},
            ),
            const SizedBox(width: 18),
            _ImageButton(
              name: '游戏记录',
              iconPath: Assets.gamePage.iconYxjlGame.path,
              onTap: () {
                Get.toNamed(AppRoutes.gameRecord);
              },
            ),
          ],
        ));
  }
}

class _ImageButton extends StatelessWidget {
  final String name;
  final String iconPath;
  final VoidCallback? onTap;

  const _ImageButton({
    Key? key,
    required this.name,
    required this.iconPath,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset(
            iconPath,
            width: 38,
            height: 38,
          ),
          const SizedBox(height: 5),
          Text(
            name,
            style: const TextStyle(
              fontSize: 12,
              color: Color(0xff333333),
            ),
          ),
        ],
      ),
    );
  }
}
