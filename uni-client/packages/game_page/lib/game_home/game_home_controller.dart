import 'package:base/app_routes.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:get/get.dart';
import 'package:services/models/res/game/game_banner_bean.dart';
import 'package:services/models/res/game/game_bean.dart';

import 'game_home_provider.dart';

class GameHomeController extends GetXBaseController
    with StateMixin<List<GameBean>?>, GetTickerProviderStateMixin {
  static GameHomeController get to => Get.find();

  final RxString marqueeContent = ''.obs;
  final RxList<GameSlideData> bannerList = <GameSlideData>[].obs;

  final RxDouble balance = 0.0.obs;

  //点击下标
  final RxInt _selectIndex = 0.obs;

  get selectIndex => _selectIndex.value;

  set selectIndex(value) => _selectIndex.value = value;

  //是否打开标签
  final RxBool _showDialogOpen = false.obs;

  get showDialogOpen => _showDialogOpen.value;

  set showDialogOpen(value) => _showDialogOpen.value = value;

  @override
  void onInit() {
    _getGameData();
    super.onInit();

    _getBannerList();
    _getAdvNotice();
    getUserWallet();
  }

  ///获取游戏页面的list数据
  _getGameData() async {
    GameHomeProvider.getGameData().then(
      (value) => {
        change(value, status: RxStatus.success()),
      },
      onError: (err) => {
        change(null, status: RxStatus.error(err.toString())),
      },
    );
  }

  ///获取游戏页面的banner数据
  _getBannerList() async {
    GameHomeProvider.getBannerList().then(
        (value) => {bannerList.value = value ?? []},
        onError: (err) => {});
  }

  ///获取游戏页面的notice数据
  _getAdvNotice() async {
    GameHomeProvider.getAdvNotice().then(
      (value) => {marqueeContent.value = value?.noticeContent ?? ''},
      onError: (err) => {},
    );
  }

  ///获取余额
  getUserWallet() async {
    GameHomeProvider.getUserWallet().then(
      (value) => {balance.value = value?.balance ?? 0.0},
      onError: (err) => {},
    );
  }

  @override
  void onClose() {}

  onTapItem(GameList game) {
    Get.toNamed(
      AppRoutes.gameWeb,
      arguments: {
        'gameType': game.gameType,
        'categoryId': game.categoryId,
      },
    );
  }

  void toTransactionPage() {
    Get.toNamed(
      AppRoutes.gameTransaction,
    );
  }
}
