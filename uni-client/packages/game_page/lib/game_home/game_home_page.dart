import 'package:base/assets.gen.dart';
import 'package:base/bases/get_base_view.dart';
import 'package:base/commons/utils/extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:ol_image_cache/src/ol_image_widget.dart';

import 'game_home_controller.dart';
import 'widgets/game_banner.dart';
import 'widgets/game_bar.dart';
import 'widgets/game_user_info.dart';
import 'widgets/game_list.dart';
import 'widgets/game_notice.dart';

/// Game
class GameHomePage extends GetBaseView<GameHomeController> {
  const GameHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      body: SafeArea(
        child: Column(
          children: [
            const GameBar(),
            Expanded(
              child: SingleChildScrollView(
                child: Obx(() => Column(
                      children: [
                        GameBanner(
                          items: controller.bannerList
                              .map((e) => e.advImg ?? '')
                              .toList(),
                        ),
                        GameNotice(
                          onTransactionTap: controller.toTransactionPage,
                          marqueeContent: controller.marqueeContent.value,
                        ),
                        GameUserInfo(
                          onRefresh: () => controller.getUserWallet(),
                          balance: controller.balance.value,
                        ),
                        _buildGameBody()
                      ],
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildGameBody() {
    return controller.obx(
      (state) {
        if (state?.isEmpty ?? true) {
          return Container();
        }
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Obx(
            () {
              return Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    children: state!.mapIndexed((item, index) {
                      return GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onTap: () => controller.selectIndex = index,
                        child: Container(
                            width: 60,
                            height: 60,
                            margin: const EdgeInsets.only(top: 10),
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image: AssetImage(
                                  controller.selectIndex == index
                                      ? Assets.gamePage.icGameSelectedBg.path
                                      : Assets.gamePage.icGameUnselectedBg.path,
                                ),
                                fit: BoxFit.fill,
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                OLImage(
                                  imageUrl: item.iconUrl ?? '',
                                  width: 36,
                                  height: 36,
                                ),
                                const SizedBox(height: 2),
                                Text(
                                  item.name ?? "",
                                  style: const TextStyle(
                                    fontSize: 12,
                                    color: Color(0XFF5A3AB5),
                                  ),
                                )
                              ],
                            )),
                      );
                    }).toList(),
                  ),
                  Expanded(
                    child: GameListWidget(
                      gameList: state[controller.selectIndex].gameList ?? [],
                      onTapItem: controller.onTapItem,
                    ),
                  ),
                ],
              );
            },
          ),
        );
      },
    );
  }
}
