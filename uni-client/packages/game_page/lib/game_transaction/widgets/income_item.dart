import 'package:flutter/material.dart';
import 'package:services/models/res/game/game_transaction_list.dart';

class IncomeItem extends StatelessWidget {
  final DetailList? item;
  final double? height;

  const IncomeItem({
    Key? key,
    this.item,
    this.height = 30,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      height: height,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(
            flex: 1,
            child: Text(
              item?.amount?.toString() ?? '金额',
              textAlign: TextAlign.center,
            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              item?.billTypeName ?? '币种',
              textAlign: TextAlign.center,

            ),
          ),
          Expanded(
            flex: 2,
            child: Text(
              item?.createTime ?? '储值时间',
              textAlign: TextAlign.center,

            ),
          ),
          Expanded(
            flex: 1,
            child: Text(
              item?.stateName ?? '状态',
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }
}
