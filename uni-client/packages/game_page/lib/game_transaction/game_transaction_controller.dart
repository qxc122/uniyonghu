import 'package:base/bases/get_base_controller.dart';
import 'package:base_service/manager/ol_user_manager.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:services/models/res/game/game_transaction_list.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'game_transaction_provider.dart';

enum TransactionType {
  income,
  expenditure,
}

class GameTransactionController extends GetXBaseController
    with GetSingleTickerProviderStateMixin {
  static GameTransactionController get to => Get.find();

  late TabController tabController;
  List<String> tabs = ['充值记录', '提现记录'];

  int get tabIndex => tabController.index;

  List<TransactionListController> transactionControllers = [
    TransactionListController(type: TransactionType.income),
    TransactionListController(type: TransactionType.expenditure),
  ];

  @override
  void onInit() {
    tabController = TabController(vsync: this, length: tabs.length);
    super.onInit();
  }

  @override
  void onClose() {
    tabController.dispose();
    super.onClose();
  }
}

class TransactionListController extends GetxController
    with GetSingleTickerProviderStateMixin {
  final TransactionType type;

  late RefreshController refreshController;

  int pageSize = 10;
  int lastItemId = 0;
  bool _noMore = false;

  List<DetailList> transactionList = [];

  TransactionListController({required this.type});

  Future<void> _getData({bool isRefresh = false}) async {
    if (isRefresh) lastItemId = 0;
    GameTransactionProvider.getBill(lastItemId)
        .then(
          (value) => {
            if (value != null) fetchList(value, isRefresh),
          },
        )
        .whenComplete(() => transactionList.addAll([
              DetailList(
                  amount: 100,
                  billType: 1,
                  createTime: '2023-2-10 15:19',
                  orderStatus: 1,
                  changeTypeName: 'aa'),
              DetailList(
                  amount: 100,
                  billType: 1,
                  createTime: '2023-2-10 15:19',
                  orderStatus: 2,
                  changeTypeName: 'aa'),
              DetailList(
                  amount: 100,
                  billType: 1,
                  createTime: '2023-2-10 15:19',
                  orderStatus: 5,
                  changeTypeName: 'aa'),
            ]));
  }

  fetchList(GameTransactionModel model, bool isRefresh) {
    final items = model.detailList ?? [];
    if (isRefresh) {
      transactionList.clear();
    }
    transactionList.addAll(items);
    if (items.isNotEmpty) {
      _noMore = lastItemId == items.last.id;
      lastItemId = items.last.id ?? 0;
    } else {
      _noMore = true;
    }
    update();
  }

  void onRefresh() {
    _getData(isRefresh: true).then((_) {
      refreshController.refreshCompleted(resetFooterState: true);
    }).catchError((_) {
      refreshController.refreshFailed();
    });
  }

  void onLoading() {
    if (!_noMore) {
      _getData().then((_) {
        refreshController.loadComplete();
      }).catchError((_) {
        refreshController.loadFailed();
      });
    } else {
      refreshController.loadNoData();
    }
  }

  void onCancel(String? orderNO) {
    if (orderNO?.isNotEmpty ?? false) {
      GameTransactionProvider.cancelWithdraw(
        OLUserManager.shared.info?.id,
        orderNO!,
      ).then((value) => {onRefresh()}, onError: (err) => {});
    }
  }

  @override
  void onInit() {
    refreshController = RefreshController();
    super.onInit();
  }

  @override
  void onReady() {
    onRefresh();
    super.onReady();
  }

  @override
  void onClose() {
    refreshController.dispose();
    super.onClose();
  }
}
