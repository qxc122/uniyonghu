import 'package:base/assets.gen.dart';
import 'package:base/bases/get_base_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'game_web_controller.dart';
import 'widgets/circle_menu/circular_menu.dart';
import 'widgets/circle_menu/circular_menu_item.dart';
import 'widgets/draggable_fab.dart';

/// GameWeb
class GameWebPage extends GetBaseView<GameWebController> {
  const GameWebPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller.obx((state) => Obx(() => Padding(
          padding: EdgeInsets.only(
              top: controller.loadingProgress.value != 100
                  ? 0
                  : context.mediaQueryViewPadding.top),
          child: Scaffold(
            body: Stack(
              children: [
                WebView(
                  javascriptMode: JavascriptMode.unrestricted,
                  initialUrl: state?.data,
                  onPageFinished: (String url) {},
                  onPageStarted: (String url) {},
                  onProgress: controller.onProgress,
                ),
                GestureDetector(
                  onTap: () {
                    controller.showSubView = !controller.showSubView;
                  },
                  child: DraggableFab(
                    child: Image.asset(
                      Assets.gamePage.icFButton.path,
                      height: 40,
                      width: 40,
                    ),
                    initPosition: const Offset(40, 40),
                    changed: (offset) {
                      controller.posLeft = offset.dx;
                      controller.posTop = offset.dy;
                      if (controller.posTop >
                          (MediaQuery.of(context).size.height / 2 -
                              MediaQuery.of(context).padding.top)) {
                        controller.posTop = controller.posTop - 120;
                      } else {
                        controller.posTop = controller.posTop + 40;
                      }
                      controller.showSubView = false;
                    },
                  ),
                ),
                if (controller.showSubView)
                  Positioned(
                    left: controller.posLeft,
                    top: controller.posTop,
                    child: SizedBox(
                      width: 40,
                      child: Column(
                        children: <Widget>[
                          _buildMenuItem(
                            name: '充值',
                            onTap: () => Get.back(),
                          ),
                          _buildMenuItem(
                            name: '刷新',
                            onTap: () => Get.back(),
                          ),
                          _buildMenuItem(
                            name: '退出',
                            onTap: () => Get.back(),
                          ),
                        ],
                      ),
                    ),
                  ),
              ],
            ),
          ),
        )));
  }

  Widget _buildMenuItem({
    required String name,
    VoidCallback? onTap,
  }) {
    return InkWell(
      onTap: () {
        controller.showSubView = false;
        onTap?.call();
      },
      child: Container(
        width: 40,
        height: 40,
        alignment: Alignment.center,
        decoration: const ShapeDecoration(
          color: Colors.white,
          shape: CircleBorder(side: BorderSide(color: Colors.black, width: 2)),
        ),
        child: Text(
          name,
          style: const TextStyle(color: Colors.black, fontSize: 15),
        ),
      ),
    );
  }
}
