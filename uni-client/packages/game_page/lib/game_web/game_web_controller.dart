import 'package:base/bases/get_base_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:services/models/res/base_response.dart';
import 'package:services/models/res/game/game_transaction_list.dart';

import '../game_record/game_record_provider.dart';
import 'game_web_provider.dart';

class GameWebController extends GetXBaseController
    with StateMixin<BaseResponse?> {
  String? gameType;
  int? categoryId;

  static GameWebController get to => Get.find();

  final RxDouble _posLeft = 0.0.obs;

  double get posLeft => _posLeft.value;

  set posLeft(val) => _posLeft.value = val;

  final RxDouble _posTop = 0.0.obs;

  double get posTop => _posTop.value;

  set posTop(val) => _posTop.value = val;

  final RxBool _showSubView = false.obs;

  bool get showSubView => _showSubView.value;

  set showSubView(value) => _showSubView.value = value;

  final RxInt loadingProgress = 0.obs;

  @override
  void onInit() {
    gameType = Get.arguments['gameType'] as String?;
    categoryId = Get.arguments['categoryId'] as int?;
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    gameLogin();
  }

  @override
  void onClose() {}

  gameLogin() async {
    if (/*gameType == null || */ categoryId == null) {
      return;
    }
    GameWebProvider.gameLogin(
      gameType ?? '',
      categoryId!,
    ).then(
      (value) => {
        change(value, status: RxStatus.success()),
      },
      onError: (err) => {
        change(null, status: RxStatus.error(err.toString())),
      },
    );
  }

  void onProgress(int progress) {
    if (loadingProgress.value != 100) {
      loadingProgress.value = progress;
    }
  }
}
