

import 'dart:developer';
import 'dart:ui';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/cupertino.dart';
import 'firebase_options.dart';
import 'package:flutter/foundation.dart';


class FireBaseConfig {

  static final shared = FireBaseConfig();

  FireBaseConfig() {
    log("fire base init");
  }

  Future<void> loadFire() async {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );

    FlutterError.onError = (errorDetails) {
      // If you wish to record a "non-fatal" exception, please use `FirebaseCrashlytics.instance.recordFlutterError` instead
      FirebaseCrashlytics.instance.recordFlutterFatalError(errorDetails);
    };

    // flutter 版本不支持
    // PlatformDispatcher.instance.onError = (error, stack) {
    //   // If you wish to record a "non-fatal" exception, please remove the "fatal" parameter
    //   FirebaseCrashlytics.instance.recordError(error, stack, fatal: true);
    //   return true;
    // };

  }


}