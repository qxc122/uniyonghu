import 'dart:ui';

import 'package:base/assets.gen.dart';
import 'package:base/bases/get_base_controller.dart';
import 'package:demo_page/discover_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:game_page/game_home/game_home_page.dart';
import 'package:get/get.dart';
import 'package:home_page/view/home_page.dart';
import 'package:mine_page/mine_root/mine_root_page.dart';
import 'package:video_page/video_home/video_home_page.dart';
import 'package:wallet_page/wallet_root/wallet_root_page.dart';

/// 通用的入口参数
class LiveMainController extends GetXBaseController {
  List<TabItem> tabItems = <TabItem>[].obs;

  /// 选择的那个tab_bar
  final selectIndex = 0.obs;

  /// PageView controller
  final PageController pageController = PageController();

  @override
  void onInit() {
    mainTabItems();
    super.onInit();
  }

  @override
  void onClose() {
    pageController.dispose();
    super.onClose();
  }

  /// 主页tabs
  void mainTabItems() {
    tabItems.add(TabItem(
      basePageString("tab_home"),
      Assets.rootPage.images.tabHomeNormal.path,
      Assets.rootPage.animations.homeLottie,
    ));

    tabItems.add(TabItem(
      basePageString("tab_game"),
      Assets.rootPage.images.tabGameNormal.path,
      Assets.rootPage.animations.gameLottie,
    ));
    tabItems.add(TabItem(
      basePageString("tab_wallet"),
      Assets.rootPage.images.tabPayNormal.path,
      Assets.rootPage.animations.walletLottie,
    ));
    tabItems.add(TabItem(
      basePageString("tab_mine"),
      Assets.rootPage.images.tabMyNormal.path,
      Assets.rootPage.animations.mineLottie,
    ));

    tabItems.add(TabItem(
      basePageString("tab_video"),
      Assets.rootPage.images.tabVideoNormal.path,
      Assets.rootPage.animations.videoLottie,
    ));

    tabItems.add(TabItem(
      "测试",
      Assets.rootPage.images.tabPayNormal.path,
      Assets.rootPage.animations.walletLottie,
    ));
  }

  /// 获取tab对应的页面
  List<Widget> getTabItemPages() {
    List<Widget> list = [];
    list.add(const HomePage());
    list.add(const GameHomePage());
    list.add(WalletRootPage());
    list.add(MineRootPage());
    list.add(const VideoHomePage());
    list.add(DiscoverPage(tindex: 0, name: "测试"));
    return list;
  }
}

/// tab实体对象
class TabItem {
  String name = '';
  String iconName = '';
  String selectIconName = '';
  Color? selectColor; // 选择的文字颜色
  Color? color; // 默认的颜色

  TabItem(this.name, this.iconName, this.selectIconName,
      {this.color, this.selectColor});
}
