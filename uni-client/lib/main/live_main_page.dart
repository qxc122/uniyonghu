import 'dart:developer';

import 'package:base/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_uni_client/main/live_main_controller.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

/// 主页面
class LiveMainPage extends GetView<LiveMainController> {
  const LiveMainPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: PageView(
          physics: const NeverScrollableScrollPhysics(),
          controller: controller.pageController,
          onPageChanged: (index) {
            log("pageView $index");
          },
          children: controller.getTabItemPages(),
        ),
        bottomNavigationBar: GetX<LiveMainController>(
          builder: (_) {
            return BottomNavigationBar(
              backgroundColor: Colors.white,
              selectedItemColor: controller.currentCustomThemeData().colors0xD7D7D7,
              iconSize: 38,
              unselectedItemColor: controller.currentCustomThemeData().colors0x9F44FF,
              selectedFontSize: 12,
              unselectedFontSize: 12,
              type: BottomNavigationBarType.fixed,
              items: _buildTabItems(),
              onTap: (index) {
                log("index $index");
                controller.pageController.jumpToPage(index);
                controller.selectIndex.value = index;
              },
              currentIndex: controller.selectIndex.value,
            );
          },
        ));
  }

  List<BottomNavigationBarItem> _buildTabItems() {
    return controller.tabItems
        .map((e) => BottomNavigationBarItem(
              icon: AssetGenImage(e.iconName).image(width: 38, height: 30),
              activeIcon: Lottie.asset(e.selectIconName,
                  repeat: true,
                  reverse: true,
                  animate: true,
                  width: 38,
                  height: 30),
              label: e.name,
            ))
        .toList();
  }
}
