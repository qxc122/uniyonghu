import 'package:base/app_routes.dart';
import 'package:base/commons/utils/logger.dart';
import 'package:base/res/app_dimens.dart';
import 'package:base/themes/app_theme.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_uni_client/app/app_bindings.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'app/app_init.dart';
import 'app/app_pages.dart';
import 'main/live_main_controller.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  AppInit.shared.initApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final easyLoad = EasyLoading.init();
  final botToastBuilder = BotToastInit();
  final controller = Get.put(LiveMainController());

  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ///支持刷新的全局配置
    return RefreshConfiguration(
        footerTriggerDistance: 15,
        dragSpeedRatio: 0.91,
        headerBuilder: () => const MaterialClassicHeader(),
        footerBuilder: () => const ClassicFooter(),
        enableLoadingWhenNoData: false,
        enableRefreshVibrate: false,
        enableLoadMoreVibrate: false,
        shouldFooterFollowWhenNotFull: (state) {
          // If you want load more with noMoreData state ,may be you should return false
          return false;
        },
        child: ScreenUtilInit(
            designSize: const Size(AppDimens.design_w, AppDimens.design_h),
            builder: (context, child) {
              return GetX<LiveMainController>(builder: (_) {
                return GetMaterialApp(
                  title: '直播',
                  theme: AppTheme.currentThemeData(controller),
                  darkTheme: AppTheme.light,
                  themeMode: ThemeMode.system,
                  debugShowCheckedModeBanner: false,
                  initialRoute: AppRoutes.splash,
                  getPages: AppPages.routes,
                  builder: (context, child) {
                    child = EasyLoading.init()(context, child);

                    ///支持刷新的全局配置
                    ScrollConfiguration(
                      child: child,
                      behavior: RefreshScrollBehavior(),
                    );

                    return child;
                  },
                  navigatorObservers: [BotToastNavigatorObserver()],
                  // unknownRoute: AppPages.unknownRoute,
                  enableLog: true,
                  logWriterCallback: Logger.write,
                  initialBinding: AppBindings(),
                );
              });
            }));
  }
}
