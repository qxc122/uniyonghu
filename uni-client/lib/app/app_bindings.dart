import 'package:game_page/game_home/game_home_controller.dart';
import 'package:get/get.dart';
import 'package:home_page/controller/home_controller.dart';
import 'package:home_page/controller/home_follow_controller.dart';
import 'package:home_page/controller/home_nearby_controller.dart';
import 'package:home_page/controller/home_other_controller.dart';
import 'package:home_page/controller/home_recommend_controller.dart';
import 'package:home_page/controller/home_search_controller.dart';
import 'package:home_page/controller/home_video_controller.dart';
import 'package:mine_page/mine_root/mine_root_controller.dart';
import 'package:video_page/components/card_item/card_item_controller.dart';
import 'package:video_page/components/tag_item/tag_item_controller.dart';
import 'package:video_page/video_home/video_home_controller.dart';
import 'package:wallet_page/wallet_root/wallet_root_controller.dart';

import '../main/live_main_controller.dart';

class AppBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<WalletRootController>(() => WalletRootController());
    Get.lazyPut<MineRootPageController>(() => MineRootPageController());
    Get.lazyPut<VideoHomeController>(() => VideoHomeController());
    Get.lazyPut<TagViewController>(() => TagViewController());
    Get.lazyPut<CardItemController>(() => CardItemController());
    Get.lazyPut<LiveMainController>(() => LiveMainController());
    Get.lazyPut<GameHomeController>(() => GameHomeController());
    Get.lazyPut<HomeFollowController>(() => HomeFollowController());
    Get.lazyPut<HomeRecommendController>(() => HomeRecommendController());
    Get.lazyPut<HomeOtherController>(() => HomeOtherController());
    Get.lazyPut<HomeNearbyController>(() => HomeNearbyController());
    Get.lazyPut<HomeVideoController>(() => HomeVideoController());
    Get.lazyPut<HomeSearchController>(() => HomeSearchController());
  }
}
