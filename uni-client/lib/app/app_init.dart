import 'dart:developer';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:services/api/common_apis.dart';
import 'package:services/common//config/global_config.dart';
import 'package:base/themes/get_theme_controller.dart';
import 'package:services/common//cache/page_cache_manager.dart';
import '../main/firebase/fire_base_config.dart';
import 'package:base/app_config.dart';
import 'package:base/commons//utils/sp_utils.dart';
import 'package:base/commons/utils/log_utils.dart';
import 'package:base/commons/widgets/ol_loading_widget.dart';
import 'package:base_service/manager/ol_user_manager.dart';
import 'package:flutter/material.dart';

/// 应用初始化公共处理的地方
class AppInit {
  static final shared = AppInit();

  AppInit() {
    log("AppInit init");
  }

  Future<void> initApp() async {
    AppConfig.config();
    final themeController = Get.put(GetThemeController());
    // 初始化log
    Log.init();

    /// 获取API信息 用于缓存接口
    CommonApis.of().getApiVerInfo().then((res) {
        final data = res.data;
        PageCacheManager.shared.storeCacheVersion(data);
    });

    await FireBaseConfig.shared.loadFire();

    await SpUtils.init();
    // 加载本地页面缓存
    await PageCacheManager.shared.loadData();

    await initServices();

    // 加载用户信息
    await OLUserManager.shared.load();

    OLEasyLoading.config();

    // 透明沉淀式状态栏
    SystemUiOverlayStyle systemUiOverlayStyle =
    const SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);




    log("当前登陆状态${OLUserManager.shared.isLogin() ? "已经登录" : "没有登录"}");
    log("当前环境${AppConfig.env.name}");
    log("当前themeController$themeController");
  }

  Future<void> initServices() async {
    if (kDebugMode) {
      print('starting services ...');
    }

    await Get.putAsync(() => GlobalConfigService().init());
    // await Get.putAsync(SettingsService()).init();
    if (kDebugMode) {
      print('All services started...');
    }
  }
}

class RefreshScrollBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    switch (getPlatform(context)) {
      case TargetPlatform.iOS:
        return child;
      case TargetPlatform.android:
        return GlowingOverscrollIndicator(
          child: child,
          showLeading: true,
          showTrailing: true,
          axisDirection: axisDirection,
          notificationPredicate: (notification) {
            if (notification.depth == 0) {
              // 越界了拖动触发overScroll的话就没必要展示水波纹
              if (notification.metrics.outOfRange) {
                return false;
              }
              return true;
            }
            return false;
          },
          color: Theme.of(context).primaryColor,
        );
    }
    return const Text("data");
  }
}
